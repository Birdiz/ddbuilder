Changelog
========================
2.19.0
------------
* Improve code base : Install PhpMD and improve code logic.
* Système de recherche : Ajout d'un système de recherche qui se base sur le contenu de fichiers et leur paths.

2.18.0
------------
* Refontes diverses : Mises à jour de la base de code, corrections de l'orthographe, etc...
* Mise à jour du Comptoir Magique : Ajout d'un marchant et de sa réputation avec impact sur les prix.
* Mise à jour de la page de feuille de route : Transformation en une page de dépôt d'idées sans réelle noton de temps.
* Améliorations de la génération de lieu : Ajout de contexte pour chaque type de lieu (résidence, religieux, boutique, ...).
* Refonte de la page d'accueil : Suppression des modules descriptifs (lourdeur à la maintenance).

2.17.0
------------
* Ajout du générateur de voyage : Le générateur de voyage permet de générer un déroulé des jours de voyages effectués avec un maximum d'informations sur le contenu de la journée.
* Refonte de la page de l'écran du maître : Division des éléments aux hasards des éléments statiques.
* Refonte du design de certaines pages : Les titres ont été uniformisés et la page aventure a changé.

2.16.0
------------
* Ajout des règles additionnelles : Ajout des règles des compétences cachées.
* Mise à jour de la génération de PNJ : Ajout du choix du nombre de membres de la famille du PNJ. Modification de la logique de génération de la famille pour que ce soit "réaliste".

2.15.0
------------
* Clean code : Clean and fix multiple piece of code / typo / logic.
* Add Aarakocra race : Add all information about the race Aarakocra for the PNJ generator.

2.14.0
------------
* Refonte du design : Deéplacement gu générateur de voyage dans l'onglet générateur.
* Puzzles : Ajout de pièges et autres puzzles complexes et souvent mortels...
* Retouches de design : Le menu et la page d'accueil ont été revue dans leur organisation. D'autres pages suivront...
* Ajout d'intrigues ! : Ajout d'un petit générateur d'intrigues qui offrent un peu de contexte au MJ en panne d'inspiration.
* Corrections mineures : Diverses corrections de logique et de typo sur les données.

2.13.0
------------
* Ajout des rencontres aléatoires : Quelques rencontres dynamisées se trouvent maintenant dans les éléments aléatoires de la page Environnement.
* Corrections diverses : Corrections du nommage de certaines données, correction de la logique du calcul du nombre de membres de la famille d'un PNJ.

2.12.1
------------
* Fix icons : Fix templating to show icons again.

2.12.0
------------
* Archétype compagnon : Sauvage : Ajout de l'archétype Sauvage et mise à jour de la page d'accueil.
* Archétype compagnon : Barde : Ajout de l'archétype Barde pour les compagnons et corrections des informations de l'Artificier.

2.11.0
------------
* Add sidekicks : Add sidekick system with the artificer. Update and clean code.

2.10.1
------------
* Mise à jour des librairies : Mise à jour de l'installateur, et de la configuration.

2.9.0
------------
* Mise à jour de Symfony : Montée de version de 5.3.* à 5.4.* puis à 6.*.

2.8.0
------------
* Affichage tableaux : Ajout de bordures à certains tableaux pour uniformiser.
* Génération Boss : archétypes : Ajout des 5 archétypes pour booster les bosses. Ajout également de sbires et de données relatives aux bosses.

2.7.0
------------
* Amélioration de la génération de Boss : Refonte du style de l'affichage et correction de bugs.

2.6.0
------------
* Refonte de la génération des bosses : Amélioration de la logique de récupération d'un boss basée sur le CR/FP ; nettoyage du code ; tests.

2.5.0
------------
* Remplacement de XDebug par PCOV : Le code coverage est maintenant assuré par l'extension PCOV (améliorations des performances de test).

2.4.1
------------
* Correction: page du donjon : Correction de la logique du code qui empêchait l'affichage de la page.

2.4.0
------------
* Mise à jour de la base de code : Après la mise à jour de PHPStan, corrections de la base de code.
* Ajout d'énigmes : Démarrage du'une liste d'énigmes assez courtes

2.3.0
------------
* Mise à jour de la CI : Mise à jour vers php 8.0.
* Optimisation : Le préchargement des classes sera effectué à partir d'un seul fichier.
* Mise à jour du menu : Regroupements par thème et réoganisation du menu.

2.2.0
------------
* Mise à jour de la génération de PNJ : Le formulaire propose maintenant de choisir un alignement qui permet de tirer jusqu'à trois divinités correspondantes à l'alignement parmis les divinités communes.

2.1.0
------------
* Mise à jour de la génération des PNJ : Ajout de guildes auxquelles le PNJ peut appartenir, ajout de traits de personnalité et physiques, corrections diverses et améliorations du code.

2.0.0
------------
* Mise à jour de la page Donjon : Le générateur de coffres �a été mise à jour pour permettre de prendre 4 options : trésor, butin, pièces ou gemmes & arts.
* Migration php 8 : Le site est maintenant configuré pour utiliser php 8.

1.17.1
------------
* Référencement : Ajout du sitemap et du robot.

1.17.0
------------
* Corrections diverses : Ajout de tests et améliorations du code.
* Mise à jour de l'écran du maître : Ajout de services et de bâtiments à construire.
* Mise à jour de l'écran du maître : Ajout de plan au hasard et leurs sous-plans associés.

1.16.0
------------
* Corrections diverses : Après l'installation de PhpInsights et quelques analyses, le code respecte plus de standartisation.

1.15.0
------------
* Code : Corrections et améliorations du code.
* Modification de l'Arsenal : Ajout de règles additionnelles, ajout d'armures et ajout de munitions spéciales pour les arcs et les arbalètes.
* Modifications de la recherche d'objets magiques : Les objets sont maintenant triés par ordre alphabétique.

1.14.0
------------
* Liste des objets magiques : Une nouvelle fonctionnalité en cours de développement permettra de rechercher un objet magique et de récupérer un certain nombre d'informations au hasard.
* Ajout du changelog : Retrouvez ici toutes les dernières mises à jour par version.
