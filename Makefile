-include docker/.env

CONTAINER ?= php

dev:
	cp .env.local .env

install: hooks
	cd ./docker && ./install.sh

start: hooks
	cp .env.local .env
	cd ./docker && ./start.sh

stop:
	cd ./docker && ./stop.sh

restart: stop start

hooks:
	echo "#!/bin/bash" > .git/hooks/pre-commit
	echo "make check" >> .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

sh:
	cd ./docker && docker-compose exec $(CONTAINER) bash

warmup-cache:
	bin/console cache:warmup

check:
	make fixer
	make phpstan
	make twig-lint

fixer:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src --rules=@Symfony

phpstan:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console cache:warmup --env=dev
	cd ./docker/ && docker-compose exec -T $(CONTAINER) vendor/bin/phpstan analyse src --memory-limit=4G

phpmd:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) vendor/bin/phpmd src/ ansi phpmd.xml --exclude src/Migrations/,tests/

twig-lint:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console lint:twig ./templates

test:
	cp .env.test .env
	cd ./docker/ && docker-compose exec -T $(CONTAINER) ./vendor/bin/simple-phpunit --testdox --coverage-html var/tests/coverage
	cp .env.local .env

update:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) composer update

encore:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) yarn encore dev

changelog:
	cd ./docker/ && docker-compose exec -T $(CONTAINER) bin/console app:changelog
