DDBuilder
========================

[![pipeline status](https://gitlab.com/Birdiz/ddbuilder/badges/master/pipeline.svg)](https://gitlab.com/Birdiz/ddbuilder/-/commits/master) 
[![coverage report](https://gitlab.com/Birdiz/ddbuilder/badges/master/coverage.svg)](https://gitlab.com/Birdiz/ddbuilder/-/commits/master)

This application is a complement to [Aidedd][1]. 
This is an app which provide a lot of Dungeon's Master (DM) content to help to create stories
and to improve improvisation. This app is databaseless.

Go visit : [DDBuilder][2] !

Requirements
------------

* The Make command;
* Docker & Docker Compose;

Installation
------------

Run this command:

```bash
$ make install
```

Usage
-----

Everytime you need to start your application, you only need to run :

```bash
$ make start
```

Please note that you won't be able to do any commit if your docker's container are not mounted.
Before every commit, a hook launches a bunch of `make` command, whiches run Php-cs-fixer with
the Symfony standards, Phpstan and Twig Linter.

If you need to execute commands on the php container, run :

```bash
$ make shell
```

If you need to update the packages via composer, simply run :

```bash
$ make update
```

And if you need to re-compile your assets with Webpack Encore, run :

```bash
$ make encore
```
Please note that this command works for the `dev` environment.

Tests
-----

Execute this command to run tests:

```bash
$ make test
```

[1]: https://www.aidedd.org/
[2]: https://www.ddbuilder.fr/
