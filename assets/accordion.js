import './styles/accordion.css';

document.addEventListener('DOMContentLoaded', () => {
    let accordions = document.getElementsByClassName("accordion");
    let i;

    for (i = 0; i < accordions.length; i++) {
        accordions[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            let panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
});