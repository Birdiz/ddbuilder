document.addEventListener('DOMContentLoaded', () => {
    const formScroll = document.getElementsByClassName("form-scroll");

    if (formScroll.length !== 0) {
        formScroll[0].scrollIntoView({behavior: "smooth"});
    }
});
