#!/usr/bin/env bash

set -e

# shellcheck disable=SC2046
readonly DOCKER_PATH=$(dirname $(realpath $0))
cd "${DOCKER_PATH}";

. ./lib/functions.sh
. ./.env

block_info "SSL certificates generation"

brew install nss ca-certificates wget

if [[ -z $(which mkcert) ]]; then
    echo -e "${GREEN}mkcert is not installed.${RESET}" > /dev/tty

    brew install mkcert
fi

readonly CERT_PATH="$(pwd)/certificates"

# shellcheck disable=SC2086
rm -f ${CERT_PATH}/*.key
rm -f "${CERT_PATH}"/*.pem

mkcert -cert-file "${CERT_PATH}/${HTTP_HOST}.pem" -key-file "${CERT_PATH}/${HTTP_HOST}.key" ${HTTP_HOST}
mkcert -install

echo -e "${GREEN}SSL certificates generation done!${RESET}" > /dev/tty
