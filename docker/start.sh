#!/usr/bin/env bash

set -e

# shellcheck disable=SC2046
readonly DOCKER_PATH=$(dirname $(realpath $0))
cd "${DOCKER_PATH}";

. ./lib/functions.sh
. ./.env

# Build all container in parallel to optimize your time
docker-compose build --parallel

# Start and remove useless containers
docker-compose up -d --remove-orphans

block_success "DDBuilder is started https://${HTTP_HOST}"
