<?php

namespace App\Command;

use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use App\Enum\Generator\Boss\Beginner;
use App\Enum\Generator\Boss\Divine;
use App\Enum\Generator\Boss\Expert;
use App\Enum\Generator\Boss\Hero;
use App\Enum\Generator\Boss\Mob;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\UnicodeString;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/** @codeCoverageIgnore */
#[AsCommand(
    name: 'app:check-links',
    description: 'Check every links in enums to see if it\'s accessible.',
)]
class CheckLinksCommand extends Command
{
    public function __construct(private readonly HttpClientInterface $client, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $enums = TableA::toArray() + TableB::toArray() + TableC::toArray() + TableD::toArray() + TableE::toArray()
            + TableF::toArray() + TableG::toArray() + TableH::toArray() + TableI::toArray() + Beginner::toArray()
            + Expert::toArray() + Hero::toArray() + Divine::toArray() + Mob::toArray();

        $failed = [];
        foreach ($enums as $enum) {
            if (false === array_key_exists('link', $enum)) {
                continue;
            }

            $html = $this
                ->client
                ->request(Request::METHOD_GET, $enum['link'])
                ->getContent()
            ;

            if (true === (new UnicodeString($html))->containsAny('Cet objet magique n\'existe pas.')) {
                $failed[] = $enum['link'];
            }
        }

        if ([] !== $failed) {
            foreach ($failed as $link) {
                $output->writeln($link);
            }
        }

        return Command::SUCCESS;
    }
}
