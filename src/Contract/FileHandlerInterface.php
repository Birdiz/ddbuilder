<?php

declare(strict_types=1);

namespace App\Contract;

use Symfony\Component\Filesystem\Filesystem;

interface FileHandlerInterface
{
    public const FILE_CREATED = 0;
    public const FILE_FOUND = 1;

    public function getFileSystem(): Filesystem;

    public function findOrCreateFile(?string $filepath): int;

    /**
     * @param array<mixed> $data
     */
    public function injectString(string $path, array $data, ?string $search = null): void;
}
