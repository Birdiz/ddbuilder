<?php

declare(strict_types=1);

namespace App\Contract\Generator\Travel;

interface EncounterRegistryInterface
{
    public function getStrategy(string $type): EncounterStrategyInterface;
}
