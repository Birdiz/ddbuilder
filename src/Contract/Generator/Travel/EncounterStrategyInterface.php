<?php

declare(strict_types=1);

namespace App\Contract\Generator\Travel;

interface EncounterStrategyInterface
{
    public function getType(): string;

    /**
     * @return string|array<string, mixed>
     */
    public function process(?string $levelKey = null, ?int $nbPlayers = null): string|array;
}
