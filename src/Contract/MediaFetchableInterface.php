<?php

declare(strict_types=1);

namespace App\Contract;

interface MediaFetchableInterface
{
    public const MEDIA_PATH_PNJ = 'build/images/PNJ';
    public const MEDIA_PATH_SHIPS = 'build/images/Ships';
    public const MEDIA_PATH_GENERATOR = 'build/images/Generator';

    public static function getMediaPath(): string;
}
