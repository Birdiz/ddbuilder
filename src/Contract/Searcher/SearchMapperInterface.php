<?php

namespace App\Contract\Searcher;

interface SearchMapperInterface
{
    /**
     * @param array<string>|null $realPaths
     *
     * @return array<string>|null
     */
    public function map(?array $realPaths): ?array;

    /**
     * @return array<string, array<int, string>>
     */
    public static function paths(): array;

    /**
     * @return array<string, array<int, string>>
     */
    public static function values(): array;
}
