<?php

namespace App\Contract\Searcher;

interface SearchMapperRegistryInterface
{
    /**
     * @param array<string>|null $paths
     *
     * @return array<string>|null
     */
    public function map(?array $paths): ?array;
}
