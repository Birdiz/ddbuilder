<?php

declare(strict_types=1);

namespace App\Contract;

interface WeightedInterface
{
    /**
     * @return array<int>
     */
    public static function getWeights(): array;
}
