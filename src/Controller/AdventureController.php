<?php

namespace App\Controller;

use App\Model\AbstractController;
use App\Service\AdventureManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdventureController extends AbstractController
{
    #[Route('/adventure', name: 'adventure')]
    public function index(): Response
    {
        return $this->render('adventure/adventure.html.twig', [
            'random_info' => AdventureManager::getRandomInfo(),
            'random_chase' => AdventureManager::getRandomChase(),
            'plot_hook' => AdventureManager::getRandomPlotHook(),
        ]);
    }
}
