<?php

namespace App\Controller;

use App\Model\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlchemyController extends AbstractController
{
    #[Route('/alchemy', name: 'alchemy')]
    public function index(): Response
    {
        return $this->render('alchemy/alchemy.html.twig', [
            'controller_name' => 'AlchemyController',
        ]);
    }
}
