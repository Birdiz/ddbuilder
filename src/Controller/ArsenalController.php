<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\AbstractController;
use App\Service\ArsenalManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArsenalController extends AbstractController
{
    #[Route('/arsenal', name: 'arsenal')]
    public function index(): Response
    {
        return $this->render('arsenal/arsenal.html.twig', [
            'additional_rules' => ArsenalManager::getAdditionalRules(),
            'extended_weapons' => ArsenalManager::getExtendedWeapons(),
            'extended_ammunitions' => ArsenalManager::getExtendedAmmunitions(),
            'extended_armors' => ArsenalManager::getExtendedArmors(),
            'extended_shields' => ArsenalManager::getExtendedShields(),
            'firearms' => ArsenalManager::getFirearms(),
        ]);
    }
}
