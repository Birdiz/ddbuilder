<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\BugType;
use App\Tools\SpamChecker;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class BugController extends AbstractController
{
    public function __construct(private SpamChecker $spamChecker)
    {
    }

    #[Route('/bug', name: 'bug')]
    public function index(Request $request, MailerInterface $mailer, string $to): Response
    {
        $form = $this->createForm(BugType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $message = (new Email())
                ->from($data['email'])
                ->to($to)
                ->subject('Un bug a été trouvé !')
                ->text(
                    $data['message'],
                    'text/plain'
                )
            ;

            if (1 === $this->spamChecker->getSpamScore($message, $request, $data['miellat'])) {
                throw new RuntimeException('Spam ! Go Away !');
            }

            $mailer->send($message);

            return $this->redirectToRoute('bug');
        }

        return $this->render('bug/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
