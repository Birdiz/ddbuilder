<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\DMScreenManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DMScreenController extends AbstractController
{
    #[Route('/dmscreen', name: 'dm_screen')]
    public function index(): Response
    {
        return $this->render('dm_screen/dm_screen.html.twig', [
            'damages' => DMScreenManager::getDamageInfo(),
            'vehicules' => DMScreenManager::getVehiculeInfo(),
            'buildings' => DMScreenManager::getBuildingInfo(),
            'lifestyles' => DMScreenManager::getLifestyles(),
        ]);
    }
}
