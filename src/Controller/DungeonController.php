<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\TreasureType;
use App\Model\AbstractController;
use App\Service\DungeonManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DungeonController extends AbstractController
{
    #[Route('/dungeon', name: 'dungeon')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(TreasureType::class);
        $keys = static::getEnumKeyFromInputName(
            $request,
            $form,
            [TreasureType::TYPE, TreasureType::CHEST, TreasureType::OPTIONS]
        );

        return $this->render('dungeon/dungeon.html.twig', [
            'form' => $form->createView(),
            'random_info' => DungeonManager::getRandomInfo(),
            'random_trap' => DungeonManager::getRandomTrap(),
            'random_treasure' => DungeonManager::getRandomTreasure($keys),
        ]);
    }
}
