<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\DiseaseType;
use App\Form\Type\MadnessType;
use App\Form\Type\MonthType;
use App\Model\AbstractController;
use App\Service\EnvironmentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EnvironmentController extends AbstractController
{
    #[Route('/environment', name: 'environment')]
    public function index(Request $request): Response
    {
        $weatherForm = $this->createForm(MonthType::class);
        $monthKey = static::getEnumKeyFromInputName($request, $weatherForm, MonthType::MONTH);
        $diseaseForm = $this->createForm(DiseaseType::class);
        $diseaseKey = static::getEnumKeyFromInputName($request, $diseaseForm, DiseaseType::DISEASE);
        $madnessForm = $this->createForm(MadnessType::class);
        $madnessKey = static::getEnumKeyFromInputName($request, $madnessForm, MadnessType::MADNESS);

        return $this->render('environment/environment.html.twig', [
            'random_info' => EnvironmentManager::getRandomInfo(),
            'metals' => EnvironmentManager::getMetals(),
            'random_forecast' => EnvironmentManager::getRandomForecast(),
            'weather_form' => $weatherForm->createView(),
            'month_forecast' => null === $monthKey || \is_array($monthKey)
                ? null
                : EnvironmentManager::getRandomForecast($monthKey),
            'disease_form' => $diseaseForm->createView(),
            'disease' => EnvironmentManager::getDisease($diseaseKey),
            'madness_form' => $madnessForm->createView(),
            'madness' => EnvironmentManager::getMadness($madnessKey),
        ]);
    }
}
