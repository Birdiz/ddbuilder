<?php

namespace App\Controller;

use App\Model\AbstractController;
use App\Service\ExtraRulesManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExtraRulesController extends AbstractController
{
    #[Route('/extra/rules', name: 'extra_rules')]
    public function index(): Response
    {
        return $this->render('extra_rules/extra_rules.html.twig', [
            'hidden_skills' => ExtraRulesManager::getHiddenSkills(),
        ]);
    }
}
