<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\Generator\BossType;
use App\Form\Type\Generator\PlaceType;
use App\Form\Type\Generator\PNJType;
use App\Form\Type\Generator\TravelType;
use App\Model\AbstractController;
use App\Service\Generator\BossManager;
use App\Service\Generator\PlaceManager;
use App\Service\Generator\PNJManager;
use App\Service\Generator\TravelManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/generator', name: 'generator_')]
class GeneratorController extends AbstractController
{
    public function __construct(private readonly TravelManager $travelManager)
    {
    }

    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->redirect($this->generateUrl('homepage'));
    }

    #[Route('/pnj', name: 'pnj')]
    public function pnj(Request $request): Response
    {
        $form = $this->createForm(PNJType::class);
        $keys = static::getEnumKeyFromInputName(
            $request,
            $form,
            [PNJType::RACE, PNJType::GENDER, PNJType::JOB, PNJType::ALIGNMENT, PNJType::FAMILY_MEMBERS]
        );

        return $this->render('generator/pnj/pnj.html.twig', [
            'form' => $form->createView(),
            'random_pnj' => PNJManager::getRandomPNJ($keys),
        ]);
    }

    #[Route('/boss', name: 'boss')]
    public function boss(Request $request): Response
    {
        $form = $this->createForm(BossType::class);
        $keys = static::getEnumKeyFromInputName(
            $request,
            $form,
            [BossType::LEVEL, BossType::DANGER, BossType::STUFF]
        );

        return $this->render(
            'generator/boss/boss.html.twig',
            [
                'form' => $form->createView(),
                'random_boss' => BossManager::getRandomBoss($keys),
            ]
        );
    }

    #[Route('/place', name: 'place')]
    public function place(Request $request): Response
    {
        $form = $this->createForm(PlaceType::class);
        $key = static::getEnumKeyFromInputName(
            $request,
            $form,
            PlaceType::TYPE
        );

        return $this->render(
            'generator/place/place.html.twig',
            [
                'form' => $form->createView(),
                'random_place' => PlaceManager::getRandomPlace($key),
            ]
        );
    }

    #[Route('/travel', name: 'travel')]
    public function travel(Request $request): Response
    {
        $form = $this->createForm(TravelType::class);
        $keys = static::getEnumKeyFromInputName(
            $request,
            $form,
            [TravelType::PACE, TravelType::DAYS, TravelType::PLAYERS, TravelType::LEVEL]
        );

        return $this->render(
            'generator/travel/travel.html.twig',
            [
                'form' => $form->createView(),
                'random_travel' => $this->travelManager->generateTravel($keys),
            ]
        );
    }
}
