<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\MagicItemType;
use App\Model\AbstractController;
use App\Service\MagicTradingPostManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MagicTradingPostController extends AbstractController
{
    #[Route('/magic/trading/post', name: 'magic_trading_post')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(MagicItemType::class);
        $keys = static::getEnumKeyFromInputName(
            $request,
            $form,
            MagicItemType::ITEM
        );

        return $this->render('magic_trading_post/magic_trading_post.html.twig', [
            'craft_info' => MagicTradingPostManager::getCraftInfo(),
            'sale_info' => MagicTradingPostManager::getSaleInfo(),
            'random_sale' => MagicTradingPostManager::getRandomSale(),
            'merchant' => MagicTradingPostManager::getRandomMerchant(),
            'form' => $form->createView(),
            'item' => MagicTradingPostManager::getItem($keys),
        ]);
    }
}
