<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\ShipType;
use App\Model\AbstractController;
use App\Service\SailingManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SailingController extends AbstractController
{
    #[Route('/sailing', name: 'sailing')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(ShipType::class);
        $keys = static::getEnumKeyFromInputName($request, $form, ShipType::NAME);

        return $this->render('sailing/sailing.html.twig', [
            'form' => $form->createView(),
            'ships' => SailingManager::getShips(),
            'officers' => SailingManager::getOfficers(),
            'random_crew' => SailingManager::getRandomCrew($keys),
        ]);
    }
}
