<?php

namespace App\Controller;

use App\Tools\Searcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    public function __construct(private readonly Searcher $searcher)
    {
    }

    #[Route('/search', name: 'search')]
    public function index(Request $request): Response
    {
        $needle = (string) $request->get('needle');

        return $this->render(
            'search/index.html.twig',
            [
                'needle' => $needle,
                'results' => '' === $needle ? null : $this->searcher->search($needle),
            ]
        );
    }
}
