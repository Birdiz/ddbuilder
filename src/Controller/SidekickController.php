<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\AbstractController;
use App\Service\SidekickManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SidekickController extends AbstractController
{
    #[Route('/sidekick', name: 'sidekick')]
    public function index(): Response
    {
        return $this->render(
            'sidekick/sidekick.html.twig',
            [
                'archetype_artificer' => SidekickManager::getArtificer(),
                'archetype_bard' => SidekickManager::getBard(),
                'archetype_savage' => SidekickManager::getSavage(),
            ]
        );
    }
}
