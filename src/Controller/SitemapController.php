<?php

declare(strict_types=1);

namespace App\Controller;

use App\Model\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class SitemapController extends AbstractController
{
    #[Route('/sitemap', name: 'sitemap', requirements: ['_format' => 'xml'], defaults: ['_format' => 'xml'])]
    public function index(RouterInterface $router, Request $request): Response
    {
        $urls = array_map(
            static function (\Symfony\Component\Routing\Route $route) {
                return ['loc' => $route->getPath()];
            },
            $router->getRouteCollection()->getIterator()->getArrayCopy()
        );

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');

        return $this->render(
            'sitemap/index.xml.twig',
            [
                'urls' => $urls,
                'hostname' => $request->getSchemeAndHttpHost(),
            ],
            $response
        );
    }
}
