<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\TavernManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TavernController extends AbstractController
{
    #[Route('/tavern', name: 'tavern')]
    public function index(): Response
    {
        return $this->render('tavern/tavern.html.twig', [
            'random_comfort' => TavernManager::getRandomComfort(),
            'random_ambiance' => TavernManager::getRandomAmbience(),
            'random_rooms' => TavernManager::getRandomRooms(),
            'random_miserable_meal' => TavernManager::getRandomMiserableMeal(),
            'random_poor_meal' => TavernManager::getRandomPoorMeal(),
            'random_modest_meal' => TavernManager::getRandomModestMeal(),
            'random_comfortable_meal' => TavernManager::getRandomComfortableMeal(),
            'random_rich_meal' => TavernManager::getRandomRichMeal(false),
            'random_aristocratic_meal' => TavernManager::getRandomRichMeal(true),
            'random_game' => TavernManager::getRandomGame(),
            'random_info' => TavernManager::getRandomInfo(),
        ]);
    }
}
