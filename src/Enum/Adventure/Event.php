<?php

declare(strict_types=1);

namespace App\Enum\Adventure;

use App\Model\AbstractEnum;

class Event extends AbstractEnum
{
    private const ANNIVERSARY_MONARCH = 'L\'anniversaire du règne d\'un monarque';
    private const ANNIVERSARY_EVENT = 'L\'anniversaire d\'un événement important';
    private const EVENT_ARENA = 'Un événement d\'arène';
    private const ARRIVAL_SHIP_CARAVAN = 'L\'arrivée d\'un navire ou d\'une diligence';
    private const ARRIVAL_CIRCUS = 'L\'arrivée d\'un cirque';
    private const ARRIVAL_NPC = 'L\'arrivée d\'un PNJ important';
    private const ARRIVAL_MODRONS = 'L\'arrivée d\'un défilé de Modrons';
    private const PERFORMANCE = 'Un performance artistique';
    private const EVENT_ATHLETIC = 'Un événement athlétique';
    private const BIRTH_CHILD = 'Une fête pour la naissance d\'un enfant';
    private const BIRTH_NPC = 'Une fête pour la naissance d\'un PNJ important';
    private const FESTIVAL_CIVIC = 'Un festival civil';
    private const COMET = 'L\'apparition d\'une comète';
    private const TRAGEDY = 'Une commémoration d\'une tragédie ancienne';
    private const TEMPLE = 'La consécration d\'un nouveau temple';
    private const CORONATION = 'Un nouveau couronnement';
    private const COUNCIL = 'La rencontre d\'un haut conseil';
    private const EQUINOX_SOLSTICE = 'Une fête pour l\'Équinoxe ou le Solstice';
    private const EXECUTION = 'Une exécution';
    private const FESTIVAL_FERTILITY = 'Le festival de la Fertilité';
    private const MOON_FULL = 'Une fête pour les pleines Lune';
    private const FUNERAL = 'Des funérailles';
    private const CADET_GRADUATION = 'La remise des diplômes des disciples d\'une école de magie';
    private const FESTIVAL_HARVEST = 'Un festival de la Récolte';
    private const HOLY_DAY = 'Un jour Saint';
    private const INVESTITURE_NOBLE = 'L\'investiture d\'un chevalier ou d\'un noble';
    private const ECLIPSE_LUNAR = 'Une éclipse lunaire';
    private const FESTIVAL_MIDSUMMER = 'Un festival de l\'été';
    private const FESTIVAL_MIDWINTER = 'Un festival de l\'hiver';
    private const MIGRATION = 'La migration de monstres';
    private const BALL = 'Le bal d\'un monarque';
    private const MOON_NEW = 'Une fête pour les nouvelles Lune';
    private const NEW_YEAR = 'La fête du nouvel an';
    private const PARDONING = 'La grâce d\'un prisonnier';
    private const CONJONCTION = 'Une conjonction de plans (ils apparaissent très proches)';
    private const ALIGNMENT = 'L\'Alignement planétaire';
    private const INVESTITURE_PRIEST = 'L\'investiture d\'un prêtre';
    private const GHOSTS = 'Une procession de fantômes';
    private const REMEMBERANCE = 'Un hommage à un soldat mort à la guerre';
    private const PROCLAMATION = 'La proclamation ou décret royal(e)';
    private const AUDIENCE = 'Un jour des doléances';
    private const TREATY = 'La signature d\'un traité';
    private const ECLIPSE_SOLAR = 'Une éclipse solaire';
    private const TOURNAMENT = 'Un tournois';
    private const TRIAL = 'Un jugement';
    private const UPRISING = 'Un soulèvement social violent';
    private const WEDDING = 'Un mariage ou anniversaire de mariage';
}
