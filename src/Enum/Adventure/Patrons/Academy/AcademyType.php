<?php

declare(strict_types=1);

namespace App\Enum\Adventure\Patrons\Academy;

use App\Model\AbstractEnum;

class AcademyType extends AbstractEnum
{
    private const BOARDING_SCHOOL = 'Un internat qui offre une relation paisible entre sa faculté et ses étudiants.';
    private const ARCANE_ENCLAVE = 'Une enclave arcanique qui renferme les plus ferveurs chercheurs et passionnés de
    magie.';
    private const SECRET_MONASTERY = 'Un monastère secret millénaire qui promeut de rigoureux entraînement.';
    private const ELITE_INSTITUTE = "Un collège élitiste des sciences et des arts qui n'acceptent que la crème de la 
    crème.";
    private const VAULT_SECRETS = 'La chambre des secrets ou cette conspiration qui se bat pour conserver ou détruire 
    tout le savoir lié à une vérité.';
    private const MUSEUM_DREAMS = 'Un musée des rêves qui rassemble les spécialistes de la communication ou du partage 
    du monde des rêves.';
}
