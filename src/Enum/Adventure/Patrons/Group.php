<?php

declare(strict_types=1);

namespace App\Enum\Adventure\Patrons;

use App\Model\AbstractEnum;

class Group extends AbstractEnum
{
    private const ACADEMY = 'Academie';
    private const ANCIENT_BEING = 'Être ancien';
    private const ARISTOCRAT = 'Aristocrate';
    private const CRIMINAL_SYNDICAT = 'Syndicat criminel';
    private const GUILD = 'Guilde';
    private const MILITARY_FORCE = 'Force militaire';
    private const RELIGIOUS_ORDER = 'Ordre religieux';
    private const SOVEREIGN = 'Souverain';
}
