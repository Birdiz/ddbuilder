<?php

declare(strict_types=1);

namespace App\Enum\Adventure\Plane;

use App\Model\AbstractEnum;
use App\Util\WeightedRandTrait;

class Layer extends AbstractEnum
{
    use WeightedRandTrait;

    private const MOUNT_CELESTIA = [
        'Lunia' => '50',
        'Mercuria' => '25',
        'Venya' => '12',
        'Solania' => '6',
        'Mertion' => '5',
        'Jovar' => '1',
        'Chronias' => '1',
    ];
    private const BYTOPIA = [
        'Dothion' => '60',
        'Shurrock' => '40',
    ];
    private const ELYSIUM = [
        'Amoria' => '60',
        'Eronia' => '20',
        'Belierin' => '5',
        'Thalasia' => '15',
    ];
    private const BEASTLANDS = [
        'Krigala' => '40',
        'Brux' => '30',
        'Karasuthra' => '30',
    ];
    private const ARBOREA = [
        'Arvandor' => '70',
        'Aquallor' => '25',
        'Mithardir' => '5',
    ];
    private const YSGARD = [
        'Ysgard' => '70',
        'Muspelheim' => '15',
        'Nidavellir' => '15',
    ];
    private const ARCADIA = [
        'Abellio' => '70',
        'Buxenus' => '30',
    ];

    private const ABYSS = [
        'Grande Abysse (Premier niveau)' => '20',
        'Principalement les niveaux sauvages' => '20',
        'Niveaux sauvages mortels' => '20',
        'Niveau infesté de démons' => '20',
        'Niveau controlé par un seigneur-démon' => '20',
    ];
    private const PANDEMONIUM = [
        'Pandesmos' => '45',
        'Cocytus' => '35',
        'Phlegethon' => '15',
        'Agathion' => '5',
    ];
    private const CARCERI = [
        'Orthrys' => '40',
        'Cathrys' => '30',
        'Minethys' => '15',
        'Colothys' => '10',
        'Porphatys' => '4',
        'Agathys' => '1',
    ];
    private const HADES = [
        'Oinos' => '60',
        'Niflheim' => '30',
        'Pluton' => '10',
    ];
    private const GEHENNA = [
        'Khalas' => '60',
        'Chamada' => '20',
        'Mungoth' => '15',
        'Krangath' => '5',
    ];
    private const NINE_HELLS = [
        'Avernus' => '30',
        'Dis' => '20',
        'Minauros' => '15',
        'Phlegethos' => '9',
        'Stygia' => '8',
        'Malbolge' => '6',
        'Maladomini' => '6',
        'Cania' => '4',
        'Nessus' => '2',
    ];
    private const ACHERON = [
        'Avalas' => '60',
        'Thuldanin' => '20',
        'Tintibulus' => '10',
        'Ocanthus' => '10',
    ];

    public static function getLayer(string $plane): ?string
    {
        $planeKey = array_search($plane, Plane::toArray(), true);
        if (false === $planeKey) {
            return null;
        }

        $layers = static::toArray();
        if (array_key_exists($planeKey, $layers)) {
            $layer = $layers[$planeKey];

            return static::getRandomizedValue(array_keys($layer), array_values($layer));
        }

        return null;
    }
}
