<?php

declare(strict_types=1);

namespace App\Enum\Adventure\Plane;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class Plane extends AbstractEnum implements WeightedInterface
{
    private const MATERIAL = 'Matériel';
    private const ALTERNATE_MATERIAL = 'Matériel aternatif';
    private const DEEP_ETHEREAL = 'Éthéré';
    private const FEYWILD = 'Féérie';
    private const SHADOWFELL = 'Gisombre';
    private const ELEMENT_AIR = 'Élémentaire (air)';
    private const ELEMENT_WATER = 'Élémentaire (eau)';
    private const ELEMENT_EARTH = 'Élémentaire (terre)';
    private const ELEMENT_FIRE = 'Élémentaire (feu)';
    private const POSITIVE = 'Positif';
    private const NEGATIVE = 'Négatif';
    private const ASTRAL = 'Astral';
    private const OUTLANDS = 'Extérieur';

    private const MOUNT_CELESTIA = 'Mont Céleste (LB)';
    private const BYTOPIA = 'Bytopie (LB, NB)';
    private const ELYSIUM = 'Élysée (NB)';
    private const BEASTLANDS = 'Terre des Bêtes (NB, CB)';
    private const ARBOREA = 'Arborée (CB)';
    private const YSGARD = 'Ysgard (CB, CN)';
    private const LIMBO = 'Limbes (CN)';
    private const PANDEMONIUM = 'Pandémonium (CN, CM)';
    private const ABYSS = 'Abysses (CM)';
    private const CARCERI = 'Carcères (CM, NM)';
    private const HADES = 'Hadès (NM)';
    private const GEHENNA = 'Géhenne (NM, LM)';
    private const NINE_HELLS = 'Les Neufs Enfers (LM)';
    private const ACHERON = 'Achéron (LM, LN)';
    private const MECHANUS = 'Méchanus (LN)';
    private const ARCADIA = 'Arcadie (LN, LB)';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 4, 4, 4, 4, 1, 1, 5, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3];
    }
}
