<?php

declare(strict_types=1);

namespace App\Enum\Adventure;

use App\Model\AbstractEnum;

class PlotHook extends AbstractEnum
{
    private const STONE_THROW = [
        'name' => 'Une pierre lancée',
        'hook' => 'Lors de sa cérémonie de couronnement, le Prince est frappé à la tête par une pierre lancée de la 
        foule, sa blessure est grande et tombe dans le coma.',
        'interaction' => '{character}, le frère du roi, revendique immédiatement le trône car son neveu n\'est plus 
        capable de régner. {character} prétend qu\'il y a une guerre civile à l\'horizon à moins que quelqu\'un puisse 
        guérir les blessures du garçon et prouver l\'implication de l\'oncle.',
        'locales' => 'Ville',
    ];

    private const AMBUSHED_WITHIN = [
        'name' => 'Ambuscade interne',
        'hook' => 'L\'un des wagons que vous escortez perd étrangement une roue au milieu d\'un chemin forestier. Les 
        autres gardes chuchotent entre eux et préparent leurs armes.',
        'interaction' => 'Le maître de la caravane a été convaincu d\'embaucher ces gardes dans la ville précédente. 
        {character} se méfie des nouveaux gardes.',
        'locales' => 'Forêt, Village',
    ];

    private const SIREN_CALL = [
        'name' => 'L\'appel de la sirène',
        'hook' => 'Vous avez réservé un passage sur un navire. À mi-chemin du voyage, vous vous réveillez pour ne 
        trouver que vous et vos compagnons à bord. Le navire est ancré à côté d\'une petite île et le chant d\'une 
        sirène remplit l\'air...',
        'interaction' => '{character} met en garde contre le danger sur les mers. {character} croit avoir rencontré une 
        créature de la mer avec une voix chantante envoûtante.',
        'locales' => 'Littoral',
    ];

    private const RELIC = [
        'name' => 'La relique',
        'hook' => 'En parcourant un magasin local, vous êtes attiré par un vieille épée rouillée. Elle vous semble 
        familier dans votre main et lorsque vous le retournez, vous trouvez votre nom gravé sur la lame.',
        'interaction' => '{character}, le commerçant âgé, dit que l\'épée est dans ce magasin depuis aussi longtemps 
        qu\'il peut se rappeller. {character} vous reconnaît, mais ne sait pas exactement d\'où.',
        'locales' => 'Village, Ville',
    ];

    private const WHITE_WOMAN = [
        'name' => 'La femme en blanc',
        'hook' => 'Vous êtes hantés par la vision d\'une femme en blanc. Elle disparaît lorsque vous marchez dans une 
        direction particulière, mais sinon elle vous barre le chemin.',
        'interaction' => '{character} ne vous regardera pas dans les yeux. {character} vous appelle "hanté", et fait un 
        signe de mise en garde.',
        'locales' => 'Village, Ville, Montagnes',
    ];

    private const CREEPING_FOREST = [
        'name' => 'Forêst rampante',
        'hook' => 'Quand vous vous réveillez, le village est entouré de tous côtés par une forêt inconnue.',
        'interaction' => '{character} attend à la lisière de la forêt. {character} vous demande de parler au nom du 
        village',
        'locales' => 'Forêt, Village',
    ];

    private const STRANGER = [
        'name' => 'L\'étranger',
        'hook' => 'La porte de la taverne s\'ouvre, et un vieux compagnon du bon vieu temps entre. Vous êtes choqué, 
        en voyant mourir les gens autour de vous.',
        'interaction' => 'Le compagnon ne se souvient pas de vous.',
        'locales' => 'Village, Ville',
    ];

    private const MURDER = [
        'name' => 'Le meurte',
        'hook' => 'Un grand groupe de corbeaux s\'abat sur la ville dans laquelle vous vous trouvez. Ils vous observent 
        pendant que vous marchez dans les rues.',
        'interaction' => '{character} dit que les corbeaux sont de mauvais augure. {character} déteste les oiseaux.',
        'locales' => 'Ville',
    ];

    private const CROSSROADS = [
        'name' => 'Carrefour',
        'hook' => 'En voyageant, vous passez un carrefour avec un petit cimetière. Quelques heures plus tard, vous 
        passez par le même carrefour, bien que vous n\'ayez pas changé de direction.',
        'interaction' => '{character} vous dit de ne pas emprunter cette route. {character} sourit légèrement trop en 
        vous parlant.',
        'locales' => 'Village',
    ];

    private const ONE_WHO_WATCHES = [
        'name' => 'Celui qui observe',
        'hook' => 'Depuis que vous avez pillé un ancien lieu de sépulture, vous avez rêvé d\'une étrange créature. Vous 
        avez maintenant commencé à la voir lorsque vous êtes éveillé.',
        'interaction' => '{character} ne peut pas voir la créature que vous décrivez. {character} a commencé à vous 
        traiter étrangement après que vous ayez confié vos peurs.',
        'locales' => 'Village, Donjon',
    ];

    private const PAST_PRESENT = [
        'name' => 'Votre passé, votre présent',
        'hook' => 'Vous entrez dans un village et réalisez qu\'il ressemble exactement au village dans lequel vous 
        avez grandi. En fait, vous voyez un enfant qui vous ressemble étonnamment jouer dans les champs.',
        'interaction' => '{character} ne vous reconnaît pas. {character} ne fait pas confiance aux étrangers.',
        'locales' => 'Village',
    ];

    private const SLAVERS = [
        'name' => 'Esclavagistes !',
        'hook' => 'Vous suivez les instructions jusqu\'au lieu de la quête, seulement pour vous retrouver entouré 
        d\'esclavagistes !',
        'interaction' => '{character} vous assure que c\'est un travail facile.',
        'locales' => 'Village, Montagnes',
    ];

    private const HELM_SLAUGHTER = [
        'name' => 'Le casque de massacre',
        'hook' => 'Vous trouvez un casque magnifiquement conçu. Vous le mettez et au bout de quelques instants vous 
        entendez une voix: "Oui... que le massacre commence..". Vous ne parvenez pas à retirer le casque.',
        'interaction' => '{character} vous suggère de ne pas porter le casque jusqu\'à ce qu\'il soit inspecté par un 
        assistant. {character} dit qu\'un sorcier essaiera simplement de le prendre, autant le porter.',
        'locales' => 'Ville, Donjon, Forêt',
    ];

    private const FAMILY_HEIRLOOM = [
        'name' => 'Héritage familial',
        'hook' => 'Vous entrez dans une ville pour la première fois, et après quelques heures d\'errance, vous voyez un 
        homme portant une épée particulière. Vous le reconnaissez, car c\'était l\'épée de votre père (ou mère).',
        'interaction' => '{character} n\'a aucune idée de qui vous êtes ou de quoi vous parlez. {character} a déjà vu 
        cet homme portant cette épée.',
        'locales' => 'Village, Ville, Donjon',
    ];

    private const DOOR = [
        'name' => 'La porte',
        'hook' => 'Vous êtes réveillé au milieu de la nuit par l\'aubergiste. Il est pâle et tremble. Il vous conduit 
        à une grande porte métallique au sous-sol. Il jure sur sa vie que cette porte n\'était pas là hier.',
        'interaction' => '{character} est propriétaire de l\'auberge depuis des années et a vécu ici toute sa vie. Il 
        n\'a jamais vu cette porte.',
        'locales' => 'city, town',
    ];

    private const EXPERIMENT = [
        'name' => 'L\'expérience',
        'hook' => 'En enquêtant sur une personne disparue, vous tombez sur un laboratoire abandonné. À votre grand 
        désarroi, vous trouvez des notes et des schémas faisant référence à vous.',
        'interaction' => '{character} a entendu des cris venant de cette partie des égouts la nuit. {character} a 
        disparu la semaine dernière, leur mère est malade d\'inquiétude.',
        'locales' => 'Ville',
    ];

    private const RED_STAR = [
        'name' => 'L\'étoile rouge',
        'hook' => 'Un prophète de malheur local a affirmé que la fin est proche. La nuit de sa "prédiction", une étoile 
        rouge traverse le ciel et s\'écrase sur une vieille ruine dans les collines voisines.',
        'interaction' => '{character} a dit par les dieux quand le monde se terminera. {character} pense que les 
        étoiles filantes sont des phénomènes naturels et ne suggèrent pas la fin du monde.',
        'locales' => 'Ville, Donjon',
    ];

    private const IMPOSTERS = [
        'name' => 'Imposteurs',
        'hook' => 'Vous arrivez à l\'auberge locale, mais le regard sur le visage de l\'aubergiste lorsque vous 
        franchissez la porte d\'entrée vous fait reculer. "Mais... vous êtes tous allés à l\'étage pour vous reposer, 
        il y a quelques instants, dit-il en tremblant.',
        'interaction' => '{character} ne vous regardera pas dans les yeux. {character} jure que vous venez de lui 
        parler et que vous aviez l\'air en colère',
        'locales' => 'Village, Ville, Forêt, Littoral',
    ];

    private const SPREAD = [
        'name' => 'L\'épidémie',
        'hook' => 'Une maladie se répand lentement dans toute la ville. Elle semble être non détectée par Détection de 
        poison ou de maladie. Les demi-orques ne sont pas affectés par la maladie.',
        'interaction' => '{character} pense que les tribus orcs ont dû faire cela. {character} refuse de vous laisser 
        quitter la ville, pour éviter toute propagation.',
        'locales' => 'Ville, Donjon',
    ];

    private const SAILOR_WARNING = [
        'name' => 'La mise en garde du marin',
        'hook' => 'Il est midi et la mer s\'éloigne soudainement du rivage, laissant les navires amarrés échoués et les 
        créatures aquatiques se précipitent pour se mettre à l\'abri. Quelques instants plus tard, des silhouettes 
        squelettiques se lèvent de la marée descendante et un souffle surnaturel brise le calme.',
        'interaction' => '{character} dit que la mer agit étrangement ce matin. {character} remarque que la marée est 
        complètement fausse.',
        'locales' => 'Littoral, Village, Ville',
    ];

    private const WEIRD_RELIC = [
        'name' => 'L\'étrange relique',
        'hook' => 'Vous mettez la main dans votre sac et tirez une relique inconnue. Il s\'agit d\'une petite figurine 
        en forme de poupée, sculptée dans l\'os. "Aidez-moi..." crie-t-elle, et de petites gouttes d\'eau commencent à 
        couler de ses yeux sculptés.',
        'interaction' => '{character} ce sac est ancien, doit être l\'un des premiers en son genre,',
        'locales' => '',
    ];

    private const UNREQUITED = [
        'name' => 'Non partagé',
        'hook' => 'Au centre du village se dresse une statue magnifiquement sculptée d\'une femme en armure de combat 
        complète, un héros de la ville natale. Un jeune prêtre aventurier, obsédé par la célèbre guerrière, invoque un 
        sort de transformation de pierre à la chair sur la statue dans un vain espoir d\'être avec elle. Cela 
        fonctionne.',
        'interaction' => '{character} a aimé les histoires du héros toute sa vie, et veut juste qu\'elle soit réelle. 
        {character} pense que le clerc a la tête dans les nuages. 
        {character} dit que les circonstances entourant la mort du héros ont toujours été étranges.',
        'locales' => 'Village, Ville, Littoral, Montagnes, Forêt',
    ];

    private const BLODDENING = [
        'name' => 'Le bain de sang',
        'hook' => 'Un cri brise le silence du petit matin. Une femme se tient à côté du puits de la ville, frappée 
        d\'horreur, alors que le sang coule du seau d\'eau dans ses mains. Il y a un coup de tonnerre et les nuages 
        lourds au-dessus libèrent leur déluge. Les rues de la ville sont rouges de sang.',
        'interaction' => '{character} perd la tête, la fin est arrivée. {character} pense que cela doit être une 
        punition pour avoir permis à ce sorcier de pratiquer ses arts sombres en ville.',
        'locales' => 'Village, Ville, Montagnes',
    ];

    private const MIRROT = [
        'name' => 'Le mirroir',
        'hook' => 'Un vieux miroir se trouve dans la tour abandonnée d\'un sorcier. En vous regardant dans le miroir, 
        votre reflet vous jette un regard grave et parle "Je suis venu vous prévenir... quelque chose arrive et votre 
        monde n\'est pas préparé.',
        'interaction' => '{character} dit que ce sorcier était un vieil homme gentil, mais quelque chose a changé en 
        lui, le rendant paranoïaque. {character} est le petit-fils du défunt sorcier.',
        'locales' => 'Village, Ville, Montagnes, Donjon',
    ];

    private const ARRIVAL = [
        'name' => 'L\'arrivée',
        'hook' => 'En se reposant sous une ancienne pierre dressée, vous ressentez un grondement soudain, et une 
        étincelle de lumière apparaît. Après son passage, il y a un être bipède étendu inconscient à la base de la 
        pierre portant des vêtements étranges. Il est inconsciente.',
        'interaction' => '{character} a toujours cru que les anciennes pierres sont plus que de simples marqueurs de 
        voyage',
        'locales' => 'Village, Forêt, Plaines',
    ];

    private const NEEDS_FEED = [
        'name' => 'Ça doit manger',
        'hook' => 'Un fermier paniqué vous montre son poulailler. À l\'intérieur, vous trouverez une créature sans 
        forme ressemblant à une goutte avec de longues vrilles. Il n\'y a pas de poules, et la créature semble gagner 
        lentement en taille.',
        'interaction' => '{character} n\'a jamais vu une créature comme celle-ci. {character} était un aventurier et se 
        souvient qu\'un cube ou un limon gélatineux était similaire',
        'locales' => 'Village, Donjon, Plaines, Montagnes, Forêt',
    ];

    private const PAUPER_KING = [
        'name' => 'Le pauvre roi',
        'hook' => 'Vous trouvez un mendiant sur le bord de la route à l\'extérieur d\'une grande ville, l\'air confus et 
        de mauvaise humeur. Avec un sursaut, vous le reconnaissez comme le roi local.',
        'interaction' => '{character} dit que le roi a disparu, certains pensent qu\'il a été assassiné',
        'locales' => 'Village, Ville',
    ];

    private const ENNEMY_ENNEMY = [
        'name' => 'Ennemi de mon ennemi',
        'hook' => 'Un cri d\'avertissement résonne à travers les champs entourant le village, en regardant vers la 
        lisière de la forêt vous voyez une grande bande d\'orcs charger. En dégainant vos armes, vous réalisez que les 
        orcs n\'attaquent pas, ils fuient quelque chose d\'autre.',
        'interaction' => '{character} pense que les orcs sont dangereux et ne devraient pas être autorisés à entrer 
        dans la ville. {character} pense qu\'il est préférable de travailler avec les orcs pour toutes les personnes 
        impliquées',
        'locales' => 'Village, Ville, Forêt',
    ];

    private const WANING = [
        'name' => 'Le déclin',
        'hook' => 'Les torches, les bougies et tout ce qui n\'est pas des feux magiques brûlent faiblement dans la 
        ville. Un épais brouillard s\'installe lentement sur la ville. Les mouvements dans le brouillard montrent 
        parfois des formes humanoïdes fantomatiques se déplaçant.',
        'interaction' => '{character} la dernière fois que cela s\'est produit, les plus jeunes enfants de chaque 
        famille ont disparu. {character} dit que vous devez toujours garder le feu allumé lorsque le brouillard est là',
        'locales' => 'Village, Ville, Forêt, Plaines',
    ];

    private const DOUBLECROSS = [
        'name' => 'Trahison',
        'hook' => 'Vous êtes engagé par un commerçant aisé pour protéger son entrepôt. Soudain, l\'entrepôt grouille de 
        gardes et vous êtes arrêté pour avoir volé l\'artefact même que vous avez été engagé pour protéger.',
        'interaction' => '{character} croit cet argent facile. {character} a un mauvais pressentiment à ce sujet.',
        'locales' => 'Ville, Littoral',
    ];

    private const FREEDOM = [
        'name' => 'Liberté',
        'hook' => 'Vous escortez une caravane entre les villes. Tard dans la nuit, vous êtes de quart et vous entendez 
        un bruit provenant de la cargaison. Vous enquêtez sur le son et vous trouvez qu\'il vient de l\'intérieur de la 
        cargaison. Une petite voix dit "Libérez-nous".',
        'interaction' => '{character} ne sait pas ce qu\'il y a dans les caisses, et ils ne veulent pas non plus le 
        savoir.',
        'locales' => 'Village, Ville, Plaines, Forêt, Montagnes',
    ];

    private const GOLD_FILCH = [
        'name' => 'Voleur d\'or',
        'hook' => 'Vous atteignez votre porte-monnaie le payez pour l\'article et remarquez, à votre grand désarroi, 
        que tout votre or est manquant.',
        'interaction' => '{character}, le commerçant, vérifie son coffre-fort et trouve également son or manquant.',
        'locales' => '',
    ];

    private const WOLVES_SHEEPS = [
        'name' => 'Les loups et les moutons',
        'hook' => 'En arrivant en ville, vous remarquez que les gens sont opprimés. De plus, l\'intérieur de la taverne 
        est saccagé. Un groupe de brigands vit à proximité.',
        'interaction' => '{character} craint que les brigands contrôlant la route commerciale hors de la ville ne tuent 
        tout le monde dans la ville. {character} sait où les brigands attaquent les caravanes',
        'locales' => 'Ville, Forêt, Montagnes',
    ];

    private const GOBLIN_TOWER = [
        'name' => 'Tour gobeline',
        'hook' => 'Un sorcier local demande de l\'aide. Il est gêné d\'admettre qu\'il a perdu le contrôle de sa tour 
        de sorcier à proximité au profit d\'une tribu de gobelins et ce n\'est qu\'une question de temps avant qu\'ils 
        ne fassent accidentellement un trou dans la réalité.',
        'interaction' => '{character} a toujours ressenti le sorcier était dangereux, et cela prouve juste qu\'il est 
        aussi stupide. {character} prétend avoir vu des lumières rougeoyantes venant de la tour la nuit précédente.',
        'locales' => 'Village, Forêt, Montagnes, Marais, Plaines',
    ];

    private const WATER_EVERYWHERE = [
        'name' => 'De l\'eau, partout',
        'hook' => 'Les gens tombent malades. L\'eau de la rivière est devenue aigre la semaine précédente. Quelqu\'un 
        doit explorer la rivière pour trouver le problème.',
        'interaction' => '{character} prétend qu\'un druide est censé protéger la forêt et la rivière du nord. 
        {character} est tombé gravement malade en buvant de l\'eau contaminée.',
        'locales' => 'Village, Littoral, Ville, Plaines, Froêt, Rivière, Marais',
    ];

    private const UNDDER_ATTACK = [
        'name' => 'Sous le siège',
        'hook' => 'Une lettre arrive d\'un petit noble voisin. Leur fort est assiégé par une force ennemie inconnue 
        qui attaque depuis la forêt voisine. Ils demandent l\'aide d\'aventuriers de renom.',
        'interaction' => '{character} pense que cette attaque s\'est produite parce que le fort est juste à la lisière 
        d\'une forêt magique. {character} peut vous guider vers votre destination moyennant des frais.',
        'locales' => 'Village, Ville, Forêt',
    ];

    private const INVADERS_INSIDE = [
        'name' => 'Intrus !',
        'hook' => 'Les gobelins sont au milieu de la ville ! Une série de grottes se sont ouvertes dans les égouts et 
        les rues et la ville est attaquée de l\'intérieur !',
        'interaction' => '{character} ne croit pas que les gobelins auraient pu le faire eux-mêmes. {character} demande 
        aux aventuriers d\'aider sa famille dans une auberge locale',
        'locales' => 'Ville',
    ];

    private const NIGHT = [
        'name' => 'Être de la nuit',
        'hook' => 'On vous dit de rencontrer un donneur de quête potentiel dans un cimetière voisin. À votre arrivée, 
        il est clair que cette personne est un vampire...',
        'interaction' => '{character} vous avertit que {character}, le donneur de quête, est étrange et potentiellement 
        dangereux.',
        'locales' => 'Ville',
    ];

    private const UNICORN = [
        'name' => 'La Licorne',
        'hook' => 'Vous êtes réveillé de votre repos au camp au son d\'un cheval. En suivant le son, vous voyez la 
        vérité : c\'est une licorne et elle est blessée.',
        'interaction' => '{character} croit qu\'une créature magique vit dans les bois à proximité.',
        'locales' => 'Forêt, Village, Ville',
    ];

    private const BREWFEST = [
        'name' => 'Festival de la bière',
        'hook' => 'Le festival est arrivé! Mais la caravane transportant la bière n\'est pas arrivée, et cela fait 
        maintenant une semaine de retard.',
        'interaction' => '{character} a commandé les bières de la ville voisine il y a des semaines. {character} est 
        prêt à annuler le petit-déjeuner si les bières ne sont pas trouvées.',
        'locales' => 'Village, Ville',
    ];

    private const ROYAL_SUMONS = [
        'name' => 'Une convocation royale',
        'hook' => 'Un messager arrive. La terre est attaquée et la reine a fait appel à vous pour l\'aider à défendre 
        ses terres.',
        'interaction' => 'Le messager est impatient. {character} est inquiet que vous soyez troublés.',
        'locales' => '',
    ];

    private const MOUNTAIN_KING = [
        'name' => 'Horde du roi de la montagne',
        'hook' => 'Vous trouvez une carte au bas d\'un coffre. C\'est écrit en nain et, s\'elle est lue, identifie une 
        horde de trésors cachés haut dans les montagnes',
        'interaction' => '{character} pense que c\'est une chance unique de le rendre riche. {character} est préoccupé 
        par ce qui pourrait garder le butin...',
        'locales' => 'Montagnes, Donjon',
    ];

    private const UNLUCKY_NUMBER = [
        'name' => 'Un chiffre porte-malheur',
        'hook' => 'Une compagnie de douze guerriers est à la recherche d\'un treizième. Il s\'avère que vous avez été 
        nominé.',
        'interaction' => 'L\'un des douze, nommé {character}, semble jeter de nombreux regards significatifs sur 
        {character}.',
        'locales' => '',
    ];

    private const ERRANT_WIZARD = [
        'name' => 'Le magicien nomade',
        'hook' => 'Un sorcier célèbre était censé vous rencontrer, mais n\'est jamais arrivé. Les rumeurs disent que le 
        sorcier est allé sur la côte sud à la recherche d\'un artefact et n\'est jamais revenu.',
        'interaction' => '{character} pense que vous devriez aller le chercher. Le sorcier est bien connu pour sa 
        distraction, et poursuivant ses recherches malgré les dangers ou autres soucis',
        'locales' => 'Ville, Littoral, Donjon',
    ];

    private const EVIL_LURKS = [
        'name' => 'Un mal se cache',
        'hook' => 'Une forteresse abandonnée depuis longtemps au nord a été vue éclairée la nuit par des lumières 
        bleues flottantes. Un air vicié imprègne la terre voisine.',
        'interaction' => '{character} avertit que d\'étranges créatures fées vivent dans la région. {character} ne 
        s\'approcherait pas de la forteresse même si vous le payiez.',
        'locales' => 'Désert, Forêt, Montagnes, Donjon',
    ];

    private const NO_ENTRY = [
        'name' => 'No Entry',
        'hook' => 'Les portes de la ville sont barrées. Des gardes étranges d\'une espèce que vous ne reconnaissez pas 
        empêchent votre entrée.',
        'interaction' => '{character} ne reconnaît pas l\'espèce de ceux qui protègent les portes. {character} les 
        portes ont été barrées comme ça pendant pas plus d\'une semaine.',
        'locales' => 'Ville',
    ];

    private const LAMBS_SLAUGHTER = [
        'name' => 'Agneaux au massacre',
        'hook' => 'Une compagnie entière de héros a été envoyé pour faire face à un mal croissant à l\'est. Une seule 
        revient : elle est aveugle et à moitié folle.',
        'interaction' => '{character} a peur pour son frère, qui était l\'éclaireur du groupe de héros.',
        'locales' => '',
    ];

    private const REFUGE_FIRE = [
        'name' => 'Refuge contre le feu',
        'hook' => 'Un matin, quelques nains descendent des montagnes, cherchant refuge dans la ville. Le lendemain, 
        beaucoup d\'autres arrivent. Un démon du feu a élu domicile dans leur montagne.',
        'interaction' => '{character} pense que ces nains peuvent trouver un autre endroit pour trouver refuge. 
        {character} veut aider les nains.',
        'locales' => 'Ville, Montagnes, Donjon',
    ];

    private const CIRCUS_PAIN = [
        'name' => 'Un cirque de douleur',
        'hook' => 'Un zoo itinérant s\'est installé dans un ville. On vous a demandé d\'enquêter sur des rumeurs selon 
        lesquelles l\'attraction est un être intelligent, asservi.',
        'interaction' => '{character} veut aller voir la nouvelle créature. {character} considère ce que fait le 
        gardien de zoo comme de l\'esclavage et il devrait être tenu pour responsable.',
        'locales' => 'Ville',
    ];

    private const HOG_LIFE = [
        'name' => 'Une vie de porc',
        'hook' => 'Après une nuit de beuverie et de festin dans la ville, vous vous réveillez pour trouver la ville vide 
        de villageois et à la place elle est remplie de porcs',
        'interaction' => '{character} dit que la ville vénère une déesse respectable de la terre, récolte et la 
        charité. {character} n\'autorise que ceux qui sont strictement invités aux exploits à participer.',
        'locales' => 'Village, Ville',
    ];

    private const ARACHNOPHOBIA = [
        'name' => 'Arachnophobie',
        'hook' => 'Les araignées rampent depuis le bas du village ! Les citoyens sont paniqués.',
        'interaction' => '{character} dit "Des araignées, pourquoi fallait-il que ce soient des araignées ?"',
        'locales' => 'Village',
    ];

    private const WRITHES_DEEPS = [
        'name' => 'Ça se tort dans les profondeurs',
        'hook' => 'Un monstre marin massif et terrifiant s\'échoue sur le rivage à côté de la ville avec une morsure 
        massive retirée de son côté. Au cours de la semaine suivante, de nombreux navires sont perdus en mer.',
        'interaction' => '{character} pense qu\'il y a toujours un plus gros poisson. {character} est un prophète de 
        malheur qui croit que c\'est un présage de la fin.',
        'locales' => 'Ville, Littoral',
    ];

    private const KING_FAVOR = [
        'name' => 'La requête du roi',
        'hook' => 'Un concours d\'archerie se déroule dans la capitale. La récompense est une faveur raisonnable du 
        roi.', 'interaction' => '{character} est un archer renommé et le vainqueur attendu.',
        'locales' => 'Ville',
    ];

    private const TOURNEY_TROUBLES = [
        'name' => 'Problèmes au tournoi',
        'hook' => 'Vous participez à la grande mêlée, la compétition de joute lors du tournoi d\'été. Au milieu du 
        combat, des cris fusent de la foule. Les terrains du tournoi sont attaqués !',
        'interaction' => '{character}, un seigneur local, et ses chevaliers n\'ont pas été invités au tournoi. 
        {character} est un capitaine de garde qui boit beaucoup.',
        'locales' => 'Ville',
    ];

    private const AWAKENS = [
        'name' => 'Il se réveille',
        'hook' => 'Il y a une vieille tombe oubliée et scellée que les enfants locaux aiment visiter. Après le dégel 
        de l\'hiver, la tombe est laissée ouverte et vide... cassée de l\'intérieur.',
        'interaction' => '{character} rappelle souvent aux enfants de ne pas s\'approcher de la tombe. {character} a vu 
        le fantôme d\'un vieil homme près de la tombe.',
        'locales' => 'Donjon',
    ];

    private const MISSING_CAT = [
        'name' => 'Chat disparu. Offre récompense',
        'hook' => 'Un noble local vous a chargé de retrouver son pauvre chat disparu, Tabitha. Le chat est en fait un 
        tabaxi qui s\'est enfui de son propriétaire d\'esclave.',
        'interaction' => '{character} a besoin de quelqu\'un pour retrouver son adorable chat, il parle de la beauté 
        de la bête. {character} a vu quelqu\'un se faufiler hors du manoir la nuit et se précipiter vers les bois 
        voisins.',
        'locales' => '',
    ];

    private const WINNER = [
        'name' => 'Tout le monde peut gagner',
        'hook' => 'La taverne propose une série de tournois : cartes, boisson, fléchettes et musique. Les prix incluent 
        des objets magiques et des trésors.',
        'interaction' => '{character} vous met au défi d\'un jeu à boire. {character} collecte des paris sur les jeux.',
        'locales' => 'Ville',
    ];

    private const ONE_MORE_DAY = [
        'name' => 'Un jour de plus',
        'hook' => 'Les fantômes des morts commencent à marcher dans les rues de la ville, pas pour blesser quelqu\'un, 
        mais pour vaquer à leurs occupations. Au soleil levant, ils retournent tous à la crypte.',
        'interaction' => '{character} a vu sa mère la nuit dernière, elle est venue le voir mais elle est décédée 
        l\'hiver précédent. {character} a vu une femme, pas un fantôme, marcher dans les rues avec les fantômes 
        chantant pour elle-même.',
        'locales' => 'Ville, Donjon',
    ];

    private const PERSISTENT_CHILL = [
        'name' => 'Une glace persistente',
        'hook' => 'Alors le printemps arrive dans la vallée, le dégel hivernal ne se produit jamais. La rivière reste 
        gelée et un vent froid souffle du col de la montagne au nord.',
        'interaction' => '{character} craint qu\'ils ne manquent de bois. {character} ne peut pas sortir sur le lac 
        pour pêcher.',
        'locales' => '',
    ];

    private const RELUCTANT_KILLER = [
        'name' => 'Le tueur rétissant',
        'hook' => 'Vous entendez des cris, et au coin de la rue pour trouver un torse nu debout dans une position de 
        combat entraînée avec sa lame prête. Il est entouré de voyous locaux. Vous devriez aider, mais vous ne savez 
        pas qui menace qui.',
        'interaction' => '{character} est venu dans cette ville pour trouver sa fille, mais a été rejeté par les 
        humains qui vivent dans la ville.',
        'locales' => '',
    ];

    private const DAWN_BRINGS = [
        'name' => 'Qu\'apporte l\'aube?',
        'hook' => 'Chaque matin dans cette ville, vous êtes réveillé par le chant du coq. Ce matin, vous êtes réveillé 
        par un hurlement horrible et un silence inquiétant juste après.',
        'interaction' => '{character}, l\'aubergiste, ne trouve pas sa fille. {character} est également manquant.',
        'locales' => '',
    ];

    private const RIDER = [
        'name' => 'Où est ton cavalier ?',
        'hook' => 'Un cheval sans cavalier, avec sa scelle et ses brides, s\'approche de vous et ne vous lâche plus. 
        Lorsque vous montez sur son dos, il galope vers l\'ouest.',
        'interaction' => '{character} a disparu dans cette région depuis des jours.',
        'locales' => '',
    ];

    private const FETCH_QUEST = [
        'name' => 'Rapporter la quête',
        'hook' => 'Le garde forestier vous a demandé d\'abattre sept ours noirs car leur population est devenue 
        incontrôlable. L\'aubergiste vous demande de lui apporter cinq foies d\'ours intacts.',
        'interaction' => 'Tout le monde en ville répète les mêmes salutations encore et encore. {character} ne vous 
        parlera pas tant que vous n\'aurez pas fait vos preuves en ville.',
        'locales' => '',
    ];

    private const FOOL_GOLD = [
        'name' => 'L\'or du fou',
        'hook' => 'Vous recevez un pièce d\'or pour votre service à un vieil homme étrange. Chaque fois que vous 
        ouvrez votre porte-monnaie, vous ne trouvez que cette pièce, même si vous placez d\'autres pièces avec.',
        'interaction' => '{character} vous offre un objet magique pour votre service. {character} avertit que la 
        cupidité consomme tout.',
        'locales' => '',
    ];

    private const FALSE_IDOL = [
        'name' => 'Fausse idôle',
        'hook' => 'Pendant des générations, les habitants de cette vallée ont fait une offrande à une divinité locale 
        sur une colline, et une bonne récolte et la prospérité ont suivi. Récemment, les habitants ont érigé un 
        monument sur le lieu de l\'offrande. Depuis lors, leurs récoltes ont commencé à échouer.',
        'interaction' => '{character} pense que les offrandes de nourriture étaient un gaspillage, et que le monument 
        est une démonstration plus permanente de leur dévotion. {character} craint que le monument ne soit une erreur, 
        et maintenant une plus grande démonstration de dévotion est nécessaire.',
        'locales' => '',
    ];

    private const HEIST = [
        'name' => 'Le bracage',
        'hook' => 'Les rebelles d\'un royaume dirigé par un tyran vous ont chargé de voler sa précieuse pierre d\'Ioun. 
        Ils ont organisé une invitation au bal d\'hiver annuel du roi. Le reste dépend de vous.',
        'interaction' => '{character} refuse de laisser le règne du roi continuer. {character} vous propose de vous payer 
        pour infiltrer les rebelles et trouver leur base.',
        'locales' => '',
    ];

    private const BLADE_PEER = [
        'name' => 'Une lame sans pareil',
        'hook' => 'Un forgeron légendaire s\'est installé dans un village voisin. Elle a entendu dire qu\'une étoile 
        de métal est tombée dans les collines voisines et propose de fabriquer une arme de magnificence pour quiconque 
        lui apportera le métal.',
        'interaction' => '{character} peut vous guider jusqu\'à l\'endroit où l\'étoile est tombée. {character} a 
        remarqué plus de gobelins et d\'orcs dans les collines à proximité de l\'étoile déchue.',
        'locales' => '',
    ];

    private const SHERRIF = [
        'name' => 'Le shérif',
        'hook' => 'Le shérif de la ville propose de vous guider à travers le col de la montagne si vous l\'aidez à tuer une 
        bête qui attaque les lignes de ravitaillement de sa ville.',
        'interaction' => '{character} veut aider. mais sa ville doit être sa priorité. {character} ne pense pas que le 
        shérif fait du bon travail et veut que vous sabotiez sa prochaine mission pour prouver son incompétence.',
        'locales' => '',
    ];

    private const TOME_UNSPEAKABLE_EVIL = [
        'name' => 'Le livre du mal indicible',
        'hook' => 'Une expédition archéologique met au jour un livre ancien. En moins d\'une semaine, tous les membres 
        de l\'expédition sont morts et l\'archéologue principal kidnappé.',
        'interaction' => '{character} est un expert en anciennes traditions elfiques et a financé le projet. {character} 
        a dirigé l\'expédition jusqu\'à leur emplacement mais est rentré chez lui immédiatement.',
        'locales' => '',
    ];

    private const LESSER_EVIL = [
        'name' => 'Le moindre mal',
        'hook' => 'Il y a un mal qui monte dans la région et il utilise un objet magique terrible. Il n\'y a qu\'une 
        seule personne qui peut aider. Elle a été la dernière à essayer d\'utiliser l\'artefact pour dominer le monde, 
        et elle est enfermée dans une prison magique.',
        'interaction' => '{character} a passé plusieurs décennies piégé dans la prison magique. {character} refuse de 
        quitter le côté du personnage maléfique, ils ne lui font pas confiance.',
        'locales' => 'Ville, Donjon',
    ];

    private const DOMESTICATION_SLAVERY = [
        'name' => 'Domestication ou esclavage ?',
        'hook' => 'Un groupe d\'elfes des bois est venu en ville pendant la nuit et a libéré tous les animaux de la 
        captivité, refusant de les rendre. Les habitants de la ville sont prêts à faire la guerre à ce sujet.',
        'interaction' => '{character} ne libérera pas leurs animaux, leur survie en dépend.',
        'locales' => 'Village, Forêt',
    ];

    private const CREEPING_CORRUPTION = [
        'name' => 'Une corruption rampante',
        'hook' => 'Un ranger a trouvé une maladie qui se répand au fond de la forêt qui tue toute la végétation et 
        rend les animaux fous. La propagation atteindra bientôt la périphérie d\'une ville locale et les fermes qui la 
        nourrissent.',
        'interaction' => '{character} pense qu\'il existe un druide qui pourrait être en mesure d\'aider. {character} 
        a vu le druide pour la dernière fois plus d\'un mois auparavant lorsqu\'il a acheté des potions au druide.',
        'locales' => 'Forêt, Marais, Plaines',
    ];

    private const GRIMOIRE_GONE = [
        'name' => 'Grimoire disparu',
        'hook' => 'Le grimoire d\'un grand sorcier a été volé. Le livre de sorts contient l\'un des sorts les plus 
        rares et les plus destructeurs.',
        'interaction' => '{character} est terrifié à l\'idée que le sort du sorcier soit dans le monde. {character} 
        peut avoir des informations sur un éventuel voleur.',
        'locales' => 'Ville',
    ];

    private const TROUGH_FADE = [
        'name' => 'À travers la disparition',
        'hook' => 'Chaque fois que vous dormez, vous rêvez d\'être surveillé par un impossible créature aux nombreux 
        yeux, vous torturant pour obtenir des réponses. Après le dernier rêve, vous vous réveillez avec des marques 
        physiques de votre torture sur votre corps.',
        'interaction' => '{character} ne peut pas identifier d\'où viennent les blessures. {character} vous suggère de 
        parler au voyant local.',
        'locales' => 'Ville, Donjon',
    ];

    private const SURGEON = [
        'name' => 'Le chirugien',
        'hook' => 'Vous passez une nuit à boire avec les habitants. Vous vous souvenez peu de la nuit précédente 
        lorsque vous vous réveillez, attaché à une table d\'opération.',
        'interaction' => '{character} vous dit de faire attention en ville la nuit, certaines personnes ont disparu. 
        {character} ne trouve pas sa mère.',
        'locales' => 'Ville, Donjon',
    ];

    private const SILENT_GRAVE = [
        'name' => 'Silencieux comme une tombe',
        'hook' => 'Vous remarquez qu\'aucun oiseau ne chante ce matin. Au cours de votre voyage, vous remarquerez 
        également qu\'il n\'y a aucun insecte ou animal de quelque sorte que ce soit dans cette partie du pays.',
        'interaction' => '{character}, un druide, est gravement préoccupé par la forêt le long de la route du nord.',
        'locales' => 'Forêt, Marais, Montagnes',
    ];

    private const FOG = [
        'name' => 'Qu\'est-ce qui se cache dans le brouillard ?',
        'hook' => 'Le village vous accueille rapidement avec sa nourriture et son feu. Le soir, un épais brouillard 
        s\'installe et entoure le village. Les villageois commencent à enfiler des armures et des armes, et vous 
        entendez les cris surnaturels qui résonnent dans le brouillard.',
        'interaction' => '{character} croit que les créatures du brouillard sont les morts, s\'étant élevées des 
        terres tumultueuses à l\'est. {character} s\'excuse de vous avoir trompé, mais explique qu\'ils ont 
        désespérément besoin de votre aide.',
        'locales' => 'Village, Forêt',
    ];

    private const WARDSTONE = [
        'name' => 'La pierre de vigilance',
        'hook' => 'La ville est protégée par un cristal magique qui est situé en son centre. Hier soir, quelque chose 
        a volé le cristal. La ville est maintenant vulnérable aux maux qui résident dans le lac et les forêts à 
        proximité.',
        'interaction' => '{character} a vu un grand démon ailé descendre des montagnes et prendre la gemme. 
        {character} craint que la ville ne soit incapable de se défendre sans la gemme.',
        'locales' => 'Village, Forêt',
    ];
}
