<?php

declare(strict_types=1);

namespace App\Enum\Adventure;

use App\Model\AbstractEnum;

class Prophecy extends AbstractEnum
{
    private const GOLD_LION = 'L\'arrivée du Lion d\'or offrira l\'abondance.';

    private const REBORN_KING = 'La renaissance de l\'enfant roi apaisera les conflits de la Terre.';

    private const RATS = '3 rats apperçus au pas de la porte annonceront la Peste dans la région.';

    private const FLOOD = 'Grenouille sautant des toits.<br/> 
    Poisson glissant des arbres.<br/> 
    Torrent rasant les herbes.<br/>';

    private const GODS_WARS = 'Lorsque le Soleil sera caché par la Lune, la guerre entre les Dieux sera déclarée. Ce 
    momentum causera le chaos dans le plan Astral.';

    private const FIELON_INVASION = 'Le puit du centre de la Terre s\'ouvrira et un torrent de fièlons envahira le 
    monde. La fin de l\'existence matérielle sera complète.';

    private const REVERSE = 'La nuit devient le jour et le jour devient la nuit. Êtres du Soleil endormez-vous, êtres de
    la Lune réveillez-vous.';

    private const MAGES = 'Le magicien gris trouvera son chemin.<br/>
    Le magicien rouge trouvera sa fin.<br/>
    Le magicien noir trouvera son sacrilège.<br/>
    Le magicien blanc trouvera la neige.';

    private const WINTER = 'Trois étés sans Soleil.<br/>
    Trois mésanges sans nids.<br/>
    Trois ruches sans abeilles.<br/>
    Trois hivers s\'éveillent.';

    private const TIME = 'Lorsque les planètes une ligne formeront, le temps s\'arrêtera.';
}
