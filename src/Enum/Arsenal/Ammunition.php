<?php

declare(strict_types=1);

namespace App\Enum\Arsenal;

use App\Model\AbstractEnum;

class Ammunition extends AbstractEnum
{
    private const BODKIN = [
        'name' => 'Pointe Bodkin (10)',
        'price' => '2 PO',
        'special' => 'Lorsque vous obtenez le maximum sur les dés de dégâts de l\'arme, vous lancez 1d4 supplémentaire 
        et l\'ajoutez aux dégâts finaux.',
    ];

    private const BROADHEAD = [
        'name' => 'Pointe à tete large (10)',
        'price' => '2 PO',
        'special' => 'Lorsque vous infligez des dégâts à une créature portant une armure légère ou sans armure, ou avec 
        une armure naturelle avec une CA de 16 ou moins, vous infligez 1 dégât tranchant supplémentaire. Les créatures 
        qui comptent pour ce bonus sont à la discrétion du MD.',
    ];

    private const BARBED = [
        'name' => 'Pointe/carreau barbelé',
        'price' => '1 PO',
        'special' => 'Cette munition provoque des blessures qui saignent. Une créature touchée par une flèche ou un 
        carreau barbelé subit 1 point de dégâts perforants au début de son tour jusqu\'à ce qu\'elle utilise son action 
        pour s\'en libérer, ou jusqu\'à ce qu\'elle réussisse un jet de sauvegarde de Constitution DD 15, qu\'elle peut 
        effectuer à la fin de chaque tour. Si la créature réussit le jet de sauvegarde, toutes les flèches barbelées 
        actuellement dans la créature sont détruites et elles sont immunisées à cet effet pendant 10 minutes. Une fois 
        retirées de la blessure, les munitions sont détruites.',
    ];

    private const SPLINTERING = [
        'name' => 'Pointe/carreau Shrapnel',
        'price' => '4 PO',
        'special' => 'Lorsque vous frappez une créature avec une flèche ou un carreau Shrapnel, elle explose en éclats 
        de bois ou d\'os. En plus d\'infliger des dégâts normaux, chaque créature dans un rayon de 3 mètres autour de 
        la cible doit effectuer un jet de sauvegarde de dextérité DD12. Une cible subit 1d4 dégâts perforants, ou la 
        moitié en cas de sauvegarde réussie. Une fois qu\'elles explosent, les munitions sont détruites.',
    ];

    private const SMOLDERING = [
        'name' => 'Pointe/carreau explosive/-if',
        'price' => '10 PO',
        'special' => 'Cette munition a une pointe creuse contenant le feu d\'alchimiste et un mécanisme d\'allumage 
        simple. En plus d\'infliger des dégâts normaux, une créature que vous touchez avec une flèche ou un carreau 
        explosive/-if est obligée de faire un jet de sauvegarde de dextérité DD10, ou subit 1d4 dégâts de feu au début 
        de son prochain tour. Ces dégâts ne se cumulent pas avec plusieurs pièces de munitions explosives. Par une 
        action, une créature enflammée peut effectuer un test de Dextérité DD10 pour éteindre tout feu qu\'elle a sur 
        elle causé par des flèches explosives. Si la créature réussit le jet de sauvegarde ou le test, elle est 
        immunisée à cet effet pendant 1 minute. Une fois enflammées, les munitions sont détruites.',
    ];

    private const SMOKING = [
        'name' => 'Pointe/carreau fumant(e)',
        'price' => '6 PO',
        'special' => 'Cette munition laisse échapper un souffle de fumée au contact. En plus d\'infliger des dégâts 
        normaux, une sphère de 6 mètres de rayon centrée sur la créature ou le point que vous touchez est fortement 
        obscurcie jusqu\'à la fin de votre prochain tour. Une fois activées, les munitions sont détruites.',
    ];
}
