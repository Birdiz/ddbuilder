<?php

declare(strict_types=1);

namespace App\Enum\Arsenal;

use App\Model\AbstractEnum;

class Explosive extends AbstractEnum
{
    private const SHOU_ROCKET = [
        'name' => 'Missile Shou',
        'price' => '150 PO',
        'damage' => '3d6 dégâts de feu',
        'weight' => '0.5 kg',
        'properties' => 'Les missiles Shou sont tirées à partir d\'un traîneau en bois réutilisable (5 PA, 2 kg) qui 
        guide l\'arme. Par une action, une créature peut allumer la fusée et la viser jusqu\'à 18 mètres. La fusée 
        vole vers le point choisi et chaque créature à 1,50 mètre ou moins de ce point doit réussir un jet de 
        sauvegarde de Dextérité DD12 ou subir 3d6 dégâts de feu.',
    ];

    private const GRENADE = [
        'name' => 'Grenade',
        'price' => '150 PO',
        'damage' => '3d6 dégâts perforants',
        'weight' => '0.5 kg',
        'properties' => 'Par une action, une créature peut allumer une grenade et la lancer jusqu\'à 18 mètres de 
        distance. Chaque créature à 1,50 mètre ou moins de ce point doit effectuer un jet de sauvegarde de Dextérité 
        DD12, subissant 3d6 dégâts perforants en cas d\'échec, ou la moitié des dégâts en cas de réussite.',
    ];

    private const SMOKEPOWDER_KEG = [
        'name' => 'Poudre-fumée (baril)',
        'price' => '250 PO',
        'damage' => '7d6 dégâts de feu',
        'weight' => '10 kg',
        'properties' => 'La poudre de fumée est un objet merveilleux peu commun, elle explose si elle est incendiée, 
        lachée ou manipulée sans attention, elle inflige les dégâts dans un rayon de 6m lorsqu\'elle explose, mettre 
        le feu à une 30g de celle-ci provoque une petite explosion plutôt qu\'une combustion lente, et lancer 
        Dissipation de la Lagie dessus le rend définitivement inerte.',
    ];

    private const SMOKEPOWDER_PACKET = [
        'name' => 'Poudre-fumée (paquet)',
        'price' => '10 PO',
        'damage' => '1d6 dégâts de feu',
        'weight' => '0.25 kg',
        'properties' => '',
    ];

    private const FOESHREDDER = [
        'name' => 'Foeshredder',
        'price' => '250 PO',
        'damage' => '3d6 dégâts perforants',
        'weight' => '0.5 kg',
        'properties' => 'Un foeshredder est une grenade spécialisée avec une poignée attachée, lui permettant d\'être 
        lancée deux fois plus loin. Un foeshredder fonctionne de la même manière qu\'une grenade.',
    ];

    private const SMOKEBOMB = [
        'name' => 'Bombe fumigène',
        'price' => '250 PO',
        'damage' => '',
        'weight' => '0.5 kg',
        'properties' => 'Par une action, une créature peut allumer une bombe fumigène et la lancer jusqu\'à 18 mètres 
        de distance. Un tour après l\'atterrissage d\'une bombe fumigène, elle émet un nuage de fumée qui crée une zone 
        fortement obscurcie dans un rayon de 6m. Un vent modéré (au moins 15km/h) disperse la fumée en 4 tours; un vent 
        fort (30km/h ou plus) le disperse en 1 tour.',
    ];
}
