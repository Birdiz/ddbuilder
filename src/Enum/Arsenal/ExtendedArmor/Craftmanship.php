<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedArmor;

use App\Model\AbstractEnum;

class Craftmanship extends AbstractEnum
{
    private const POOR = [
        'name' => 'Pauvre',
        'weight' => '+50%',
        'properties' => '-1 CA',
        'cost-mod' => '/2',
    ];

    private const COMMON = [
        'name' => 'Commun',
        'weight' => '-',
        'properties' => '-',
        'cost-mod' => '-',
    ];

    private const GOOD = [
        'name' => 'Bien',
        'weight' => '-10%',
        'properties' => '-',
        'cost-mod' => 'x1.5',
    ];

    private const SUPERB = [
        'name' => 'Superbe',
        'weight' => '-15%',
        'properties' => 'Retires Restrictions',
        'cost-mod' => 'x3',
    ];

    private const MASTER = [
        'name' => 'Maître',
        'weight' => '-20%',
        'properties' => 'Retires Restrictions et Bruyant',
        'cost-mod' => 'x5',
    ];
}
