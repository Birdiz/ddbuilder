<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedArmor;

use App\Model\AbstractEnum;

class HeavyArmor extends AbstractEnum
{
    public const TATAMI_DO = [
        'name' => 'Tatami-do',
        'price' => '500 PO',
        'AC' => '18',
        'strength' => 'For 15',
        'properties' => 'Désavantage',
        'weight' => '25 kg',
    ];
}
