<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedArmor;

use App\Model\AbstractEnum;

class LightArmor extends AbstractEnum
{
    public const CHITINE = [
        'name' => 'Chitine',
        'price' => '5 PO',
        'AC' => '11 + Mod.Dex',
        'strength' => '-',
        'properties' => '-',
        'weight' => '3.5 kg',
    ];

    public const WOOD = [
        'name' => 'Bois',
        'price' => '10 PO',
        'AC' => '12 + Mod.Dex',
        'strength' => '-',
        'properties' => 'Désavantage',
        'weight' => '5 kg',
    ];
}
