<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedArmor;

use App\Model\AbstractEnum;

class MediumArmor extends AbstractEnum
{
    public const BRIGANDINE = [
        'name' => 'Brigandine',
        'price' => '35 PO',
        'AC' => '13 + Mod.Dex (max +2)',
        'strength' => '-',
        'properties' => '-',
        'weight' => '6.5 kg',
    ];

    public const BONE_ARMOR = [
        'name' => 'Armure en os',
        'price' => '20 PO',
        'AC' => '13 + Mod.Dex (max +2)',
        'strength' => '-',
        'properties' => '-',
        'weight' => '4 kg',
    ];
}
