<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class Craftmanship extends AbstractEnum
{
    private const POOR = [
        'name' => 'Pauvre',
        'mod' => '-1',
        'cost-base' => '-',
        'cost-mod' => '/2',
        'cost-max' => '-',
    ];

    private const COMMON = [
        'name' => 'Commun',
        'mod' => '0',
        'cost-base' => '-',
        'cost-mod' => '-',
        'cost-max' => '-',
    ];

    private const GOOD = [
        'name' => 'Bien',
        'mod' => '+1',
        'cost-base' => '200 PO',
        'cost-mod' => 'x10',
        'cost-max' => '1000 PO',
    ];

    private const SUPERB = [
        'name' => 'Superbe',
        'mod' => '+2',
        'cost-base' => '4 000 PO',
        'cost-mod' => 'x25',
        'cost-max' => '10 000 PO',
    ];

    private const MASTER = [
        'name' => 'Maître',
        'mod' => '+3',
        'cost-base' => '15 000 PO',
        'cost-mod' => 'x50',
        'cost-max' => '30 000 PO',
    ];
}
