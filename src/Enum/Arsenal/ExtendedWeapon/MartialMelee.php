<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class MartialMelee extends AbstractEnum
{
    private const BARDICHE = [
        'name' => 'Bardiche',
        'price' => '10 PO',
        'damage' => '1d6 dégâts tranchants',
        'weight' => '2 kg',
        'properties' => 'Légère, allonge',
        'special' => '',
    ];

    private const BASTARD_SWORD = [
        'name' => 'Épée bâtarde',
        'price' => '75 PO',
        'damage' => '1d10 dégâts tranchants ou perforants',
        'weight' => '3 kg',
        'properties' => 'Lourde, Polyvalente (1d12), spécial',
        'special' => 'Cette arme particulière est trop grande pour être utilisée normalement d\'une seule main, sauf 
        si vous répondez à une exigence de force spécifique. Vous devez avoir un score de Force d\'au moins 20 ou subir 
        un désavantage sur toutes les attaques effectuées avec cette arme.',
    ];

    private const BRASS_KNUCKLES = [
        'name' => 'Poing américain',
        'price' => '5 PO',
        'damage' => '1d dégâts contondants',
        'weight' => '0.5 kg',
        'properties' => 'Légère, spécial',
        'special' => 'Si vous avez le style de combat à mains nues ou si vous appartenez à une race avec des armes 
        naturelles, le dé de dégâts de cette arme passe à 2d4. Les attaques effectuées à l\'aide de poings américains 
        sont des attaques d\'armes, pas des attaques à mains nues.',
    ];

    private const CLAW_BRACER = [
        'name' => 'Griffes',
        'price' => '10 PO',
        'damage' => '1d6 dégâts tranchants ou perforants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère, spécial',
        'special' => 'Vous ne pouvez pas être désarmé de cette arme.',
    ];

    private const FALCHION = [
        'name' => 'Fauchon',
        'price' => '50 PO',
        'damage' => '2d4 dégâts tranchants',
        'weight' => '4 kg',
        'properties' => 'Lourde, à deux mains, spécial',
        'special' => 'Un critique est obtenu avec 19 ou 20.',
    ];

    private const FULL_BLADE = [
        'name' => 'Lame pleine',
        'price' => '80 PO',
        'damage' => '2d8 dégâts tranchants',
        'weight' => '10 kg',
        'properties' => 'Lourde, à deux mains, spécial',
        'special' => 'Cette arme particulière est trop grande pour être utilisée normalement d\'une seule main, sauf 
        si vous répondez à une exigence de force spécifique. Vous devez avoir un score de Force d\'au moins 20 ou subir 
        un désavantage sur toutes les attaques effectuées avec cette arme.',
    ];

    private const FLAIL = [
        'name' => 'Fléau',
        'price' => '10 PO',
        'damage' => '1d8-1 dégâts contondants',
        'weight' => '1 kg',
        'properties' => 'Polyvalente (2d6-1)',
        'special' => '',
    ];

    private const HEAVY_FLAIL = [
        'name' => 'Fléau lourd',
        'price' => '15 PO',
        'damage' => '2d6 dégâts contondants',
        'weight' => '5 kg',
        'properties' => 'Lourde, à deux mains',
        'special' => '',
    ];

    private const KAMA = [
        'name' => 'Kama',
        'price' => '2 PO',
        'damage' => '1d4 dégâts tranchants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère',
        'special' => '',
    ];

    private const KATANA = [
        'name' => 'Katana',
        'price' => '25 PO',
        'damage' => '1d8 dégâts tranchants',
        'weight' => '1,5 kg',
        'properties' => 'Finesse, Polyvalente (1d10)',
        'special' => '',
    ];

    private const KATAR = [
        'name' => 'Katar',
        'price' => '5 PO',
        'damage' => '1d6 dégâts perforants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère',
        'special' => '',
    ];

    private const KUSARI_GAMA = [
        'name' => 'Kusari gama',
        'price' => '2 PO',
        'damage' => '1d4 dégâts tranchants ou contondants',
        'weight' => '1,5 kg',
        'properties' => 'Finesse, légère, allonge, à deux mains, spécial',
        'special' => 'Les ennemis ont un désavantage sur toutes les sauvegardes d\'une attaque qui vous permettrait de 
        les assommer ou de les désarmer lors d\'une attaque avec cette arme. Vous pouvez traiter cette arme comme si 
        vous utilisiez deux armes et appliquer toutes caractéristiques pertinentes pour lesquelles vous utilisez cette 
        arme. Cette arme peut être utilisé comme une arme de moine.',
    ];

    private const NUNCHAKU = [
        'name' => 'Nunchaku',
        'price' => '2 PO',
        'damage' => '1d4 dégâts contondants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère, spécial',
        'special' => 'Les ennemis ont un désavantage sur les sauvegardes d\'une attaque qui vous permettrait de les 
        désarmer lors d\'une attaque avec cette arme. Cela peut être utilisé comme une arme de moine.',
    ];

    private const SAI = [
        'name' => 'Sai',
        'price' => '1 PO',
        'damage' => '1d4 dégâts perforants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère, spécial',
        'special' => 'Les ennemis ont un désavantage sur les sauvegardes d\'une attaque qui vous permettrait de les 
        désarmer lors d\'une attaque avec cette arme. Cela peut être utilisé comme une arme de moine.',
    ];

    private const WAR_SCYTHE = [
        'name' => 'Faux de guerre',
        'price' => '50 PO',
        'damage' => '1d12 dégâts tranchants',
        'weight' => '6 kg',
        'properties' => 'Lourde, allonge, à deux mains, spécial',
        'special' => 'Vous avez un désavantage lorsque vous essayez d\'attaquer un ennemi à moins de 1,50 mètre de 
        vous.',
    ];

    private const WHIP_DAGGER = [
        'name' => 'Fouet-dague',
        'price' => '5 PO',
        'damage' => '1d6 dégâts tranchants',
        'weight' => '1.5 kg',
        'properties' => 'Finesse, allonge',
        'special' => 'Vous avez un désavantage lorsque vous essayez d\'attaquer un ennemi à moins de 1,50 mètre de 
        vous.',
    ];

    private const PONIARD = [
        'name' => 'Poignard',
        'price' => '3 PO',
        'damage' => '1d6 dégâts perforants',
        'weight' => '1 kg',
        'properties' => 'Finesse, lancer (6m/20m)',
        'special' => '',
    ];
}
