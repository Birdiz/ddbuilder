<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class MartialRanged extends AbstractEnum
{
    private const BOLAS = [
        'name' => 'Bolas',
        'price' => '1 PO',
        'damage' => '1d4 dégâts contondants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère, lancer (portée 10m/30m), spécial',
        'special' => 'Les ennemis ont un désavantage sur les sauvegardes d\'une attaque qui vous permettrait de les 
        assommer avec cette arme.',
    ];

    private const BOOMERANG = [
        'name' => 'Boomerang',
        'price' => '1 PO',
        'damage' => '1d4 dégâts contondants',
        'weight' => '1 kg',
        'properties' => 'Finesse, légère, lancer (portée 20m/60m), spécial',
        'special' => 'Cette arme vous reviendra si vous ne parvenez pas à atteindre votre cible. En utilisant votre 
        réaction, vous pouvez l\'attraper en réussissant un test de Dextérité DD 10.',
    ];

    private const SHURIKEN = [
        'name' => 'Shuriken',
        'price' => '5 PA',
        'damage' => '1d4 dégâts perforants',
        'weight' => '0.25 kg',
        'properties' => 'Finesse, légère, Munition (portée 10m/30m), lancer, spécial',
        'special' => 'Vous avez l\'avantage aux jets d\'Escamotage effectués avec cette arme. Cela peut être utilisé 
        comme une arme de moine.',
    ];

    private const ATLATL = [
        'name' => 'Propulseur',
        'price' => '1 PO',
        'damage' => '1d8 dégâts perforants',
        'weight' => '0.5 kg',
        'properties' => 'Finesse, Munition (portée 20m/60m), rechargement',
        'special' => '',
    ];
}
