<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class Shield extends AbstractEnum
{
    private const BUCKLER = [
        'name' => 'Boucle',
        'price' => '5 PO',
        'AC' => '+1',
        'damage' => '1d4 dégâts contondants',
        'weight' => '1.5 kg',
        'properties' => 'Léger',
        'special' => '',
    ];

    private const HEAVY_SHIELD = [
        'name' => 'Bouclier lourd',
        'price' => '30 PO',
        'AC' => '+2',
        'damage' => '1d8 dégâts contondants',
        'weight' => '7.5 kg',
        'properties' => 'Heavy, à deux mains',
        'special' => '',
    ];

    private const SPICKED_SHIELD = [
        'name' => 'Bouclier à pointes',
        'price' => '15 PO',
        'AC' => '+1',
        'damage' => '1d6 dégâts perforants',
        'weight' => '3 kg',
        'properties' => '',
        'special' => '',
    ];

    private const TOWER_SHIELD = [
        'name' => 'Bouclier-tour',
        'price' => '75 PO',
        'AC' => '+2',
        'damage' => '',
        'weight' => '9 kg',
        'properties' => 'Lourd, spécial, polyvalent (tenu à deux mains, la AC est de +3)',
        'special' => 'L\'utilisation du bouclier-tour bloque plus que les attaques ordinaires. Vous recevez un bonus de 
        +2 aux jets de sauvegarde de Dextérité lorsque vous l\'utilisez. Si vous portez ce bouclier, vous ne pouvez pas 
        lancer de sorts qui nécessitent des composants somatiques ou matériels à moins d\'avoir le don Mage de guerre.',
    ];
}
