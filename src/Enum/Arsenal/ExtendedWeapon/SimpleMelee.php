<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class SimpleMelee extends AbstractEnum
{
    private const PUNCH_DAGGER = [
        'name' => 'Poignard',
        'price' => '2 PO',
        'damage' => '1d4 dégâts perforants',
        'weight' => '0.5 kg',
        'properties' => 'Finesse, légère, spécial',
        'special' => 'Vous avez l\'avantage aux jet d\'Escamotage effectués avec cette arme.',
    ];

    private const SCYTHE = [
        'name' => 'Faux',
        'price' => '20 PO',
        'damage' => '1d10 dégâts tranchants ou perforants',
        'weight' => '5 kg',
        'properties' => 'Lourde, à deux mains',
        'special' => '',
    ];

    private const SICKLE = [
        'name' => 'Faucille+',
        'price' => '1 PO',
        'damage' => '1d4 dégâts tranchants',
        'weight' => '1 kg',
        'properties' => 'Légère, spécial',
        'special' => 'Vous pouvez ajouter votre modificateur aux dégâts de la faucille même si vous n\'avez pas le 
        style de combat à deux armes.',
    ];

    private const WAKIZASHI = [
        'name' => 'Wakizashi',
        'price' => '10 PO',
        'damage' => '1d4 dégâts tranchants',
        'weight' => '1.5 kg',
        'properties' => 'Finesse, polyvalente (1d6)',
        'special' => 'Cette arme peut être utilisé comme une arme de moine.',
    ];
}
