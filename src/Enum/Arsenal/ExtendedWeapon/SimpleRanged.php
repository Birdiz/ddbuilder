<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\ExtendedWeapon;

use App\Model\AbstractEnum;

class SimpleRanged extends AbstractEnum
{
    private const BLOWGUN = [
        'name' => 'Sarbacane',
        'price' => '10 PO',
        'damage' => '1d4 dégâts perforants',
        'weight' => '0.5 kg',
        'properties' => 'Munition (portée 8m/35m), rechargement, spécial',
        'special' => 'Lorsque vous vous cachez, manquer avec un jet d\'attaque ne révèle pas votre position.',
    ];

    private const DENSE_ROCK = [
        'name' => 'Gros caillou',
        'price' => '2 PA',
        'damage' => '1d6 dégâts contondants',
        'weight' => '1.5 kg',
        'properties' => 'Lancer (portée 3m/10m)',
        'special' => '',
    ];

    private const SIMPLE_HAND_CROSSBOW = [
        'name' => 'Arbalète de point simple',
        'price' => '30 PO',
        'damage' => '1d4 dégâts perforants',
        'weight' => '2 kg',
        'properties' => 'Légère, Munition (portée 10m/30m), Rechargement',
        'special' => '',
    ];
}
