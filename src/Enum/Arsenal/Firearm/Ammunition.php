<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\Firearm;

use App\Model\AbstractEnum;

class Ammunition extends AbstractEnum
{
    private const BULLET = [
        'name' => 'Balle (5)',
        'price' => '1 PO',
        'weight' => '0.5 kg',
        'properties' => '',
    ];

    private const LEAD_SHOT = [
        'name' => 'Grenaille de plomb (5)',
        'price' => '2.5 PO',
        'weight' => '0.5 kg',
        'properties' => 'La grenaille de plomb peut être chargée dans n\'importe quelle arme à feu qui tire normalement 
        des balles. Charger une arme à feu avec du plomb réduit de moitié ses portées normale et longue, mais confère 
        la propriété Diffusion lors de l\'attaque.',
    ];

    private const HANDGUNNE_BALL = [
        'name' => 'Balle d\'escopette',
        'price' => '3 PO',
        'weight' => '0.5 kg',
        'properties' => 'Le prix est pour une balle.',
    ];
}
