<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\Firearm;

use App\Model\AbstractEnum;

/**
 * @method static MagicAmmunition BALDRICK_BULLET()
 * @method static MagicAmmunition ILLUMINATION_BULLET()
 * @method static MagicAmmunition EXPLOSIVE_BULLET()
 * @method static MagicAmmunition POISONOUS_BULLET()
 * @method static MagicAmmunition NAMED_BULLET()
 */
class MagicAmmunition extends AbstractEnum
{
    private const BALDRICK_BULLET = [
        'name' => 'Balle de Baldrick',
        'properties' => 'Vous bénéficiez d\'un bonus de +1 aux jets d\'attaque et de dégâts effectués avec cette balle
         magique.',
    ];

    private const ILLUMINATION_BULLET = [
        'name' => 'Balle éclairante',
        'properties' => 'Cette balle magique fait briller la cible comme une torche pendant 10 tours (1 minute).<br/>
        Si elle est tirée en l\'air, il retombe lentement au sol tout en émettant de la lumière comme une torche. Elle 
        descend à une vitesse de 20m par tour, vers le bas de la portée maximale de l\'arme à feu, et s\'arrête de 
        briller après 10 tours (1 minute).',
    ];

    private const EXPLOSIVE_BULLET = [
        'name' => 'Balle explosive',
        'properties' => 'Cette balle magique explose en une grande boule de feu à l\'impact. Chaque créature dans une 
        sphère de 6m de rayon autour du point où elle frappe doit effectuer un jet de Dextérité DD15. Chaque créature 
        affectée subit 6d6 dégâts de feu en cas d\'échec, ou la moitié en cas de réussite.<br/> Le feu se propage dans 
        les coins et enflamme les objets inflammables qui ne sont pas portés ou transportés. Une fois déclenchée, la 
        balle est détruite.',
    ];

    private const POISONOUS_BULLET = [
        'name' => 'Balle empoisonnée',
        'properties' => 'Une créature touchée par cette balle magique doit réussir un jet de sauvegarde de Constitution 
        DD15 ou subir 2d10 dégâts de poison et devenir empoisonnée pendant 10 tours (1 minute).',
    ];

    private const NAMED_BULLET = [
        'name' => 'Balle nommée',
        'properties' => 'Une balle nommée est une balle magique destinée à tuer une personne en particulier et 
        uniquement cette personne. Cette balle fonctionne comme une 
        <a class="link-animation has-text-danger-dark" href="https://www.aidedd.org/dnd/om.php?vf=fleche-tueuse">
        Flèche tueuse</a>. De plus, une balle nommée a un avantage sur les jets d\'attaque effectués contre sa 
        cible prévue.<br/> Après avoir infligé des dégâts supplémentaires à sa cible, la balle est rendue non magique.',
    ];

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function getPricedMagicAmmunitions(): array
    {
        $ammunitions = static::toArray();

        $ammunitions[static::BALDRICK_BULLET()->getKey()]['price'] = random_int(101, 500).' PO';
        $ammunitions[static::ILLUMINATION_BULLET()->getKey()]['price'] = random_int(101, 500).' PO';
        $ammunitions[static::EXPLOSIVE_BULLET()->getKey()]['price'] = random_int(501, 5000).' PO';
        $ammunitions[static::POISONOUS_BULLET()->getKey()]['price'] = random_int(501, 5000).' PO';
        $ammunitions[static::NAMED_BULLET()->getKey()]['price'] = random_int(5001, 50000).' PO';

        return $ammunitions;
    }
}
