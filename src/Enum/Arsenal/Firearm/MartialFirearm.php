<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\Firearm;

use App\Model\AbstractEnum;

class MartialFirearm extends AbstractEnum
{
    private const PISTOL = [
        'name' => 'Pistolet',
        'price' => '250 PO',
        'damage' => '1d10 dégâts perforants',
        'weight' => '1.5 kg',
        'properties' => 'Arme à feu (portée 15m/50m)',
    ];

    private const MUSKET = [
        'name' => 'Mousquet',
        'price' => '500 PO',
        'damage' => '1d12 dégâts perforants',
        'weight' => '7.5 kg',
        'properties' => 'Arme à feu (portée 50m/200m), enfourché (FOR 13), lourd, à deux mains',
    ];

    private const HANDGUNNE = [
        'name' => 'Escopette',
        'price' => '750 PO',
        'damage' => '3d6 dégâts contondants',
        'weight' => '7.5 kg',
        'properties' => 'Arme à feu (portée 25m/110m), enfourché (FOR 15), lourd, à deux mains',
    ];
}
