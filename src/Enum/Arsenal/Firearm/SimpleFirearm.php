<?php

declare(strict_types=1);

namespace App\Enum\Arsenal\Firearm;

use App\Model\AbstractEnum;

class SimpleFirearm extends AbstractEnum
{
    private const BLUNDERBUSS = [
        'name' => 'Tromblon',
        'price' => '50 PO',
        'damage' => '1d8 dégâts perforants',
        'weight' => '2.5 kg',
        'properties' => 'Arme à feu (portée 20m/80m), à deux mains',
    ];

    private const ARQUEBUS = [
        'name' => 'Arquebuse',
        'price' => '75 PO',
        'damage' => '1d8 dégâts perforants',
        'weight' => '3.5 kg',
        'properties' => 'Arme à feu (portée 25m/110m), lourd, à deux mains',
    ];

    private const CALIVER = [
        'name' => 'Caliver',
        'price' => '150 PO',
        'damage' => '1d10 dégâts perforants',
        'weight' => '4.5 kg',
        'properties' => 'Arme à feu (portée 35m/135m), lourd, à deux mains',
    ];
}
