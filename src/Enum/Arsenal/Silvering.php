<?php

declare(strict_types=1);

namespace App\Enum\Arsenal;

use App\Model\AbstractEnum;

class Silvering extends AbstractEnum
{
    private const POOR = [
        'name' => 'Pauvre',
        'cost' => '50 PO',
        'duration' => '1 jour',
    ];

    private const COMMON = [
        'name' => 'Commun',
        'cost' => '100 PO',
        'duration' => '3 jours',
    ];

    private const GOOD = [
        'name' => 'Bien',
        'cost' => '400 PO',
        'duration' => '7 jours',
    ];

    private const SUPERB = [
        'name' => 'Superbe',
        'cost' => '800 PO',
        'duration' => '14 jours',
    ];

    private const MASTER = [
        'name' => 'Maître',
        'cost' => '1 500 PO',
        'duration' => '21 jours',
    ];
}
