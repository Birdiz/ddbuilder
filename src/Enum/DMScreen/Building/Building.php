<?php

declare(strict_types=1);

namespace App\Enum\DMScreen\Building;

use App\Model\AbstractEnum;

class Building extends AbstractEnum
{
    private const CROPLAND = [
        'name' => 'Champ',
        'price' => '100 PO',
        'rent' => '5 PO',
        'duration' => '-',
    ];

    private const COTTAGE = [
        'name' => 'Cottage',
        'price' => '400 PO',
        'rent' => '5 PO',
        'duration' => '15',
    ];

    private const STATUE = [
        'name' => 'Statue',
        'price' => '500 PO',
        'rent' => '-',
        'duration' => '15',
    ];

    private const WORKSHOP = [
        'name' => 'Atelier',
        'price' => '500 PO',
        'rent' => '20 PO',
        'duration' => '30',
    ];

    private const QUARTERS = [
        'name' => 'Quartiers',
        'price' => '1 000 PO',
        'rent' => '20 PO',
        'duration' => '25',
    ];

    private const SMOKEHOUSE = [
        'name' => 'Fumoir',
        'price' => '1 000 PO',
        'rent' => '15 PO',
        'duration' => '100',
    ];

    private const WOOD_BUILDING = [
        'name' => 'Grange en bois',
        'price' => '1 500 PO',
        'rent' => '5 PO',
        'duration' => '20',
    ];

    private const STABLES = [
        'name' => 'Étable',
        'price' => '2 000 PO',
        'rent' => '10 PO',
        'duration' => '20',
    ];

    private const MILL = [
        'name' => 'Moulin',
        'price' => '2 000 PO',
        'rent' => '25 PO',
        'duration' => '180',
    ];

    private const GUILDHALL = [
        'name' => 'Hall de guilde',
        'price' => '5 000 PO',
        'rent' => '30 PO',
        'duration' => '60',
    ];

    private const POST = [
        'name' => 'Comptoir de commerce',
        'price' => '5 000 PO',
        'rent' => '100 PO',
        'duration' => '60',
    ];

    private const BREWERY = [
        'name' => 'Brasserie',
        'price' => '6 000 PO',
        'rent' => '50 PO',
        'duration' => '40',
    ];

    private const TOWER = [
        'name' => 'Tour',
        'price' => '6 000 PO',
        'rent' => '15 PO',
        'duration' => '40',
    ];

    private const SCHOOL = [
        'name' => 'École',
        'price' => '8 000 PO',
        'rent' => '100 PO',
        'duration' => '150',
    ];

    private const THEATER = [
        'name' => 'Théâtre',
        'price' => '10 000 PO',
        'rent' => '250 PO',
        'duration' => '250',
    ];

    private const LIBRARY = [
        'name' => 'Bibliothèque',
        'price' => '10 000 PO',
        'rent' => '200 PO',
        'duration' => '200',
    ];

    private const FORT = [
        'name' => 'Avant-post ou fort',
        'price' => '15 000 PO',
        'rent' => '100 PO',
        'duration' => '100',
    ];

    private const FORTIFIED_TOWER = [
        'name' => 'Tour fortifiée',
        'price' => '15 000 PO',
        'rent' => '100 PO',
        'duration' => '100',
    ];

    private const ENCHANTER_WORKSHOP = [
        'name' => 'Atelier d\'enchanteur',
        'price' => '15 000 PO',
        'rent' => '150 PO',
        'duration' => '80',
    ];

    private const MANOR = [
        'name' => 'Domaine noble avec manoir',
        'price' => '25 000 PO',
        'rent' => '500 PO',
        'duration' => '150',
    ];

    private const ALCHEMY_LAB = [
        'name' => 'Laboratoire d\'alchimie',
        'price' => '25 000 PO',
        'rent' => '500 PO',
        'duration' => '200',
    ];

    private const MUSEUM = [
        'name' => 'Musée',
        'price' => '25 000 PO',
        'rent' => '300 PO',
        'duration' => '200',
    ];

    private const ABBEY = [
        'name' => 'Abbaye',
        'price' => '50 000 PO',
        'rent' => '1 000 PO',
        'duration' => '400',
    ];

    private const CASTLE_SMALL = [
        'name' => 'Donjon ou petit château',
        'price' => '50 000 PO',
        'rent' => '1 500 PO',
        'duration' => '400',
    ];

    private const TEMPLE = [
        'name' => 'Temple',
        'price' => '50 000 PO',
        'rent' => '1 250 PO',
        'duration' => '400',
    ];

    private const CASTLE_LARGE = [
        'name' => 'Palais ou grand château',
        'price' => '500 000 PO',
        'rent' => '5 000 PO',
        'duration' => '1 200',
    ];
}
