<?php

declare(strict_types=1);

namespace App\Enum\DMScreen\Building;

use App\Model\AbstractEnum;

class Maintenance extends AbstractEnum
{
    private const LODGE = [
        'name' => 'Cabane de chasse',
        'cost' => '5 PA',
        'worker_unqualified' => '-',
        'worker_qualified' => 1,
    ];

    private const FARM = [
        'name' => 'Ferme',
        'cost' => '5 PA',
        'worker_unqualified' => 2,
        'worker_qualified' => 1,
    ];

    private const TEMPLE_SMALL = [
        'name' => 'Petit temple',
        'cost' => '1 PO',
        'worker_unqualified' => '-',
        'worker_qualified' => 2,
    ];

    private const SHOP = [
        'name' => 'Boutique',
        'cost' => '2 PO',
        'worker_unqualified' => '-',
        'worker_qualified' => 1,
    ];

    private const INN_TOWN = [
        'name' => 'Auberge (ville)',
        'cost' => '5 PO',
        'worker_unqualified' => 1,
        'worker_qualified' => 5,
    ];

    private const GUILDHALL = [
        'name' => 'Hall de guilde',
        'cost' => '5 PO',
        'worker_unqualified' => 3,
        'worker_qualified' => 5,
    ];

    private const INN_RURAL = [
        'name' => 'Auberge (campagne)',
        'cost' => '10 PO',
        'worker_unqualified' => 5,
        'worker_qualified' => 10,
    ];

    private const POST = [
        'name' => 'Comptoir de commerce',
        'cost' => '10 PO',
        'worker_unqualified' => 2,
        'worker_qualified' => 4,
    ];

    private const ESTATE = [
        'name' => 'Domaine noble',
        'cost' => '10 PO',
        'worker_unqualified' => 15,
        'worker_qualified' => 3,
    ];

    private const ABBEY = [
        'name' => 'Abbaye',
        'cost' => '20 PO',
        'worker_unqualified' => 25,
        'worker_qualified' => 5,
    ];

    private const TEMPLE_LARGE = [
        'name' => 'Grand temple',
        'cost' => '25 PO',
        'worker_unqualified' => 10,
        'worker_qualified' => 10,
    ];

    private const TOWER = [
        'name' => 'Tour fortifiée',
        'cost' => '25 PO',
        'worker_unqualified' => '-',
        'worker_qualified' => 10,
    ];

    private const FORT = [
        'name' => 'Avant-poste ou fort',
        'cost' => '50 PO',
        'worker_unqualified' => 40,
        'worker_qualified' => 20,
    ];

    private const CASTLE_SMALL = [
        'name' => 'Donjon ou petit château',
        'cost' => '100 PO',
        'worker_unqualified' => 50,
        'worker_qualified' => 50,
    ];

    private const CASTLE_LARGE = [
        'name' => 'Palais ou grand château',
        'cost' => '400 PO',
        'worker_unqualified' => 100,
        'worker_qualified' => 200,
    ];
}
