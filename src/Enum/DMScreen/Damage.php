<?php

declare(strict_types=1);

namespace App\Enum\DMScreen;

use App\Model\AbstractEnum;

class Damage extends AbstractEnum
{
    private const VERY_LIGHT = [
        '1d10' => [
            'Brûler par quelque chose',
            'Écraser par une armoire',
            'Piquer par une aiguille empoisonnée',
            'Tomber de ~4m',
            'Prendre un coup de pelle',
            'Se faire assommer',
            'Prendre un cône de vapeur',
        ],
    ];

    private const LIGHT = [
        '2d10' => [
            'Frapper par la foudre',
            'Trébucher dans un feu de camp',
            'Écraser par une statue',
            'Écraser par un cheval',
        ],
    ];

    private const MEDIUM = [
        '4d10' => [
            'Se faire prendre dans un éboulement',
            'Tomber dans une cuve d\'acide',
            'Prendre un pieu de barricadement',
            'Écraser par une charette',
        ],
    ];

    private const HEAVY = [
        '10d10' => [
            'Écraser par des murs se compactant',
            'Toucher par une roue de lames en fer',
            'Traverser un champ de lave',
        ],
    ];

    private const VERY_HEAVY = [
        '18d10' => [
            'Tomber dans la lave',
            'Écraser par une forteresse volante',
        ],
    ];

    private const TREMENDOUS = [
        '24d10' => [
            'Se faire prendre dans un tourbillon de feu dans le plan du Chaos Élémentaire de Feu',
            'Se faire broyer par les mâchoires d\'une créature divine ou d\'un monstre de la taille d\'une lune',
        ],
    ];

    /**
     * @return array<string, array<string>>
     */
    public static function formatDamage(): array
    {
        $damages = [];

        foreach (static::toArray() as $damage) {
            $damageKey = array_key_first($damage);

            $damages[$damageKey] = $damage[$damageKey];
        }

        return $damages;
    }
}
