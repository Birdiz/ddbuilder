<?php

declare(strict_types=1);

namespace App\Enum\DMScreen;

use App\Contract\WeightedInterface;
use App\Enum\Tavern\Service;
use App\Model\AbstractNamedEnum;

/**
 * Class Room.
 *
 * @method static Lifestyle MISERABLE()
 * @method static Lifestyle SORDID()
 * @method static Lifestyle POOR()
 * @method static Lifestyle MODEST()
 * @method static Lifestyle COMFORTABLE()
 * @method static Lifestyle RICH()
 * @method static Lifestyle ARISTOCRATIC()
 */
class Lifestyle extends AbstractNamedEnum implements WeightedInterface
{
    private const MISERABLE = [
        'name' => 'Misérable',
        'desc' => '<p>Vous vivez dans des conditions inhumaines. En l\'absence de chez-soi, vous vous abritez où vous 
        pouvez, dans des greniers, blotti dans de vieilles caisses, et comptez sur les bonnes grâces des gens mieux 
        lotis que vous. Un train de vie misérable expose à de nombreux dangers.<br/>
        La violence, la maladie et la faim vous suivent où que vous alliez. D\'autres personnes misérables comme vous 
        convoitent votre armure, vos armes et votre équipement d\'aventurier, qui représentent une fortune à leurs 
        yeux. Vous n\'attirez pas l\'attention de la plupart des gens.</p>',
        'price' => '-',
    ];

    private const SORDID = [
        'name' => 'Sordide',
        'desc' => '<p>Vous vivez dans une écurie qui prend l\'eau, dans une hutte en terre séchée en dehors de la ville 
        ou vous louez une chambre infestée de vermines dans le pire quartier de la ville. Vous avez un abri contre les 
        éléments, mais vivez dans un environnement désespéré et souvent violent, dans des endroits en proie à la 
        maladie, à la faim et au malheur. Vous n\'attirez pas l\'attention de la plupart des gens et avez très peu de 
        protections légales.<br/> 
        La plupart des gens de ce niveau de vie ont subi un terrible revers, qu\'ils aient été perturbés, fichés comme 
        des exilés ou qu\'ils aient souffert de maladie.</p>',
        'price' => '1PA/j',
    ];

    private const POOR = [
        'name' => 'Pauvre',
        'desc' => '<p>Un train de vie pauvre signifie devoir se passer du confort disponible dans une communauté stable.
         Une alimentation et un logement simples, des vêtements usés jusqu\'à la corde et des conditions imprévisibles 
        conduisent à une expérience suffisante mais probablement désagréable.<br/>
        Votre hébergement peut être une petite chambre louée pour pas grand-chose ou la salle commune au-dessus 
        d\'une taverne. Vous bénéficiez de certaines protections légales, mais devez toujours composer avec la 
        violence, le crime et la maladie. À ce niveau de vie, les gens ont tendance à être des ouvriers non qualifiés, 
        des marchands ambulants, des colporteurs, des voleurs, des mercenaires et autres types peu recommandables.</p>',
        'price' => '2PA/j',
    ];

    private const MODEST = [
        'name' => 'Modeste',
        'desc' => '<p>Un train de vie modeste vous permet de rester hors des bidonvilles et vous assure de pouvoir 
        maintenir votre équipement. Vous vivez dans une partie ancienne de la ville, louant une chambre dans une 
        pension, une auberge ou un temple.<br/>
        Vous n\'avez ni faim ni soif, et vos conditions de vie sont propres, même si elles sont simples. Les gens 
        ordinaires qui ont ce niveau de vie modeste sont des soldats avec une famille, des travailleurs, des étudiants, 
        des prêtres, des magiciens en exil, etc.</p>',
        'price' => '1PO/j',
    ];

    private const COMFORTABLE = [
        'name' => 'Confortable',
        'desc' => '<p>Le choix d\'un train de vie confortable signifie que vous pouvez vous permettre des vêtements plus
         agréables et que vous pouvez facilement maintenir votre équipement. Vous vivez dans une petite maison dans un 
        quartier de classe moyenne ou dans une salle privée d\'une belle auberge. Vous côtoyez des commerçants, des 
        artisans talentueux et des officiers militaires.</p>',
        'price' => '2PO/j',
    ];

    private const RICH = [
        'name' => 'Riche',
        'desc' => '<p>Le choix d\'un train de vie riche, c\'est vivre une vie de luxe, même si vous n\'avez peut-être 
        pas obtenu ce statut social associé à la vieille noblesse. Vous vivez un style de vie comparable à celle d\'un 
        marchand qui a réussi, d\'un serviteur privilégié de la royauté ou d\'un propriétaire de petits commerces.<br/>
        Vous avez un logement plus que respectable, généralement une maison spacieuse dans un beau quartier de la ville 
        ou une suite confortable dans une belle auberge. Vous avez probablement une petite équipe de serviteurs.</p>',
        'price' => '4PO/j',
    ];

    private const ARISTOCRATIC = [
        'name' => 'Aristocratique',
        'desc' => '<p>Vous vivez une vie d\'abondance et de confort. Vous fréquentez les milieux habités par les 
        personnes les plus puissantes de la communauté. Vous possédez un logement de qualité, peut-être une maison dans 
        le plus beau quartier de la ville ou des chambres dans la plus belle auberge. Vous dînez dans les meilleurs 
        restaurants, fréquentez le meilleur tailleur et avez des serviteurs présents pour tous vos besoins. Vous 
        recevez des invitations pour des événements sociaux de la part des riches et des puissants, et passez des 
        soirées en compagnie de politiciens, de chefs de guilde, de grands prêtres et de la noblesse.<br/>
        Vous devez aussi composer avec les plus hauts niveaux de tromperie et de trahison. Plus vous vous enrichissez, 
        plus vous avez de chance d\'être entraîné dans une intrigue politique, comme pion ou comme participant.</p>',
        'price' => '10PO/j minimum',
    ];

    /**
     * @return array<array<string, string>>
     */
    public static function getServices(string $key): array
    {
        return match ($key) {
            static::MODEST()->getKey() => [
                Service::MESSENGER()->getValue(),
                Service::TRANSPORT_INTRA()->getValue(),
            ],
            static::COMFORTABLE()->getKey() => [
                Service::WORKER_UNQUALIFIED()->getValue(),
                Service::MESSENGER()->getValue(),
                Service::TRANSPORT_INTRA()->getValue(),
                Service::TRANSPORT_EXTRA()->getValue(),
                Service::PRIVATE_GUARD()->getValue(),
            ],
            static::RICH()->getKey(), static::ARISTOCRATIC()->getKey() => Service::toArray(),
            default => [],
        };
    }

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 25, 35, 15, 10, 5];
    }
}
