<?php

declare(strict_types=1);

namespace App\Enum\DMScreen\Vehicule;

use App\Model\AbstractEnum;

class Boat extends AbstractEnum
{
    private const AIRSHIP = [
        'name' => 'Dirigeable',
        'price' => '20 000 PO',
        'rent' => '100 PO',
    ];

    private const ROWBOAT = [
        'name' => 'Barque',
        'price' => '50 PO',
        'rent' => '5 PA',
    ];

    private const KEELBOAT = [
        'name' => 'Quillard',
        'price' => '3 000 PO',
        'rent' => '100 PO',
    ];

    private const SAILING_SHIP = [
        'name' => 'Voilier',
        'price' => '10 000 PO',
        'rent' => '150 PO',
    ];

    private const LONG_SHIP = [
        'name' => 'Drakkar',
        'price' => '10 000 PO',
        'rent' => '150 PO',
    ];

    private const GALLEY = [
        'name' => 'Galère',
        'price' => '30 000 PO',
        'rent' => '200 PO',
    ];

    private const WARSHIP = [
        'name' => 'Navire de guerre',
        'price' => '25 000 PO',
        'rent' => '200 PO',
    ];
}
