<?php

declare(strict_types=1);

namespace App\Enum\DMScreen\Vehicule\Mounting;

use App\Model\AbstractEnum;

class Equipment extends AbstractEnum
{
    private const BARD = [
        'name' => 'Bardes (armures)',
        'price' => 'x4',
        'charge' => 'x2',
    ];

    private const SADDLEBAGS = [
        'name' => 'Fontes',
        'price' => '4 PO',
        'charge' => '4 kg',
    ];

    private const FEED = [
        'name' => 'Fourrage (par jour)',
        'price' => '5 PC',
        'charge' => '5 kg',
    ];

    private const BIT_BRIDDLE = [
        'name' => 'Morts et brides',
        'price' => '2 PO',
        'charge' => '500g',
    ];

    private const SADDLE_RIDING = [
        'name' => 'Selle d\'équitation',
        'price' => '10 PO',
        'charge' => '12,5 kg',
    ];

    private const SADDLE_PACK = [
        'name' => 'Selle de bât',
        'price' => '5 PO',
        'charge' => '7,5 kg',
    ];

    private const SADDLE_EXOTIC = [
        'name' => 'Selle exotique',
        'price' => '60 PO',
        'charge' => '20 kg',
    ];

    private const SADDLE_MILITARY = [
        'name' => 'Selle militaire',
        'price' => '20 PO',
        'charge' => '15 kg',
    ];

    private const CARRIAGE = [
        'name' => 'Carrosse',
        'price' => '100 PO',
        'charge' => '300 kg',
    ];

    private const CHARIOT = [
        'name' => 'Char',
        'price' => '250 PO',
        'charge' => '50 kg',
    ];

    private const WAGON = [
        'name' => 'Charrette',
        'price' => '35 PO',
        'charge' => '200 kg',
    ];

    private const STABLING = [
        'name' => 'Écurie (par jour)',
        'price' => '5 PA',
        'charge' => '-',
    ];

    private const SLED = [
        'name' => 'Traîneau',
        'price' => '20 PO',
        'charge' => '150 kg',
    ];
}
