<?php

declare(strict_types=1);

namespace App\Enum\DMScreen\Vehicule\Mounting;

use App\Model\AbstractEnum;

class Mount extends AbstractEnum
{
    private const DUNK = [
        'name' => 'Âne ou mûle',
        'price' => '8 PO',
        'rent' => '5 PA',
        'charge' => '210 kg',
    ];

    private const CAMEL = [
        'name' => 'Chameau',
        'price' => '50 PO',
        'rent' => '3 PO',
        'charge' => '240 kg',
    ];

    private const WARHORSE = [
        'name' => 'Cheval de guerre',
        'price' => '400 PO',
        'rent' => '20 PO',
        'charge' => '270 kg',
    ];

    private const HORSE = [
        'name' => 'Cheval de selle',
        'price' => '75 PO',
        'rent' => '5 PO',
        'charge' => '240 kg',
    ];

    private const WORKHORSE = [
        'name' => 'Cheval de trait',
        'price' => '75 PO',
        'rent' => '5 PO',
        'charge' => '240 kg',
    ];

    private const ELEPHANT = [
        'name' => 'Éléphant',
        'price' => '200 PO',
        'rent' => '15 PO',
        'charge' => '660 kg',
    ];

    private const MASTIFF = [
        'name' => 'Molosse',
        'price' => '25 PO',
        'rent' => '1 PO',
        'charge' => '95 kg',
    ];

    private const PONEY = [
        'name' => ' Poney',
        'price' => '30 PO',
        'rent' => '1 PO',
        'charge' => '95 kg',
    ];
}
