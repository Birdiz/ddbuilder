<?php

declare(strict_types=1);

namespace App\Enum;

use App\Model\AbstractEnum;

/**
 * Class Difficulty.
 *
 * @method static Danger EASY()
 * @method static Danger MEDIUM()
 * @method static Danger HARD()
 * @method static Danger MORTAL()
 */
class Danger extends AbstractEnum
{
    private const EASY = 'Facile';
    private const MEDIUM = 'Moyen';
    private const HARD = 'Difficile';
    private const MORTAL = 'Mortel';
}
