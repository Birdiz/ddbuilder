<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

class Adventure extends AbstractEnum
{
    private const STOP_MONSTERS = 'Empêcher les monstrueux habitants d\'un donjon d\'atteindre la surface.';
    private const FOIL_VILLAIN = 'Faire échouer le plan démoniaque d\'un ennemi.';
    private const DESTROY_MAGIC = 'Détruire la source magique menaçante à l\'intérieur d\'un donjon.';
    private const TREASURE = 'Récupérer un trésor.';
    private const ITEM = 'Récupérer un objet particulier pour une raison particulière.';
    private const ITEM_STOLEN = 'Récupérer un objet volé caché dans un donjon.';
    private const INFORMATION = 'Trouver une information requise pour un but précis.';
    private const PRISONER = 'Sauver un prisonnier ou une prisonnière.';
    private const FATE_PARTY = 'Découvrir ce qu\'il s\'est passé avec la dernière équipe d\'aventuriers.';
    private const NPC = 'Retrouver un PNJ qui a disparu dans la zone.';
    private const DRAGON = 'Tuer un dragon or un autre challenge de cet ordre.';
    private const STRANGE = 'Découvrir la nature et l\'origine d\'un endroit ou d\'un phénomène étrange.';
    private const PURSUE = 'Poursuivre les ennemis qui se réfugient dans un donjon.';
    private const ESCAPE = 'S\'échapper d\'un donjon.';
    private const RUIN = 'Fouiller une ruine et la libérer pour qu\'elle puisse être reconstruite.';
    private const DISCOVER_VILLAIN = 'Découvrir pourquoi un ennemi est intéressé pour un donjon.';
    private const SURVIVING = 'Gagner un pari ou effectuer un rite de passage en survivant dans un donjon pendant un 
    certain temps.';
    private const PARLEY = 'Effectuer un Pour-parler avec un ennemi dans un donjon.';
    private const HIDE = 'Se cacher d\'une menance extérieure à l\'intérieur d\'un donjon.';
}
