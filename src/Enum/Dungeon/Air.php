<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

/**
 * Class Air.
 *
 * @method static Air CLEAR_DAMP()
 * @method static Air CLEAR_DRAFTY()
 * @method static Air CLEAR_COLD()
 * @method static Air CLEAR_MIST()
 * @method static Air CLEAR_WARM()
 * @method static Air CLEAR_SMOKE()
 * @method static Air CLEAR_WINDY()
 * @method static Air FOGGY_COLD()
 * @method static Air HAZY_HUMID()
 * @method static Air SMOKY_STEAMY()
 */
class Air extends AbstractEnum
{
    private const CLEAR_DAMP = 'Clair et humide';
    private const CLEAR_DRAFTY = 'Clair et courants d\'air';
    private const CLEAR_COLD = 'Clair et froid';
    private const CLEAR_MIST = 'Clair avec du brouillard au sol';
    private const CLEAR_WARM = 'Clair et chaud';
    private const CLEAR_SMOKE = 'Clair, avec de la fumée au plafond';
    private const CLEAR_WINDY = 'Clair et venteux';
    private const FOGGY_COLD = 'Brumeux ou brouillard et froid';
    private const HAZY_HUMID = 'Fumeux et humide';
    private const SMOKY_STEAMY = 'Fumé ou Vapeur';
}
