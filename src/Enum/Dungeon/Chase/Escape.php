<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Chase;

use App\Model\AbstractEnum;

class Escape extends AbstractEnum
{
    private const ADVANTAGE = [
        'Beaucoup de cachette',
        'Bondé',
        'Bruyant',
    ];

    private const DISADVANTAGE = [
        'Peu de cachette',
        'Vide',
        'Calme',
    ];
}
