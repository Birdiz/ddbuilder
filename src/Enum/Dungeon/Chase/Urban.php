<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Chase;

use App\Model\AbstractEnum;

class Urban extends AbstractEnum
{
    private const OBSTACLE = 'Un large obstacle comme un cheval ou une charette bloque votre chemin. Effectuer un test 
    DD15 DEX (Acrobatie) pour esquiver. En cas d\'échec, l\'obstacle compte comme 3m de terrain difficile.';

    private const CROWD = 'Une foule bloque votre chemin. Effectuer un test DD10 FOR (Athletics) ou DEX (Acrobaties)
    pour traverser la foule sans être piétiner. En cas d\'échec, la foule compte comme 3m de terrain difficile.';

    private const WINDOW = 'Une fenêtre de vitraux ou une barrière équivalente bloque votre passage. Effectuer un Jet 
    de Sauvegarde DD10 FOR pour passer à travers la barrière et continuer. En cas d\'échec, vous rebondissez contre la 
    barrière et tombez à terre.';

    private const CRATES = 'Un lot de caisses, tonneaux ou obstacles similaires se trouvent sur votre chemin. Effectuer
    un test DD10 DEX (Acrobaties) ou INT (de votre choix) pour traverser l\'obstacle. En cas d\'échec, le lot compte 
    comme 3m de terrain difficile';

    private const SLIPPERY = 'Le terrain sous vos pieds est glissant à cause de la pluie, d\'huile ou autre liquide.
    Effectuer un Jet de Sauvegarde DD10 DEX. En cas d\'échec, vous tombez à terre.';

    private const DOGS = 'Vous tombez sur une meute de chiens se battant pour de la nourriture. Effectuer un test DD10 
    DEX (Acrobaties) pour traverser la meute sans entrave. En cas d\'échec, vous êtes mordu, subissez 1d4 de dégâts et 
    les chiens comptent comme 1,5m de terrain difficile.';

    private const BRAWL = 'Vous venez sur une bagarre en cours. Effectuer un test DD15 FOR (Athlétisme), DEX 
    (Acrobaties) ou CHA (Intimidation) (de votre choix) pour passer la bagarre sans entrave. En cas d\'échec, vous 
    subissez 2d4 dégâts contondants et les bagarreurs comptent pour 10m de terrain difficile.';

    private const BEGGAR = 'Un mendiant vous barre la route. Effectuer un test DD10 FOR (Athlétisme), DEX (Acrobaties) 
    ou CHA (Intimidation) (de votre choix) pour passer devant le mendiant. Vous réussissez automatiquement si vous jeter
     une pièce au mendiant. En cas d\'échec, le mendiant compte pour 1.5m de terrain difficile.';

    private const GUARD = 'Un garde trop zélé vous prend pour quelqu\'un d\'autre. Si vous bougez de plus de 6m pendant 
    le combat, le garde profite d\'une attaque d\'opportunité à la lance (+3 à l\'attaque ; 1d6+1 de dégâts perçants).';

    private const COLLINGING = 'Vous êtes forcé de faire un virage serré pour éviter quelque chose d\'inévitable. 
    Effectuer un Jet de Sauvegarde DD10 DEX pour réussir à réagir. En cas d\'échec, vous heurtez quelque chose de dur et
     subissez 1d4 de dégâts contondants.';

    private const NOTHING = 'Pas de complication';

    /**
     * @return array<int>
     */
    public static function getWeigths(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 50];
    }
}
