<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Chase;

use App\Model\AbstractEnum;

class Wilderness extends AbstractEnum
{
    private const BUSH = 'Votre chemin vous mène à travers d\'un groupe de buissons denses. Effectuer un test DD10 FOR 
    (Athletics) ou DEX (Acrobaties). En cas d\'échec, les buissons comptent comme 1,5m de terrain difficile.';

    private const GROUND = 'Le relief inégal vous ralentit. Effectuer un test DD10 DEX (Acrobaties) pour traverser le 
    terrain sans problème. En cas d\'échec, la foule compte comme 3m de terrain difficile.';

    private const INSECTS = 'Vous courrez dans une nuée d\'insectes. Elle effectue une attaque d\'opportunité contre 
    vous (+3 à l\'attaque ; 4d4 dégâts perforants).';

    private const OBSTACLE = 'Un torrent, un ravin ou un lit de rocher bloque votre passage. Effectuer un test DD10 FOR 
    (Athlétisme) ou DEX (Acrobaties) (de votre choix) pour traverser l\'obstacle. En cas d\'échec, le lot compte 
    comme 3m de terrain difficile';

    private const BLIND = 'Effectuer un Jet de Sauvegarde DD10 CON. En cas d\'échec, vous êtes aveuglé par du sable, 
    de la terre, de la cendre, de la neige ou du pollen jusqu\'à la fin de votre tour. Pendant la durée de 
    l\'aveuglement, votre vitesse est réduite de moitié.';

    private const DROP = 'Un chute soudaine vous surprend. Effectuer un test DD10 DEX pour traverser l\'obstacle sans 
     problème. En cas d\'échec, vous tombez 1d4 x 1,5m, subissez 1d6 de dégâts contondants pour chaque 1,5m de chute, 
     et terminez à terre.';

    private const SNARE = 'Vous tombez dans un piège de chasse. Effectuer un Jet de Sauvegarde DD15 DEX pour l\'éviter. 
    En cas d\'échec, vous êtes pris dans un filet.';

    private const ANIMALS = 'Vous êtes pris dans une débandade d\'animaux effrayés. Effectuer un Jet de Sauvegarde DD10 
    DEX. En cas d\'échec, vous subissez 1d4 dégâts contondants et 1d4 dégâts perçants.';

    private const RAZORVINE = 'Votre chemin vous conduit près d\'une vigne-rasoir. Effectuer un Jet de Sauvegarde DD15 
    DEX ou utiliser 3m de mouvement (de votre choix) pour éviter la vigne. En cs d\'échec, vous prenez 1d10 dégâts 
    tranchants';

    private const CREATURE = 'Une créature indigène à la zone vous poursuit. Le MJ choisit la créature appropriée.';

    private const NOTHING = 'Pas de complication';

    /**
     * @return array<int>
     */
    public static function getWeigths(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 50];
    }
}
