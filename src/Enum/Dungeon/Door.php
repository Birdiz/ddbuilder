<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Enum\Object\AC;
use App\Enum\Object\Integrity;
use App\Enum\Object\Material;
use App\Model\AbstractEnum;
use App\Util\ArrayTrait;

/**
 * Class Door.
 *
 * @method static Door WOODEN()
 * @method static Door WOODEN_LOCKED()
 * @method static Door WOODEN_BARRED()
 * @method static Door STONE()
 * @method static Door STONE_LOCKED()
 * @method static Door STONE_BARRED()
 * @method static Door IRON()
 * @method static Door IRON_LOCKED()
 * @method static Door IRON_BARRED()
 * @method static Door PORTCULLIS()
 * @method static Door PORTCULLIS_LOCKED()
 * @method static Door SECRET()
 * @method static Door SECRET_LOCKED()
 * @method static Door SECRET_BARRED()
 */
class Door extends AbstractEnum
{
    use ArrayTrait;

    private const WOODEN = 'En bois';
    private const WOODEN_LOCKED = 'En bois, verrouillée';
    private const WOODEN_BARRED = 'En bois, barrée';
    private const STONE = 'En pierre';
    private const STONE_LOCKED = 'En pierre, verrouillée';
    private const STONE_BARRED = 'En pierre, barrée';
    private const IRON = 'En fer';
    private const IRON_LOCKED = 'En fer, verrouillée';
    private const IRON_BARRED = 'En fer, barrée';
    private const PORTCULLIS = 'Herse';
    private const PORTCULLIS_LOCKED = 'Herse verrouillée';
    private const SECRET = 'Secrète ';
    private const SECRET_LOCKED = 'Secrète, verrouillée';
    private const SECRET_BARRED = 'Secrète, barrée';

    public static function getACFromValue(string $value): string
    {
        $acs = [
            AC::MEDIUM()->getValue() => [
                static::WOODEN()->getValue(),
            ],
            AC::HARD()->getValue() => [
                static::WOODEN_LOCKED()->getValue(),
                static::WOODEN_BARRED()->getValue(),
                static::STONE()->getValue(),
            ],
            AC::VERY_HARD()->getValue() => [
                static::STONE_LOCKED()->getValue(),
                static::STONE_BARRED()->getValue(),
            ],
            AC::NEARLY_INDESTRUCTIBLE()->getValue() => [
                static::IRON()->getValue(),
                static::IRON_LOCKED()->getValue(),
                static::IRON_BARRED()->getValue(),
                static::PORTCULLIS()->getValue(),
                static::PORTCULLIS_LOCKED()->getValue(),
                static::SECRET()->getValue(),
                static::SECRET_LOCKED()->getValue(),
                static::SECRET_BARRED()->getValue(),
            ],
        ];

        return static::arraySearchMultidimensional($value, $acs);
    }

    public static function getIntegrityFromValue(string $value): string
    {
        $integrities = [
            Integrity::NORMAL()->getKey() => [
                static::WOODEN()->getValue(),
                static::WOODEN_LOCKED()->getValue(),
                static::WOODEN_BARRED()->getValue(),
            ],
            Integrity::RESILIENT()->getKey() => [
                static::STONE()->getValue(),
                static::STONE_LOCKED()->getValue(),
                static::STONE_BARRED()->getValue(),
                static::IRON()->getValue(),
                static::IRON_LOCKED()->getValue(),
                static::IRON_BARRED()->getValue(),
                static::PORTCULLIS()->getValue(),
                static::PORTCULLIS_LOCKED()->getValue(),
                static::SECRET()->getValue(),
                static::SECRET_LOCKED()->getValue(),
                static::SECRET_BARRED()->getValue(),
            ],
        ];

        return static::arraySearchMultidimensional($value, $integrities);
    }

    public static function getMaterialFromValue(string $value): string
    {
        $materials = [
            Material::WOOD()->getValue() => [
                static::WOODEN()->getValue(),
                static::WOODEN_LOCKED()->getValue(),
                static::WOODEN_BARRED()->getValue(),
            ],
            Material::STONE()->getValue() => [
                static::STONE()->getValue(),
                static::STONE_LOCKED()->getValue(),
                static::STONE_BARRED()->getValue(),
            ],
            Material::METAL()->getValue() => [
                static::IRON()->getValue(),
                static::IRON_LOCKED()->getValue(),
                static::IRON_BARRED()->getValue(),
                static::PORTCULLIS()->getValue(),
                static::PORTCULLIS_LOCKED()->getValue(),
            ],
            Material::UNKNOW()->getValue() => [
                static::SECRET()->getValue(),
                static::SECRET_LOCKED()->getValue(),
                static::SECRET_BARRED()->getValue(),
            ],
        ];

        return static::arraySearchMultidimensional($value, $materials);
    }
}
