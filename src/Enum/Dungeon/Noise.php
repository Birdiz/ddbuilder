<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

/**
 * Class Noise.
 *
 * @method static Noise BANG_SLAM()
 * @method static Noise BELLOWING()
 * @method static Noise BUZZING()
 * @method static Noise CHANTING()
 * @method static Noise CHIMING()
 * @method static Noise CHIRPING()
 * @method static Noise CLANKING()
 * @method static Noise CLASHING()
 * @method static Noise CLICKING()
 * @method static Noise COUGHING()
 * @method static Noise CREAKING()
 * @method static Noise DRUMMING()
 * @method static Noise FOOTSTEPS_AHEAD()
 * @method static Noise FOOTSTEPS_APPROACHING()
 * @method static Noise FOOTSTEPS_BEHIND()
 * @method static Noise FOOTSTEPS_RECEDING()
 * @method static Noise FOOTSTEPS_SIDE()
 * @method static Noise GIGGLING()
 * @method static Noise GONG()
 * @method static Noise GRATING()
 * @method static Noise GROANING()
 * @method static Noise GRUNTING()
 * @method static Noise HISSING()
 * @method static Noise HORN_TRUMPET()
 * @method static Noise HOWLING()
 * @method static Noise HUMMING()
 * @method static Noise JINGLING()
 * @method static Noise KNOCKING()
 * @method static Noise LAUGHTER()
 * @method static Noise MOANING()
 * @method static Noise MURMURING()
 * @method static Noise MUSIC()
 * @method static Noise RATTLING()
 * @method static Noise RINGING()
 * @method static Noise RUSTLING()
 * @method static Noise SCRATCHING_SCRABBLING()
 * @method static Noise SCREAMING()
 * @method static Noise SCUTTLING()
 * @method static Noise SHUFFLING()
 * @method static Noise SLITHERING()
 * @method static Noise SNAPPING()
 * @method static Noise SNEEZING()
 * @method static Noise SOBBING()
 * @method static Noise SPLASHING()
 * @method static Noise SPLINTERING()
 * @method static Noise SQUEAKING()
 * @method static Noise SQUEALING()
 * @method static Noise TAPPING()
 * @method static Noise THUD()
 * @method static Noise THUMPING()
 * @method static Noise TINKLING()
 * @method static Noise TWANGING()
 * @method static Noise WHINING()
 * @method static Noise WHISPERING()
 * @method static Noise WHISTLING()
 */
class Noise extends AbstractEnum
{
    private const BANG_SLAM = 'Fracas ou claquement';
    private const BELLOWING = 'Mugissement';
    private const BUZZING = 'Bourdenement';
    private const CHANTING = 'Psalmodie';
    private const CHIMING = 'Carillons';
    private const CHIRPING = 'Chants';
    private const CLANKING = 'Crépitements';
    private const CLASHING = 'Affrontement';
    private const CLICKING = 'Cliquetis';
    private const COUGHING = 'Toussotement';
    private const CREAKING = 'Craquements';
    private const DRUMMING = 'Battement';
    private const FOOTSTEPS_AHEAD = 'Pas en face';
    private const FOOTSTEPS_APPROACHING = 'Pas en approche';
    private const FOOTSTEPS_BEHIND = 'Pas derrière';
    private const FOOTSTEPS_RECEDING = 'Pas fuyants';
    private const FOOTSTEPS_SIDE = 'Pas sur le côté';
    private const GIGGLING = 'Fou rire';
    private const GONG = 'Gong';
    private const GRATING = 'Grincement';
    private const GROANING = 'Gémissements';
    private const GRUNTING = 'Grognements';
    private const HISSING = 'Sifflements';
    private const HORN_TRUMPET = 'Cor ou trompette';
    private const HOWLING = 'Hurlements';
    private const HUMMING = 'Fredonnements';
    private const JINGLING = 'Tintements';
    private const KNOCKING = 'Frappes';
    private const LAUGHTER = 'Rires';
    private const MOANING = 'Plaintes';
    private const MURMURING = 'Murmures';
    private const MUSIC = 'Musique';
    private const RATTLING = 'Cliquetis';
    private const RINGING = 'Sonnerie';
    private const RUSTLING = 'Bruissements';
    private const SCRATCHING_SCRABBLING = 'Gratements ou râclages';
    private const SCREAMING = 'Cris';
    private const SCUTTLING = 'Course précipitée';
    private const SHUFFLING = 'Pieds qui traînent';
    private const SLITHERING = 'Ondulements';
    private const SNAPPING = 'Claquements';
    private const SNEEZING = 'Éternuements';
    private const SOBBING = 'Sanglots';
    private const SPLASHING = 'Éclaboussure';
    private const SPLINTERING = 'Éclats';
    private const SQUEAKING = 'Piaillements';
    private const SQUEALING = 'Couinements';
    private const TAPPING = 'Tapotements';
    private const THUD = 'Bruits sourds';
    private const THUMPING = 'Martèlements';
    private const TINKLING = 'Tintements';
    private const TWANGING = 'Tremblements';
    private const WHINING = 'Pleunicheries';
    private const WHISPERING = 'Chuchotements';
    private const WHISTLING = 'Sifflements';
}
