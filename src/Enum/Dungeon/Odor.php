<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

/**
 * Class Odor.
 *
 * @method static Odor ACRID()
 * @method static Odor CHLORINE()
 * @method static Odor DANK_MOLDY()
 * @method static Odor EARTHY()
 * @method static Odor MANURE()
 * @method static Odor OZONE()
 * @method static Odor PUTRID()
 * @method static Odor ROTTING_VEGETATION()
 * @method static Odor SALTY_WET()
 * @method static Odor SMOKY()
 * @method static Odor STALE()
 * @method static Odor SULFUROUS()
 * @method static Odor URINE()
 */
class Odor extends AbstractEnum
{
    private const ACRID = 'Âcre';
    private const CHLORINE = 'Chlorée';
    private const DANK_MOLDY = 'Humide ou moisie';
    private const EARTHY = 'Terreuse';
    private const MANURE = 'Purin';
    private const OZONE = 'Ozone';
    private const PUTRID = 'Putride';
    private const ROTTING_VEGETATION = 'Végétation en décomposition';
    private const SALTY_WET = 'Salée et humide';
    private const SMOKY = 'Fumée';
    private const STALE = 'Viciée';
    private const SULFUROUS = 'Souffrée';
    private const URINE = 'Urine';
}
