<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

class Puzzle extends AbstractEnum
{
    private const ABYSS = 'Les personnages rencontrent une porte verrouillée qui doit être défoncée pour passer. 
    Qu\'est-ce qu\'il y a de l\'autre côté ? Un abîme. Enfoncer la porte peut coûter un ou deux personnages à vos 
    joueurs.';

    private const MIMIC = 'Les personnages se heurtent à une mimique qui a pris la forme d\'une porte. Tourner la 
    poignée de porte peut coûter la main à un personnage ! Ajoutez des morceaux de bois éclatés autour de la porte pour 
    donner un « indice » aux joueurs.';

    private const BLOB = 'Une créature gelée/blob est écrasée à mi-chemin à travers une porte, avec environ 30cm de 
    celle-ci qui dépasse. Il s\'avère que ce n\'est que la "partie émergée de l\'iceberg", pour ainsi dire - un petit 
    morceau qui a dû suinter quelque part parce que la pièce de 3 x 3m de l\'autre côté est déjà complètement remplie 
    de l\'énorme volume de cette chose.';

    private const BUTTONS = 'Les personnages trouvent une porte avec plusieurs boutons. Les mauvais boutons déclenchent 
    des pièges.';

    private const BARRICADES = 'Le groupe fait face à une porte totalement barricadée. Si les personnages abattent les 
    barricades, ils trouvent contre quoi la porte était barricadée...';

    private const DOUBLE_HEAVY_DOORS = 'Les joueurs se retrouvent face à face avec une double porte géante en acier 
    recouverte d\'une série de trous sombres et symétriquement alignés. L\'ouverture de la porte déclenche une batterie 
    de 40 flèches tirées à travers la porte.';

    private const PILLAR = 'Les joueurs trouvent une porte importante qu\'ils doivent franchir, mais un pilier massif 
    est tombé directement dedans, la coinçant et bloquant le passage.';

    private const LIVING_DOOR = 'Une porte vivante avec un visage accueille les personnages lorsqu\'ils entrent dans la 
    pièce. Il ne veut pas les laisser passer, mais s\'ils persistent, alors il dit qu\'il a besoin d\'un sacrifice 
    vivant. S\'ils essaient quand même de passer, il en choisit un au hasard et le piège dans un bocal magique 
    étreignant. Il peut être persuadé de laisser tomber son objectif, mais seulement à un prix plus élevé. S\'ils 
    paient le prix, il les laisse passer.';

    private const ELECTRICITY = 'Les joueurs sont coincés devant une grande porte qui ne bouge pas. De chaque côté de 
    la porte, deux de chaque côté, se trouvent quatre longs fils de cuivre flexibles qui sont enroulés. Dans la salle se 
    trouvent également un certain nombre d\'arbres fruitiers, notamment des oranges et des raisins, et un certain 
    nombre de pots en argile. L\'astuce pour passer est de fabriquer quatre piles primitives pour alimenter la porte 
    en remplissant des pots de jus de fruits acides et en y mettant les fils de cuivre.';

    private const KEYS = 'Il y a une porte avec un grand trou de serrure inhabituel. La pièce est remplie de clés et 
    d\'une poignée d\'objets aléatoires. Toutes les clés sauf la bonne sont enduites de poison (poison du sommeil, 
    flétrissement, etc. - c\'est à vous de décider), et la bonne s\'avère être quelque chose de vraiment aléatoire comme 
    une bouteille de vin ou un godemiché ( qui va vouloir toucher ça ?).';

    private const DUST_LOCK = 'Il y a une pièce d\'apparence simple avec une porte verrouillée et une clé sur un 
    piédestal. Lorsque les personnages ramassent la clé et essaient de l\'insérer dans la serrure, elle tombe en 
    poussière dès qu\'elle touche le trou de la serrure. La vraie clé est toujours dans la pièce, mais elle est cachée 
    quelque part - sous le piédestal, sous une brique dans le sol, etc. Les options sont infinies.';

    private const HIDDEN_KEYS = 'Le groupe atteint une grande porte centrale avec 3 à 6 trous de serrure. Chaque clé 
    doit être insérée pour l\'ouvrir. Cependant, chacune des clés est cachée dans une partie différente du donjon/de 
    l\'installation, soit dans un endroit vraiment évident (et donc irritant), comme au-dessus d\'un insert de 
    cheminée, ou à la fin d\'un défi de taille. Qu\'il y ait quelque chose de bon derrière cette grande porte centrale 
    dépend de vous. Il pourrait y avoir juste du gaz neurotoxique derrière ou quelque chose d\'aussi dangereux.';

    private const MUSIC_BOX = 'Les personnages trouvent une boîte à musique dans une partie aléatoire du donjon/de 
    l\'installation. Elle joue un air simple. Plus tard (combien de temps dépendra de vous), ils rencontrent une porte 
    infranchissable remplie de trous de la taille d\'une lame de poignard. Lorsqu\'un poignard est inséré dans un 
    trou, il produit un ton spécifique et unique. La porte est une serrure basée sur le son ; les personnages doivent 
    jouer la chanson de la boîte à musique pour ouvrir la porte.';

    private const MONOLITH = 'Il y a une porte infranchissable dans une pièce avec un grand monolithe à l\'intérieur. 
    Sur la face du monolithe se trouvent des runes brillantes qui sont chacune d\'une couleur différente et brillent 
    de leur lumière sur la face de la porte. Ce qui ouvre la porte dépend de vous - peut-être que les personnages 
    doivent couvrir un mot clé pour ouvrir la porte, ou peut-être qu\'ils doivent couvrir toutes les runes à 
    l\'exception de celles qui forment le mot, ou peut-être qu\'ils doivent créer un certain couleur 
    (ou série de couleurs, comme pour les serrures multiples) afin d\'ouvrir la porte.';

    private const DOME = 'Les personnages entrent dans une pièce avec une porte verrouillée et un dôme de verre 
    au-dessus duquel ils peuvent voir le ciel. Vous pouvez mettre la clé dans un endroit vraiment évident pour les 
    énerver, comme sur un crochet à côté de la porte. Le vrai truc est que s\'ils brisent le verre du dôme, 
    l\'illusion du ciel se brise également et un lac souterrain (précédemment retenu par le dôme) se déverse dans la 
    pièce/le donjon/l\'installation par le trou. Vous pouvez également complexifier la porte pour vraiment donner un 
    avantage et donner aux personnages une raison supplémentaire de casser le dôme.';

    private const GLASS_PANELS = 'Les personnages se retrouvent dans une pièce avec un certain nombre de médaillons de 
    verre scellés et une porte verrouillée et infranchissable avec la note "En cas d\'incendie, brisez le verre" 
    épinglé dessus. La plupart des boîtes en verre sont remplies de gaz Halon, qui explose et étouffe les personnages 
    qui y sont exposés (ou vous pourriez mettre quelque chose de pire). L\'un des médaillons ouvre la porte (ou 
    peut-être inonde simplement la pièce, si la porte s\'avère être un raté dans votre projet).';

    private const GALERIE = 'Les joueurs entrent dans une galerie de peintures étranges, et les deux portes se ferment 
    et se verrouillent. Si quelqu\'un s\'arrête pour regarder les peintures, il se sent attiré par elles, comme s\'il 
    pouvait simplement s\'avancer et être dans ce royaume. S\'ils essaient, ils se laissent entraîner dans le tableau 
    et doivent s\'occuper de ses habitants (combattre un chevalier, combattre une créature faite d\'horloges, être 
    soumis au monde de "Le Cri"). La clé est dans l\'un de ces mondes de la peinture.';

    private const BIP = 'La pièce contient une porte verrouillée et infranchissable qui, lorsqu\'on la touche, émet un bip 
    et dit : « Mot de passe, s\'il vous plaît ». À proximité se trouve une grande pyramide d\'onyx sur un piédestal 
    qui, lorsqu\'elle est touchée, vole l\'esprit de la personne qui l\'a touchée et injecte au hasard l\'esprit de 
    l\'un de ses autres captifs dans le corps vacant. L\'une de ces âmes connaît le mot de passe, et un certain nombre 
    d\'entre elles sont des aventuriers violents, fous ou avec leurs propres volontés qui ne seront pas trop enclins à 
    abandonner le corps dans lequel ils se trouvent.';

    private const TAPESTRIES = 'Il y a une pièce avec une porte de 10 à 20m de haut. Les murs sont recouverts de 
    tapisseries. Un mot aléatoire a été gravé sur le mur. Il s\'avère que c\'est le mot de passe pour l\'une des 
    tapisseries qui vous permet de traiter la tapisserie comme un tapis volant, mais il doit être retiré du crochet 
    anti-magie qui le maintient au mur avant qu\'il ne réponde au mot.';

    private const GALERIE_2 = 'Les personnages entrent dans une galerie pleine de peintures. La porte de sortie est 
    verrouillée et infranchissable, mais elle semble avoir une étrange qualité de maille. Comment s\'en sortent-ils ? 
    Il y a une lettre écrite au dos de chacune des peintures, et lorsqu\'elles sont combinées (et organisées) et 
    que le mot de passe est prononcé à voix haute, la porte s\'ouvre. Vous pouvez en faire quelque chose de vraiment 
    ringard comme « Sésame ouvre-toi ». Alternativement, vous pouvez mettre des mots complets au dos des peintures et 
    demander aux personnages de composer une phrase qui pourrait même être un indice pour un autre puzzle plus loin.';

    private const CHAINES = 'Il y a deux chaînes rétractées dans les murs à 30 pieds de distance. Les joueurs doivent 
    rassembler les deux (un exploit de force) pour ouvrir la porte. La porte se ferme rapidement, ils doivent donc 
    également trouver un moyen de connecter les chaînes pour garder la porte ouverte.';

    private const HEIGHT_HOLES = 'Le groupe trouve huit trous dans le mur, chacun avec une tige d\'acier à 
    l\'intérieur. Une tige ouvre la porte, tandis que les sept autres choquent le personnage (1d4/tour pour déterminer 
    ce qui se passe). Si un personnage est choqué, il est presque impossible de se libérer ; les autres personnages 
    devront généralement les détacher.';

    private const RING = 'Les joueurs entrent dans une pièce spacieuse avec un grand anneau de pierre dépassant d\'un 
    mur. Des personnages fantomatiques (autant qu\'il y a de personnages) traversent le mur, l\'un portant un ballon. 
    Ils sont tous assez forts, et cela devrait vraiment être un défi pour les joueurs de les battre, s\'ils le font. 
    Vous voyez, les gagnants sont emmenés et sacrifiés, et comme les joueurs de balle aztèques, les fantômes pensent 
    que cette mort honorable est souhaitable.';

    private const SCULPTURE = 'Au centre de la pièce, il y a un piédestal avec une jolie petite sculpture de lapin dessus.
    Cela ne fait rien, mais si jamais les personnages détournent le regard d\'un coup (comme s\'ils partaient), cela 
    rugit et secoue la pièce.';

    private const ELABORATED_TRAP = 'Le donjon contient un piège factice élaboré, comme une pièce remplie de plaques de 
    pression évidentes (peut-être marquées, comme un puzzle de mots runiques sur lequel il faut marcher) ou de grandes 
    lignes en gras, ce genre de chose. Soit le piège a été désactivé, soit il est là pour déranger les gens. Ajoutez 
    des incitations/la peur en plaçant des objets électriques d\'apparence dangereuse à la fin de la porte, des buses 
    menaçantes, des tuiles cassées qui tombent dans le magma, etc.';

    private const DIMENSION = 'Les personnages tombent sur un piège ou une situation qui utilise quelque chose d\'une 
    autre dimension/temps. Pour des décors fantastiques, jetez-leur quelques mines Claymore. Pour les décors 
    futuristes, lancez-leur une monstruosité à vapeur ou utilisez quelque chose impliquant de la magie (comme un mage 
    lanceur de boules de feu).';

    private const CAVE = 'Les joueurs entrent dans une pièce avec des murs de terre/pierre, comme l\'intérieur d\'une 
    grotte. Cela a l\'air totalement ordinaire, mais il pourrait y avoir un certain nombre de choses cachées derrière 
    les murs (gros animaux/monstres, lasers miniers, mécanismes qui poussent les murs ensemble pour écraser ce qu\'il 
    y a dans la pièce, etc.).';

    private const ROOMCEPTION = 'Le groupe entre dans une pièce avec une autre pièce plus petite à l\'intérieur. Du 
    sang s\'est infiltré par la porte de la plus petite pièce et a trempé le sol. Si les joueurs entrent à l\'intérieur, 
    ils découvrent une scène macabre et écoeurante de cadavres démembrés, puis les serrures de la porte et les murs 
    (ou le plafond) poussent des couteaux et commencent à bouger.';

    private const DELAYED = 'Les retardataires du groupe se font prendre au piège. Un bon exemple serait un couloir en 
    forme de L qui a des portes aux deux extrémités et un sol recouvert d\'environ trois pouces d\'essence. Dès que la 
    porte du coffre du L est ouverte, la porte opposée s\'enfonce dans le sol de 5 pieds, révélant un grand 
    lance-flammes qui grille n\'importe quoi dans la longue partie du couloir et allume l\'essence en feu. Celui qui a 
    ouvert la porte (et toute autre personne avec lui) ferait mieux de battre des pieds vite et d\'espérer que tous 
    ceux qui ont été touchés par le feu s\'en soient bien sortis.';

    private const ELECTRIFIED_NET = 'Une pièce est parée de ce qui ressemble (au départ) à un filet de fins filaments 
    fantomatiques. Celui-ci s\'avère être un fil monofilaire à intervalles aléatoires, rendant la pièce difficile à 
    traverser. Le monofil est un fil qui peut couper n\'importe quoi aussi facilement que s\'il passait dans l\'air, et 
    il est difficile de le voir tant que vous n\'êtes pas juste au-dessus. Vos joueurs devront être très prudents.';

    private const BARRILS = 'Il y a une pièce pleine de barils pourris remplis de dynamite en décomposition. Il n\'y a 
    pas de place pour les contourner, le franchir est le seul moyen.';

    private const BAMBOO = 'Le groupe fait face à une pièce remplie de tiges de bambou aiguisées à intervalles 
    réguliers, du sol au plafond. Il n\'y a pas vraiment assez de place pour passer, et si l\'un des poteaux est 
    touché, il prend vie, infligeant 1d4 points de dégâts au personnage.';

    private const AXED_FLOOR = 'Les aventuriers tombent sur un sol d\'apparence parfaitement ordinaire, une pièce vide, 
    etc. Lorsqu\'ils entrent, les portes se ferment et se verrouillent, et le sol commence à s\'incliner. C\'est sur un 
    axe, donc le mouvement l\'incline dans un sens ou dans l\'autre, et il faudra des réflexes rapides pour que les 
    personnages ne glissent pas dans la fosse punji en dessous. Malheureusement, la clé qui déverrouille la porte est 
    fixée à un crochet au sol de manière à ce qu\'elle ne tombe pas tant que les joueurs sont de l\'autre côté de la 
    pièce, mais si le sol s\'incline dans l\'autre sens, alors la clé se détache et tombe dans la fosse punji.';

    private const RIFT = 'Un gouffre de 15m plein de lames acérées comme des rasoirs se dresse entre les personnages et 
    leur objectif ou la porte d\'à côté.';

    private const CONNECTED_FLOOR = 'Les personnages entrent dans une pièce avec une porte à l\'autre bout. La pièce 
    est vide et la porte est verrouillée, mais celui qui essaie d\'ouvrir la porte déclenche un piège qui ouvre le sol 
    sous lui et il tombe dans une fosse peu profonde pour un minimum de dégâts (c\'est surtout juste surprenant). 
    S\'ils trouvent un moyen de franchir la porte, ils découvrent que c\'est juste pour le spectacle ; c\'est juste de 
    la pierre de l\'autre côté. La vraie porte de sortie est une trappe au fond de la fosse.';

    private const PIT = 'Un piège à fosse s\'ouvre sous un personnage, puis se scelle au-dessus de sa tête en 
    déclenchant des verrous sur toutes les portes d\'entrée ou de sortie de la pièce. À l\'intérieur du piège et dans 
    la pièce se trouvent une série d\'interrupteurs qui remplissent l\'espace opposé (piège à fosse ou pièce au-dessus) 
    avec de l\'eau ou libèrent du gaz endormi ou remplissent la pièce d\'arcs électriques - des choses amusantes qui 
    peuvent être combinées pour créer une situation vraiment poilue (comme un flot d\'essence et plus tard une boule de 
    feu). Heureusement, les interrupteurs sont des bascules marche/arrêt, même si la machine est ancienne et peut 
    prendre un moment ou deux pour répondre. Il y a aussi un interrupteur qui ouvre les portes, mais les deux doivent 
    être allumés pour fonctionner.';

    private const LEVERS = 'Le donjon contient une pièce remplie d\'innombrables leviers. Un déverrouille et ouvre la 
    porte opposée. Les autres ont divers effets désagréables ; par exemple, ils pourraient libérer des gaz endormis ou 
    hilarants, inonder la pièce, faire tomber un monstre d\'un tuyau au plafond (il pourrait y avoir un mécanisme 
    complexe avec tout un tas de monstres en cage), etc.';

    private const EIGHT_LEVERS = 'La pièce comporte huit leviers dans des douilles qui doivent tous être tournés à la fois.';

    private const EIGHT_POOLS = 'Les personnages entrent dans une pièce avec huit bassins d\'eau et huit leviers de 
    chasse d\'eau. Des plaques de pression dans le sol permettent d\'utiliser les leviers. Sept d\'entre eux jettent 
    les personnages dans une fosse pleine d\'eau et de zombies. Une ouvre la porte.';

    private const STAIRS = 'Un escalier incurvé descendant domine la pièce. Les plaques de pression sur l\'escalier 
    déclenchent une arbalète/une arme automatisée en bas, donnant l\'impression que quelqu\'un est là-bas. Cependant, 
    ce n\'est qu\'une statue qui est câblée pour tirer des boulons/balles. La porte de la pièce voisine est à côté de 
    la statue.';

    private const YELLOW_LINES = 'Le groupe trouve une pièce avec des lignes jaunes qui bordent les murs et se brisent 
    à des espaces à intervalles réguliers 2 à 3 fois. Là où les lignes se cassent, il y a un piège sensible à la 
    pression qui fait tourner toute la section du couloir vers la gauche et éjecte les personnages dans une fosse à 
    une autre. Ajoutez des punjis, des zombies, des monstres, etc. pour les recevoir lorsqu\'ils tombent, le cas 
    échéant.';

    private const STATUES = 'Il y a une pièce pleine de statues (environ huit) tenant des fioles bouchées. Le 
    déclenchement de plaques de pression cachées fait tomber les flacons des statues, libérant un gaz neurotoxique. La 
    poignée de porte est une lame de couteau.';

    private const ZODIAC = 'Le groupe découvre une pièce avec un zodiaque réparti sur le sol. Une observation attentive 
    montre qu\'il a un treizième symbole ; une observation plus approfondie révèle qu\'il s\'agit d\'un symbole sans 
    rapport (faites preuve de créativité), et une inspection encore plus approfondie révèle qu\'il s\'agit d\'une 
    plaque de pression.';

    private const SUBSONIC = 'Il existe un appareil (ou une pièce abritant un appareil) qui déclenche de manière 
    subsonique les centres de plaisir du cerveau chez tous ceux qui y sont exposés. Il pourrait être déclenché par une 
    plaque de pression au centre de la pièce. Les personnages peuvent être pris en permanence au centre de la pièce, 
    ne voulant pas (ou incapables) de bouger simplement parce qu\'ils vivent quelque chose qui ressemble à un orgasme 
    massif et sans fin. Bien sûr, ils finiront par mourir de faim, ou peut-être que vous pourriez avoir une sorte de 
    créature qui les grignote d\'abord.';

    private const ICE_PARTICULES = 'La pièce est remplie de particules glaciales qui collent à la peau et ralentissent 
    considérablement les personnages. Ajoutez des monstres ou d\'autres défis.';

    private const GRAVITIY = 'Les joueurs entrent dans une pièce avec la gravité tirant dans quatre directions (haut, bas, 
    gauche, droite). Chaque "mur" est une fosse d\'or en fusion de 5 pieds de profondeur.';

    private const MIST = 'Le groupe doit traverser une salle remplie d\'une brume bleu/rouge/vert. Il peut s\'agir 
    simplement de brume ou d\'une illusion, ou il peut s\'agir d\'un vampirique, d\'une absorption de sang (PV), de 
    statistiques ou même de pouvoirs d\'artefact/objet. Ou peut-être qu\'il rend toute la poudre à canon qu\'il touche 
    non fonctionnelle - il y a beaucoup de place pour la créativité ici.';

    private const RED_MIST = 'Les personnages entrent dans une pièce remplie d\'une brume rouge, et il y a une étrange 
    saveur de fer dans l\'air. Vous pouvez inclure des choses qui se cachent dans la brume, mais la présence de la 
    brume à elle seule devrait suffire à déranger les personnages. Une pièce ou deux plus loin, les portes se ferment 
    (elles n\'ont pas besoin de verrouiller) et avec un crépitement et un bourdonnement d\'électricité, un 
    électro-aimant hypersensible qui couvre tout le plafond s\'anime. Cela saisira des armes, des armures et tout ce 
    qui est magnétique, y compris les objets qui n\'ont pas été correctement nettoyés après la salle de brume rouge. 
    Ensuite, la pièce commence à se remplir d\'eau (ou d\'autre chose, cela dépend à quel point vous voulez être 
    méchant). Cependant, il y a un drain de l\'autre côté de chaque porte, donc s\'ils peuvent se libérer de l\'aimant 
    avant de se noyer, il y a toujours un espoir de s\'échapper.';

    private const STRNAGE_LIGHT = 'Le donjon comprend une pièce avec une source de lumière inhabituelle (comme une 
    torche dans un tube dans le sol) avec beaucoup d\'ombres. Une ou plusieurs de ces ombres sont vivantes et 
    affamées.';

    private const MAGIC_AIR = 'Les personnages entrent dans un couloir rempli d\'air enchanté pour éteindre toute 
    source de lumière, ils doivent donc trébucher dans le noir. Cela conduit à une grande pièce qui est également 
    enchantée, mais s\'ils marchent involontairement sur une plaque de pression dans cette pièce, cela désactive 
    l\'enchantement et déclenche une cache de 50 pièces qui tombe du plafond. Chaque pièce est enchantée par un sort 
    de lumière continue très brillant. La lumière combinée des sources lumineuses des joueurs et des pièces de monnaie 
    est aveuglante, surtout combinée au fait que toute la pièce a été recouverte de miroirs. Ajoutez des monstres qui 
    bondissent et sautent sur des joueurs aveugles comme bon vous semble.';

    private const STROBOSCOP = 'Les joueurs entrent dans une pièce vide avec une lumière stroboscopique lente (basée sur 
    les sorts ou la technologie, tout ce qui fonctionne dans votre campagne). Ce qu\'ils ne voient pas, c\'est le 
    monstre/ninja/assassin collé au plafond au-dessus d\'eux.';

    private const DEFORMED_MIRRORS = 'Il y a une salle éclairée par stroboscope avec des miroirs déformés. Chaque fois 
    que la lumière clignote, quelque chose de grotesque apparaît. Ajoutez des doppelgängers pour amplifier le plaisir, 
    si vous le souhaitez.';

    private const TOO_OLD = 'Une pièce a un champ hyper-vieillissant installé à l\'intérieur qui transforme une minute 
    en un an. A l\'intérieur du champ, la pièce a fait place à une forêt luxuriante avec des fleurs qui s\'épanouissent, 
    se transforment en fruits et tombent en l\'espace d\'une minute. Il y a une porte de l\'autre côté.';

    private const CLOCK = 'Les personnages atteignent une pièce d\'apparence normale avec des meubles décents 
    (un canapé, une table basse, plusieurs fauteuils, une bibliothèque, une horloge, etc.). Il y a aussi une fontaine 
    d\'eau et un bol de fruits sur la table. Lorsque le groupe entre à l\'intérieur, la porte se verrouille derrière 
    eux et devient infranchissable (comme l\'autre porte l\'est déjà). Comment s\'en sortent-ils ? L\'horloge sur le 
    mur est ouverte et magique ; le tourner fait avancer (ou l\'inverser) le temps dans la pièce, mais uniquement sur 
    les objets de la pièce (et les portes). Ils peuvent l\'inverser pour rouvrir la première porte, ou ils peuvent la 
    pousser vers l\'avant jusqu\'à ce que les portes pourrissent et tombent des charnières. (Ou ouvrir automatiquement, 
    quelque chose comme ça.)';

    private const CANYON = 'Un puissant sorcier crée un gouffre caverneux sous la ville dans laquelle se trouvent les 
    joueurs.';

    private const PIRATES = 'Une horde de pirates fantômes traverse les murs et attaque les joueurs.';

    private const ARENA = 'Les joueurs ouvrent une porte d\'apparence fragile et se retrouvent face à face avec une 
    immense salle de style arène remplie d\'un public de 10 000 à 20 000 zombies affamés.';

    private const GORILLAS = 'Le groupe pénètre dans une pièce remplie d\'environ 30 gorilles qui lèvent tous les yeux 
    lorsque la porte est ouverte. Regardez vos joueurs paniquer. Les gorilles sont en fait très doux ; ils ne se 
    soucient pas de ce que font les joueurs, tant qu\'ils ne blessent aucun d\'entre eux. S\'ils le font, les gorilles 
    s\'enfuient ou tentent de se défendre.';

    private const CORPSES = 'La pièce est remplie de cadavres dans divers états de décomposition. Ils peuvent soit 
    attendre le moment de se lever et d\'attaquer, ou peut-être ne sont-ils vraiment que des cadavres. Ou vous pouvez 
    faire preuve de créativité et les faire s\'élever comme par magie, mais n\'être rien de plus que les jouets d\'un 
    marionnettiste.';

    private const SCARY_ROOM = 'Il y a une pièce vraiment effrayante avec des choses vraiment effrayantes que les 
    joueurs peuvent explorer, jouer avec ou même emporter avec eux. Considérez votre salon, puis imaginez si chaque 
    meuble était fait d\'os et de peau humaine mal étirée, ce genre de chose.';

    private const SINGULARITY = 'Alors que les personnages sont dans une ville pour acheter des fournitures, chercher 
    du travail ou se détendre, un sorcier de cette même ville ouvre involontairement une bulle de singularité dans sa 
    tour, faisant de la ville le point focal d\'un bébé trou noir dans lequel le monde entier va imploser lentement. 
    (C\'est un bon début pour une nouvelle quête.)';

    private const BOATS = 'Le groupe voit un long passage d\'eau trouble et sans profondeur devant lui. Deux bateaux 
    sont amarrés à l\'entrée : un qui a l\'air branlant et qui a un peu d\'eau au fond, et un qui a l\'air neuf et 
    vraiment étanche, avec des bords dorés. Il s\'avère que le "nouveau" bateau est en fait une illusion, et il 
    disparaît dans une bouffée de fumée à mi-chemin du passage. Ajoutez des rencontres aquatiques, des tentacules, des 
    piranhas, des kelpies et autres.';

    private const ILLUSION_WALL = 'Les personnages font face à un mur illusoire de feu, de glace, d\'eau et/ou de 
    foudre.';

    private const FOUNTAIN = 'Il y a une pièce circulaire avec une fontaine au centre. On dirait que le sol est 
    recouvert d\'environ 10cm d\'eau et que des pièces d\'or sont réparties sur le fond. Cela s\'avère être une 
    illusion : l\'eau a en fait plus de trois mètres de profondeur avec quelque chose d\'effrayant et d\'affamé au 
    fond (comme des tentacules et une grande bouche pleine de dents, une autre vase ou une horde de zombies gonflés 
    d\'eau, etc.).';

    private const BRUMISATORS = 'Le groupe doit traverser un long couloir rempli de brumisateurs. Au centre se trouve 
    un piédestal qui ne fait rien, mais on dirait qu\'il fait quelque chose. L\'astuce est dans les brumisateurs : ils 
    distribuent du LSD liquide, et il commence à faire effet avant trop longtemps. Plus l\'exposition est longue, plus 
    les hallucinations deviennent vives.';

    private const FLOWER_FIELD = 'Les joueurs atteignent un grand champ de fleurs et de fruits et de paix. Le fruit a 
    une toxine du sommeil. Tout cela n\'est qu\'une illusion, et le levier "effacement" peut être trouvé avec un peu de 
    recherche. Il s\'avère que toute la "nourriture" était en fait des eaux usées.';

    private const STRANGE_MIST = 'Il y a une étrange brume dorée dans la pièce. Au centre de la pièce se trouve un 
    bébé hurlant sur une estrade. Si les joueurs le récupèrent, il sourit et rit, puis se transforme en une liasse de 
    chair mutante en colère avec des serres qui essaie de s\'accrocher aux visages et ainsi de suite. À ce moment-là, 
    le reste de la pièce se transforme en quelque chose de vivant, comme l\'intérieur d\'un estomac.';

    private const JACKAL = 'Les joueurs entrent dans une pièce juste à temps pour voir un homme à tête de chacal 
    (Anubis) affronter un homme qui ressemble à un autre aventurier. Il dit : « Seul celui dont le cœur est si pur 
    qu\'il pèse moins qu\'une plume peut passer de l\'autre côté. Montre-moi ta poitrine afin que je puisse juger. 
    L\'aventurier le fait, et Anubis tend la main et enlève son cœur, puis le pèse. Trouvant qu\'il pèse plus qu\'une 
    plume, il met le cœur dans la bouche d\'un crocodile voisin, et l\'aventurier meurt sur le coup. Anubis regarde 
    alors les personnages et répète sa première ligne. En vérité, tout cela est une illusion, mais le choc d\'avoir le 
    cœur dévoré comme tel suffit à tuer un personnage.';

    private const SAGE = 'Un sage/messie à l\'air sage (ou célèbre) attend les personnages dans une pièce sans portes 
    visibles. Il les salue et leur parle. Si on leur demande où aller ou comment procéder, le sage leur dit que ce 
    n\'est que dans la mort que l\'on peut voir et franchir la porte qui se trouve dans cette pièce. Il est verrouillé 
    sur tout le reste. Les tentatives pour trouver la porte devraient échouer. Le sage continuera à les "aider" à 
    accepter qu\'ils doivent mourir et les tuera volontiers s\'ils le souhaitent (absorbant leurs esprits en guise de 
    paiement et les empêchant de passer dans l\'autre monde afin qu\'ils soient simplement morts et partis pour 
    toujours). Le seul moyen est d\'affronter le sage, qui, une fois que les joueurs commencent à devenir agressifs, 
    se transforme en une sorte de monstre hideux qui s\'avère être un véritable test des forces des personnages. Tuer 
    le monstre révèle la porte et la déverrouille.';

    private const NIGHTMARE = 'La pièce est pleine de bulles qui leur montrent les cauchemars, les mauvais souvenirs 
    et les peurs des joueurs.';

    private const HOLE = 'La pièce a un trou déchiqueté et sans fond au centre. Une inspection plus approfondie révèle 
    des marques de dents massives sur le bord du trou et un son de respiration profond venant de loin dans le fond. 
    C\'est un excellent moyen d\'effrayer vos personnages. Ajoutez un wyrm mangeur d\'hommes géant pour un peu plus de 
    piquant.';

    private const ONE_MIRROR = 'Un seul miroir se trouve au fond de la pièce. Le regarder dedans révèle un visage qui 
    saute du miroir et crie. Si un personnage surmonte sa peur et crie en retour, il est attrapé et tiré de l\'autre 
    côté.';

    private const HANGED_MAN = 'Le groupe pénètre dans une pièce avec un homme pendu au fond, les poignets et les 
    chevilles enchaînés au mur et au-dessus de la porte. Se rapprocher de lui resserre les chaînes. Il hurle de 
    douleur et implore sa pitié à chaque fois qu\'ils se resserrent. Cela pourrait être une illusion – vous décidez à 
    quel point vous pouvez jouer avec le sens du bien et du mal de vos personnages.';

    private const PLATE_FORM = 'Les aventuriers atteignent une plate-forme circulaire surélevée. La lumière descend 
    dessus. Seul le pénitent passera.';

    private const SEPARATION = 'Les personnages entrent dans une pièce, et soudain des murs d\'acier tombent du 
    plafond et les séparent, créant des couloirs individuels d\'où il semble qu\'il n\'y ait aucune issue. Les murs 
    doivent également être insonorisés. Dès que le personnage détourne le regard ou devient désespéré, apparaît 
    quelqu\'un en qui il aime ou en qui il a vraiment confiance, et ce quelqu\'un essaie de le maintenir là par tous les 
    moyens nécessaires. Ajoutez de l\'eau montante ou de la chaleur ou d\'autres inducteurs de panique mortels pour 
    plus de plaisir. Comment les personnages s\'en sortent-ils ? Ils doivent faire preuve de courage pour attaquer et 
    tuer la personne illusoire.';

    private const STRANGE_STATUE = 'Le groupe trouve une statue d\'un grand homme chamois avec un sourire sur son 
    visage. Il tient son biceps d\'une main et étend son bras, l\'autre main serrée à mi-chemin entre l\'ouverture et 
    le poing. Les joueurs doivent faire un bras de fer contre la statue pour passer.';

    private const LAUGTHING_STATUE = 'Il y a un long couloir de gravier avec une lourde statue d\'un gars riant en 
    tenue de football tenant une corde à une extrémité et un coffre de voiture à l\'autre. Les personnages doivent 
    faire glisser la statue à travers le gravier jusqu\'au coffre de la voiture et les connecter pour passer.';

    private const CHICKEN_STATUE = 'Le groupe arrive sur une table avec deux chaises et une statue d\'un poussin 
    souriant et un manchot qui prend vie et commence à verser des boissons. Les personnages doivent boire plus que le 
    manchot.';

    private const NO_KEY = 'Les personnages atteignent une jonction délicate : dans une pièce se trouve un piège/défi et 
    une porte verrouillée et incassable mais pas de clé. Dans l\'autre se trouve la clé, mais elle est posée sur un 
    piédestal au milieu d\'un groupe de statues menaçantes de guerriers armés qui la regardent tous. Que les statues 
    prennent vie ou non lorsque la clé est prise, c\'est à vous de décider.';

    private const BOILING_POOL = 'La pièce comprend une piscine d\'eaux usées bouillonnantes avec un tuyau en dessous 
    menant à une cuvette de toilettes à travers laquelle les joueurs doivent passer.';

    private const TSUNAMI = 'Un énorme raz-de-marée de style tsunami se lève immédiatement pour écraser les 
    personnages. Ils peuvent choisir de s\'enfuir par peur ou d\'y faire face. Cela s\'avère être une illusion.';

    private const FISHES = 'Il y a une pièce remplie d\'eau et de poissons qui sont contenus dans la pièce par magie 
    ou technologie. Quoi que ce soit, vous pouvez le parcourir, et vous devrez le faire si vous voulez voir ce qu\'il 
    y a de l\'autre côté. L\'eau est sombre et un peu trouble, mais il n\'y a que des poissons d\'apparence inoffensive 
    visibles, donc ça ne peut pas être si grave... jusqu\'à ce que vous soyez à mi-chemin et qu\'il y ait un requin ou 
    une pièce plus grande avec un plus gros requin, ou quelque chose comme ça.';

    private const SUBAQUATIC = 'Les personnages marchent dans un long passage venteux avec des grilles espacées à 
    intervalles réguliers le long du sol. Un examen plus attentif révélera des murs anormalement lisses, leur surface 
    presque vitreuse. Avant que les personnages n\'entrent trop loin, la porte par laquelle ils sont venus se referme, 
    ouvrant un large tuyau qui commence à remplir la pièce de liquide alors que toutes les grilles du sol se scellent. 
    Le liquide peut être de l\'eau simple ou, si vous vous sentez particulièrement désagréable, en faire quelque chose 
    de plus dangereux, comme de l\'acide sulfurique. Vous pouvez également faire de la porte à l\'autre bout du tunnel 
    un puzzle à ouvrir, pour plus de plaisir et de panique.';

    private const TRAP2 = 'Lorsque les personnages entrent dans la pièce, la porte se ferme derrière eux et devient 
    infranchissable. Il n\'y a pas d\'autres portes visibles et la pièce commence à être inondée. Quelle est 
    l\'astuce pour sortir ? Il y a une trappe dissimulée au plafond qui ne peut être atteinte qu\'en nageant lorsque 
    la pièce se remplit.';

    private const STICKY_FLOOR = 'Les joueurs rencontrent une pièce avec un sol en terre battue et une porte à 
    l\'extrémité opposée. Ce sol est collant, comme du sable mouvant, et il fait environ 4m de profondeur.';

    private const STICKY_THING = 'La pièce comporte une ouverture/porte circulaire de la taille d\'un homme qui 
    descend dans le sol. Il est rempli de quelque chose de collant et de visqueux, comme du miel ou de la mélasse. 
    Au-delà du sol (où il ne peut être vu qu\'en plongeant), il se rétrécit progressivement jusqu\'à une ouverture de 
    la taille d\'un poing avec un levier à portée de bras maximum qui ouvre une porte secrète (menant à la pièce 
    suivante), mais c\'est de retour au-delà de l\'embouchure du tuyau.';

    private const EAR_BLOWER = 'Le groupe atteint un mur de force acoustique qui fait exploser les oreilles et ne peut 
    être traversée que par la pure force de la volonté. Cela rend les personnages temporairement sourds.';

    private const ICE_WALL = 'Un épais mur de glace bloque le passage et projette des lames de 10 pouces au toucher 
    (1d6).';

    private const BALLS = 'Les joueurs tombent sur une pièce remplie de ballons de basket rebondissants dans lesquels 
    ils ont du mal à traverser. Au milieu quelque part, des balles commencent à tirer sur les personnages au hasard, 
    les frappant à des endroits de type « étourdissant », comme le visage et l\'entre-jambe.';

    private const PIPES = 'La pièce est remplie d\'un étrange réseau de tuyaux qui rend les déplacements difficiles. 
    Une inspection plus approfondie révèle que certains des tuyaux sont fabriqués à partir de substances autres que le 
    métal (comme la peau ou le bois) et que les tuyaux réagissent au toucher. Si la pièce se sent menacée de quelque 
    manière que ce soit, ou si elle pense qu\'elle peut tuer quiconque s\'y trouve, elle fera éclater les tuyaux à 
    proximité de cette personne, les aspergeant de vapeur, de verre dépoli, d\'acide ou de tout nombre d\'autres 
    choses que le DM peut proposer... Les personnages morts et les objets abandonnés laissés dans la pièce sont 
    absorbés pour fabriquer de nouveaux tuyaux.';

    private const HEXAGONE = 'Le groupe découvre une pièce hexagonale sans issue évidente et une flèche dessinée au 
    sol face à un mur. Il y a des portes secrètes sur chaque mur, chacune menant à une autre pièce hexagonale 
    similaire avec une autre flèche aléatoire. (Vous voudrez peut-être cartographier cela afin de ne pas vous perdre, 
    même si les joueurs le font.) Cela fait un grand labyrinthe.';

    private const LABYRINTHE = 'Le groupe doit naviguer dans un long labyrinthe de passages d\'éponges jaunes 
    gluantes qui sont juste assez grands pour qu\'une personne puisse ramper à travers. Les éponges commencent à 
    rétrécir et à durcir si les joueurs mettent trop de temps à passer.';
}
