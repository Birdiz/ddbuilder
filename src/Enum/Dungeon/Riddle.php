<?php

declare(strict_types=1);

namespace App\Enum\Dungeon;

use App\Model\AbstractEnum;

class Riddle extends AbstractEnum
{
    private const MAP = 'J\'ai des villes sans habitants, des forêts sans arbres et des rivières sans eau. 
    Que suis-je ? - Une carte.';
    private const E = 'Je n\'existe ni au début, ni à la fin. Que suis-je ? - La lettre e.';
    private const SUN = 'Je suis l\'oeil d\'un visage bleu. Mon regard nourrit le monde. Si je deviens aveugle, alors 
    le monde l\'est aussi. Que suis-je ? - Le Soleil.';
    private const FOOTPRINT = 'Le plus vous en laissez, le plus vous en faites. Que suis-je ? - Des pas.';
    private const RIDING_PERSON = 'Qu\'est-ce qui a 6 pattes mais ne marche qu\'avec 4 ? - Un cavalier.';
    private const PAPER = 'Peu importe si la matière est desséchée, peu importe si elle est roulée. Peu importe si elle 
    est magique, peu importe son âge. Que suis-je ? - Du papier.';
    private const NOTHING = 'Le riche le veut, le pauvre l\'a, mais les deux meurrent s\'ils le mangent. Que suis-je ? 
    - Rien.';
    private const SILENCE = 'Nomme-moi et tu me briseras. Que suis-je ? - Le Silence.';
    private const NAME = 'Passé de père en fils et partagé entre frères, pourtant utilisé par beaucoup d\'autres. Que 
    suis-je ? - Un nom.';
    private const BOOK = 'Je porte un manteau de cuir pour que ma peau fonctionne. Je vous transporte vers d\'autres 
    royaumes sans portail magique. Que suis-je ? - Un livre.';
    private const TOWEL = 'Je gonfle quand je sèche. Que suis-je ? - Une serviette.';
    private const ECHO = 'Beaucoup m\'ont entendu, mais pourtant jamais vu. Je ne réponds que si l\'on me parle. Que 
    suis-je ? - L\'écho.';
    private const WOUND = 'Mieux vaut m\'avoir vieille que jeune. Plus je suis en santé, plus je suis petite. Que 
    suis-je ? - Une cicatrice.';
    private const BOOTS = 'Deux amies se tiennent ensemblent et voyagent ensemble. L\'une presque inutile sans 
    l\'autre. Que suis-je ? - Des bottes.';
    private const SAND = 'Je construis les châteaux et pourtant je defais les montagnes. J\'aveugle certains quand je 
    rends la vue à d\'autres. Que suis-je ? - Du sable.';
    private const TIME = 'L\'homme fou me perd, l\'homme commun me passe et l\'homme sage me prend. Que suis-je ? - 
    Le temps.';
    private const PIANO = 'Je suis fait de cinq lettres et je suis fait de 7 lettres. J\'ai des clés sans serrures et le
     temps m\'importe mais pas les horloges. Que suis-je ? - Un piano.';
    private const TEETH = 'Trente-deux chevaux sur une colline rouge. Ils mâchent, ils érasent puis reste immobiles. 
    Que sont-ils ? - Les dents.';
    private const ICE_POISON = 'Deux hommes boivent un thé glacé empoisonné. L\'un le boit vite et vit. L\'autre le 
    boit lentement et meurt. Comment est-ce possible ? - Le poison est dans la glace qui fond lentement.';
    private const TRUTH = 'On me souhaite en publique mais l\'on me craint en privé. Que suis-je ? - La vérité.';
    private const CARDS = 'Apportées à table, coupées et servies mais jamais mangées. Que sommes-nous ? - Des cartes.';
    private const RUST = 'Je peux percer les meilleurs armures et faire de la poussières des épées. Pourtant, je ne 
    peux rien contre un bâton. Que suis-je ? - De la rouille.';
    private const ORANGE = 'On m\'enlève de chez moi, on déchire ma chair, on boit mon doux sang et on jette ma peau. 
    Que suis-je ? - Une orange.';
    private const SECRET = 'Si l\'on me garde, je dure pour des siècles et si l\'on me partage, je disparaîs dans la 
    seconde. Que suis-je ? - Un secret.';
    private const SHOE = 'Le plus tu me marches dessus, le plus je m\'habitue à toi. D\'autres pourraient m\'utiliser, 
    mais c\'st à toi que j\'appartiens. Que suis-je ? - Une chaussure.';
    private const WINE = 'Ils disent que je donne de la joie et l\'hilarité, que je sens vieux. Je peux pourtant donner 
     l\'audace à l\'homme timide. Que suis-je ? - Du vin.';
    private const CANDLE = 'Mon corps est fin et élancé, et je rapetisse chaque jour. Je m\'assure de guider de nuit. 
    Placée sur un gâteau, je suis sûr que ma vie va bientôt s\'effacer. Que suis-je ? - Une bougie.';
    private const ELEMENTAL = 'Né par le feu, la pierre ou la pluie, je me sens plus à l\'aise à la maison sur mon 
    plan. Quand je suis hors de mon élément, je ressens beaucoup de désarroi. Que suis je? - Un élémentaire.';
    private const SICKNESS = 'Je ne vous suis offert que lorsque je ne suis pas désiré. J\'ai le pouvoir de tuer les 
    rois ou les plus humbles pauvres. Ma force est incontestée et je me déplace loin, mais mon pouvoir peut faiblir à 
    cause de potions absorbées. Que suis je? - Une maladie.';
    private const HOLE = 'Le plus tu me prends, le plus grand je suis. Que suis je? - Un trou.';
    private const CASTLE = 'Halo d\'eau, langue de bois, peau de pierre et longtemps resté debout. Mes courts 
    doigts portent vers le ciel, à l\'intérieur de mon cœur les hommes vivent et meurent. Que suis je? - Un château.';
    private const CLOUD = 'Volant sur des ailes invisibles. Je suis de taille massive. Alors si mon maître commande, je 
    suis aussi petit qu\'il le souhaite. Tous les hommes veulent m\'attraper, mais quand je les touche, ils ne peuvent 
    pas me toucher. Je pleure quand je suis avec mes frères. L\'obscurité me suit partout où je vais. Je suis un ami, 
    je suis un ennemi. Je suis la liberté. Que suis je? - Un nuage.';
    private const COAL = 'Qu\'est-ce qui est noir lorsque vous l\'achetez, rouge lorsque vous l\'utilisez et blanc 
    lorsque vous le jetez ? - Du charbon.';
    private const ZERO = 'Si je suis devant je n\'ai pas d\'importance, si je suis derrière je fais que tout soit plus, 
    je suis quelque chose pourtant je ne suis rien. Que suis je ? - Zéro.';
    private const M = 'Qu\'est-ce qui vient une fois par minute, deux fois par moment, mais jamais en cent ans ? - 
    La lettre M.';
    private const CHICK = 'J\'ai une petite maison dans laquelle je vis tout seul. Il n\'a ni portes ni fenêtres, et si 
    je veux sortir, je dois percer le mur. Que suis-je ? - Un poussin.';
    private const SCISSORS = 'Mettez vos doigts dans mes yeux et j\'ouvrirai grand mes mâchoires. Toile de lin, plumes 
    ou papier, tout se fendra devant moi. Que suis-je ? - Des ciseaux.';
    private const MOON = 'J\'existe depuis des millions d\'années, mais je n\'ai pas plus d\'un mois. Que suis-je ? - 
    La Lune.';
    private const TONGUE = 'Qu\'est-ce qui a meilleur goût que ça ne sent? - La langue.';
}
