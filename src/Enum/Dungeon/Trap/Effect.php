<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Trap;

use App\Model\AbstractEnum;

/**
 * Class SaveAndAttack.
 *
 * @method static Effect SETBACK()
 * @method static Effect DANGEROUS()
 * @method static Effect DEADLY()
 */
class Effect extends AbstractEnum
{
    private const SETBACK = [
        'name' => 'Facile',
        'save' => '10-11',
        'attack' => '+3 à +5',
        'damage' => [
            'Débutant' => '1d10',
            'Expert' => '2d10',
            'Héros' => '4d10',
            'Divin' => '10d10',
        ],
    ];

    private const DANGEROUS = [
        'name' => 'Dangereux',
        'save' => '12-15',
        'attack' => '+6 à +8',
        'damage' => [
            'Débutant' => '2d10',
            'Expert' => '4d10',
            'Héros' => '10d10',
            'Divin' => '18d10',
        ],
    ];

    private const DEADLY = [
        'name' => 'Mortel',
        'save' => '16-20',
        'attack' => '+9 à +12',
        'damage' => [
            'Débutant' => '4d10',
            'Expert' => '10d10',
            'Héros' => '18d10',
            'Divin' => '24d10',
        ],
    ];
}
