<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Trap;

use App\Model\AbstractEnum;

/**
 * Class Trap.
 *
 * @method static Trap MAGIC_MISSILES()
 * @method static Trap COLLAPSING_STAIRCASE()
 * @method static Trap CEILING_BLOCK()
 * @method static Trap CEILING_LOWER()
 * @method static Trap CHUTE()
 * @method static Trap CLANGING_NOISE()
 * @method static Trap DISINTEGRATE()
 * @method static Trap CONTACT_POISON()
 * @method static Trap FIRE_SHOOT()
 * @method static Trap FLESH_TO_STONE()
 * @method static Trap COLLAPSING_FLOOR()
 * @method static Trap GAS()
 * @method static Trap ELECTRIFIED_FLOOR()
 * @method static Trap GLYPH_OF_WARDING()
 * @method static Trap WHEELED_STATUE()
 * @method static Trap LIGHTNING_BOLT()
 * @method static Trap FLOOD()
 * @method static Trap DARTS_SHOOT()
 * @method static Trap ANIMATED_OBJECT()
 * @method static Trap PENDULUM()
 * @method static Trap HIDDEN_PIT()
 * @method static Trap HIDDEN_PIT_FLOOD()
 * @method static Trap LOCKED_PIT()
 * @method static Trap BLADES()
 * @method static Trap SPEARS()
 * @method static Trap BRITTLE_STAIRS()
 * @method static Trap THUNDERWAVE()
 * @method static Trap JAWS()
 * @method static Trap STONE_BLOCK()
 * @method static Trap SYMBOL()
 * @method static Trap WALLS_SLIDE()
 */
class Trap extends AbstractEnum
{
    private const MAGIC_MISSILES = 'Missiles magiques lancés depuis une statue ou un objet';
    private const COLLAPSING_STAIRCASE = 'Un escalier se brise créant une rampe jusqu\'à une fosse';
    private const CEILING_BLOCK = 'Un morceau ou l\'ensemble du plafond s\'éffondre';
    private const CEILING_LOWER = 'Le plafond descend doucement dans une pièce fermée';
    private const CHUTE = 'Une fosse s\'ouvre dans le sol';
    private const CLANGING_NOISE = 'Des bruits de claquements attirent les monstres environnants';
    private const DISINTEGRATE = 'Lance le sort de Désintégration';
    private const CONTACT_POISON = 'Une porte ou un objet est enduit de poison';
    private const FIRE_SHOOT = 'Des traits de feu sont lancés depuis le sol, les murs ou un objet';
    private const FLESH_TO_STONE = 'Lance le sort de Pétrification';
    private const COLLAPSING_FLOOR = 'Le sol s\'effondre ou en donne l\'illusion';
    private const GAS =
        'Une ventilation relâche du gaz : aveuglant, acide, bloquant, paralysant, empoisonnant ou soporifique';
    private const ELECTRIFIED_FLOOR = 'Le sol devient électrifié';
    private const GLYPH_OF_WARDING = 'Déclenche le sort Glyphe de protection';
    private const WHEELED_STATUE = 'Une grosse statue roule le long du couloir';
    private const LIGHTNING_BOLT = 'Lance le sort d\'Éclair depuis un mur ou un objet';
    private const FLOOD = 'Une pièce fermée se remplit d\'eau ou d\'acide';
    private const DARTS_SHOOT = 'Tire des flechettes depuis un coffre ouvert';
    private const ANIMATED_OBJECT = 'Une arme, une armure, ou un tapis s\'anime et attaque';
    private const PENDULUM =
        'Un pendule, armé ou alourdi pour servir de bélier, se balance à travers la pièce ou le couloir';
    private const HIDDEN_PIT =
        'Un trou caché s\'ouvre sous le personnage (25% de chance qu\'une gélatine recouvre le fond)';
    private const HIDDEN_PIT_FLOOD = 'Un trou caché se remplit d\'eau ou de feu';
    private const LOCKED_PIT = 'Un trou caché se verrouille et se remplit d\'eau';
    private const BLADES = 'Des lames faucheuses émergent d\'un mur ou d\'un objet';
    private const SPEARS = 'Des lances (possiblement empoisonnées) sortent du sol';
    private const BRITTLE_STAIRS = 'Des escaliers fragiles se rompent au-dessus de pieux';
    private const THUNDERWAVE = 'Lance le sort Onde de choc qui poussent les personnages dans un trou ou des pieux';
    private const JAWS = 'Une mâchoire de pierre ou d\'acier retient un personnage';
    private const STONE_BLOCK = 'Une bloc de pierre s\'écrase au milieu du couloir';
    private const SYMBOL = 'Lance le sort Symbole';
    private const WALLS_SLIDE = 'Les murs se rapprochent';
    private const BEAR_TRAP = 'Un piège à loup se referme et empêche de bouger (DD FOR).';
}
