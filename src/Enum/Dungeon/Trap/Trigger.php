<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Trap;

use App\Model\AbstractEnum;

/**
 * Class Trigger.
 *
 * @method static Trigger STEPPED_ON()
 * @method static Trigger MOVED_THROUGH()
 * @method static Trigger TOUCHED()
 * @method static Trigger OPENED()
 * @method static Trigger LOOKED_AT()
 * @method static Trigger MOVED()
 */
class Trigger extends AbstractEnum
{
    private const STEPPED_ON = 'Marcher sur quelque chose';
    private const MOVED_THROUGH = 'Passer à travers quelque chose';
    private const TOUCHED = 'Toucher quelque chose';
    private const OPENED = 'Ouvrir quelque chose';
    private const LOOKED_AT = 'Regarder quelque chose';
    private const MOVED = 'Se Déplacer ou déplacer quelque chose';
}
