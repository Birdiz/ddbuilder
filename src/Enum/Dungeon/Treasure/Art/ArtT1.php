<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Art;

use App\Model\AbstractEnum;

class ArtT1 extends AbstractEnum
{
    private const EWER = 'Aiguière en argent';
    private const STATUETTE = 'Statuette en os gravée';
    private const BRACELET = 'Petit bracelet en or';
    private const VESTMENTS = 'Tissu de vêtements d\'or';
    private const MASK = 'Masque en velours noir cousu aux fils d\'argent';
    private const CHALICE = 'Calice de cuivre avec des filigranes d\'argent';
    private const DICE = 'Pair de dés en os gravés';
    private const MIRROR = 'Petit miroir au cadre peint';
    private const HANDKERCHIEF = 'Mouchoir brodé aux fils d\'argent';
    private const LOCKET = 'Médaillon en or avec un portait peint à l\'intérieur';
}
