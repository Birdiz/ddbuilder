<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Art;

use App\Model\AbstractEnum;

class ArtT2 extends AbstractEnum
{
    private const RING = 'Anneau d\'or incrusté de pierres de sang';
    private const STATUETTE = 'Statuette en ivoire gravée';
    private const BRACELET = 'Large bracelet en or';
    private const NECKLACE = 'Collier en argent avec un pendentif en pierre précieuse';
    private const CROWN = 'Couronne de bronze';
    private const ROBE = 'Robe en soie avec des broderies d\'or';
    private const TAPESTRY = 'Grande tapisserie de bonne faction';
    private const MUG = 'Gobelet en laiton incrusté de jade';
    private const BOX = 'Boîte contenant des figurines d\'animaux en turquoise';
    private const CAGE = 'Cage à oiseaux en or avec de l\'électrum en filigrane';
}
