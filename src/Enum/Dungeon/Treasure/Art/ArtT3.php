<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Art;

use App\Model\AbstractEnum;

class ArtT3 extends AbstractEnum
{
    private const CHALICE = 'Calice d\'argent décoré de pierres de Lune';
    private const LONGSWORD = 'Épée long à la lame d\'argent et à la poignée sertie de jais';
    private const HARP = 'Harpe courbée en bois exotique incrustée d\'ivoire et de gemmes de zircon';
    private const IDOL = 'Petite idole d\'or';
    private const COMB = 'Peigne d\'or en forme de dragon avec des grenats pour les yeux';
    private const BOTTLE_STOPPER = 'Bouchon de bouteille gaufré de feuille d\'or et serti d\'améthyste';
    private const DAGGER = 'Dague cérémoniale en électrum avec une perle noire dans le pommeau';
    private const BROOCH = 'Broche d\'or et d\'argent';
    private const STATUETTE = 'Statuette en obsidienne avec des raccords d\'or';
    private const MASK = 'Masque de guerre en or peint';
}
