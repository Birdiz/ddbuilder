<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Art;

use App\Model\AbstractEnum;

class ArtT4 extends AbstractEnum
{
    private const CHAIN = 'Chaîne en or de très bonne qualité incrustée d\'opale de feu';
    private const PAINTING = 'Vieille oeuvre de peinture';
    private const MANTLE = 'Manteau brodé de soie et de velours garni de nombreuses pierres de Lune';
    private const BRACELET = 'Bracelet de platine incrusté de saphire';
    private const GLOVE = 'Gants brodés garnis d\'éclats de joyaux';
    private const ANKLET = 'Bracelet de cheville orné de joyaux';
    private const BOX = 'Boîte à musique en or';
    private const CIRCLET = 'Cercle d\'or serti de quatre aquamarines';
    private const EYE_PATCH = 'Cache-oeil avec un faux oeil fait de saphire et de pierre de lune';
    private const NECKLACE = 'Collier chaîne en petites perles roses';
}
