<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Art;

use App\Model\AbstractEnum;

class ArtT5 extends AbstractEnum
{
    private const CROWN = 'Couronne d\'or garnie de joyaux';
    private const RING = 'Bague de platine incrustée de joyaux';
    private const STATUETTE = 'Petite statuette d\'or sertie de rubis';
    private const CUP = 'Coupe en or incrustée d\'émeuraudes';
    private const BOX = 'Boîte à bijoux en or avec du filigrane de platine';
    private const SARCOPHAGUS = 'Sarcophage d\'enfant en or peint';
    private const GAME_BOARD = 'Plateau de jeu en jade avec des pièces de jeu en or massif';
    private const HORN = 'Corne à boire en ivoire ornée de joyaux avec du filigrane d\'or';
}
