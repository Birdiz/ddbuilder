<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class Container extends AbstractEnum implements WeightedInterface
{
    private const ASH = 'Cendres';
    private const BARK = 'Écorce';
    private const ORGANS = 'Organes';
    private const BONES = 'Os';
    private const CINDERS = 'Charbon';
    private const CRYSTALS = 'Cristaux';
    private const DUST = 'Poussière';
    private const FIBERS = 'Fibres';
    private const GELATIN = 'Gélatine';
    private const GRAINS = 'Graines';
    private const GREASE = 'Graisse';
    private const HUSKS = 'Cosses';
    private const LEAVES = 'Feuilles';
    private const LIQUID_THIN = 'Liquide mince';
    private const LIQUID_VISCOUS = 'Liquide visqueux';
    private const LUMPS = 'Morceaux non-identifiables';
    private const OIL = 'Huile';
    private const PASTE = 'Pâte';
    private const PELLETS = 'Granulés';
    private const POWDER = 'Poudre';
    private const SEMILIQUID_SUSPENSION = 'Un semi-liquide en suspension';
    private const SKIN = 'Peau';
    private const SPHERES = 'Sphères (métal, pierre ou bois)';
    private const SPLINTERS = 'Copeaux';
    private const STALKS = 'Tiges';
    private const STRANDS = 'Brins';
    private const STRIPS = 'Galons';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [3, 3, 3, 5, 3, 5, 4, 2, 3, 4, 3, 3, 5, 8, 5, 2, 3, 4, 3, 13, 2, 2, 2, 2, 2, 3, 3];
    }
}
