<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT1 extends AbstractEnum
{
    private const AZURITE = 'Azurite';
    private const AGATE_BANDED = 'Agate à bandes';
    private const BLUE_QUARTZ = 'Quartz bleu';
    private const AGATE_EYE = 'Agate oeillée';
    private const HEMATITE = 'Hématite';
    private const LAPIS_LAZULI = 'Lapis lazuli';
    private const MALACHITE = 'Malachite';
    private const AGATE_MOSS = 'Agate mousse';
    private const OBSIDIAN = 'Obsidienne';
    private const RHODOCHROSITE = 'Rhodochrosite';
    private const TIGER_EYE = 'Oeil de tigre';
    private const TURQUOISE = 'Turquoise';
}
