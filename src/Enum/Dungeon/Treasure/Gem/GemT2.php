<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT2 extends AbstractEnum
{
    private const BLOODSTONE = 'Pierre de sang';
    private const CARNELIAN = 'Cornaline';
    private const CHALCEDONY = 'Calcédoine';
    private const CHRYSOPRASE = 'Chrysoprase';
    private const CITRINE = 'Citrine';
    private const JASPER = 'Jaspe';
    private const MOONSTONE = 'Pierre de Lune';
    private const ONYX = 'Onyx';
    private const QUARTZ = 'Quartz';
    private const SARDONYX = 'Sardonyx';
    private const STAR_ROSE_QUARTZ = 'Quartz rose étoile';
    private const ZIRCON = 'Zircon';
}
