<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT3 extends AbstractEnum
{
    private const AMBER = 'Ambre';
    private const AMETHYST = 'Améthyste';
    private const CHRYSOBERYL = 'Chrysobéryl';
    private const CORAL = 'Corail';
    private const GARNET = 'Grenat';
    private const JADE = 'Jade';
    private const JET = 'Jais';
    private const PEARL = 'Perle';
    private const SPINEL = 'Spinelle';
    private const TOURMALINE = 'Tourmaline';
}
