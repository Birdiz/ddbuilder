<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT4 extends AbstractEnum
{
    private const ALEXANDRINE = 'Alexandrine';
    private const AQUAMARINE = 'Aquamarine';
    private const BLACK_PEARL = 'Perle noire';
    private const BLUE_SPINEL = 'Spinelle bleue';
    private const PERIDOT = 'Péridot';
    private const TOPAZ = 'Topaz';
}
