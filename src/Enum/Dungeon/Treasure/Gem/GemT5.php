<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT5 extends AbstractEnum
{
    private const BLACK_OPAL = 'Opale noire';
    private const BLUE_SAPPHIRE = 'Saphire bleu';
    private const EMERALD = 'Émeuraude';
    private const FIRE_OPAL = 'Opale de feu';
    private const OPAL = 'Opale';
    private const STAR_RUBY = 'Rubis étoile';
    private const STAR_SAPPHIRE = 'Saphire étoile';
    private const YELLOW_SAPPHIRE = 'Saphire jaune';
}
