<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure\Gem;

use App\Model\AbstractEnum;

class GemT6 extends AbstractEnum
{
    private const BLACK_SAPPHIRE = 'Saphire noir';
    private const DIAMOND = 'Diamant';
    private const JACINTH = 'Jacinte';
    private const RUBY = 'Rubis';
}
