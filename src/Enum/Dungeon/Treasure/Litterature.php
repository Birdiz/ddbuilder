<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class Litterature extends AbstractEnum implements WeightedInterface
{
    private const ACCOUNT_RECORDS = 'Livre de comptes';
    private const ALCHMIST_NOTEBOOK = 'Notes d\'alchémiste';
    private const ALMANAC = 'Almanac';
    private const BESTIARY = 'Bestiaire';
    private const BIOGRAPHY = 'Biographie';
    private const BOOK_HERALDRY = 'Livre de hérault';
    private const BOOK_MYTHS = 'Livre de mythes';
    private const BOOK_FLOWERS = 'Herbier';
    private const CALENDAR = 'Calendrier';
    private const CATALOG = 'Catalogue';
    private const CONTRACT = 'Contrat';
    private const DIARY = 'Journal';
    private const DICTIONNARY = 'Dictionnaire';
    private const SKETCHES = 'Croquis';
    private const FORGED_DOCUMENTS = 'Documents falsifiés';
    private const GRAMMAR_WORKBOOK = 'Livre d\'apprentissage de grammaire';
    private const HERETICAL_TEXT = 'Texte hérétique';
    private const HISTORICAL_TEXT = 'Texte historique';
    private const TESTAMENT = 'Dernier souhait et testament';
    private const LEGAL_CODE = 'Code de loi';
    private const LETTER = 'Lettre';
    private const LUNATIC_RAVING = 'Délires fous';
    private const MAGIC_TRICKS = 'Tours de magie (pas un livre de sorts)';
    private const MAGIC_SCROLL = 'Parchemin magique';
    private const MAP = 'Carte ou atlas';
    private const MEMOIR = 'Mémoire';
    private const NAVIGATIONAL_CHART = 'Carte de navigation ou de constellation';
    private const NOVEL = 'Nouvelle';
    private const PAINTING = 'Peinture';
    private const POETRY = 'Poésie';
    private const PRAYER_BOOK = 'Livre de prières';
    private const PROPERTY_DEED = 'Acte de propriété';
    private const RECIPE_BOOK = 'Livre de cuisine';
    private const RECORD_TRIAL = 'Notes d\'un procès criminel';
    private const ROYAL_PROCLAMATION = 'Proclamation royale';
    private const SHEET_MUSIC = 'Feuille de partition';
    private const SPELL_BOOK = 'Livre de sorts';
    private const TEXT_ARMOR = 'Texte sur la fabrication d\'armures';
    private const TEXT_ASTROLOGY = 'Texte sur l\'astrologie';
    private const TEXT_BREWING = 'Texte sur le brassage';
    private const TEXT_EXOTIC = 'Texte sur une flore ou une faune exotique';
    private const TEXT_HERBALISM = 'Texte sur l\'herborisme';
    private const TEXT_FLORA = 'Texte sur la flore locale';
    private const TEXT_MATH = 'Texte sur les mathématiques';
    private const TEXT_MASONRY = 'Texte sur la maçonnerie';
    private const TEXT_MEDICINE = 'Texte sur la médecine';
    private const THEOLOGICAL_TEXT = 'Texte théologique';
    private const TOME_FORBIDDEN_LORE = 'Tome de traditions interdites';
    private const TRAVELLOG_EXOTIC = 'Journal d\'expéditions dans un pays exotique';
    private const TRAVELLOG_PLAN = 'Journal d\'expéditions dans les plans';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [2, 2, 2, 2, 3, 3, 1, 1, 1, 5, 2, 3, 2, 3, 1, 1, 2, 5, 2, 2, 8, 1, 1, 1, 3, 1, 2, 2, 1, 2, 2, 1, 4, 1,
            1, 2, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 3, 1, ];
    }
}
