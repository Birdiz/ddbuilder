<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableA extends AbstractEnum implements WeightedInterface
{
    private const HEALING_POTION = [
        'name' => 'Potion de soins',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const SPELL_SCROLL_CANTRIP = [
        'name' => 'Parchemin de sort (Sort mineur)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const CLIMBING_POTION = [
        'name' => 'Potion d\'escalade',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-escalade',
    ];

    private const SPELL_SCROLL_1 = [
        'name' => 'Parchemin de sort (Sort niv.1)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const SPELL_SCROLL_2 = [
        'name' => 'Parchemin de sort (Sort niv.2)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const GREATER_HEALING_POTION = [
        'name' => 'Potion de soins majeurs',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const SAC_SANS_FOND = [
        'name' => 'Sac sans fond',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-sans-fond',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-holding.jpg',
    ];

    private const FLOATING_GLOBE = [
        'name' => 'Globe flottant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=globe-flottant',
        'image' => 'https://www.aidedd.org/dnd/images-om/driftglobe.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [50, 10, 10, 20, 4, 4, 1, 1];
    }
}
