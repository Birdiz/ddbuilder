<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableB extends AbstractEnum implements WeightedInterface
{
    private const GREATER_HEALING_POTION = [
        'name' => 'Potion de soins majeurs',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const FIRE_BREATHING_POTION = [
        'name' => 'Potion de souffle enflammé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-souffle-enflamme',
    ];

    private const RESISTANCE_POTION = [
        'name' => 'Potion de résistance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-resistance',
    ];

    private const AMMUNITION_1 = [
        'name' => 'Munition +1',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=munition-1-2-ou-3',
    ];

    private const ANIMAL_FRIENDSHIP_POTION = [
        'name' => 'Potion d\'amitié avec les animaux',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-amitie-avec-les-animaux',
    ];

    private const GIANT_STRENGH_POTION = [
        'name' => 'Potion de force de géant des collines',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];

    private const ENLARGEMENT_POTION = [
        'name' => 'Potion d\'agrandissement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-agrandissement',
    ];

    private const AQUATIC_BREATHING_POTION = [
        'name' => 'Potion de respiration aquatique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-respiration-aquatique',
    ];

    private const SPELL_SCROLL_2 = [
        'name' => 'Parchemin de sort (Sort niv.2)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const SPELL_SCROLL_3 = [
        'name' => 'Parchemin de sort (Sort niv.3)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const SAC_SANS_FOND = [
        'name' => 'Sac sans fond',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-sans-fond',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-holding.jpg',
    ];

    private const KEOGHTOM_BALM = [
        'name' => 'Baume de Keoghtom',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baume-de-keoghtom',
        'image' => 'https://www.aidedd.org/dnd/images-om/keoghtom-s-ointment.jpg',
    ];

    private const HUILE_INSAISISSABILITE = [
        'name' => 'Huile d\'insaisissabilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=huile-d-insaisissabilite',
    ];

    private const DISPARITION_DUST = [
        'name' => 'Poussière de disparition',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=poussiere-de-disparition',
        'image' => 'https://www.aidedd.org/dnd/images-om/dust-of-disappearance.jpg',
    ];

    private const POUSSIERE_ASSECHEMENT = [
        'name' => 'Poussière d\'assechement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=poussiere-dessiccative',
    ];

    private const POUDRE_ETERNUER = [
        'name' => 'Poudre à éternuer',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=poudre-a-eternuer',
        'image' => 'https://www.aidedd.org/dnd/images-om/dust-of-sneezing-and-choking.jpg',
    ];

    private const ELEMENTARY_GEM = [
        'name' => 'Gemme élémentaire',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gemme-elementaire',
        'image' => 'https://www.aidedd.org/dnd/images-om/elemental-gem.jpg',
    ];

    private const LOVE_PHILTER = [
        'name' => 'Philtre d\'amour',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=philtre-d-amour',
    ];

    private const CRUCHE_ALCHIMIQUE = [
        'name' => 'Cruche alchimique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cruche-alchimique',
        'image' => 'https://www.aidedd.org/dnd/images-om/alchemy-jug.jpg',
    ];

    private const AQUATIC_BREATHING_HOODED = [
        'name' => 'Capuchon de respiration aquatique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=capuchon-de-respiration-aquatique',
    ];

    private const MANTA_RAY_CAPE = [
        'name' => 'Cape de la raie manta',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-la-raie-manta',
        'image' => 'https://www.aidedd.org/dnd/images-om/cloak-of-manta-ray.jpg',
    ];

    private const FLOATING_GLOBE = [
        'name' => 'Globe flottant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=globe-flottant',
        'image' => 'https://www.aidedd.org/dnd/images-om/driftglobe.jpg',
    ];

    private const NIGHT_GOOGLE = [
        'name' => 'Lunettes de nuit',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=lunettes-de-nuit',
        'image' => 'https://www.aidedd.org/dnd/images-om/goggles-of-night.jpg',
    ];

    private const HEAUME_COMPREHENSION_LANGUES = [
        'name' => 'Heaume de compréhension des langues',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=heaume-de-comprehension-des-langues',
        'image' => 'https://www.aidedd.org/dnd/images-om/helm-of-comprehending-languages.jpg',
    ];

    private const SCEPTRE_INAMOVIBLE = [
        'name' => 'Sceptre inamovible',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-inamovible',
        'image' => 'https://www.aidedd.org/dnd/images-om/immovable-rod.jpg',
    ];

    private const REVELATION_LANTERN = [
        'name' => 'Lanterne de révelation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=lanterne-de-revelation',
        'image' => 'https://www.aidedd.org/dnd/images-om/lantern-of-revealing.jpg',
    ];

    private const SAILOR_ARMOR = [
        'name' => 'Armure de marin',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-matelot',
    ];

    private const MITHRIL_ARMOR = [
        'name' => 'Armure de mithral',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-mithral',
    ];

    private const POISON_POTION = [
        'name' => 'Potion de poison',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-poison',
    ];

    private const SWIMMING_RING = [
        'name' => 'Anneau de nage',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-nage',
    ];

    private const ROBE_CAMELOT = [
        'name' => 'Robe de camelot',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=robe-de-camelot',
        'image' => 'https://www.aidedd.org/dnd/images-om/robe-of-useful-items.jpg',
    ];

    private const BOULDERING_ROPE = [
        'name' => 'Corde d\'escalade',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=corde-d-escalade',
    ];

    private const RIDER_SADDLE = [
        'name' => 'Selle du cavalier',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=selle-du-cavalier',
    ];

    private const MAGIC_DETECTION_WAND = [
        'name' => 'Baguette de détection de la magie',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-detection-de-la-magie',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-magic-detection.jpg',
    ];

    private const BAGUETTE_DES_SECRETS = [
        'name' => 'Baguette des secrets',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-des-secrets',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-secrets.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 7, 7, 5, 5, 5, 5, 5, 5, 5, 3, 3, 3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, ];
    }
}
