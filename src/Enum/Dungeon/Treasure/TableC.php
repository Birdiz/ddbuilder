<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableC extends AbstractEnum implements WeightedInterface
{
    private const SUPERIOR_HEALING_POTION = [
        'name' => 'Potion de soins supérieurs',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const SPELL_SCROLL_4 = [
        'name' => 'Parchemin de sort (Sort niv.4)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const AMMUNITION_2 = [
        'name' => 'Munition +2',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=munition-1-2-ou-3',
    ];

    private const POTION_CLAIRVOYANCE = [
        'name' => 'Potion de clairvoyance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-clairvoyance',
    ];

    private const POTION_DIMINUTION = [
        'name' => 'Potion de rétrécissement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-diminution',
    ];

    private const POTION_GASEOUS_FORM = [
        'name' => 'Potion de forme gazeuse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-forme-gazeuse',
    ];

    private const POTION_FROST_GIANT_FORCE = [
        'name' => 'Potion de force de géant du givre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];

    private const POTION_STONES_GIANT_FORCE = [
        'name' => 'Potion de force de géant des pierres',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];

    private const POTION_HEROISM = [
        'name' => 'Potion d\'héroïsme',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-heroisme',
    ];

    private const POTION_INVULNERABILITY = [
        'name' => 'Potion d\'invulnérabilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-invulnerabilite',
    ];

    private const POTION_MIND_READING = [
        'name' => 'Potion de lecture des pensées',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-lecture-des-pensees',
    ];

    private const SPELL_SCROLL_5 = [
        'name' => 'Parchemin de sort (Sort niv.5)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const ELIXIR_HEALTH = [
        'name' => 'Élixir de santé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=elixir-de-sante',
    ];

    private const OIL_ETHEREALNESS = [
        'name' => 'Huile éthérée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=huile-etheree',
    ];

    private const POTION_FIRE_GIANT_FORCE = [
        'name' => 'Potion de force de géant du feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];

    private const QUAAL_FEATHER_TOKEN = [
        'name' => 'Plume magique de Quaal',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=plume-de-quaal',
    ];

    private const SCROLL_PROTECTION = [
        'name' => 'Parchemin de protection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-protection',
    ];

    private const BAG_BEANS = [
        'name' => 'Sac de haricots',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-de-haricots',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-beans.jpg',
    ];

    private const BEAD_FORCE = [
        'name' => 'Perle de force',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=perle-de-force',
    ];

    private const CHIME_OPENING = [
        'name' => 'Carillon d\'ouverture',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=carillon-d-ouverture',
        'image' => 'https://www.aidedd.org/dnd/images-om/chime-of-opening.jpg',
    ];

    private const CARAFE_INTARISSABLE = [
        'name' => 'Carafe intarissable',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=carafe-intarissable',
        'image' => 'https://www.aidedd.org/dnd/images-om/decanter-of-endless-water.jpg',
    ];

    private const MAGNIFYING_EYES = [
        'name' => 'Yeux grossissants',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=yeux-grossissants',
    ];

    private const BOAT_FOLDING = [
        'name' => 'Bateau pliable',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bateau-pliable',
        'image' => 'https://www.aidedd.org/dnd/images-om/folding-boat.jpg',
    ];

    private const HEWARD_HANDY_HAVERSACK = [
        'name' => 'Havresac magique d\'Hévard',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=havresac-magique-d-hevard',
        'image' => 'https://www.aidedd.org/dnd/images-om/handy-haversack.jpg',
    ];

    private const HORSESHOES_SPEED = [
        'name' => 'Fers à cheval de rapidité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=fers-de-rapidite',
    ];

    private const NECKLACE_FIREBALLS = [
        'name' => 'Collier de boules de feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=collier-de-boules-de-feu',
        'image' => 'https://www.aidedd.org/dnd/images-om/necklace-of-fireballs.jpg',
    ];

    private const GOOD_HEALTH_AMULET = [
        'name' => 'Amulette de bonne santé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-de-bonne-sante',
        'image' => 'https://www.aidedd.org/dnd/images-om/periapt-of-health.jpg',
    ];

    private const PIERRE_COMMUNICATION_DISTANCE = [
        'name' => 'Pierres de communication à distance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierres-de-communication-a-distance',
        'image' => 'https://www.aidedd.org/dnd/images-om/sending-stones.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 7, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 3, 3, 3, 3, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    }
}
