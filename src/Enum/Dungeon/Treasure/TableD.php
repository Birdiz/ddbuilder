<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableD extends AbstractEnum implements WeightedInterface
{
    private const SUPREME_HEALING_POTION = [
        'name' => 'Potion de soins suprêmes',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const POTION_INVISIBILITY = [
        'name' => 'Potion d\'invisibilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-d-invisibilite',
    ];

    private const POTION_SPEED = [
        'name' => 'Potion de vitesse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-vitesse',
    ];

    private const SPELL_SCROLL_6 = [
        'name' => 'Parchemin de sort (Sort niv.6)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const SPELL_SCROLL_7 = [
        'name' => 'Parchemin de sort (Sort niv.7)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const AMMUNITION_3 = [
        'name' => 'Munition +3',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=munition-1-2-ou-3',
    ];

    private const OIL_SHARPNESS = [
        'name' => 'Huile d\'affûtage',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=huile-d-affutage',
    ];

    private const POTION_FLYING = [
        'name' => 'Potion de vol',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-vol',
    ];

    private const POTION_CLOUD_GIANT_FORCE = [
        'name' => 'Potion de force de géant des nuages',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];

    private const POTION_LONGEVITY = [
        'name' => 'Potion de longevité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=elixir-de-jouvence',
    ];

    private const POTION_VITALITY = [
        'name' => 'Potion de vitalité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-vitalite',
    ];

    private const SPELL_SCROLL_8 = [
        'name' => 'Parchemin de sort (Sort niv.8)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const HORSESHOES_ZEPHYR = [
        'name' => 'Fers à cheval de zéphyr',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=fers-de-zephyr',
    ];

    private const NOLZUR_MARVELOUS_PIGMENTS = [
        'name' => 'Pigments merveilleux de Nolzur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pigments-merveilleux-de-nolzur',
        'image' => 'https://www.aidedd.org/dnd/images-om/marvelous-pigments.jpg',
    ];

    private const BAG_DEVOURING = [
        'name' => 'Sac dévoreur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-devoreur',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-devouring.jpg',
    ];

    private const PORTABLE_HOLE = [
        'name' => 'Puits portatif',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=puits-portatif',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [20, 10, 10, 10, 7, 5, 5, 5, 5, 5, 5, 5, 3, 3, 1, 1];
    }
}
