<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableE extends AbstractEnum implements WeightedInterface
{
    private const SPELL_SCROLL_8 = [
        'name' => 'Parchemin de sort (Sort niv.8)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const POTION_STROM_GIANT_FORCE = [
        'name' => 'Potion de force de géant des tempêtes',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-force-de-geant',
    ];
    private const SUPREME_HEALING_POTION = [
        'name' => 'Potion de soins suprêmes',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=potion-de-guérison',
    ];

    private const SPELL_SCROLL_9 = [
        'name' => 'Parchemin de sort (Sort niv.9)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=parchemin-de-sort',
    ];

    private const UNIVERSAL_SOLVENT = [
        'name' => 'Solvant universel',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=solvant-universel',
    ];

    private const ARROW_SLAYING = [
        'name' => 'Flèche tueuse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=fleche-tueuse',
    ];

    private const SOVEREIGN_GLUE = [
        'name' => 'Colle universelle',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=colle-universelle',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [30, 25, 25, 5, 8, 5, 2];
    }
}
