<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableF extends AbstractEnum implements WeightedInterface
{
    private const WEAPON_1 = [
        'name' => 'Arme +1',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arme-1-2-ou-3',
    ];

    private const SHIELD_1 = [
        'name' => 'Bouclier +1',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-1-2-ou-3',
    ];

    private const SENTINEL_SHIELD = [
        'name' => 'Bouclier de la sentinelle',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-sentinelle',
    ];

    private const ANTI_DETECTION_AMULET = [
        'name' => 'Amulette d\'anti-détection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-d-anti-detection',
        'image' => 'https://www.aidedd.org/dnd/images-om/amulet-of-proof-against-detection.jpg',
    ];

    private const ELVEN_BOOTS = [
        'name' => 'Bottes elfiques',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-elfiques',
        'image' => 'https://www.aidedd.org/dnd/images-om/boots-of-elvenkind.jpg',
    ];

    private const WALKING_AND_JUMPING_BOOTS = [
        'name' => 'Bottes de sept lieues',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-de-sept-lieues',
        'image' => 'https://www.aidedd.org/dnd/images-om/boots-of-striding-and-springing.jpg',
    ];

    private const MARKSMAN_WRISTBAND = [
        'name' => 'Bracelets d\'archer',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bracelets-d-archer',
        'image' => 'https://www.aidedd.org/dnd/images-om/bracers-of-archery.jpg',
    ];

    private const PROTECTION_PIN = [
        'name' => 'Broche de protection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=broche-de-defense',
        'image' => 'https://www.aidedd.org/dnd/images-om/brooch-of-shielding.jpg',
    ];

    private const FLYING_BROOM = [
        'name' => 'Balai volant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=balai-volant',
        'image' => 'https://www.aidedd.org/dnd/images-om/broom-of-flying.jpg',
    ];

    private const ELVEN_CAPE = [
        'name' => 'Cape elfique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-elfique',
        'image' => 'https://www.aidedd.org/dnd/images-om/cloak-of-elvenkind.jpg',
    ];

    private const PROTECTION_CAPE = [
        'name' => 'Cape de protection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-protection',
        'image' => 'https://www.aidedd.org/dnd/images-om/cloak-of-protection.jpg',
    ];

    private const OGRE_POWER_GAUNTLET = [
        'name' => 'Gantelets de puissance d\'ogre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gantelets-de-puissance-d-ogre',
        'image' => 'https://www.aidedd.org/dnd/images-om/gauntlets-of-ogre-power.jpg',
    ];

    private const CHAPEAU_DE_DEGUISEMENT = [
        'name' => 'Chapeau de déguisement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=chapeau-de-deguisement',
    ];

    private const THUNDER_JAVELIN = [
        'name' => 'Javeline de foudre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=javeline-de-foudre',
        'image' => 'https://www.aidedd.org/dnd/images-om/javelin-of-lightning.jpg',
    ];

    private const POWER_PEARL = [
        'name' => 'Perle de pouvoir',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=perle-de-pouvoir',
    ];

    private const PACT_GUARDIAN_SCEPTER_1 = [
        'name' => 'Sceptre du gardien des pactes +1',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-du-gardien-des-pactes',
    ];

    private const SPIDER_LEGS_SLIPPERS = [
        'name' => 'Chaussons de pattes d\'araignée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=chaussons-de-pattes-d-araignee',
        'image' => 'https://www.aidedd.org/dnd/images-om/slippers-of-spider-climbing.jpg',
    ];

    private const ADDER_STAFF = [
        'name' => 'Baton de la vipère',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-la-vipere',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-the-adder.jpg',
    ];

    private const PYTHON_STAFF = [
        'name' => 'Baton du python',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-du-python',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-the-python.jpg',
    ];

    private const VENGEFUL_SWORD = [
        'name' => 'Epée vengeresse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-vengeresse',
    ];

    private const FISH_DOMINATION_TRIDENT = [
        'name' => 'Trident de domination aquatique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=trident-de-domination-aquatique',
        'image' => 'https://www.aidedd.org/dnd/images-om/trident-of-fish-command.jpg',
    ];

    private const MAGIC_TRAITS_WAND = [
        'name' => 'Baguette de projectiles magiques',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-projectiles-magiques',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-magic-missiles.jpg',
    ];

    private const BAGUETTE_DU_MAGE_DE_GUERRE_1 = [
        'name' => 'Baguette du mage de guerre +1',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-du-mage-de-guerre-1-2-ou-3',
    ];

    private const SPIDERWEB_WAND = [
        'name' => 'Baguette de toile d\'araignée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-toile-d-araignee',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-web.jpg',
    ];

    private const VIGILANTE_WEAPON = [
        'name' => 'Arme vigilante',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arme-vigilante',
    ];

    private const ADAMANTIUM_ARMOR_MAIL = [
        'name' => 'Armure en adamantium (cotte de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const ADAMANTIUM_ARMOR_SHIRT = [
        'name' => 'Armure en adamantium (chemise de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const ADAMANTIUM_ARMOR_SCALE = [
        'name' => 'Armure en adamantium (écailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const SAC_MALICES_GRAY = [
        'name' => 'Sac à malices (gris)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-a-malices',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-tricks.jpg',
    ];

    private const SAC_MALICES_RUST = [
        'name' => 'Sac à malices (rouille)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-a-malices',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-tricks.jpg',
    ];

    private const SAC_MALICES_TAN = [
        'name' => 'Sac à malices (ocre)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sac-a-malices',
        'image' => 'https://www.aidedd.org/dnd/images-om/bag-of-tricks.jpg',
    ];

    private const BOTTES_DES_CONTREES_HIVERNALES = [
        'name' => 'Bottes des contrées hivernales',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-des-terres-gelees',
        'image' => 'https://www.aidedd.org/dnd/images-om/boots-of-winterlands.jpg',
    ];

    private const DESTRUCTION_TIARA = [
        'name' => 'Diadème de destruction',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=diademe-de-destruction',
        'image' => 'https://www.aidedd.org/dnd/images-om/circlet-of-blasting.jpg',
    ];

    private const ILLUSION_CARDS = [
        'name' => 'Cartes d\'illusion',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cartes-d-illusion',
    ];

    private const SMOKE_BOTTLE = [
        'name' => 'Bouteille fumigène',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouteille-fumigene',
        'image' => 'https://www.aidedd.org/dnd/images-om/eversmoking-bottle.jpg',
    ];

    private const CHARMING_EYES = [
        'name' => 'Yeux de charme',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=regard-charmeur',
        'image' => 'https://www.aidedd.org/dnd/images-om/eyes-of-charming.jpg',
    ];

    private const LYNX_EYES = [
        'name' => 'Yeux de lynx',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=yeux-de-lynx',
        'image' => 'https://www.aidedd.org/dnd/images-om/eyes-of-the-eagle.jpg',
    ];

    private const MARVELOUS_FIGURINE = [
        'name' => 'Figurine merveilleuse (corbeau d\'argent)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=figurine-merveilleuse',
    ];

    private const ILLUMINATION_GEM = [
        'name' => 'Gemme d\'illumination',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gemme-d-illumination',
        'image' => 'https://www.aidedd.org/dnd/images-om/gem-of-brightness.jpg',
    ];

    private const ANTI_PROJECTILE_GAUNTLET = [
        'name' => 'Gants anti-projectiles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gants-piegeurs-de-projectiles',
        'image' => 'https://www.aidedd.org/dnd/images-om/glove-of-missile-snaring.jpg',
    ];

    private const SWIMMING_BOULDERING_GAUNTLET = [
        'name' => 'Gants de nage et d\'escalade',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gants-de-nage-et-d-escalade',
    ];

    private const BURGLAR_GLOVES = [
        'name' => 'Gants de chapardeur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gants-de-chapardeur',
    ];

    private const INTELLIGENCE_HEADBAND = [
        'name' => 'Bandeau d\'intelligence',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bandeau-d-intelligence',
        'image' => 'https://www.aidedd.org/dnd/images-om/headband-of-intellect.jpg',
    ];

    private const TELEPATHIE_HELMET = [
        'name' => 'Heaume de télépathie',
            'link' => 'https://www.aidedd.org/dnd/om.php?vf=heaume-de-telepathie',
        'image' => 'https://www.aidedd.org/dnd/images-om/helm-of-telepathy.jpg',
    ];

    private const BARD_INSTRUMENTS_LUTE = [
        'name' => 'Instruments de barde (Luth de Doss)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const BARD_INSTRUMENTS_BANDORE = [
        'name' => 'Instruments de barde (Bandore de Fochluchan)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const BARD_INSTRUMENTS_CITTERN = [
        'name' => 'Instruments de barde (Cistre de Mac-Fuirmidh)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const THINKING_MEDAL = [
        'name' => 'Médaillon des pensées',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=medaillon-des-pensees',
        'image' => 'https://www.aidedd.org/dnd/images-om/medallion-of-thoughts.jpg',
    ];

    private const ADAPTATION_NECKLACE = [
        'name' => 'Collier d\'adaptation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=collier-d-adaptation',
        'image' => 'https://www.aidedd.org/dnd/images-om/necklace-of-adaptation.jpg',
    ];

    private const CICATRISATION_AMULET = [
        'name' => 'Amulette de cicatrisation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-de-cicatrisation',
        'image' => 'https://www.aidedd.org/dnd/images-om/periapt-of-wound-closure.jpg',
    ];

    private const TERRIFYING_FLUTE = [
        'name' => 'Flûte terrifiante',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=flute-terrifiante',
        'image' => 'https://www.aidedd.org/dnd/images-om/pipes-of-haunting.jpg',
    ];

    private const SEWERS_FLUTE = [
        'name' => 'Flûte des égouts',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=flute-des-egouts',
        'image' => 'https://www.aidedd.org/dnd/images-om/pipes-of-the-sewers.jpg',
    ];

    private const JUMPING_RING = [
        'name' => 'Anneau de saut',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-saut',
    ];

    private const MENTAL_PROTECTION_RING = [
        'name' => 'Anneau de protection mentale',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-barriere-mentale',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-mind-shielding.jpg',
    ];

    private const WARM_RING = [
        'name' => 'Anneau de chaleur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-chaleur-constante',
    ];

    private const WATER_WALKING_RIN = [
        'name' => 'Anneau de marche sur l\'eau',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-marche-sur-l-eau',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-water-walking.jpg',
    ];

    private const EHLONNA_QUIVER = [
        'name' => 'Carquois d\'Ehlonna',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=carquois-d-ehlonna',
        'image' => 'https://www.aidedd.org/dnd/images-om/quiver-of-ehlonna.jpg',
    ];

    private const LUCKY_STONE = [
        'name' => 'Pierre porte-bonheur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-porte-bonheur',
        'image' => 'https://www.aidedd.org/dnd/images-om/stone-of-good-luck.jpg',
    ];

    private const ENCHANTED_FAN = [
        'name' => 'Eventail enchanté',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=eventail-enchante',
        'image' => 'https://www.aidedd.org/dnd/images-om/wind-fan.jpg',
    ];

    private const WINGED_BOOTS = [
        'name' => 'Bottes ailées',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-ailees',
        'image' => 'https://www.aidedd.org/dnd/images-om/winged-boots.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ];
    }
}
