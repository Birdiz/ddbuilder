<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableG extends AbstractEnum implements WeightedInterface
{
    private const WEAPON_2 = [
        'name' => 'Arme +2',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arme-1-2-ou-3',
    ];

    private const MARVELOUS_FIGURINE = [
        'name' => 'Figurine merveilleuse (lancer 1d8)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=figurine-merveilleuse',
        'special' => [
            '1 - Griffon de bronze',
            '2 - Mouche d\'ébène',
            '3 - Lions d\'or',
            '4 - Boucs d\'ivoire',
            '5 - Éléphant de marbre',
            '6-7 - Chien d\'onyx',
            '8 - Chouette en serpentine',
        ],
    ];

    private const ADAMANTIUM_ARMOR_BREAST = [
        'name' => 'Armure en adamantium (cuirasse)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const ADAMANTIUM_ARMOR_SPLINT = [
        'name' => 'Armure en adamantium (clibanion)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const AMULET_HEALTH = [
        'name' => 'Amulette de santé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-de-sante',
        'image' => 'https://www.aidedd.org/dnd/images-om/amulet-of-health.jpg',
    ];

    private const ARMOR_VULNERABILITY = [
        'name' => 'Armure de vulnérabilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-vulnerabilite',
    ];

    private const SHIELD_ARROWCATCHING = [
        'name' => 'Bouclier antiprojectiles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-antiprojectiles',
    ];

    private const BELT_DWARVENKIND = [
        'name' => 'Ceinturon des nains',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-des-nains',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-dwarvenkind.jpg',
    ];

    private const BELT_GIANT_STRENGTH = [
        'name' => 'Ceinturon de force de géant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-de-force-de-geant',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-stone-giant-strength.jpg',
    ];

    private const BERSERKER_AXE = [
        'name' => 'Hache du berserker',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=hache-du-berserker',
        'image' => 'https://www.aidedd.org/dnd/images-om/berserker-axe.jpg',
    ];

    private const BOOTS_LEVITATION = [
        'name' => 'Bottes de lévitation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-de-levitation',
    ];

    private const BOOTS_SPEED = [
        'name' => 'Bottes de rapidité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bottes-de-rapidite',
        'image' => 'https://www.aidedd.org/dnd/images-om/boots-of-speed.jpg',
    ];

    private const BOWL_COMMANDING_WATER_ELEMENTALS = [
        'name' => 'Jatte de contrôle des élémentaires de l\'eau',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=jatte-de-controle-des-elementaires-de-l-eau',
        'image' => 'https://www.aidedd.org/dnd/images-om/bowl-of-commanding-water-elementals.jpg',
    ];

    private const BRACERS_DEFENSE = [
        'name' => 'Bracelets de défense',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bracelets-de-defense',
        'image' => 'https://www.aidedd.org/dnd/images-om/bracers-of-defense.jpg',
    ];

    private const BRAZIER_COMMANDING_FIRE_ELEMENTALS = [
        'name' => 'Brasero de contrôle des élémentaires du feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=brasero-de-controle-des-elementaires-du-feu',
        'image' => 'https://www.aidedd.org/dnd/images-om/brazier-of-commanding-fire-elementals.jpg',
    ];

    private const CLOAK_MOUNTEBANK = [
        'name' => 'Cape du prestidigitateur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-du-prestidigitateur',
        'image' => 'https://www.aidedd.org/dnd/images-om/cape-of-mountebank.jpg',
    ];

    private const CENSER_CONTROLLING_AIR_ELEMENTALS = [
        'name' => 'Encensoir de contrôle des élémentaires de l\'air',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=encensoir-de-controle-des-elementaires-de-l-air',
        'image' => 'https://www.aidedd.org/dnd/images-om/censer-of-controlling-air-elementals.jpg',
    ];

    private const ARMOR_MAIL_1 = [
        'name' => 'Armure +1 (cotte de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_MAIL = [
        'name' => 'Armure de résistance (cotte de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const ARMOR_SHIRT_1 = [
        'name' => 'Armure +1 (chemise de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_SHIRT = [
        'name' => 'Armure de résistance (chemise de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const CLOAK_DISPLACEMENT = [
        'name' => 'Cape de déplacement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-deplacement',
        'image' => 'https://www.aidedd.org/dnd/images-om/cloak-of-displacement.jpg',
    ];

    private const CLOAK_BAT = [
        'name' => 'Cape de la chauve-souris',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-la-chauve-souris',
    ];

    private const CUBE_FORCE = [
        'name' => 'Cube de force',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cube-de-force',
        'image' => 'https://www.aidedd.org/dnd/images-om/cube-of-force.jpg',
    ];

    private const DAERN_INSTANT_FORTRESS = [
        'name' => 'Forteresse instantanée de Daern',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=forteresse-instantanee-de-daern',
        'image' => 'https://www.aidedd.org/dnd/images-om/instant-fortress.jpg',
    ];

    private const DAGGER_VENOM = [
        'name' => 'Dague venimeuse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=dague-venimeuse',
        'image' => 'https://www.aidedd.org/dnd/images-om/dagger-of-venom.jpg',
    ];

    private const DIMENSIONAL_SHACKLES = [
        'name' => 'Menottes dimensionnelles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=menottes-dimensionnelles',
        'image' => 'https://www.aidedd.org/dnd/images-om/dimensional-shackles.jpg',
    ];

    private const DRAGON_SLAYER = [
        'name' => 'Tueuse de dragons',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=tueuse-de-dragons',
        'image' => 'https://www.aidedd.org/dnd/images-om/dragon-slayer.jpg',
    ];

    private const ELVEN_CHAIN = [
        'name' => 'Mailles elfiques',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=mailles-elfiques',
        'image' => 'https://www.aidedd.org/dnd/images-om/elven-chain.jpg',
    ];

    private const FLAME_TONGUE = [
        'name' => 'Épée ardente',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-ardente',
        'image' => 'https://www.aidedd.org/dnd/images-om/flame-tongue.jpg',
    ];

    private const GEM_VISION = [
        'name' => 'Gemme de vision',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gemme-de-vision',
        'image' => 'https://www.aidedd.org/dnd/images-om/gem-of-seeing.jpg',
    ];

    private const GIANT_SLAYER = [
        'name' => 'Tueuse de géants',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=tueuse-de-geants',
        'image' => 'https://www.aidedd.org/dnd/images-om/giant-slayer.jpg',
    ];

    private const GLAMOURED_STUDDED_LEATHER = [
        'name' => 'Armure de cuir clouté enchantée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-cuir-cloute-enchantee',
    ];

    private const HELM_TELEPORTATION = [
        'name' => 'Heaume de téléportation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=heaume-de-teleportation',
        'image' => 'https://www.aidedd.org/dnd/images-om/helm-of-teleportation.jpg',
    ];

    private const HORN_BLASTING = [
        'name' => 'Cor de devastation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cor-de-devastation',
        'image' => 'https://www.aidedd.org/dnd/images-om/horn-of-blasting.jpg',
    ];

    private const HORN_VALHALLA = [
        'name' => 'Cor du valhalla (argent ou airain)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cor-du-valhalla',
        'image' => 'https://www.aidedd.org/dnd/images-om/horn-of-valhalla.jpg',
    ];

    private const BARD_INSTRUMENTS_MANDOLIN = [
        'name' => 'Instruments de barde (Mandoline de Canaith)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const BARD_INSTRUMENTS_LYRE = [
        'name' => 'Instruments de barde (Lyre de Cli)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const STONE_IOUN_AWARENESS = [
        'name' => 'Pierre de Ioun (vigilance)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_PROTECTION = [
        'name' => 'Pierre de Ioun (protection)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_RESERVE = [
        'name' => 'Pierre de Ioun (réserve)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_SUSTENANCE = [
        'name' => 'Pierre de Ioun (nourriture)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const ARMOR_LEATHER_1 = [
        'name' => 'Armure +1 (cuir)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_LEATHER = [
        'name' => 'Armure de résistance (cuir)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const MACE_DISRUPTION = [
        'name' => 'Masse d\'anéantissement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=masse-d-aneantissement',
        'image' => 'https://www.aidedd.org/dnd/images-om/mace-of-disruption.jpg',
    ];

    private const MACE_SMITING = [
        'name' => 'Masse destructrice',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=masse-destructrice',
        'image' => 'https://www.aidedd.org/dnd/images-om/mace-of-smiting.jpg',
    ];

    private const MACE_TERROR = [
        'name' => 'Masse terrifiante',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=masse-terrifiante',
        'image' => 'https://www.aidedd.org/dnd/images-om/mace-of-terror.jpg',
    ];

    private const MANTLE_SPELL_RESISTANCE = [
        'name' => 'Manteau de résistance aux sorts',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=manteau-de-resistance-aux-sorts',
        'image' => 'https://www.aidedd.org/dnd/images-om/mantle-of-spell-resistance.jpg',
    ];

    private const NECKLACE_PRAYER_BEADS = [
        'name' => 'Collier de perles de prière',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=collier-de-perles-de-priere',
        'image' => 'https://www.aidedd.org/dnd/images-om/necklace-of-prayer-beads.jpg',
    ];

    private const PERIAPT_PROOF_POISON = [
        'name' => 'Amulette antidote',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-antidote',
    ];

    private const RING_ANIMAL_INFLUENCE = [
        'name' => 'Anneau d\'influence sur les animaux',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-d-influence-sur-les-animaux',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-animal-influence.jpg',
    ];

    private const RING_EVASION = [
        'name' => 'Anneau d\'esquive totale',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-d-esquive-totale',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-evasion.jpg',
    ];

    private const RING_FEATHER_FALLING = [
        'name' => 'Anneau de feuille morte',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-feuille-morte',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-feather-falling.jpg',
    ];

    private const RING_FREE_ACTION = [
        'name' => 'Anneau d\'action libre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-d-action-libre',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-free-action.jpg',
    ];

    private const RING_PROTECTION = [
        'name' => 'Anneau de protection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-protection',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-protection.jpg',
    ];

    private const RING_RESISTANCE = [
        'name' => 'Anneau de résistance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-fire-resistance.jpg',
    ];

    private const RING_SPELL_STORING = [
        'name' => 'Anneau de stockage de sort',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-stockage-de-sort',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-spell-storing.jpg',
    ];

    private const RING_RAM = [
        'name' => 'Anneau du bélier',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-du-belier',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-the-ram.jpg',
    ];

    private const RING_X_RAY_VISION = [
        'name' => 'Anneau de vision aux rayons X',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-rayons-x',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-x-ray-vision.jpg',
    ];

    private const ROBE_EYES = [
        'name' => 'Robe aux yeux multiples',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=robe-de-vision-totale',
        'image' => 'https://www.aidedd.org/dnd/images-om/robe-of-eyes.jpg',
    ];

    private const ROD_RULERSHIP = [
        'name' => 'Sceptre de suzeraineté',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-de-suzerainete',
        'image' => 'https://www.aidedd.org/dnd/images-om/rod-of-rulership.jpg',
    ];

    private const ROD_PACT_KEEPER_2 = [
        'name' => 'Sceptre du gardien des pactes +2',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-du-gardien-des-pactes',
    ];

    private const ROPE_ENTANGLEMENT = [
        'name' => 'Corde d\'enchevêtrement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=corde-d-enchevetrement',
        'image' => 'https://www.aidedd.org/dnd/images-om/rope-of-entanglement.jpg',
    ];

    private const ARMOR_SCALE_1 = [
        'name' => 'Armure +1 (écailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_SCALE = [
        'name' => 'Armure de résistance (écailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const SHIELD_2 = [
        'name' => 'Bouclier +2',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-1-2-ou-3',
    ];

    private const SHIELD_MISSILE_ATTRACTION = [
        'name' => 'Bouclier d\'attraction des projectiles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-d-attraction-des-projectiles',
        'image' => 'https://www.aidedd.org/dnd/images-om/shield-of-missile-attraction.jpg',
    ];

    private const STAFF_CHARMING = [
        'name' => 'Bâton d\'envoûtement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-d-envoutement',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-charming.jpg',
    ];

    private const STAFF_HEALING = [
        'name' => 'Bâton de guérison',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-guerison',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-healing.jpg',
    ];

    private const STAFF_SWARMING_INSECTS = [
        'name' => 'Bâton de grand essaim',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-grand-essaim',
        'iamge' => 'https://www.aidedd.org/dnd/images-om/staff-of-swarming-insects.jpg',
    ];

    private const STAFF_WOODLANDS = [
        'name' => 'Bâton des forêts',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-des-forets',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-the-woodlands.jpg',
    ];

    private const STAFF_WITHERING = [
        'name' => 'Bâton d\'envoûtement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-fletrissement',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-withering.jpg',
    ];

    private const STONE_CONTROLLING_EARTH_ELEMENTALS = [
        'name' => 'Pierre de contrôle des élémentaires de la terre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-controle-des-elementaires-de-la-terre',
        'image' => 'https://www.aidedd.org/dnd/images-om/stone-of-controlling-earth-elementals.jpg',
    ];

    private const SUN_BLADE = [
        'name' => 'Épée radieuse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-radieuse',
        'image' => 'https://www.aidedd.org/dnd/images-om/sun-blade.jpg',
    ];

    private const SWORD_LIFE_STEALING = [
        'name' => 'Épée voleuse de vie',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-voleuse-de-vie',
        'image' => 'https://www.aidedd.org/dnd/images-om/sword-of-life-stealing.jpg',
    ];

    private const SWORD_WOUNDING = [
        'name' => 'Épée incisive',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-incisive',
    ];

    private const ROD_TENTACLE = [
        'name' => 'Sceptre tentacule',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-tentacule',
        'image' => 'https://www.aidedd.org/dnd/images-om/tentacle-rod.jpg',
    ];

    private const WEAPON_VICIOUS = [
        'name' => 'Arme vicieuse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arme-vicieuse',
        'image' => 'https://www.aidedd.org/dnd/images-om/vicious-weapon.jpg',
    ];

    private const WAND_BINDING = [
        'name' => 'Baguette d\'entraves',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-d-entraves',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-binding.jpg',
    ];

    private const WAND_ENEMY_DETECTION = [
        'name' => 'Baguette de détection de l\'ennemi',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-detection-de-l-ennemi',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-enemy-detection.jpg',
    ];

    private const WAND_FEAR = [
        'name' => 'Baguette de peur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-peur',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-fear.jpg',
    ];

    private const WAND_FIREBALLS = [
        'name' => 'Baguette de boules de feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-boules-de-feu',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-fireballs.jpg',
    ];

    private const WAND_LIGHTNING_BOLTS = [
        'name' => 'Baguette d\'éclairs',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-d-eclairs',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-lightning-bolts.jpg',
    ];

    private const WAND_PARALYSIS = [
        'name' => 'Baguette de paralysie',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-paralysie',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-paralysis.jpg',
    ];

    private const WAND_WAR_MAGE_2 = [
        'name' => 'Baguette du mage de guerre +2',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-du-mage-de-guerre-1-2-ou-3',
    ];

    private const WAND_WONDER = [
        'name' => 'Baguette des merveilles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-des-merveilles',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-wonder.jpg',
    ];

    private const WINGS_FLYING = [
        'name' => 'Cape de vol',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-vol',
        'image' => 'https://www.aidedd.org/dnd/images-om/wings-of-flying.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [11, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ];
    }
}
