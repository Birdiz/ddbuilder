<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableH extends AbstractEnum implements WeightedInterface
{
    private const WEAPON_3 = [
        'name' => 'Arme +3',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arme-1-2-ou-3',
    ];
    private const AMULET_PLANES = [
        'name' => 'Amulette des plans',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=amulette-des-plans',
        'image' => 'https://www.aidedd.org/dnd/images-om/amulet-of-the-planes.jpg',
    ];

    private const CARPET_FLYING = [
        'name' => 'Tapis volant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=tapis-volant',
    ];

    private const CRISTAL_BALL = [
        'name' => 'Boule de cristal (très rare)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=boule-de-cristal',
    ];

    private const RING_REGENERATION = [
        'name' => 'Anneau de régénération',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-regeneration',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-regeneration.jpg',
    ];

    private const RING_SHOOTING_STARS = [
        'name' => 'Anneau de feu d\'étoiles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-feu-d-etoiles',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-shooting-stars.jpg',
    ];

    private const RING_TELEKINESIS = [
        'name' => 'Anneau de télékinesie',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-telekinesie',
        'image' => 'https://www.aidedd.org/dnd/images-om/ring-of-telekinesis.jpg',
    ];

    private const ROBE_SCINTILLATING_COLORS = [
        'name' => 'Robe prismatique',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=robe-prismatique',
    ];

    private const ROBE_STARS = [
        'name' => 'Robe aux étoiles',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=robe-aux-etoiles',
        'image' => 'https://www.aidedd.org/dnd/images-om/robe-of-stars.jpg',
    ];

    private const ROD_ABSORPTION = [
        'name' => 'Sceptre d\'absorption',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-d-absorption',
        'image' => 'https://www.aidedd.org/dnd/images-om/rod-of-absorption.jpg',
    ];

    private const ROD_ALERTNESS = [
        'name' => 'Sceptre de vigilance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-de-vigilance',
    ];

    private const ROD_SECURITY = [
        'name' => 'Sceptre de sécurité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-de-securite',
    ];

    private const ROD_PACT_KEEPER_3 = [
        'name' => 'Sceptre du gardien des pactes +3',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-du-gardien-des-pactes',
    ];

    private const SCIMITAR_SPEED = [
        'name' => 'Cimeterre de rapidité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cimeterre-de-rapidite',
    ];

    private const SHIELD_3 = [
        'name' => 'Bouclier +3',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-1-2-ou-3',
    ];

    private const STAFF_FIRE = [
        'name' => 'Bâton de feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-feu',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-fire.jpg',
    ];

    private const STAFF_FROST = [
        'name' => 'Bâton de givre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-givre',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-frost.jpg',
    ];

    private const STAFF_POWER = [
        'name' => 'Bâton de surpuissance',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-surpuissance',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-power.jpg',
    ];

    private const STAFF_STRIKING = [
        'name' => 'Bâton percussif',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-percussif',
    ];

    private const STAFF_THUNDER_LIGHTNING = [
        'name' => 'Bâton de tonnerre et de foudre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-de-tonnerre-et-de-foudre',
        'image' => 'https://www.aidedd.org/dnd/images-om/staff-of-thunder-and-lightning.jpg',
    ];

    private const SWORD_SHARPNESS = [
        'name' => 'Épée acérée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-aceree',
        'image' => 'https://www.aidedd.org/dnd/images-om/sword-of-sharpness.jpg',
    ];

    private const WAND_POLYMORPH = [
        'name' => 'Baguette de métamorphose',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-de-metamorphose',
        'image' => 'https://www.aidedd.org/dnd/images-om/wand-of-polymorph.jpg',
    ];

    private const WAND_WAR_MAGE_3 = [
        'name' => 'Baguette du mage de guerre +3',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baguette-du-mage-de-guerre-1-2-ou-3',
    ];

    private const ADAMANTIUM_ARMOR_HALF_PLATE = [
        'name' => 'Armure en adamantium (demi-plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const ADAMANTIUM_ARMOR_PLATE = [
        'name' => 'Armure en adamantium (plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-adamantium',
    ];

    private const SHIELD_ANIMATED = [
        'name' => 'Bouclier animé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-anime',
        'image' => 'https://www.aidedd.org/dnd/images-om/animated-shield.jpg',
    ];

    private const BELT_GIANT_STRENGTH_FIRE = [
        'name' => 'Ceinturon de force de géant de feu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-de-force-de-geant',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-stone-giant-strength.jpg',
    ];

    private const BELT_GIANT_STRENGTH_FROST = [
        'name' => 'Ceinturon de force de géant du givre (ou de la pierre)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-de-force-de-geant',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-stone-giant-strength.jpg',
    ];

    private const ARMOR_BREAST_1 = [
        'name' => 'Armure +1 (cuirasse)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_BREAST = [
        'name' => 'Armure de résistance (cuirasse)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const CANDLE_INVOCATION = [
        'name' => 'Cierge d\'invocation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cierge-d-invocation',
        'image' => 'https://www.aidedd.org/dnd/images-om/candle-of-invocation.jpg',
    ];

    private const ARMOR_MAIL_2 = [
        'name' => 'Armure +2 (cotte de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_SHIRT_2 = [
        'name' => 'Armure +2 (chemise de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const CLOAK_SPIDER = [
        'name' => 'Cape de l\'arachnide',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-de-l-arachnide',
    ];

    private const SWORD_DANCING = [
        'name' => 'Épée dansante',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-dansante',
        'image' => 'https://www.aidedd.org/dnd/images-om/dancing-sword.jpg',
    ];

    private const ARMOR_DEMON = [
        'name' => 'Armure démoniaque',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-demoniaque',
        'image' => 'https://www.aidedd.org/dnd/images-om/demon-armor.jpg',
    ];

    private const ARMOR_DRAGON_SCALE = [
        'name' => 'Armure d\'écailles de dragon',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-ecailles-de-dragon',
        'image' => 'https://www.aidedd.org/dnd/images-om/dragon-scale-mail.jpg',
    ];

    private const DWARF_PLATE = [
        'name' => 'Harnois nain',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=harnois-nain',
        'image' => 'https://www.aidedd.org/dnd/images-om/dwarven-plate.jpg',
    ];

    private const DWARVEN_THROWER = [
        'name' => 'Marteau de lancer nain',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=marteau-de-lancer-nain',
        'image' => 'https://www.aidedd.org/dnd/images-om/dwarven-thrower.jpg',
    ];

    private const BOTTLE_EFREETI = [
        'name' => 'Bouteille éfrit',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouteille-de-l-efrit',
        'image' => 'https://www.aidedd.org/dnd/images-om/efreeti-bottle.jpg',
    ];

    private const FIGURINE_WONDROUS_POWER = [
        'name' => 'Cheval d\'obsidienne',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=figurine-merveilleuse',
    ];

    private const SWORD_FROST = [
        'name' => 'Fer gelé',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=fer-gele',
        'image' => 'https://www.aidedd.org/dnd/images-om/frost-brand.jpg',
    ];

    private const HELM_BRILLIANCE = [
        'name' => 'Heaume scintillant',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=heaume-scintillant',
        'image' => 'https://www.aidedd.org/dnd/images-om/helm-of-brilliance.jpg',
    ];

    private const HORN_VALHALLA = [
        'name' => 'Cor du valhalla (bronze)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cor-du-valhalla',
        'image' => 'https://www.aidedd.org/dnd/images-om/horn-of-valhalla.jpg',
    ];

    private const INSTRUMENT_BARD_HARP = [
        'name' => 'Instrument de barde (Harpe d\'Anstruth)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const STONE_IOUN_ABSORPTION = [
        'name' => 'Pierre de Ioun (absorption)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_AGILITY = [
        'name' => 'Pierre de Ioun (agilité)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_FORTITUDE = [
        'name' => 'Pierre de Ioun (vigueur)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_INSIGHT = [
        'name' => 'Pierre de Ioun (intuition)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_INTELLECT = [
        'name' => 'Pierre de Ioun (intellect)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_LEADERSHIP = [
        'name' => 'Pierre de Ioun (dirigeant)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_STRENGTH = [
        'name' => 'Pierre de Ioun (force)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const ARMOR_LEATHER_2 = [
        'name' => 'Armure +2 (cuir)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const MANUAL_BODILY_HEALTH = [
        'name' => 'Manuel de vitalite',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=manuel-de-vitalite',
        'image' => 'https://www.aidedd.org/dnd/images-om/manual-of-bodily-health.jpg',
    ];

    private const MANUAL_GAINFUL_EXERCISE = [
        'name' => 'Manuel d\'exercices physiques',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=manuel-d-exercices-physiques',
    ];

    private const MANUAL_GOLEMS = [
        'name' => 'Manuel des golems',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=manuel-des-golems',
        'image' => 'https://www.aidedd.org/dnd/images-om/manual-of-iron-golems.jpg',
    ];

    private const MANUAL_QUICKNESS_ACTION = [
        'name' => 'Manuel de vivacité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=manuel-de-vivacite',
        'image' => 'https://www.aidedd.org/dnd/images-om/manual-of-quickness-of-action.jpg',
    ];

    private const MIRROR_LIFE_TRAPPING = [
        'name' => 'Miroir d\'emprisonnement',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=miroir-d-emprisonnement',
        'image' => 'https://www.aidedd.org/dnd/images-om/mirror-of-life-trapping.jpg',
    ];

    private const NINE_LIVES_STEALER = [
        'name' => 'Voleuse des neuf vies',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=voleuse-des-neuf-vies',
        'image' => 'https://www.aidedd.org/dnd/images-om/nine-lives-stealer.jpg',
    ];

    private const OATHBOW = [
        'name' => 'Arc du serment',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=arc-du-serment',
        'image' => 'https://www.aidedd.org/dnd/images-om/oathbow.jpg',
    ];

    private const ARMOR_SCALE_2 = [
        'name' => 'Armure +2 (écailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const SHIELD_SPELLGUARD = [
        'name' => 'Bouclier gardesort',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=bouclier-gardesort',
        'image' => 'https://www.aidedd.org/dnd/images-om/spellguard-shield.jpg',
    ];

    private const ARMOR_SPLINT_1 = [
        'name' => 'Armure +1 (clibanion)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_SPLINT = [
        'name' => 'Armure de résistance (clibanion)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const ARMOR_STUDDER_LEATHER_1 = [
        'name' => 'Armure +1 (cuir clouté)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_RESISTANCE_STUDDER_LEATHER = [
        'name' => 'Armure de résistance (cuir clouté)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const TOME_CLEAR_THOUGHT = [
        'name' => 'Traité de perspicacité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=traite-de-perspicacite',
        'image' => 'https://www.aidedd.org/dnd/images-om/tome-of-clear-thought.jpg',
    ];

    private const TOME_LEADERSHIP_INFLUENCE = [
        'name' => 'Traité d\'autorité et d\'influence',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=traite-d-autorite-et-d-influence',
        'image' => 'https://www.aidedd.org/dnd/images-om/tome-of-leadership.jpg',
    ];

    private const TOME_UNDERSTANDING = [
        'name' => 'Traité de compréhension',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=traite-de-comprehension',
        'image' => 'https://www.aidedd.org/dnd/images-om/tome-of-understanding.jpg',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [10, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ];
    }
}
