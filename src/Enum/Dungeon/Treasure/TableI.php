<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;

class TableI extends AbstractEnum implements WeightedInterface
{
    private const DEFENDER = [
        'name' => 'Gardienne',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=gardienne',
    ];

    private const HAMMER_THUNDERBOLTS = [
        'name' => 'Marteau de tonnerre',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=marteau-de-tonnerre',
    ];

    private const BLADE_LUCKY = [
        'name' => 'Lame porte-bonheur',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=lame-porte-bonheur',
    ];

    private const SWORD_ANSWERING = [
        'name' => 'Épée de réponse',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-de-reponse',
    ];

    private const HOLY_AVENGER = [
        'name' => 'Vengeresse-sacrée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=vengeresse-sacree',
    ];

    private const RING_DJINNI_SUMMONING = [
        'name' => 'Anneau de convocation de djinn',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-convocation-de-djinn',
    ];

    private const RING_INVISIBILITY = [
        'name' => 'Anneau d\'invisibilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-d-invisibilite',
    ];

    private const RING_SPELL_TURNING = [
        'name' => 'Anneau de renvoi des sorts',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-renvoi-des-sorts',
    ];

    private const ROD_LORDLY_MIGHT = [
        'name' => 'Sceptre de puissance seigneuriale',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-de-puissance-seigneuriale',
    ];

    private const STAFF_MAGI = [
        'name' => 'Bâton du thaumaturge',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=baton-du-thaumaturge',
    ];

    private const SWORD_VORPAL = [
        'name' => 'Épée vorpale',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=epee-vorpale',
    ];

    private const BELT_GIANT_STRENGTH_CLOUD = [
        'name' => 'Ceinturon de force de géant des nuages',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-de-force-de-geant',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-stone-giant-strength.jpg',
    ];

    private const ARMOR_BREAST_2 = [
        'name' => 'Armure +2 (cuirasse)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_MAIL_3 = [
        'name' => 'Armure +3 (cotte de maille)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_SHIRT_3 = [
        'name' => 'Armure +3 (chemise de mailles)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const CLOAK_INVISIBILITY = [
        'name' => 'Cape d\'invisibilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cape-d-invisibilite',
    ];

    private const CRYSTAL_BALL = [
        'name' => 'Boule de cristal (légendaire)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=boule-de-cristal',
    ];

    private const ARMOR_HALF_PLATE_1 = [
        'name' => 'Armure +1 (demi-plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const FLASK_IRON = [
        'name' => 'Flasque de fer',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=flasque-de-fer',
    ];

    private const ARMOR_LEATHER_3 = [
        'name' => 'Armure +3 (cuir)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_PLATE_1 = [
        'name' => 'Armure +1 (plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ROBE_ARCHMAGI = [
        'name' => 'Robe de l\'archimage',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=robe-de-l-archimage',
    ];

    private const ROD_RESURRECTION = [
        'name' => 'Sceptre de résurrection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sceptre-de-resurrection',
    ];

    private const ARMOR_SCALE_1 = [
        'name' => 'Armure +1 (écaille)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const SCARAB_PROTECTION = [
        'name' => 'Scarabée de protection',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=scarabee-de-protection',
    ];

    private const ARMOR_SPLINT_2 = [
        'name' => 'Armure +2 (clibanion)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const ARMOR_STUDDED_LEATHER_2 = [
        'name' => 'Armure +2 (cuir clouté)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
    ];

    private const WELL_MANY_WORLDS = [
        'name' => 'Puits des mondes',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=puits-des-mondes',
    ];

    private const MAGIC_ARMOR = [
        'name' => 'Armure magique (lancer 1d12)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-1-2-ou-3',
        'special' => [
            '1-2 - armure +2 (demi-plate)',
            '3-4 - armure +2 (plate)',
            '5-6 - armure +3 (cuir-clouté)',
            '7-8 - armure +3 (cuirasse)',
            '9-10 - armure +3 (clibanion)',
            '11 - armure +3 (demi-plate)',
            '12 - armure +3 (plate)',
        ],
    ];

    private const BELT_GIANT_STRENGTH_STORM = [
        'name' => 'Ceinturon de force de géant des tempêtes',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=ceinturon-de-force-de-geant',
        'image' => 'https://www.aidedd.org/dnd/images-om/belt-of-stone-giant-strength.jpg',
    ];

    private const APPARATUS_KWALISH = [
        'name' => 'Submersible de Kwalish',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=submersible-de-kwalish',
    ];

    private const ARMOR_INVULNERABILITY = [
        'name' => 'Armure d\'invulnérabilité',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-d-invulnerabilite',
    ];

    private const GATE_CUBIC = [
        'name' => 'Cube des plans',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cube-des-plans',
    ];

    private const DECK_MANY_THINGS = [
        'name' => 'Cartes merveilleuses',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cartes-merveilleuses',
    ];

    private const EFREETI_CHAIN = [
        'name' => 'Mailles éfrit',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=mailles-efrit',
    ];

    private const ARMOR_RESISTANCE_HALF_PLATE = [
        'name' => 'Armure de résistance (demi-plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const HORN_VALHALLA = [
        'name' => 'Cor du valhalla (fer)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=cor-du-valhalla',
        'image' => 'https://www.aidedd.org/dnd/images-om/horn-of-valhalla.jpg',
    ];

    private const INSTRUMENT_BARD = [
        'name' => 'Instrument de barde (Harpe de Ollamh)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=instrument-de-barde',
    ];

    private const STONE_IOUN_ABSORPTION = [
        'name' => 'Pierre de Ioun (absorption supérieure)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_MASTERY = [
        'name' => 'Pierre de Ioun (maîtrise)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const STONE_IOUN_REGENERATION = [
        'name' => 'Pierre de Ioun (régénération)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=pierre-de-ioun',
    ];

    private const ARMOR_ETHEREALNESS = [
        'name' => 'Harnois éthéré',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=harnois-ethere',
    ];

    private const ARMOR_RESISTANCE_PLATE = [
        'name' => 'Armure de résistance (plate)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=armure-de-resistance',
        'image' => 'https://www.aidedd.org/dnd/images-om/plate-armor-of-resistance.jpg',
    ];

    private const RING_ELEMENTAL_COMMAND_AIR = [
        'name' => 'Anneau de contrôle des élémentaires (air)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-controle-des-elementaires',
    ];

    private const RING_ELEMENTAL_COMMAND_EARTH = [
        'name' => 'Anneau de contrôle des élémentaires (terre)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-controle-des-elementaires',
    ];

    private const RING_ELEMENTAL_COMMAND_FIRE = [
        'name' => 'Anneau de contrôle des élémentaires (feu)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-controle-des-elementaires',
    ];

    private const RING_THREE_WISHES = [
        'name' => 'Anneau de triple souhait',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-triple-souhait',
    ];

    private const RING_ELEMENTAL_COMMAND_WATER = [
        'name' => 'Anneau de contrôle des élémentaires (eau)',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=anneau-de-controle-des-elementaires',
    ];

    private const SPHERE_ANNIHILATION = [
        'name' => 'Sphère d\'annihilation',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=sphere-d-annihilation',
    ];

    private const TALISMAN_PURE_GOOD = [
        'name' => 'Talisman du bien ultime',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=talisman-du-bien-ultime',
    ];

    private const TALISMAN_SPHERE = [
        'name' => 'Talisman de la sphère',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=talisman-de-la-sphere',
    ];

    private const TALISMAN_PURE_EVIL = [
        'name' => 'Talisman du mal absolu',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=talisman-du-mal-absolu',
    ];

    private const TOME_STILLED_TONGUE = [
        'name' => 'Tome de la langue nouée',
        'link' => 'https://www.aidedd.org/dnd/om.php?vf=tome-de-la-langue-nouee',
    ];

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, ];
    }
}
