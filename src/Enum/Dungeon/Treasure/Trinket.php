<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Model\AbstractEnum;

class Trinket extends AbstractEnum
{
    private const HAND = 'Une main de gobelin momifiée';
    private const CRYSTAL = 'Un morceau de cristal qui brille faiblement au clair de lune';
    private const COIN_UNKNOWN = 'Une pièce d\'or d\'une terre inconnue';
    private const JOURNAL = 'Un journal écrit dans une langue que vous ne connaissez pas';
    private const COPPER_RING = 'Un anneau de cuivre qui ne ternit pas';
    private const CHEST_PIECE = 'Une vieille pièce d\'échecs en verre';
    private const DICES = 'Une paire de dés en osselet, chacun portant le symbole d\'un crâne sur la face qui montrerait
     normalement le 6';
    private const IDOL = 'Une petite idole représentant une créature cauchemardesque qui vous donne des rêves troublants
     quand vous dormez près d\'elle';
    private const NECKLACE = 'Un collier en corde duquel pendent quatre doigts elfes momifiés';
    private const PROPERTY_DEED = 'L\'acte d\'une parcelle de terrain d\'un domaine que vous ne connaissez pas';
    private const MATERIAL_UNKNOWN = 'Un bloc de 30 grammes d\'un matériau inconnu';
    private const DOLL = 'Une petite poupée de chiffon piquée avec des aiguilles';
    private const TOOTH = 'Une dent d\'une bête inconnue';
    private const SCALE = 'Une énorme écaille, peut-être d\'un dragon';
    private const FEATHER = 'Une plume vert clair';
    private const ORACLE_CARD = 'Une vieille carte de divination portant votre portrait';
    private const ORB_GLASS = 'Un orbe en verre rempli de fumée qui se déplace';
    private const EGG = 'Un oeuf de 30 grammes avec une coque rouge vif';
    private const PIPE = 'Une pipe qui fait des bulles';
    private const STRANGE_POT = 'Un pot en verre contenant un morceau de chair bizarre qui flotte dans un liquide salé';
    private const MUSIC_BOX = 'Une petite boîte à musique de gnome qui joue une chanson qui vous rappelle vaguement 
    votre enfance';
    private const STATUE = 'Une petite statuette en bois d\'un halfelin béat';
    private const ORB_COPPER = 'Un orbe en cuivre gravé de runes étranges';
    private const DISK = 'Un disque de pierre multicolore';
    private const ICON = 'Une petite icône d\'argent représentant un corbeau';
    private const BACKPACK_TEETH = 'Un sac contenant quarante-sept dents humanoïdes, dont l\'une est cariée';
    private const OBSIDIAN = 'Un fragment d\'obsidienne qui se sent toujours chaud au toucher';
    private const CLAW = 'Une griffe osseuse d\'un dragon suspendue à un collier de cuir lisse';
    private const SOCKS = 'Une paire de vieilles chaussettes';
    private const STRANGE_BOOK = 'Un livre blanc dont les pages refusent de retenir l\'encre, la craie, la graphite ou 
    toute autre substance ou marquage';
    private const STAR = 'Un badge en argent qui représente une étoile à cinq branches';
    private const KNIFE = 'Un couteau qui appartenait à un parent';
    private const BOTTLE_NAIL = 'Un flacon de verre rempli de rognures d\'ongles';
    private const STRANGE_TOOL = 'Un dispositif métallique et rectangulaire avec deux petites coupes en métal à une 
    extrémité et qui jette des étincelles lorsqu\'il est mouillé';
    private const GLOVE = 'Un gant blanc pailleté aux dimensions d\'un humain';
    private const VEST = 'Une veste avec une centaine de minuscules poches';
    private const STONE = 'Un petit bloc de pierre léger';
    private const SKETCH = 'Un petit dessin qui représente le portrait d\'un gobelin';
    private const BOTTLE_PERFUM = 'Un flacon de verre vide qui sent le parfum lorsqu\'il est ouvert';
    private const COAL = 'Une pierre précieuse qui ressemble à un morceau de charbon pour tout le monde, sauf pour 
    vous';
    private const BANNIER = 'Un morceau de tissu d\'une vieille bannière';
    private const BADGE = 'Un insigne de grade d\'un légionnaire perdu';
    private const CLOCK = 'Une cloche en argent minuscule et sans battant';
    private const CANARI = 'Un canari mécanique à l\'intérieur d\'une lampe de gnome';
    private const BOX = 'Un petit coffre avec de nombreux pieds sculptés sur le fond';
    private const BOTTLE_PIXIE = 'Une pixie morte à l\'intérieur d\'une bouteille en verre transparent';
    private const BOW_OPENLESS = 'Une boîte métallique qui n\'a pas d\'ouverture mais qui sonne comme si elle était 
    remplie de liquide, de sable, d\'araignées ou de verre brisé (au choix)';
    private const ORB_WATER = 'Un orbe de verre rempli d\'eau, dans lequel nage un poisson rouge mécanique';
    private const SPOON = 'Une cuillère d\'argent avec un M gravé sur le manche';
    private const WHISTLE = 'Un sifflet en bois de couleur or';
    private const BEETLE = 'Un scarabée mort de la taille de votre main';
    private const SOLDIERS = 'Deux soldats de plomb, l\'un avec la tête manquante';
    private const BOX_BUTTONS = 'Une petite boîte remplie de boutons de différentes tailles';
    private const CANDLE = 'Une bougie qui ne peut pas être allumée';
    private const CAGE = 'Une petite cage sans porte';
    private const OLD_KEY = 'Une vieille clé';
    private const TREASURE_MAP = 'Une carte au trésor indéchiffrable';
    private const SWORD_HANDLE = 'Une poigne d\'épée brisée';
    private const RABBIT_FOOT = 'Une patte de lapin';
    private const GLASS_EYE = 'Un œil de verre';
    private const CAMEE = 'Un camée (pendentif) sculpté à l\'image d\'une personne hideuse';
    private const SKULL = 'Un crâne en argent de la taille d\'une pièce de monnaie';
    private const MASK = 'Un masque d\'albâtre';
    private const PYRAMID = 'Une pyramide de bâtonnets d\'encens noir qui sent très mauvais';
    private const NIGHTCAP = 'Un bonnet de nuit qui, lorsqu\'il est porté, vous donne des rêves agréables';
    private const PIKES = 'Une chausse-trappe unique fabriquée à partir d\'un os';
    private const MONOCLE = 'Un cadre de monocle en or sans la lentille';
    private const CUBE = 'Un cube de 2,50 cm d\'arête, avec chaque face peinte d\'une couleur différente';
    private const DOOR_BUTTON = 'Un bouton de porte en cristal';
    private const PACKET = 'Un petit paquet rempli de poussière rose';
    private const SONG = 'Un fragment d\'une belle chanson, écrite avec des notes de musique sur deux morceaux de 
    parchemin';
    private const EARRING = 'Une boucle d\'oreille en forme de goutte d\'argent faite à partir d\'une vraie larme';
    private const EGGSHELL = 'La coquille d\'un oeuf peint avec des scènes de misère humaine d\'un détail troublant';
    private const FAN = 'Un éventail qui, une fois déplié, montre un chat endormi';
    private const TUBES = 'Un ensemble de tubes d\'os';
    private const CLOVER = 'Un trèfle à quatre feuilles à l\'intérieur d\'un livre qui traite des bonnes manières et de 
    l\'étiquette';
    private const BLUEPRINT_ENGIN = 'Une feuille de parchemin sur laquelle est dessiné un engin mécanique complexe';
    private const SCABBARD = 'Un fourreau orné dans lequel à ce jour aucune lame ne rentre';
    private const INVIT = 'Une invitation à une fête où un assassinat a eu lieu';
    private const PENTACLE = 'Un pentacle de bronze avec la gravure d\'une tête de rat au centre';
    private const HANDKERCHIEF = 'Un mouchoir violet brodé avec le nom d\'un puissant archimage';
    private const BLUEPRINT_BUILDING = 'La moitié du plan d\'un temple, d\'un château, ou d\'une autre structure';
    private const HAT = 'Un peu de tissu plié qui, une fois déplié, se transforme en un élégant chapeau';
    private const BANK_DEED = 'Un récépissé de dépôt dans une banque d\'une ville très éloignée';
    private const JOURNAL_PARTIAL = 'Un journal avec sept pages manquantes';
    private const SNUFFBOX = 'Une tabatière en argent vide et portant une inscription sur le dessus qui dit « rêves »';
    private const SYMBOL = 'Un symbole sacré en fer et consacré à un dieu inconnu';
    private const BOOK_LEGEND = 'Un livre qui raconte l\'histoire de l\'ascension et la chute d\'un héros légendaire, 
    avec le dernier chapitre manquant';
    private const BOTTLE_DRAGON = 'Un flacon de sang de dragon';
    private const ARROW = 'Une ancienne flèche de conception elfique';
    private const NEEDLE = 'Une aiguille qui ne se plie pas';
    private const BROOCH = 'Une broche ornée de conception naine';
    private const WINE_BOTTLE = 'Une bouteille de vin vide portant une jolie étiquette qui dit « Le magicien des vins, 
    Cuvée du Dragon Rouge, 331422-W »';
    private const LID = 'Un couvercle avec une mosaïque multicolore en surface';
    private const MOUSE = 'Une souris pétrifiée';
    private const FLAG = 'Un drapeau de pirate noir orné d\'un crâne et des os croisés d\'un dragon';
    private const CRABE = 'Un petit crabe ou araignée mécanique qui se déplace quand il n\'est pas observé';
    private const GLASS_POT = 'Un pot de verre contenant du lard avec une étiquette qui dit « Graisse de griffon »';
    private const WOOD_BOX = 'Une boîte en bois avec un fond en céramique qui contient un ver vivant avec une tête à 
    chaque extrémité de son corps';
    private const URNE = 'Une urne en métal contenant les cendres d\'un héros';
}
