<?php

declare(strict_types=1);

namespace App\Enum\Dungeon\Treasure;

use App\Model\AbstractEnum;
use App\Util\TreasureTrait;

/**
 * Class Type.
 *
 * @method static Type BEGINNER()
 * @method static Type EXPERT()
 * @method static Type HERO()
 * @method static Type GOD_LIKE()
 */
class Type extends AbstractEnum
{
    use TreasureTrait;

    private const BEGINNER = 'Débutants';
    private const EXPERT = 'Experts';
    private const HERO = 'Héros';
    private const GOD_LIKE = 'Divins';

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getTreasureFromKey(string $key, int $number, bool $needOnlyCoins = false): array
    {
        $method = match ($key) {
            static::BEGINNER()->getKey() => 'getBeginnerTreasure',
            static::EXPERT()->getKey() => 'getExpertTreasure',
            static::HERO()->getKey() => 'getHeroTreasure',
            static::GOD_LIKE()->getKey() => 'getGodLikeTreasure',
            default => '',
        };

        if ('' === $method) {
            throw new \LogicException('Method for treasure does not exist.');
        }

        $treasures = [];
        for ($i = 1; $i <= $number; ++$i) {
            $treasures[] = static::$method($needOnlyCoins);
        }

        return $treasures;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getHoardFromKey(string $key, int $number): array
    {
        $method = match ($key) {
            static::BEGINNER()->getKey() => 'buildBeginnerHoard',
            static::EXPERT()->getKey() => 'buildExpertHoard',
            static::HERO()->getKey() => 'buildHeroHoard',
            static::GOD_LIKE()->getKey() => 'buildGodLikeHoard',
            default => '',
        };

        if ('' === $method) {
            throw new \LogicException('Method for hoard does not exist.');
        }

        $hoards = [];
        for ($i = 1; $i <= $number; ++$i) {
            $hoards[] = static::$method();
        }

        return $hoards;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getGemsAndArtFromKey(string $key, int $number): array
    {
        $method = match ($key) {
            static::BEGINNER()->getKey() => 'getBeginnerReward',
            static::EXPERT()->getKey() => 'getExpertReward',
            static::HERO()->getKey() => 'getHeroReward',
            static::GOD_LIKE()->getKey() => 'getGodLikeReward',
            default => '',
        };

        if ('' === $method) {
            throw new \LogicException('Method for gems and art does not exist.');
        }

        $rewards = [];
        for ($i = 1; $i <= $number; ++$i) {
            $rewards[] = static::$method();
        }

        return $rewards;
    }
}
