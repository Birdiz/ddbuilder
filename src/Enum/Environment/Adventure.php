<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

class Adventure extends AbstractEnum
{
    private const DUNGEON = 'Trouver la position d\'un donjon ou un autre site d\'intérêt.';
    private const DISASTER = 'Rendre compte des résultats d\'un désastre naturel, ou non.';
    private const ESCORT = 'Escorter un PNJ jusq\'à une destination.';
    private const DESTINATION = 'Arrivez à une destination sans être repérer par les ennemis.';
    private const STOP_MONSTERS = 'Stopper des monstres qui attaquent des transports et des fermes.';
    private const TRADE = 'Établir un accord commercial avec un ville distante.';
    private const PROTECT = 'Protéger un transport vers une ville distante.';
    private const MAP = 'Cartographier une nouvelle région.';
    private const COLONY = 'Trouver un endroit où établir une colonie.';
    private const RESOURCE = 'Trouver une ressource naturelle.';
    private const HUNT = 'Tuer un monster spécifique.';
    private const HOME = 'Retourner chez soi après un long voyage.';
    private const ERMIT = 'Obtenir des informations d\'un ermite.';
    private const OBJECT = 'Retrouver un objet perdu dans la nature.';
    private const MISSING_GROUP = 'Découvrir l\'état d\'un groupe d\'explorateurs disparu.';
    private const PURSUE = 'Poursuivre des ennemis qui tentent de fuire.';
    private const ARMY = 'Estimer la taille d\'une armée qui approche.';
    private const TYRANT = 'S\'échaper du joug d\'un tyran.';
    private const PROTECT_SITE = 'Protéger un lieu naturel de ses attaquants.';
}
