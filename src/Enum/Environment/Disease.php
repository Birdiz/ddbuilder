<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractNamedEnum;

/**
 * Class Disease.
 *
 * @method static Disease ASHEN_INFLAMMATION()
 * @method static Disease BLOOD_FEVER()
 * @method static Disease BLOODMOLD()
 * @method static Disease BUBONIC_PLAGUE()
 * @method static Disease CACKLE_FEVER()
 * @method static Disease CALCIFICATION_VIRUS()
 * @method static Disease GOBLIN_GOUT()
 * @method static Disease GRUNGE()
 * @method static Disease GREEN_DECAY()
 * @method static Disease HANTA_VIRUS()
 * @method static Disease HEMOPHILIA()
 * @method static Disease LEECH_FEVER()
 * @method static Disease MARBLEWIFE_SYNDROME()
 * @method static Disease MUMMY_ROT()
 * @method static Disease MARROW_OOZE()
 * @method static Disease PESTILENCE()
 * @method static Disease RISEN_SICKNESS()
 * @method static Disease ROSEN_DOOM()
 * @method static Disease SEWER_PLAGUE()
 * @method static Disease SHADOWED_TYPHUS()
 * @method static Disease SIGHT_ROT()
 * @method static Disease SLIMY_DOOM()
 * @method static Disease STOMACH_PARASITES()
 * @method static Disease TELEPATHITIS()
 * @method static Disease TERROR_PLAGUE()
 * @method static Disease TINEA_CRURIS()
 * @method static Disease TSATHOGGAN_ROTTING_DISEASE()
 */
class Disease extends AbstractNamedEnum
{
    private const ASHEN_INFLAMMATION = [
        'name' => 'Inflammation cendrée',
        'description' => '<p>
        Cette maladie est une fièvre montante qui finit par consumer ses victimes dans le feu. Ceux qui entrent en 
        contact avec des individus infectés ou les cendres des victimes immolées doivent effectuer un jet de 
        sauvegarde de Constitution DD13 ou être infectés. Les créatures qui ne respirent pas reçoivent un avantage à ce 
        jet de sauvegarde.
        <br/><br/>
        Les premiers symptômes de la maladie apparaissent en 1d4 + 1 heures, lorsque les personnes infectées commencent 
        à développer une forte fièvre et une éruption cutanée. La créature infectée subit 1 niveau d\'épuisement ; 
        alors que la créature infectée a au moins 1 niveau d\'épuisement, elle est également vulnérable aux dégâts de 
        froid. À la fin de chaque repos long, la créature infectée doit effectuer un jet de sauvegarde de Constitution 
        DD15. En cas d\'échec, ils subissent 2 (1d4) dégâts de feu pour chaque niveau d\'épuisement qu\'ils possèdent 
        et gagnent un autre niveau d\'épuisement. En cas de réussite, ils ne subissent aucun dégât et perdent un niveau 
        d\'épuisement. Si la créature atteint le niveau 5 d\'épuisement, ils explosent en flammes vives et deviennent 
        ensuite un tas de cendres libres. Toutes les créatures situées à 3 mètres ou moins de la créature mourante 
        doivent effectuer un jet de sauvegarde de Dextérité DD14 ou subir 27 (5d10) dégâts de feu en cas d\'échec, ou 
        la moitié des dégâts en cas de réussite. Une fois que la créature infectée perd tous les niveaux d\'épuisement, 
        la maladie prend fin.</p>',
    ];

    private const BLOOD_FEVER = [
        'name' => 'Fièvre Sanguine',
        'description' => '<p>
        Quiconque consomme de la nourriture ou de l\'eau contaminée par le sang d\'un titan doit réussir un jet de 
        sauvegarde de Constitution DD14 ou subir les effets de la maladie connue sous le nom de fièvre sanguine. La 
        période d\'incubation de la fièvre sanguine est de 24 heures. Lorsque l\'incubation est terminée, la victime 
        réduit son maximum de points de vie de 5 (1d10), s\'empoisonne jusqu\'à ce que la maladie soit guérie et 
        commence à avoir des convulsions, transpirant littéralement du sang. La victime hallucine également des visions 
        inquiétantes de violence et de mort. Toutes les 24 heures qui s\'écoulent, l\'infecté doit répéter le jet de 
        sauvegarde de Constitution, réduisant son maximum de points de vie de 5 (1d10) en cas d\'échec.
        <br/><br/>
        Cette réduction du maximum de points de vie de la créature dure jusqu\'à ce que la maladie soit guérie. La 
        maladie est guérie sur un succès. La victime meurt si la maladie réduit son maximum de points de vie à 0. Un 
        personnage qui meurt de cette manière se transforme en zombie sanguinaire un jour après sa mort.
        <br/><br/>
        Quiconque entre en contact physique avec une victime de la fièvre sanguine risque également d\'attraper la 
        maladie, bien que le jet de sauvegarde de Constitution contre la fièvre sanguine contractée de cette manière 
        soit DD10. Si un personnage réussit un jet de sauvegarde, il est immunisé contre la fièvre sanguine pendant 48 
        heures.
        <br/><br/>
        La fièvre sanguine peut être guérie par un sort de restauration moindre et d\'autres effets similaires.</p>',
    ];

    private const BLOODMOLD = [
        'name' => 'Moisissure Sanguine',
        'description' => '<p>
        Les blessures infectées par des spores vénéneuses flottant dans l\'air de la jungle peuvent transporter des 
        toxines dangereuses dans la circulation sanguine, ce qui rend difficile la guérison des blessures. Lorsqu\'une 
        créature est réduite à moins de la moitié de ses points de vie alors qu\'elle est à l\'intérieur de la jungle 
        pendant la nuit, elle doit réussir un jet de sauvegarde de Constitution DD13 sous peine d\'être infectée. Il 
        faut 2d4 heures pour que les symptômes de la moisissure sanguine se manifestent chez une créature infectée. Les 
        symptômes comprennent des étourdissements et une désorientation. La créature infectée ne restaure que deux fois 
        moins de points de vie que d\'habitude grâce aux soins magiques, lorsqu\'elle dépense des dés de vie pendant un 
        repos court ou après un repos long. À la fin de chaque long repos, une créature infectée doit effectuer un jet 
        de sauvegarde de Constitution DD13. Après 2 jets de sauvegarde réussis, la créature se remet de la maladie.
        </p>',
    ];

    private const BUBONIC_PLAGUE = [
        'name' => 'Peste Bubonique',
        'description' => '<p>
        La première fois qu\'une créature s\'approche à moins de 1m50 d\'une créature porteuse de la peste bubonique, 
        et ensuite toutes les heures à proximité, elle doit réussir un jet de sauvegarde de Constitution DD15. Le début 
        est après 1d4 jours, après quoi la victime devient contagieuse. La victime perd 1d4 points de constitution et 1 
        point de charisme par jour par la suite ; ils peuvent tenter un jet de sauvegarde de Constitution DD15 chaque 
        jour pour éviter les dégâts de la capacité ; deux jets de sauvegarde réussis d\'affilée éliminent la maladie. 
        Si la victime est toujours en vie après 5 jets de sauvegarde ratés, elle devient incapable de souffrir de 
        crampes et de douleurs. Une fois que la victime n\'est plus malade, la perte de capacité guérit normalement à 
        un point de chaque capacité après chaque repos long.</p>',
    ];

    private const CACKLE_FEVER = [
        'name' => 'Fièvre du caquet',
        'description' => '<p>
        Cette maladie cible les humanoïdes, bien que les gnomes soient étrangement immunisés. Aux prises avec cette 
        maladie, les victimes succombent fréquemment à des fous rires, donnant à la maladie son nom commun et son 
        surnom morbide : « l\'incontinence ».
        <br/><br/>
        Les symptômes se manifestent 1d4 heures après l\'infection et comprennent de la fièvre et une désorientation. 
        La créature infectée gagne un niveau d\'épuisement qui ne peut être supprimé tant que la maladie n\'est pas 
        guérie.
        <br/><br/>
        Tout événement qui cause un stress important à la créature infectée, y compris entrer en combat, subir des 
        dégâts, ressentir de la peur ou faire un cauchemar, force la créature à effectuer un jet de sauvegarde de 
        Constitution DD13. En cas d\'échec, la créature subit 5 (1d10) dégâts psychiques et est frappée d\'un fou rire 
        pendant 1 minute. La créature peut retenter le jet de sauvegarde à la fin de chacun de ses tours, mettant fin 
        au fou rire et à l\'incapacité en cas de réussite.
        <br/><br/>
        Toute créature humanoïde qui commence son tour à moins de 3 mètres d\'une créature infectée en proie à un rire 
        fou doit réussir un jet de sauvegarde de Constitution DD10 ou être également infectée par la maladie. Une fois 
        qu\'une créature réussit ce jet, elle est immunisée contre le rire fou de cette créature infectée pendant 24 
        heures.
        <br/><br/>
        À la fin de chaque repos long, une créature infectée peut effectuer un jet de sauvegarde de Constitution DD13. 
        En cas de sauvegarde réussie, le DD de cette sauvegarde et de la sauvegarde pour éviter une attaque de fou rire 
        diminue de 1d6.
        <br/><br/>
        Lorsque le DD du jet de sauvegarde tombe à 0, la créature se remet de la maladie. Une créature qui échoue à 
        trois de ces jets de sauvegarde gagne une forme de folie indéfinie déterminée au hasard.</p>',
    ];

    private const CALCIFICATION_VIRUS = [
        'name' => 'Virus de la calcification',
        'description' => '<p>
        Ce virus transforme lentement les fluides corporels de la victime en une substance solide calcifiée. Les 
        personnes atteintes étouffent généralement avec leur propre sang bien avant que leur sang ne se calcifie 
        complètement. Au fur et à mesure que le corps se décompose, les fluides se calcifient également, jusqu\'à ce 
        que le corps se transforme en os et en poussière.
        <br/><br/>
        La poussière est porteuse de la maladie et peut devenir infectieuse si elle est inhalée ou ingérée. Les 
        créatures exposées à la maladie doivent réussir un jet de sauvegarde de Constitution DD14 ou être infectées.
        <br/><br/>
        En une journée, la victime commence à ressentir de la douleur et de la fatigue. Après 3 jours, la créature 
        commence à développer des zones rugueuses sur la peau qui sont dures et épaisses.
        <br/><br/>
        A ce stade, la créature a un désavantage aux tests de Constitution (mais pas aux jets de sauvegarde). Au bout 
        de 8 jours, les créatures deviennent aveugles et développent des articulations raides, ce qui leur désavantage 
        aux tests de Dextérité et réduit leur vitesse de déplacement de moitié.
        <br/><br/>
        Après 18 jours, la créature infectée doit effectuer un jet de sauvegarde de Constitution DD12 toutes les heures 
        ou commencer à suffoquer. Il n\'y a pas de remède contre le virus du cristal.
        <br/><br/>
        Les créatures souffrant de la maladie restent infectées jusqu\'à ce que la maladie soit guérie par magie ou 
        qu\'elles meurent.
        <br/><br/>
        Curieusement, si une créature infectée change de forme, elle peut tenter une autre sauvegarde pour mettre fin à 
        la maladie lorsqu\'elle revient à sa forme d\'origine.</p>',
    ];

    private const GOBLIN_GOUT = [
        'name' => 'Goutte du gobelin',
        'description' => '<p>
        Chez les créatures gobelinoïdes, cette maladie provoque un niveau de réponse enragée, les rendant susceptibles 
        de se battre au lieu d\'utiliser leurs armes, et de braver irrationnellement leurs attaques. Ils font toutes 
        les attaques avec avantage, mais toutes les attaques contre eux ont un avantage. À n\'importe quel round de 
        combat, un goblinoïde malade a 50% de chances de tenter de mordre son adversaire plutôt que d\'utiliser une 
        autre attaque. Toute créature mordue par un goblinoïde infecté doit réussir un jet de sauvegarde de 
        Constitution DD15 ou contracter la maladie. Après une période d\'incubation de 1d3 jours, la victime développe 
        les symptômes de la maladie. Si la victime n\'est pas un goblinoïde, la créature développe des articulations 
        douloureuses et une raideur.
        <br/><br/>
        Le mouvement de la victime est réduit de 6 mètres (1m5 minimum) et elle a un désavantage aux jets d\'attaque, 
        aux jets de sauvegarde de Dextérité et aux tests de compétence.
        <br/><br/>
        La victime peut tenter un jet de sauvegarde de Constitution DD15 après un repos prolongé. Avec deux réussites 
        consécutives, ils stoppent les effets de la maladie.</p>',
    ];

    private const GRUNGE = [
        'name' => 'La Crasse',
        'description' => '<p>
        Cette maladie se transmet par contact avec des déchets infestés de gobelins. Il provoque une vision floue et 
        une légère paralysie. Toute créature entrant en contact avec du matériel infecté doit réussir un jet de 
        sauvegarde de Constitution DD14 ou être infectée. Une heure plus tard, la maladie s\'installe et la créature 
        subit un malus de -1 à tous ses jets de touche pendant 24 heures. Une créature qui obtient un 1 ou moins à son 
        jet de sauvegarde doit effectuer un jet de sauvegarde supplémentaire après 24 heures. Une créature qui échoue à 
        ce deuxième jet de sauvegarde perd définitivement 1 point de force. La force ne peut être récupérée qu\'avec 
        une restauration supérieure ou un sort de souhait.</p>',
    ];

    private const GREEN_DECAY = [
        'name' => 'Carie verte',
        'description' => '<p>
        La chair d\'une créature atteinte de cette maladie est lentement consommée par un champignon extraterrestre 
        virulent. Tant que la maladie persiste, la créature a un désavantage aux jets de Charisme, de Sagesse et aux 
        jets de sauvegarde de Sagesse, et elle est vulnérable aux dégâts d\'acide, de feu et nécrotiques. Une créature 
        affectée doit faire un jet de sauvegarde Constitution à la fin de chacun de ses tours. En cas d\'échec, la 
        créature subit 1d6 dégâts nécrotiques et son maximum de points de vie est réduit d\'un montant égal aux dégâts 
        nécrotiques subis. Si la créature obtient trois succès à ces jets de sauvegarde avant qu\'elle n\'obtienne 
        trois échecs, la maladie prend fin immédiatement (mais les dégâts et la réduction maximale des points de vie 
        restent en vigueur). Si la créature obtient trois échecs à ces jets de sauvegarde avant d\'obtenir trois 
        succès, la maladie dure pendant 1h et aucun autre jet de sauvegarde n\'est autorisé.</p>',
    ];

    private const HANTA_VIRUS = [
        'name' => 'Virus Hanta',
        'description' => '<p>
        La maladie est généralement contractée par contact avec des rongeurs et des déchets de rongeurs. Normalement, 
        un jet de sauvegarde de Constitution est nécessaire pour empêcher la contagion, le DD dépendant de la quantité 
        de matériel contagieux contacté et de la méthode de contact. Le virus Hanta incube 1d8+7 jours avant de 
        commencer son évolution mortelle. Pendant les quatre premiers jours après l\'étape d\'incubation, une créature 
        infectée par cette maladie doit effectuer un jet de sauvegarde de Constitution DD16 toutes les 24 heures. La 
        victime acquiert un niveau d\' épuisement pour chaque échec. L\'épuisement ne peut être supprimé tant que la 
        maladie persiste. À partir du cinquième jour, les jets de sauvegarde ratés indiquent que la victime perd 2d10 
        points de vie de leur maximum ainsi que gagner un niveau d\'épuisement. Une fois la maladie guérie, 
        l\'épuisement peut être guéri par le repos ou par magie, et la perte des points de vie maximum est restaurée 
        après un repos long. À moins d\'être guérie par magie, cette maladie est finalement mortelle.</p>',
    ];

    private const HEMOPHILIA = [
        'name' => 'Hémophilie',
        'description' => '<p>
        L\'hémophilie est généralement une maladie génétique et, en tant que telle, n\'est normalement acquise que par 
        des moyens magiques. Une créature qui a cette maladie perd 1 pv par round jusqu\'à ce qu\'elle soit magiquement 
        guérie après avoir subi des dégâts contondants, perforants ou tranchants. Les dégâts perdus par round sont 
        cumulables avec les sources de dégâts supplémentaires.</p>',
    ];

    private const LEECH_FEVER = [
        'name' => 'Fièvre de sangsue',
        'description' => '<p>
        Cette maladie causant l\'anémie provient de la morsure de sangsues infectées.
        <br/><br/>
        Il n\'est sinon pas contagieux. Toute personne mordue par une sangsue infectée doit effectuer un jet de 
        sauvegarde de Constitution DD16 ou contracter la maladie. La maladie incube pendant 1d3 jours avant de 
        provoquer des symptômes. À ce stade, l\'anémie provoque deux niveaux d\'épuisement. L\'épuisement ne peut être 
        éliminé tant que la maladie est encore active. Une fois les symptômes apparus, la victime peut tenter un jet de 
        sauvegarde de Constitution DD16 après chaque repos long. Deux jets de sauvegarde réussis consécutifs indiquent 
        que la victime a éliminé la maladie. L\'épuisement peut alors être éliminé normalement. Restauration mineure ou 
        guérison magique similaire élimine la maladie à tout moment.</p>',
    ];

    private const MARBLEWIFE_SYNDROME = [
        'name' => 'Syndrome de la femme marbrée',
        'description' => '<p>
        Bien que certaines légendes parlent de maîtres artisans créant des amants en pierre, cette maladie tire son 
        nom d\'une telle fable. La maladie transforme ses victimes en pierre et peut être contractée par une victime 
        encore non-pétrifiée, ainsi que pendant les saisons de «spores» au cours desquelles la victime pétrifiée dégage 
        une excrétion fongique qui peut affecter davantage d\'autres créatures. Ces spores saisonnières compliquent les 
        tentatives de dépétrification des victimes, ce qui conduit souvent à la destruction ou à la mise au tombeau de 
        victimes qui pourraient autrement être guéries.
        <br/><br/>
        Après un contact physique avec une créature infectée, une victime doit réussir un jet de sauvegarde de 
        Constitution DD12 ou être infectée. Si une statue infectée est en train de sporer, une créature qui s\'approche 
        à moins de 6 mètres de la statue doit effectuer le même jet de sauvegarde. Les statues de spores sont 
        recouvertes d\'une substance floconneuse qui ressemble à de la peinture séchée.
        <br/><br/>
        Une victime commence à perdre des points de son score de Dextérité au rythme de 2d4 par période de 24 heures. 
        Si cela se traduit par un score de Dextérité de 1 ou moins, la victime devient pétrifiée. Tout moyen de 
        dépétrifier la victime éliminera également la maladie, et les personnes infectées peuvent également bénéficier 
        de tout moyen qui élimine la maladie ou qui élimine l\' état pétrifié, qui guérissent tous deux l\'infecté.
        <br/><br/>
        Il y a des histoires redoutées de villes entières succombant à cette maladie, attirant les curieux et les 
        cupides, pour s\'ajouter à la sombre ménagerie de statues réalistes.</p>',
    ];

    private const MUMMY_ROT = [
        'name' => 'Pourriture de la momie',
        'description' => '<p>
        La pourriture de la momie provoque d\'abord une dessiccation puis une lente décomposition.
        <br/><br/>
        La vision commence tôt lorsque le corps perd des fluides, suivi d\'une faiblesse toujours croissante. La mort 
        survient tard, bien après que la victime soit complètement immobilisée par faiblesse.
        <br/><br/>
        Lorsqu\'un humanoïde est exposé à la maladie, soit par des moyens magiques, soit en inhalant la poussière 
        d\'une ancienne victime, la créature doit effectuer un jet de sauvegarde de Constitution. Le DD du jet de 
        sauvegarde dépend de la puissance de la malédiction pour la contagion magique et est de 14 pour respirer de la 
        poussière.
        <br/><br/>
        La pourriture de la momie se manifeste généralement immédiatement par une forte soif. La victime doit doubler 
        sa consommation d\'eau ou gagner un niveau d\'épuisement. Toutes les 24 heures, la victime doit effectuer un 
        jet de sauvegarde de Constitution DD17. Chaque échec ajoute un symptôme du tableau ci-dessous jusqu\'à ce que 
        la mort survienne. Deux passages consécutifs stoppent la maladie mais ne suppriment pas les symptômes 
        existants. Supprimer la malédiction détruit la maladie et guérit tous les dégâts. Un seul sort de restauration 
        partielle supprime le symptôme le plus récent encore présent.
        <br/><br/>
        Symptômes par ordre d\'apparition en fonction du nombre de jets de sauvegarde ratés:</p>',
        'effects' => [
            'Gagnez un niveau d\'épuisement et perdez 1d4 points de Charisme.',
            'Vision réduite de 10m.',
            'Gagnez un niveau d\'épuisement et perdez 1d4 points de Charisme.',
            'Aveugle.',
            'Gagnez un niveau d\'épuisement et perdez 1d4 points de Charisme.',
            'Gagnez un niveau d\'épuisement et le Charisme est maintenant à 1.',
            'Perdez 2d6 chacun de Constitution, Dextérité et Force.',
            'Constitution, Dextérité et Force sont désormais de 1.',
            'Perdez 1 pv pour chaque sauvegarde ratée jusqu\'à la mort.',
        ],
        'helper' => 'Le Charisme ne descend pas en dessous de 1. Si la Constitution, la Dextérité ou la Force tombent à 
        0, la victime est morte.',
    ];

    private const MARROW_OOZE = [
        'name' => 'Limon de moelle',
        'description' => '<p>
        Bien qu\'il ne s\'agisse pas techniquement d\'une maladie, le parasite de la moelle osseuse infecte néanmoins 
        ses victimes par sa présence. Un limon de moelle pénètre dans le corps par une blessure ou un orifice exposé 
        (potentiellement la bouche ou les oreilles) pendant que la victime dort et commence à consommer la moelle de 
        l\'hôte. La victime peut effectuer un jet de sauvegarde de Constitution DD13 pour résister à l\'infection.
        <br/><br/>
        En cas de succès, le limon de moelle s\'échappe en évitant d\'être détruit par le système immunitaire de 
        l\'hôte. En cas d\'échec, l\'hôte est infecté mais se sent en réalité mieux que jamais. Les créatures infectées 
        commencent à paraître plus attrayantes et sont guéries de toute déformation osseuse, affection osseuse ou 
        maladie du sang.
        <br/><br/>
        Chaque jour par la suite, une créature infectée doit effectuer un jet de sauvegarde de Constitution DD13. En 
        cas d\'échec, l\'hôte réduit son maximum de points de vie de 10, à un minimum de 1. Une fois que l\'hôte a 
        atteint ce minimum, le suintement de moelle se divise et transforme horriblement les membres de l\'hôte en de 
        nouveaux suintements de moelle dans un processus qui ressemble à la fonte de un morceau de cire. L\'hôte est 
        laissé sans membres, mais son maximum de points de vie est restauré.
        <br/><br/>
        Ces victimes courent un risque accru de développer une folie traumatique.
        <br/><br/>
        Un hôte qui réussit deux jets de sauvegarde (ils n\'ont pas besoin d\'être consécutifs) peut expulser la 
        créature, bien que le processus d\'expulsion soit douloureux et difficile.
        <br/><br/>
        Une fois que le processus d\'expulsion commence, l\'hôte commence à saigner de chaque orifice et perd des 
        fluides corporels par des émissions corporelles violentes et fréquentes.
        <br/><br/>
        Ce processus réduit les dégâts infligés par perçage et 2d6 dégâts de poison à moins que l\'hôte ne reçoive des 
        soins médicaux appropriés (nécessitant un test de Sagesse (Médecine) DD15 ). Si une victime meurt à cause de ce 
        processus violent, ses entrailles sont liquéfiées et 1d2 suintement de moelle émergent du corps.</p>',
    ];

    private const PESTILENCE = [
        'name' => 'Peste',
        'description' => '<p>
        La peste est généralement attrapée au contact des fluides corporels d\'une créature porteuse de la maladie. À 
        chaque round où une créature est en contact avec une créature infectée ou une partie de ses fluides, elle doit 
        réussir un jet de sauvegarde de Constitution DD14 ou contracter la maladie. Toute personne infectée commencera 
        à perdre des points de vie à raison d\'un par heure jusqu\'à la mort. Un jet de sauvegarde de Constitution DD10 
        est autorisé chaque heure pour éviter la perte de points de vie pendant cette heure, mais le processus se 
        poursuit ensuite. La guérison magique augmente les points de vie de la victime, mais la progression de la 
        maladie se poursuit après la guérison. Restauration partielle supprime complètement la maladie et rétablit la 
        santé de la victime, bien qu\'il ne restaure pas les points de vie perdus. Si la victime meurt des suites de la 
        maladie, le corps s\'élève comme un zombie pestiféré en 1d4+1 rounds. Un aspersion d\'eau bénite ou un sort de 
        restauration partielle lancé sur le corps empêche que cela se produise. Le corps peut être ressuscité d\'entre 
        les morts normalement, mais pas tant qu\'il est encore « vivant » en tant que zombie pestiféré.</p>',
    ];

    private const RISEN_SICKNESS = [
        'name' => 'Maladie du ressuscité',
        'description' => '<p>
        Parmi toutes les maladies, peu sont aussi redoutées que la maladie du ressucité. Réputée pour avoir renversé 
        des empires, cette maladie crée des morts-vivants avec une efficacité stupéfiante et assure au moins une 
        culture de peur et de paranoïa.
        <br/><br/>
        Même la rumeur d\'une maladie du ressuscité suffit à rallier des foules pour l\'éradiquer, et les rois sont 
        connus pour anéantir des communautés entières par mesure de sécurité, que cela soit mérité ou non.
        <br/><br/>
        Ce qui n\'est pas certain, c\'est comment la maladie commence, mais quand c\'est le cas, les cadavres 
        commencent à se lever et à avoir faim de chair. Cette maladie se propage souvent aux cadavres morts depuis 
        longtemps qui n\'ont aucune résistance à la maladie et grossissent les rangs des ressuscités.
        <br/><br/>
        Les cadavres ambulants sont considérés comme des zombies, remplaçant leur attaque de coup par une attaque de 
        morsure qui transmet l\'infection (+3 pour toucher, portée de 1m50, une cible, 1d4+1 dégâts perforants). Ces 
        cadavres infectent leurs victimes avec une maladie qui tue et soulève encore plus de cadavres ambulants. Un 
        individu exposé à une morsure doit effectuer un jet de sauvegarde de Constitution DD17. Un échec signifie que 
        la victime est infectée. Après chaque heure d\'infection, la créature infectée perd 2d6 points de vie par 
        rapport à son maximum. Si ce maximum atteint zéro, la créature infectée meurt et se transforme en zombie dans 
        la minute qui suit sa mort.
        <br/><br/>
        Il n\'y a pas de remède simple pour cette maladie en dehors de la magie, bien qu\'il existe des herbes 
        spéciales qui peuvent éliminer l\'infection avant la mort. Une victime infectée qui est guérie est également 
        restaurée à son maximum de points de vie normal. Il existe des rumeurs selon lesquelles des individus 
        aléatoires seraient naturellement immunisés contre la maladie, bien que de tels récits disent également que les 
        immunisés sont porteurs et contribuent involontairement à la propagation de la maladie.</p>',
    ];

    private const ROSEN_DOOM = [
        'name' => 'Rosen Doom',
        'description' => '<p>
        La maladie connue sous le nom de Rosen Doom est identifiée par la teinte rougeâtre envoûtante que prennent les 
        cadavres affectés. Le sang des victimes décédées reste également d\'une étrange couleur rougeâtre même des 
        décennies après leur mort, préservant partiellement les cadavres et conservant la virulence de la maladie aussi 
        longtemps.
        <br/><br/>
        Cette maladie se transmet principalement par contact avec du sang infecté, bien que tout liquide corporel 
        puisse transmettre la maladie. Une créature qui entre en contact avec un tel fluide doit effectuer un jet de 
        sauvegarde de Constitution DD16, devenant infectée en cas d\'échec.
        <br/><br/>
        Les symptômes se développent en 1d4 jours. Les créatures infectées subissent 1 niveau d\'épuisement. 
        Lorsqu\'elle est épuisée, la créature tousse du sang et développe des plaies pleurantes sur tout son corps. À 
        la fin de chaque repos long, la créature doit effectuer un jet de sauvegarde de Constitution DD16, subissant 11 
        (2d6) dégâts nécrotiques en cas d\'échec et gagnant 1 niveau d\'épuisement.
        <br/><br/>
        En cas de sauvegarde réussie, le DD du jet de sauvegarde est réduit de 1d4+1. Lorsque le DD du jet de 
        sauvegarde passe à 0, la maladie prend fin.</p>',
    ];

    private const SEWER_PLAGUE = [
        'name' => 'Peste des égouts',
        'description' => '<p>
        La peste des égouts est un terme générique pour une large catégorie de maladies qui incubent dans les égouts, 
        les tas d\'ordures et les marécages stagnants, et qui sont parfois transmises par des créatures qui habitent 
        dans ces zones, comme les rats et les otyughs.
        <br/><br/>
        Lorsqu\'une créature humanoïde est mordue par une créature porteuse de la maladie, ou lorsqu\'elle entre en 
        contact avec des ordures ou des abats contaminés par la maladie, la créature doit réussir un jet de sauvegarde 
        de Constitution DD11 ou être infectée.
        <br/><br/>
        Il faut 1d4 jours pour que les symptômes de la peste des égouts se manifestent chez une créature infectée. Les 
        symptômes comprennent la fatigue et les crampes. La créature infectée souffre d\'un niveau d\'épuisement et ne 
        récupère que la moitié du nombre normal de points de vie en dépensant des dés de vie et aucun point de vie en 
        terminant un repos long.
        <br/><br/>
        À la fin de chaque repos long, une créature infectée doit effectuer un jet de sauvegarde de Constitution DD11. 
        En cas d\'échec, le personnage gagne un niveau d\'épuisement. En cas de sauvegarde réussie, le niveau 
        d\'épuisement du personnage diminue d\'un niveau. Si un jet de sauvegarde réussi réduit le niveau d\'épuisement 
        de la créature infectée en dessous de 1, la créature récupère de la maladie.</p>',
    ];

    private const SHADOWED_TYPHUS = [
        'name' => 'Typhus ombragé',
        'description' => '<p>
        Cette maladie magique infecte l\'ombre d\'une victime, la faisant devenir lentement une ombre sombre.
        <br/><br/>
        Cette maladie peut être contractée si une créature infectée se tient dans votre ombre ou en se cachant dans la 
        même ombre qu\'une créature infectée. Les créatures de type outsider et aberration sont immunisées contre les 
        effets de la maladie, mais peuvent toujours porter et transmettre la maladie à d\'autres.
        <br/><br/>
        Les symptômes se développent au coucher du soleil après la contraction.
        <br/><br/>
        À ce moment-là, la victime commence à se sentir faible et son ombre apparaît plus sombre que d\'habitude. La 
        victime commence à craindre toute lumière et ressent de la douleur et de l\'angoisse si elle est exposée au 
        soleil (cette exposition ne cause aucun dommage physique). La créature doit faire un test de Constitution DD15 
        pour entrer volontairement dans une lumière vive.
        <br/><br/>
        Si on la laisse s\'attarder dans l\'ombre, la maladie s\'aggrave et la créature infectée commence à avoir 
        l\'air d\'être dans l\'ombre même lorsqu\'elle est à la lumière. Après trois jours sans exposition à aucune 
        lumière vive, la créature commence une transformation en ombre qui se déroule du coucher du soleil à l\'aube 
        après le troisième jour d\'obscurité prolongée. La créature infectée devient une ombre capable de créer 
        d\'autres ombres par la maladie plutôt que par sa capacité de drain de force (qui autrement reste inchangée).
        <br/><br/>
        Le seul remède contre cette maladie est une exposition prolongée au soleil (1 heure ininterrompue), des dégâts 
        radiants (égal à la moitié des points de vie maximum ), ou une restauration partielle. Les sorts et les 
        capacités qui suppriment les malédictions ou les maladies mettront également fin à cette maladie.</p>',
    ];

    private const SIGHT_ROT = [
        'name' => 'Pourriture visuelle',
        'description' => '<p>
        Cette infection douloureuse provoque des saignements des yeux et finit par aveugler la victime.
        <br/><br/>
        Une bête ou un humanoïde qui boit de l\'eau contaminée par la pourriture visuelle doit réussir un jet de 
        sauvegarde de Constitution DD15 ou être infecté. Un jour après l\'infection, la vision de la créature commence 
        à devenir floue. La créature subit un malus de -1 aux jets d\'attaque et aux tests de caractéristique basés sur 
        la vue. À la fin de chaque repos long après l\'apparition des symptômes, la pénalité s\'aggrave de 1. 
        Lorsqu\'elle atteint -5, la victime est aveuglée jusqu\'à ce que sa vue soit restaurée par magie telle qu\'une 
        restauration moindre ou une guérison.
        <br/><br/>
        La pourriture visuelle peut être guérie à l\'aide d\'une fleur rare appelée Euphraise, qui pousse dans certains 
        marécages. En une heure, un personnage qui maîtrise un kit d\'herboristerie peut transformer la fleur en une 
        dose de pommade. Appliquée sur les yeux avant un repos prolongé, une dose empêche l\'aggravation de la maladie 
        après ce repos. Après trois doses, la pommade guérit entièrement la maladie.</p>',
    ];

    private const SLIMY_DOOM = [
        'name' => 'Perte visqueuse',
        'description' => '<p>
        La Perte visqueuse est attrapé au contact des restes d\'une créature qui meurt de la maladie pourrissante de 
        Tsathoggan. Pour chaque round de contact avec la chair dissoute d\'une telle victime, une créature doit réussir 
        un jet de sauvegarde de Constitution DD16 ou succomber à la maladie sans période d\'incubation initiale. La 
        Perte visqueuse transforme une personne en gélatine de l\'intérieur. Chaque jour, la victime doit réussir un 
        jet de sauvegarde de Constitution DD16 sous peine de perdre 1d4 points de Constitution. À 0 Constitution, ils 
        ne sont rien de plus qu\'un sac charnu de pus et de mousse sanglante. Ceux qui sont guéris par un sort ou qui 
        réussissent deux jets de sauvegarde quotidiens d\'affilée doivent effectuer un jet de sauvegarde supplémentaire 
        pour chaque jour où ils ont subit de la perte de Constitution. Si ces jets de sauvegarde échouent, la victime 
        perd définitivement un point de Constitution par jet de sauvegarde raté.</p>',
    ];

    private const STOMACH_PARASITES = [
        'name' => 'Parasites de l\'estomac',
        'description' => '<p>
        Invisibles à l\'œil nu, les voyageurs savent qu\'il vaut mieux ne pas boire dans les eaux profondes de la 
        nature de peur d\'ingérer ces parasites de l\'estomac. Lorsqu\'une créature boit ou consomme de l\'eau 
        contaminée par des parasites, la créature doit réussir un jet de sauvegarde de Constitution DD12 ou être 
        infectée. Il faut 1d4 jours pour que les symptômes d\'un parasite de l\'estomac se manifestent chez une 
        créature infectée. Les symptômes comprennent des crampes d\'estomac, une peau tendue et une tension 
        musculaire extrême. Chaque semaine, la créature infectée double la quantité de nourriture et d\'eau dont elle a 
        besoin pour survivre. Une créature infectée qui ne reçoit pas assez de nourriture ne récupère aucun soin après 
        un repos court ou long. Une créature peut guérir la maladie avec une plus restauration supérieure, ou une 
        attaque ou un sort qui inflige 10 points de dégâts de force par semaine d\'infection.</p>',
    ];

    private const TELEPATHITIS = [
        'name' => 'Télépathite',
        'description' => '<p>
        État étrange et inhabituel, la télépathite se transmet par contact mental avec une créature infectée, ou plus 
        rarement par contact physique avec le tissu cérébral. Un tel contact nécessite une sauvegarde de Constitution 
        DD13 si un contact physique est établi, ou une sauvegarde de Sagesse DD11 si un contact mental est établi. 
        L\'échec entraîne une infection.
        <br/><br/>
        Une créature ainsi infectée peut commencer à lire dans les pensées, mais le fait de manière erratique, glanant 
        les pensées de surface de toutes les créatures proches. Grâce à la tension mentale, une créature infectée peut 
        affiner sa capacité et écouter des pensées spécifiques, conformément au sort de détection des pensées. Ils ne 
        peuvent utiliser cette capacité qu\'une seule fois et regagnent la capacité de le faire après chaque repos long.
        <br/><br/>
        Cependant, cela et d\'autres facteurs de stress mentaux font empirer la maladie et commencent à se détériorer. 
        Une victime infectée gagne un niveau d\'épuisement chaque fois qu\'elle s\'efforce d\'utiliser sa capacité à 
        détecter les pensées et gagne un niveau d\'épuisement supplémentaire si elle lit les pensées d\'une créature 
        avec un score d\'intelligence de 18 ou plus.
        <br/><br/>
        La créature infectée gagne également un niveau d\'épuisement si elle se trouve dans une foule de 10 personnes 
        ou plus pendant plus d\'une minute. À chaque niveau d\'épuisement gagné, l\'infecté souffre d\'un mal de tête 
        d\'intensité croissante.
        <br/><br/>
        Lorsque la créature infectée atteint 5 niveaux d\'épuisement, sa tête explose et elle meurt. Cela ne peut être 
        évité que par une privation sensorielle prolongée et un repos. La créature infectée ne peut avoir aucun niveau 
        d\'épuisement ni être exposée à des pensées pendant au moins 3 jours. Si ces conditions sont remplies, ils ne 
        sont plus infectés.
        <br/><br/>
        Curieusement, les créatures qui ressemblent à des cerveaux sont sensibles à cette maladie, qui se manifeste 
        davantage comme une maladie cancéreuse dépérissante. Les dévoreurs d\'intellect, les grells et les créatures 
        consommatrices de cerveaux sont probablement des vecteurs de cette maladie, qui, heureusement, réduit parfois 
        leur population.</p>',
    ];

    private const TERROR_PLAGUE = [
        'name' => 'Peste de terreur',
        'description' => '<p>
        Appelée par certains la «peur enragée», cette maladie attaque l\'esprit et se propage par les fluides 
        corporels, le plus souvent par la sueur. Les victimes commencent à voir des hallucinations de leurs peurs les 
        plus sombres, attaquant souvent les autres et propageant l\'infection dans une tentative confuse de combattre 
        les visions de terreur.
        <br/><br/>
        Une victime exposée aux fluides corporels d\'une personne infectée doit faire un jet de Constitution DD12 ou 
        être infectée.
        <br/><br/>
        Le début se produit au cours de la première soirée de l\'infection et les symptômes se manifestent par des 
        visions de créatures ou de dangers dangereux et mortels, ou souvent le sujet des phobies ou des doutes de la 
        victime. Une fois que ces visions commencent, la victime transpire abondamment, au point qu\'elle s\'égoutte de 
        sueur. Les créatures infectées sont incapables de dormir ou de se reposer et ne tirent aucun avantage d\'un 
        repos court ou long autre que d\'éviter l\'épuisement. Une infection prolongée peut entraîner une folie à long 
        terme (voir les règles de la folie).
        <br/><br/>
        Un remède à cette maladie est de s\'arracher les yeux. Bien qu\'il ne s\'agisse pas d\'un remède populaire, il 
        peut être nécessaire pour éviter la folie et le manque de sommeil.
        <br/><br/>
        Cela ne guérit pas tant la maladie qu\'elle prévient les symptômes, et la victime peut toujours transmettre la 
        maladie à d\'autres (bien qu\'elle ne souffre pas de la sueur abondante que les visions induisent). Le seul 
        autre remède non magique doit être obtenu à partir du sang d\'une sorcière et nécessite un test de Sagesse 
        (Médecine) DD20 pour se distiller correctement en un remède efficace. Quel que soit le lien entre les sorcières 
        et cette maladie, cela ne sert qu\'à rendre chacune encore plus terrifiante.</p>',
    ];

    private const TINEA_CRURIS = [
        'name' => 'Tinea Cruris',
        'description' => '<p>
        Porter son armure trop longtemps et ne pas se laver a des conséquences en milieu humide. Lorsqu\'une créature a 
        passé une semaine sans se baigner ou plus de 3 jours sans nettoyer une armure qu\'elle porte chaque jour, la 
        créature doit réussir un jet de sauvegarde de Constitution DD8 ou être infectée. Il faut 2d6 jours pour que les 
        symptômes de Tinea Cruris se manifestent chez une créature infectée. Les symptômes comprennent des 
        démangeaisons et une odeur dans l\'aine. La créature infectée émet une odeur qui la désavantage aux tests de 
        capacité de Charisme effectués contre les créatures à moins de 3 mètres. Chaque semaine, les infectés font un 
        jet de sauvegarde de Constitution (DD 8 + 1 par jet précédent) ou la portée de sa puanteur augmente de 1,50 
        mètre (jusqu\'à 6m maximum). Après 5 jets de sauvegarde réussis, la créature récupère de la maladie.</p>',
    ];

    private const TSATHOGGAN_ROTTING_DISEASE = [
        'name' => 'Maladie de pourriture de Tsathoggan',
        'description' => '<p>
        Cette maladie vient d\'une malédiction et n\'est pas contagieuse. Une créature ainsi maudite doit effectuer un 
        jet de sauvegarde de Charisme DD14 pour résister à l\'attrait de Tsathhogga. Une fois affligée, la victime doit 
        réussir un jet de sauvegarde de Constitution DD16 toutes les 24 heures ou perdre 1d4 points de Constitution 
        lorsque ses entrailles se transforment en gélatine. Passer deux jets de sauvegarde consécutifs arrête la 
        maladie et les dégâts des points de capacité guérissent normalement à un moment par repos long ou peuvent être 
        guéris par magie.</p>',
    ];
}
