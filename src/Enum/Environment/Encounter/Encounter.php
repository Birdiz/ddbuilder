<?php

declare(strict_types=1);

namespace App\Enum\Environment\Encounter;

use App\Model\AbstractEnum;

class Encounter extends AbstractEnum
{
    private const HUMANOIDS = [
        'dices' => 1,
        'dice' => 6,
        'mod' => 0,
        'name' => 'humanoïdes en cape noire',
        'list' => [
            ': des cultistes hostiles pronant une divinité mauvaise',
            ': des cultistes non-hostiles',
            ': des monstres essayant de voyager paisiblement',
            ': des marchants d\'objets magiques itinérants',
            ': des paysans frappés par la terreur',
            ': des voyageurs qui aiment vraiment beaucoup les capes noires',
        ],
    ];

    private const CARAVAN = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'caravane',
        'list' => [
            ' de marchants',
            ' de brocantiers',
            ' de prisonniers',
            ' militaire',
            ' d\'artistes itinérants',
            ' d\'une tribue nomade',
        ],
    ];

    private const LARGE_GROUP = [
        'dices' => 4,
        'dice' => 6,
        'mod' => 5,
        'name' => '',
        'list' => [
            ' militaires',
            ' bandits',
            ' monstres',
            ' réfugiés',
        ],
    ];

    private const SMALL_GROUP = [
        'dices' => 1,
        'dice' => 10,
        'mod' => 2,
        'name' => '',
        'list' => [
            ' militaires en patrouille',
            ' gens du voyage',
            ' bandits vaincus',
            ' monstres',
        ],
    ];

    private const LONER = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'personne solitaire',
        'list' => [
            'qui est le survivant d\'une attaque',
            'qui est un éclaireur',
            'qui est une escroc',
            'qui est un hermite',
        ],
    ];

    private const OLDMAN = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'vieil homme fatigué qui demande s\'il peut partager le feu de camp en échange d\'un conte',
        'list' => [
            ': c\'est un piège',
            ': c\'est un fugitif',
            ': il va vers la ville la plus proche récupérer un héritage familial (25% de chances qu\'il meure dans son 
            sommeil',
            ': c\'est juste un veil homme',
        ],
    ];

    private const CHAINED_TREE = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'arbre pris dans de lourdes chaînes',
        'list' => [
            ', quelqu\'un doit vraiment détester cet arbre',
            ', c\'est un sylvanien désirant retrouver sa liberté',
            ', c\'est un sylvanien corrompu',
            ', il scelle un pouvoir maléfique',
        ],
    ];

    private const WOUNDED = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'chose bouge sur le bord de la route',
        'list' => [
            ' : une créature blessée qui souffre (50% de chances de survie)',
            ' : un homme blessé qui leur demande d\'apaiser ses souffrances',
            ' : une créature blessée qui fuit pour sa vie',
            ' : un soldat blessé',
        ],
    ];

    private const LONE_SHACK = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'cabane isolée',
        'list' => [
            ' qui est un hospice tenu par une veuve',
            ' qui est une brasserie qui propose une bière forte',
            ' une ferme d\'une famille pauvre',
            ' le repaire d\'une gentille sorcière',
        ],
    ];

    private const ANOTHER_PARTY = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'autre groupe d\'aventuriers',
        'list' => [
            ' qui sont hostiles à la moindre provocation',
            ' qui sont sympathiques',
            ' qui escortent un prisonnier',
            ' qui souhaitent partager un feu de camp',
            ' qui sont de hauts niveau',
            ' qui sont de fins et fiers nazes',
        ],
    ];

    private const FARMING_VILLAGE = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'petit village fermier',
        'list' => [
            ' qui subit actuellement une attaque',
            ' qui est frappé d\'une épidémie',
            ' qui connaît la famine',
            ' paisible',
            ' en plein festival',
            ' avec un vendeur qui vend un objet moins cher que son prix normal',
        ],
    ];

    private const HERD = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'troupeau d\'animaux bloquant la route',
        'list' => [
            ' : c\'est un piège',
            ' , ce sont des animaux domestiques qui passent tranquillement',
            ' : ils sont hors de contrôle',
            ' qui semblent perdus',
            ' qui sont sauvages et dangereux',
            ' : l\'éleveur a été tué et le troupeau est en train d\'être volé',
        ],
    ];

    private const CHECKPOINT_MILITARY = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'contrôle militaire',
        'list' => [
            ' qui cherche des fugitifs',
            ' qui sont en fait un groupe de bandits',
            ' qui enrôlent',
            ' qui cherche un objet volé de grande valeur',
            ' qui sont sauvages et dangereux',
            ' qui cherche un enfant disparu',
            ' qui avertissent du danger sur la route qui suit',
        ],
    ];

    private const BREWER = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'brasseur',
        'list' => [
            ' qui fait la meilleur bière',
            ' qui fait la pire bière',
            ' qui fait des potions',
            ' qui a pour but de distribuer des bières dans tout le pays',
        ],
    ];

    private const DRAGON = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'dragon',
        'list' => [
            ' noir qui ignore le groupe et continue son vol',
            ' d\'or qui offrent des conseils',
            ' d\'argent qui est toujours partant pour une bonne blague',
            ' rouge qui demande un droit de passage',
            ' qui donne un avertissement au groupe',
            ' qui mange l\'animal le plus proche',
            ' blessé',
            ' est là pour se battre',
        ],
    ];

    private const HUNTER = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'chasseur',
        'list' => [
            ' qui a trop perdu d\'argent au jeu',
            ' affamé',
            ' qui a besoin d\'aide',
            ' qui fait un brin de route avec le groupe',
        ],
    ];

    private const GNOMES = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'couple de gnomes',
        'list' => [
            ' qui cherchent des cobayes',
            ' testent un mécanisme',
            ' qui récoltent des échantillons',
            ' qui se querelle',
        ],
    ];

    private const BARD = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'barde',
        'list' => [
            ' qui voyage pour apprendre et partager des fables',
            ' qui voyage un moment avec le groupe',
            ' qui essaye de voler des choses',
            ' qui porte un instrument maudit',
        ],
    ];

    private const WIZARD = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'magicien qui dévale la route',
        'list' => [
            ' : "laissez passer, urgence!"',
            ' qui est pourchassé',
            ' qui est fou',
            ' qui arrête le groupe et demande une direction pour un lieu inconnu',
            ' qui est perdu',
            ' qui fait un jogging matinal',
        ],
    ];

    private const MASKED_HUMANOID = [
        'dices' => 1,
        'dice' => 1,
        'mod' => 0,
        'name' => 'humanoïde masqué en grandes robes chevauchant un cheval squelettique',
        'list' => [
            ' qui ignore le groupe',
            ' qui arrête le groupe et demande une direction pour un lieu inconnu',
            ' qui cherche quelqu\'un',
            ' qui porte un message de la plus haute importance au groupe',
        ],
    ];
}
