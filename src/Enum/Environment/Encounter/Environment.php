<?php

declare(strict_types=1);

namespace App\Enum\Environment\Encounter;

use App\Model\AbstractEnum;

class Environment extends AbstractEnum
{
    private const ARCTIC = 'Arctique';
    private const COASTAL = 'Littoral';
    private const DESERT = 'Désert';
    private const FOREST = 'Forêt';
    private const GRASSLAND = 'Plaine';
    private const HILL = 'Colline';
    private const MOUNTAIN = 'Montagne';
    private const SWAMP = 'Marais';
    private const UNDERDARK = 'Outreterre';
    private const UNDERWATER = 'Sous-marin';
    private const URBAN = 'Urbain';
}
