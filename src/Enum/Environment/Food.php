<?php

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

class Food extends AbstractEnum
{
    private const MUSHROOM = 'des champignons comestibles';
    private const HERBS = 'des plantes sauvages comestibls';
    private const DEER = 'un cerf';
    private const RABBIT = 'un lapin';
    private const SQUIRREL = 'un ecureuil';
    private const FISH = 'du poisson';
    private const EGGS = 'un nid avec des oeufs';
    private const ROOTS = 'des racines comestibles';
    private const CHICKEN = 'un poule sauvage';
    private const RATS = 'des rats';
    private const BEAVER = 'un castor';
    private const BOAR = 'un sanglier';
    private const WOLF = 'un loup';
    private const FRUITS = 'des fruits comestibles';
    private const BERRIES = 'des baies comestibles';
    private const MOSS = 'de la mousse comestible';
    private const VEGETABLES = 'des légumes sauvages comestibles';
    private const HONEYPOT = 'du miel d\'une ruche sauvage';
    private const LEAVES = 'des feuilles comestibles';
    private const INSECTS = 'des insectes';
    private const ROTTEN_ANIMAL = 'une carcasse d\'animal';
}
