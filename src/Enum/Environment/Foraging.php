<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

/**
 * Class Foraging.
 *
 * @method static Foraging ABUNDANT()
 * @method static Foraging LIMITED()
 * @method static Foraging VERY_LITTLE()
 */
class Foraging extends AbstractEnum
{
    private const ABUNDANT = 'Sources d\'eau et de nourriture abondantes';
    private const LIMITED = 'Sources d\'eau et de nourriture limitées';
    private const VERY_LITTLE = 'Très peu, voire pas de sources d\'eau et de nourriture';

    public static function getDDFromValue(string $value, bool $stringify): string|int
    {
        $value = match ($value) {
            static::ABUNDANT()->getValue() => 10,
            static::VERY_LITTLE()->getValue() => 20,
            default => 15,
        };

        return true === $stringify ? "DD$value" : $value;
    }

    public static function getRationFromValue(string $value): int
    {
        return match ($value) {
            static::ABUNDANT()->getValue() => 10,
            static::VERY_LITTLE()->getValue() => 2,
            default => 5,
        };
    }
}
