<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractNamedEnum;

/**
 * @method static Madness ACROPHOBIC()
 * @method static Madness AMNESIAC()
 * @method static Madness ARCANAPHOBIC()
 * @method static Madness BESTIAL_MIND()
 * @method static Madness CLAUSTROPHOBIC()
 * @method static Madness CONFUSED()
 * @method static Madness DELUSIONS_GRANDEUR()
 * @method static Madness DEVIANT()
 * @method static Madness DISSONANT_WHISPERS()
 * @method static Madness DRUNKARD()
 * @method static Madness FEAR_DARK()
 * @method static Madness GAMBLER()
 * @method static Madness HAUNTED()
 * @method static Madness HEMOPHOBIC()
 * @method static Madness HOMICIDAL()
 * @method static Madness HOPELESS()
 * @method static Madness INSOMNIAC()
 * @method static Madness KLEPTOMANIAC()
 * @method static Madness MASOCHIST()
 * @method static Madness MUMBLER()
 * @method static Madness PARANOID()
 * @method static Madness PYROMANIAC()
 * @method static Madness SCHIZOPHRENIC()
 * @method static Madness SELFISH()
 * @method static Madness VAMPIRISM()
 */
class Madness extends AbstractNamedEnum
{
    private const ACROPHOBIC = [
        'name' => 'Acrophobie',
        'short_term' => 'Vous avez un désavantage sur les tests de compétence de Force et de Dextérité impliquant des 
        hauteurs.',
        'long_term' => 'Vous ne pouvez pas grimper ou voler à plus de 3 mètres au-dessus du sol à moins de réussir un 
        jet de sauvegarde de Sagesse DD15.',
        'indefinite' => 'Je sais que je vais mourir d\'une mauvaise chute.',
    ];

    private const AMNESIAC = [
        'name' => 'Amnésie',
        'short_term' => 'Vous ne pouvez pas distinguer les ennemis des alliés et devez utiliser votre action pour 
        attaquer la créature la plus proche à chaque tour.',
        'long_term' => 'Vous ne reconnaissez pas les autres et ne vous souvenez de rien avant que cette folie ne prenne 
        effet.',
        'indefinite' => 'J\'ai oublié des amis, des ennemis et des faits antérieurs à cette affliction.',
    ];

    private const ARCANAPHOBIC = [
        'name' => 'Arcanophobie',
        'short_term' => 'Vous ne pouvez pas lancer de sorts, utiliser des objets magiques ou être la cible volontaire 
        d\'un sort.',
        'long_term' => 'Vous devez réussir un jet de sauvegarde de Sagesse DD15 pour lancer un sort ou vous affiner 
        avec un objet magique.',
        'indefinite' => 'La magie contamine, je m\'en méfie.',
    ];

    private const BESTIAL_MIND = [
        'name' => 'Esprit bestial',
        'short_term' => 'Vous ne pouvez pas lancer de sorts et ne pouvez effectuer que des attaques à mains nues.',
        'long_term' => 'Lorsque vous infligez des dégats, vous devez réussir un jet de sauvegarde de Sagesse DD15 ou 
        continuer à attaquer cette créature jusqu\'à ce qu\'elle soit détruite.',
        'indefinite' => 'La lune répond à mes hurlements.',
    ];

    private const CLAUSTROPHOBIC = [
        'name' => 'Claustrophobie',
        'short_term' => 'Vous avez peur de tout espace de moins de 1.5m de large.',
        'long_term' => 'Vous ne pouvez pas traverser des espaces de moins de 90 cm de large à moins de réussir un jet 
        de sauvegarde de Sagesse DD15.',
        'indefinite' => 'Je déteste les espaces clos.',
    ];

    private const CONFUSED = [
        'name' => 'Confusion',
        'short_term' => 'Vous êtes confus.',
        'long_term' => 'Lorsque vous échouez à un jet de sauvegarde, vous devez réussir un jet de sauvegarde de Sagesse 
        DD15 ou être confus jusqu\'à la fin de votre prochain tour.',
        'indefinite' => 'Peu de choses font sens pour moi.',
    ];

    private const DELUSIONS_GRANDEUR = [
        'name' => 'Illusion de grandeur',
        'short_term' => 'Donnez un ordre à vos alliés. Le MJ peut décider si la tâche est suffisante ou non. Vous êtes 
        empoisonné jusqu\'à ce que vos alliés aient terminé la tâche.',
        'long_term' => 'Vous attendez une protection. Lorsque vous subissez des dégâts d\'une source à moins de 1,50 
        mètres d\'un allié, vous devenez étourdi jusqu\'à la fin de votre prochain tour.',
        'indefinite' => 'Ils doivent suivre chacun de mes ordres.',
    ];

    private const DEVIANT = [
        'name' => 'Déviant',
        'short_term' => 'Votre alignement change à l\'opposé de l\'alignement moyen de votre groupe.',
        'long_term' => 'Comme ci-dessus, et vous avez un désavantage sur les tests de compétence basés sur le Charisme 
        impliquant des créatures à l\'alignement Loyal.',
        'indefinite' => 'Les règles sont faites pour être enfreintes.',
    ];

    private const DISSONANT_WHISPERS = [
        'name' => 'Murmures dissonants',
        'short_term' => 'Vous êtes assourdi par les voix dans votre tête.',
        'long_term' => 'Vous avez un désavantage aux tests de Sagesse (Perception) impliquant l\'écoute.',
        'indefinite' => 'Les voix dans ma tête me disent des terribles vérités.',
    ];

    private const DRUNKARD = [
        'name' => 'Alcoolique',
        'short_term' => 'Vous êtes empoisonné jusqu\'à ce que vous buviez de l\'alcool.',
        'long_term' => 'Lorsque vous terminez un repos long, vous perdez 1d10 pièces d\'argent et vous êtes empoisonné 
        pendant 1d4 heures à moins que vous ne réussissiez un jet de sauvegarde de Constitution DD15.',
        'indefinite' => 'Être ivre me garde sain d\'esprit.',
    ];

    private const FEAR_DARK = [
        'name' => 'Peur du noir',
        'short_term' => 'La lumière tamisée compte pour vous comme de l\'obscurité.',
        'long_term' => 'Une lumière tamisée qui n\'est pas adjacente à une lumière vive compte comme de l\'obscurité 
        pour vous et votre vision dans le noir ne fonctionne pas.',
        'indefinite' => 'Je suis aussi aveugle qu\'une chauve-souris après le coucher du soleil.',
    ];

    private const GAMBLER = [
        'name' => 'Parieur',
        'short_term' => 'Vous prenez des risques inutiles, ce qui entraîne une pénalité de -2 à votre CA.',
        'long_term' => 'Lorsque vous vous reposez longtemps dans une zone urbaine, lancez 1d6. Sur 1–4, perdez 2d10 
        pièces d\'or. Sur un 5-6, gagnez 2d10 pièces d\'or.',
        'indefinite' => 'Il n\'y a pas de pari que je ne peux pas gagner.',
    ];

    private const HAUNTED = [
        'name' => 'Hanté',
        'short_term' => 'Les ennemis morts au cours des dix dernières minutes prennent vie sous forme de spectres avec 
        1d4 points de vie. Les morts-vivants et les constructions ne sont pas affectés.',
        'long_term' => 'Vous voyez des ombres fantomatiques à votre périphérie. Vous avez un désavantage aux jets de 
        Sagesse (Perception) impliquant la vue.',
        'indefinite' => 'Les morts m\'entourent comme des ombres fantomatiques.',
    ];

    private const HEMOPHOBIC = [
        'name' => 'Hémophobie',
        'short_term' => 'Vous êtes étourdi jusqu\'à la fin de votre prochain tour si un allié dans un rayon de 9 mètres 
        subit des dégâts perforants ou tranchants.',
        'long_term' => 'Vous avez un désavantage aux jets de Sagesse (Médecine) pour stabiliser les créatures 
        mourantes.',
        'indefinite' => 'Je m\'évanouis à la vue du sang.',
    ];

    private const HOMICIDAL = [
        'name' => 'Meurtrier',
        'short_term' => 'Vous devez utiliser votre action à chaque tour pour attaquer un ennemi avec une attaque au 
        corps à corps.',
        'long_term' => 'Vous devez attaquer l\'ennemi le plus proche à moins que vous ne réussissiez un jet de 
        sauvegarde de Sagesse DD15. Vous ne pouvez pas assommer des créatures.',
        'indefinite' => 'J\'aime tuer plus que tout.',
    ];

    private const HOPELESS = [
        'name' => 'Sans-espoir',
        'short_term' => 'Vous avez peur de la cause de cette folie.',
        'long_term' => 'Vous avez un désavantage aux jets de sauvegarde de Sagesse.',
        'indefinite' => 'Il n\'y a pas d\'espoir.',
    ];

    private const INSOMNIAC = [
        'name' => 'Insomniaque',
        'short_term' => 'Vous êtes immunisé contre les effets du sommeil et ne pouvez pas prendre de repos long.',
        'long_term' => 'Vous devez réussir un jet de sauvegarde de Constitution DD15 ou gagner un niveau de fatigue à 
        chaque fois que vous faites un repos long.',
        'indefinite' => 'Il y a plus d\'horreurs dans mes rêves, alors je reste éveillé.',
    ];

    private const KLEPTOMANIAC = [
        'name' => 'Cleptomanie',
        'short_term' => 'Tant que vous n\'êtes pas à portée de mêlée d\'un ennemi, vous devez passer vos actions à 
        piller.',
        'long_term' => 'Lorsque vous vous reposez longtemps, vous devez réussir un jet de sauvegarde de Sagesse DD15 ou 
        tenter de voler un allié au hasard.',
        'indefinite' => 'Je prends tout ce que je peux.',
    ];

    private const MASOCHIST = [
        'name' => 'Masochiste',
        'short_term' => 'Vous ne pouvez pas volontairement accepter la guérison ou vous guérir vous-même.',
        'long_term' => 'Vous avez un malus de -2 à la CA sauf si vous ne portez pas d\'armure.',
        'indefinite' => 'La douleur me rappelle que je suis vivant.',
    ];

    private const MUMBLER = [
        'name' => 'Marmonneur',
        'short_term' => 'Vous marmonnez de façon incohérente. Vous avez un désavantage sur les tests de compétence 
        basés sur le Charisme et ne pouvez pas lancer de sorts avec une composante verbale.',
        'long_term' => 'Vous avez une pénalité de -2 sur les tests de compétence basés sur le Charisme qui vous 
        obligent à parler.',
        'indefinite' => 'Ils m\'entendraient s\'ils faisaient attention.',
    ];

    private const PARANOID = [
        'name' => 'Paranoïa',
        'short_term' => 'Les autres créatures ne peuvent pas utiliser l\'action Aide pour vous aider et vous ne pouvez 
        pas accepter les potions des alliés.',
        'long_term' => 'Vous ne pouvez pas réduire vos niveaux d\'épuisement en dessous du niveau 1 lorsque vous 
        terminez un repos long. Vous bénéficiez d\'un bonus de +2 aux tests de Sagesse (Perception).',
        'indefinite' => 'Mes ennemis sont partout. Je ne fais confiance à personne ni à rien.',
    ];

    private const PYROMANIAC = [
        'name' => 'Pyromanie',
        'short_term' => 'Vous ne pouvez effectuer des attaques qu\'avec des armes et des sorts infligeant des dégâts de 
        feu.',
        'long_term' => 'Lorsque vous infligez des dégâts de feu, la créature située à 1,50 mètre ou moins de vous doit 
        réussir un jet de sauvegarde de Dextérité DD15 ou subir 1d6 dégâts de feu.',
        'indefinite' => 'Le feu brûlera tout chagrin.',
    ];

    private const SCHIZOPHRENIC = [
        'name' => 'Schizophrénie',
        'short_term' => 'Vous êtes affecté comme si vous aviez raté un jet de sauvegarde contre le sort de Confusion.',
        'long_term' => 'Lorsque vous subissez des dégâts, vous devez réussir un jet de sauvegarde de Sagesse DD15 ou 
        subir l\'effet ci-dessus pendant 1 minute.',
        'indefinite' => 'De nombreux visages me définissent ; j\'en présente toujours un différent.',
    ];

    private const SELFISH = [
        'name' => 'Égoiste',
        'short_term' => 'Vous devez utiliser votre action à chaque tour pour acquérir le plus de richesses possible.',
        'long_term' => 'Vous ne pouvez pas lancer de sorts bénéfiques ni donner d\'objets à d\'autres créatures à moins 
        que vous ne réussissiez d\'abord un jet de sauvegarde de Sagesse DD15.',
        'indefinite' => 'C\'est tout à moi !',
    ];

    private const VAMPIRISM = [
        'name' => 'Vampirisme',
        'short_term' => 'Vous ne pouvez effectuer que des attaques de morsure infligeant des dégâts perçants égaux à 
        1d4 + votre modificateur de Force.',
        'long_term' => 'Lorsque vous laissez tomber une créature à 0 PV avec une attaque au corps à corps, vous 
        commencez à vous en nourrir et êtes entravé jusqu\'à la fin de votre prochain tour.',
        'indefinite' => 'J\'ai soif de sang frais des vivants.',
    ];
}
