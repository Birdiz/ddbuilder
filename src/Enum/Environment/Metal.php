<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

class Metal extends AbstractEnum
{
    private const ADAMANTINE = [
        'name' => 'Adamantine',
        'price' => '5 000 PO',
        'ferrous' => 'Oui',
        'AC' => '23',
        'description' => 'Un alliage d\'adamant (un métal solide mais cassant), d\'argent et d\'électrum. L\'Adamantine 
        est noire, mais a un éclat vert clair à la lumière des bougies - un éclat qui s\'accentue en violet-blanc sous 
        la lumière émise par la plupart des rayonnements magiques et par les feux follets.',
    ];

    private const BRASS = [
        'name' => 'Laiton',
        'price' => '3 PA',
        'ferrous' => 'Non',
        'AC' => '16',
        'description' => 'Un métal jaune qui ressemble un peu à l\'or. Un alliage de cuivre et de zinc.',
    ];

    private const BRONZE = [
        'name' => 'Bronze',
        'price' => '4 PA',
        'ferrous' => 'Non',
        'AC' => '18',
        'description' => 'Un métal brun rougeâtre. Un alliage de cuivre et d\'étain',
    ];

    private const COLD_IRON = [
        'name' => 'Fer à froid',
        'price' => '4 PA',
        'ferrous' => 'Oui',
        'AC' => '20',
        'description' => 'Le fer à froid est du fer trouvé à l\'état pur (soit du fer météorique, soit un minerai 
        particulièrement riche) et est forgé à une température plus basse pour préserver ses propriétés délicates.',
    ];

    private const COPPER = [
        'name' => 'Cuivre',
        'price' => '5 PA',
        'ferrous' => 'Non',
        'AC' => '16',
        'description' => 'Ce métal pur bien connu a un éclat rosâtre distinctif.',
    ];

    private const ELECTRUM = [
        'name' => 'Électrum',
        'price' => '25 PO',
        'ferrous' => 'Non',
        'AC' => '20',
        'description' => 'Un alliage naturel d\'argent et d\'or.',
    ];

    private const GOLD = [
        'name' => 'Or',
        'price' => '50 PO',
        'ferrous' => 'Non',
        'AC' => '15',
        'description' => 'Ce métal pur bien connu est la plus douce des substances métalliques exploitables.',
    ];

    private const IRON = [
        'name' => 'Fer',
        'price' => '1 PA',
        'ferrous' => 'Oui',
        'AC' => '19',
        'description' => 'Le fer est un métal malléable blanc argenté qui rouille facilement dans l\'air humide, se 
        trouve natif dans les météorites et combiné dans la plupart des roches ignées. C\'est le plus utilisé des 
        métaux.',
    ];

    private const LEAD = [
        'name' => 'Plomb',
        'price' => '2 PA',
        'ferrous' => 'Non',
        'AC' => '14',
        'description' => 'Le plomb est un métal lourd, gris, mou, malléable.',
    ];

    private const MITHRAL = [
        'name' => 'Mithril',
        'price' => '2 500 PO',
        'ferrous' => 'Non',
        'AC' => '21',
        'description' => 'Ce métal bleu argenté brillant est dérivé d\'un minerai noir argenté doux et scintillant.',
    ];

    private const PLATINUM = [
        'name' => 'Platine',
        'price' => '500 PO',
        'ferrous' => 'Non',
        'AC' => '20',
        'description' => 'Ce métal gris clair avec une très légère teinte bleutée est solide, difficile à fondre et 
        résistant à la plupart des produits chimiques.',
    ];

    private const SILVER = [
        'name' => 'Argent',
        'price' => '5 PO',
        'ferrous' => 'Non',
        'AC' => '27',
        'description' => 'Ce métal précieux relativement commun est le plus associé et le plus approprié à la magie.',
    ];

    private const STEEL = [
        'name' => 'Acier',
        'price' => '4 PO',
        'ferrous' => 'Oui',
        'AC' => '19',
        'description' => 'L\'acier est un alliage composé de fer et de carbone.',
    ];

    private const TIN = [
        'name' => 'Étain',
        'price' => '3 PA',
        'ferrous' => 'Non',
        'AC' => '12',
        'description' => 'Un métal blanc argenté doux qui est souvent combiné avec d\'autres métaux ou utilisé comme 
        couche pour protéger divers métaux.',
    ];
}
