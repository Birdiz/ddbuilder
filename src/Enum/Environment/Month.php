<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

/**
 * Class Month.
 *
 * @method static Month FEU_NAISSANT()
 * @method static Month FIN_FROID()
 * @method static Month LESTES_SEMAILLES()
 * @method static Month BLANCHE_BREBIS()
 * @method static Month CUIVRES_CHAMPS()
 * @method static Month MOISSON_DOREE()
 * @method static Month RICHE_SOLEIL()
 * @method static Month DOUCE_VIE()
 * @method static Month MORTE_FEUILLE()
 * @method static Month SOMBRE_BOIS()
 * @method static Month GRISE_LUMIERE()
 * @method static Month PERCE_NEIGE()
 */
class Month extends AbstractEnum
{
    private const FEU_NAISSANT = 'Feu naissant (printemps)';
    private const FIN_FROID = 'Fin de froid (printemps)';
    private const LESTES_SEMAILLES = 'Lestes semailles (printemps)';
    private const BLANCHE_BREBIS = 'Blanche brebis (été)';
    private const CUIVRES_CHAMPS = 'Cuivres champs (été)';
    private const MOISSON_DOREE = 'Maison dorée (été)';
    private const RICHE_SOLEIL = 'Riche soleil (automne)';
    private const DOUCE_VIE = 'Douce vie (automne)';
    private const MORTE_FEUILLE = 'Morte feuille (automne)';
    private const SOMBRE_BOIS = 'Sombre bois (hiver)';
    private const GRISE_LUMIERE = 'Grise lumière (hiver)';
    private const PERCE_NEIGE = 'Perce neige (hiver)';

    /**
     * @return array<int>
     */
    public static function getPrecipitationWeigthsByMonthKey(string $key): array
    {
        $weights = [
            static::FEU_NAISSANT()->getKey() => [60, 20, 10, 0, 0, 0, 0, 0, 10, 0],
            static::FIN_FROID()->getKey() => [70, 20, 10, 0, 0, 0, 0, 0, 0, 0],
            static::LESTES_SEMAILLES()->getKey() => [60, 10, 20, 0, 0, 10, 0, 0, 0, 0],
            static::MOISSON_DOREE()->getKey() => [60, 10, 20, 0, 0, 10, 0, 0, 0, 0],
            static::BLANCHE_BREBIS()->getKey() => [60, 10, 10, 0, 0, 20, 0, 0, 0, 0],
            static::CUIVRES_CHAMPS()->getKey() => [70, 0, 10, 0, 0, 20, 0, 0, 0, 0],
            static::RICHE_SOLEIL()->getKey() => [60, 10, 10, 0, 0, 0, 0, 0, 20, 0],
            static::DOUCE_VIE()->getKey() => [50, 10, 10, 0, 0, 0, 0, 0, 10, 20],
            static::MORTE_FEUILLE()->getKey() => [50, 20, 10, 0, 0, 0, 0, 0, 10, 10],
            static::SOMBRE_BOIS()->getKey() => [50, 20, 10, 0, 0, 0, 0, 0, 10, 10],
            static::GRISE_LUMIERE()->getKey() => [30, 20, 10, 10, 10, 0, 0, 0, 10, 10],
            static::PERCE_NEIGE()->getKey() => [40, 20, 10, 10, 0, 0, 0, 0, 10, 10],
        ];

        return $weights[$key];
    }

    public static function getValueFromKey(?string $key): ?string
    {
        return null === $key ? null : static::toArray()[$key];
    }
}
