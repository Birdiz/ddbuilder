<?php

declare(strict_types=1);

namespace App\Enum\Environment;

use App\Model\AbstractEnum;

class Rumor extends AbstractEnum
{
    private const LIGHTS = 'Plusieurs personnes disent avoir vu des lumières dans l\'ancienne mine ces derniers temps 
    durant la nuit.';
    private const SHOPKEEPER = 'Un commerçant d\'un dispensaire a dit que le vieux Riston n\'était pas venu en ville 
    pour les fournitures cette semaine.';
    private const RED_WOLF = 'Il y a des histoires d\'un loup rouge géant attaquant des voyageurs au cours du dernier 
    mois.';
    private const WIZARD = 'Un vieux sorcier fou, Cilivren, offre 500PO pour un livre.';
    private const GUARD = 'L\'un des gardes de la ville a déclaré que des enfants avaient trouvé un paquet de PO dans 
    la rivière.';
    private const CORPSE = 'Un corps a été retrouvé mort près de la périphérie de la ville. Son sac à dos comprenait 
    une carte.';
    private const ELFE = 'Une femme elfique vient à l\'auberge tous les soirs depuis une semaine pour demander à 
    propos de quelqu\'un qui s\'appelle Gulien.';
    private const MERCHANTS = 'Il y a des bruits qui courent qu\'un des riches marchands de la ville recrute 
    aventuriers et mercenaires.';
    private const MAYOR = 'Le maire envisage d\'annuler le festival en raison du nombre voleurs dans la ville.';
    private const GATHERER = 'Un collectionneur d\'art s\'est installé en ville et paie des objets de valeur.';
    private const GUARD_DRUNK = 'Un garde ivre a déclaré qu\'un ancien parchemin avait été trouvé sur la place de la 
    ville.';
    private const FIRE = 'On raconte que l\'incendie du moulin, l\'autre soir, a été allumé par un vagabond. Le maire 
    veut l\'interroger.';
    private const VOYAGERS = 'Les voyageurs racontent des histoires de ruines découvertes dans le désert à propos d\'un 
    voyage de trois jours depuis la ville.';
    private const STRANGER = 'Un étranger a offert un paiement pour des informations sur la force de la garde de la 
    ville.';
    private const OLD_LADY = 'Cette vieille dame folle qui vit seule au bord du lac dit qu\'un monstre y vit.';
    private const TREASURE = 'On dit que de l\'or a été découvert dans les collines au-delà de l\'ancien forêt.';
    private const FARM = 'Une ferme a été pillée il y a quelques jours. Le fermier dit que c\'était des gobelins, bien 
    sûr que cela ne peut pas en être.';
    private const STORE = 'Il y a eu beaucoup d\'activités nocturnes au dispensaire ces derniers temps.';
    private const HORSE = 'Un cheval a été volé à l\'écurie la nuit dernière. Le garçon d\'écurie a été frappé à la 
    tête et assommé.';
    private const LIBRAIRY = 'Un groupe d\'aventuriers bruyants a quitté la ville hier. Certains disent qu\'ils les ont 
    entendus parler de la recherche d\'une bibliothèque légendaire.';
    private const WOOD = 'Une nouvelle femme d\'affaires de la ville embauche des ouvriers pour nettoyer une partie 
    du bois près de la périphérie de la ville.';
    private const CIRCUS = 'Un cirque est censé arriver en ville dans quelques jours.';
    private const FARMER = 'Le fermier Beullo a trouvé des milliers d\'oiseaux morts dans son champ il y a quelques 
    jours.';
    private const TOWER = 'Les voyageurs disent qu\'un sorcier construit une tour sur une colline en contrebas de la 
    rivière.';
    private const BARON = 'Un baron voleur collecte des «péages» auprès des voyageurs le long de la route est.';
    private const CATS_DOGS = 'Certains disent que les chiens et les chats disparaissent en ville ces derniers temps.';
    private const MERCENARY = 'Un groupe de mercenaires qui a envahi la ville il y a environ une semaine, a brisé de 
    nombreuses choses, dépensé beaucoup d’argent, puis a déménagé hier.';
    private const OLD_MAN = 'Le vieil homme qui vit dans la grotte sur la colline a embauché des ouvriers 
    dernièrement.';
    private const HALFLING = 'Un marchand halfelin riche est censé préparer une expédition océanique.';
    private const BOOKS = 'Un elfe achète beaucoup de livres dans la ville.';
    private const LOST_CHILD = 'On parle d\'une femme à la recherche de sa fille perdue.';
    private const PROTECTION = 'Un homme est venu à l\'auberge ces dernières nuits pour embaucher en tant que 
    protection.';
    private const QUARELL = 'Le différent entre les forgerons et les menuisiers est proche de devenir violent. Du 
    moins, c’est ce que dit ce gnome ivre.';
    private const DWARF = 'Un nain nommé Randarr engage des bras pour un voyage dans les montagnes.';
    private const COFFIN = 'Certains disent avoir vu un homme portant un cercueil sur le dos les deux dernières 
    nuits.';
    private const HERBALIST = 'Un magasin d\'herbes locales paie pour des spécimens rares.';
    private const INCENDARY = 'Il y a eu récemment une série d\'incendies tard dans la nuit.';
    private const SOCIETY = 'Plusieurs marchands locaux ont créé une société d\'exploration. Ils sont à la recherche 
    d\'âmes robustes pour mener des expéditions.';
    private const ASSASSIN = 'Un jeune de 12 ans a assassiné son père l\'autre soir. Il est détenu au prison, bien 
    qu\'il nie l\'avoir fait avec beaucoup de larmes.';
    private const REWARD = 'Une récompense est offerte pour la capture de certains vandales.';
    private const DISPUTE = 'Krieg a été entendu se disputer avec un inconnu l\'autre soir, maintenant il a disparu.';
    private const SPELLBOOK = 'Un enfant perdu a été retrouvé avec un livre de sorts. Les autorités enquêtent.';
    private const SPIRIT = 'Trois personnes différentes disent que l\'esprit d\'une princesse elfe leur est apparu près 
    de la place de la ville la semaine dernière.';
    private const FOUNTAIN = 'Une fontaine voisine a été retrouvée remplie de sang ce matin.';
    private const MANOR = 'Personne n\'a vu le propriétaire de l\'ancien manoir sur la colline depuis plus de deux 
    ans.';
    private const SELLING = 'Le propriétaire de l\'auberge cherche à vendre.';
    private const PICKPOCKET = 'Un pickpocket travaille dans le quartier chic.';
    private const RIVAL = 'Un magasin a brûlé la semaine dernière. Les ragots disent qu\'un marchand rival a payé le 
    pyromane.';
    private const SHIP = 'Le navire marchand, Cromwell, n\'est pas arrivé le mois dernier et est considéré comme perdu 
    avec toutes les marins.';
    private const EXPEDITION = 'Une expédition qui a commencé il y a un mois. Leurs chevaux ont été trouvés errant dans 
    la forêt, mais il n\'y avait aucun signe des aventuriers.';
    private const BLACKSMITH = 'L\'étrange forgeron nain engage des ouvriers pour ouvrir une mine. Ils le disent sera 
    incroyablement dangereux.';
    private const WIZARD_LIBRAIRY = 'Les voyageurs de l\'ouest racontent l\'histoire d\'un sorcier construisant une 
    grande bibliothèque.';
    private const ALIGMENT = 'Des sorciers et autres magiciens traversent la ville. Certains "super alignements" auront 
    lieu le mois prochain et le meilleur point d\'observation serait à l\'ouest.';
    private const GANGS = 'Certaines familles d\'agriculteurs de la périphérie de la ville affirment que plusieurs des 
    gangs de voleurs se sont installés récemment dans les collines.';
    private const GUILD_ROGUE = 'La guilde des voleurs va renverser le gouvernement de la ville. Eh bien, c\'est ce 
    qu\'un politicien déshonoré a dit.';
    private const DRUNKARD = 'Un ivrogne a cassé des tables dans l\'auberge l\'autre soir. Il a crié qu\'il ferait 
    donnez une leçon à la ville. Personne ne l\'a revu depuis.';
    private const DRAGON_SCALES = 'Certains marins disent que des écailles de dragon s\'échouent sur le rivage.';
    private const WRECK_RADE = 'L\'épave d\'un radeau a été retrouvée flottant sur la rivière par des gens faisant la 
    lessive. Ils disent qu\'il y avait un journal de bord.';
    private const WRECK_SHIP = 'L\'Épave d\'un navire échoué par les quais. Il y en avait un survivant gravement 
    blessé. Il est pris en charge par la famille du shérif.';
    private const BASKET = 'Plusieurs enfants ont trouvé un panier scellé avec du goudron flottant sur la rivière. Ils 
    disent qu\'il y avait un bébé à l\'intérieur.';
    private const ARTEFACTS = 'Un commerçant local offre une récompense à quiconque ramène des artefacts des ruines 
    dans les collines.';
    private const DISEAS = 'Il y a une maladie dans le quartier pauvre de la ville. Mais qui s\'en soucie ?';
    private const FISHERMEN = 'Plusieurs pêcheurs ont déclaré avoir vu une lueur sous l\'eau ces dernières nuits. Ces 
    vieux imbéciles boivent probablement à nouveau.';
    private const ROBBER = 'Un cambrioleur travaille dans le quartier riche de la ville. Entre toujours par le deuxième 
    étage et ne prend que des pierres précieuses.';
    private const SCLAVER = 'Des esclavagistes ont attaqué un village dans les collines il y a une semaine. Le maire 
    avait peur de diriger une enquête.';
    private const STATUE = 'Une statue d\'un dieu a été volée sur la place de la ville il y a deux nuits. Étrange 
    comme personne n\'a rien vu.';
    private const KIDNAPPINGS = 'Plusieurs disparitions ont été constatées après qu\'un étrange navire ait quitté le 
    port l\'autre nuit. Pour parlez simplement, il ne manque personne d\'important.';
    private const TEENAGERS = 'Trois adolescents sont partis de la ville il y a une semaine après avoir acheté de la 
    corde et rations. Maintenant, leurs parents offrent une récompense à quiconque peut les ramener.';
    private const BARD = 'Un étrange barde est passé par la ville récemment. Après qu\'il soit parti, des bijoux et des 
    objets de valeur ont été portés disparus.';
    private const FIRE_FARMS = 'Les voyageurs qui descendent la rivière disent que la corruption se propage parmi les 
    fermes.';
    private const CAPTAIN = 'Un capitaine de marine engage des âmes robustes pour un long voyage.';
    private const TRAPS = 'Les voyageurs racontent des histoires de pièges posés le long de la route sud.';
    private const BUSINESSMAN = 'Un homme d\'affaires engage des bras pour couper du bois sur les pentes des 
    montagnes.';
    private const WAR_LIFE = 'Une troupe de mercenaires est en train de recruter. La nourriture et les salaires sont 
    offerts, mais le principal le bénéfice est une vie de guerre.';
    private const ENTERTAINERS = 'Une troupe itinérante recherche des joueurs. Une renommée et des richesses 
    incalculables attendent ceux qui se joignent. Ils n’ont pas encore eu beaucoup de preneurs.';
    private const STRENGTH = 'Un nain musclé offre une grosse somme d\'or à quiconque peut le battre dans une série de 
    prouesses de force.';
    private const NEWERS = 'Un groupe de nouveaux arrivants se réunissent dans une maison commune locale un soir par 
    semaine. Eh bien, c’est ce qu’on dit de toute façon.';
    private const REBELLION = 'Il y a des murmures de rébellion contre les dirigeants de la ville. Est-ce qu\'un 
    nouveau chef pourrait être pire ?';
    private const ERMITE = 'Le vieil ermite est venu en ville l\'autre jour. Il a dit que le vieux barrage en amont de 
    la rivière fuit. Personne ne le croit, car il est fou. Ce barrage ne s\'est pas cassé pendant des centaines 
    d\'années.';
    private const LORD_MAYOR = 'Le seigneur-maire engage des aventuriers pour examiner les rapports d\'activité des 
    gobelins autour des ruines de la haute forêt.';
    private const DRAGON = 'Une jeune fermière a déclaré avoir vu un dragon voler vers l\'est il y a trois jours.';
    private const CLOUDS = 'D\'étranges formations nuageuses ont été observées au-dessus d\'une montagne à l\'ouest.';
    private const WEALTHY_DWARF = 'Un nain riche engage des bras pour aider à importer de la pierre d\'un bastion nain';
    private const RANGERS = 'Une bande de rangers s\'est réunie à l\'auberge tous les soirs cette semaine. Leur nombre 
    augmente et les gens du commun commencent à s\'inquiéter.';
    private const PALADIN = 'Un paladin recrute des aventuriers pour une quête sacrée. Il promet aventure, bénédictions 
    et mort presque certaine. Ne serait-ce pas glorieux ?';
    private const TREASURES = 'La ville en bas de la route a découvert de vastes trésors.';
    private const MIRACLES = 'On dit qu\'un faiseur de miracles se déplace de ville en ville. Si les histoires sont 
    vrais, il arrivera ici demain.';
    private const WITCH = 'La vieille dame qui vit à l\'extérieur de la ville est une sorcière et elle lance des 
    malédictions sur les gens. Certaines personnes veulent la chasser.';
    private const CAT = 'Si vous regardez trop longtemps le chat de l\'auberge dans les yeux, cela vous volera l\'âme. 
    C’est ce qui est arrivé au pauvre cuisinier qui est devenu fou.';
    private const VAMPIRES = 'Un groupe de vampires se déplace vers ici. Eh bien, c\'est ce que l\'homme aux yeux 
    savages a dit hier soir à l\'auberge. Qui a déjà entendu parler de telles absurdités ?';
    private const GOLDEN_EGG = 'Un agriculteur à l\'extérieur de la ville aurait une poule qui pond des œufs d\'or.';
    private const FULLMOON = 'La nuit de la pleine lune, si vous entendez l\'appel d\'un merle et lancez une pièce 
    d\'argent dans le puits de la ville, votre souhait sera exaucé.';
    private const MEAN_BARD = 'Un barde menace d\'endormir toute la ville en jouant de la flûte enchantée. Il est juste 
    en colère, parce que la ville se moquait de lui pendant qu’il jouait.';
    private const TINKER = 'L\'ingénieur de la ville a créé un moyen de transport de vol mécanique. Ce vieil imbécile 
    ne pouvait rien créer.';
    private const NOMADES = 'Une bande de nomades kidnappe des enfants alors qu\'ils se déplacent de ville en ville. Si 
    ils apparaissent ici, que ferons-nous ?';
    private const UNDERTAKER = 'Le croque-mort a découvert une clé étrange qui ouvrirait les portes de l\'enfer. De 
    toutes façons, il a toujours été un homme effrayant.';
    private const STONE = 'Au sommet d\'une montagne voisine se trouve un homme qui vous donnera une pierre qui te 
    protéger.';
    private const KING_SON = 'Le fils du roi envisage de renverser et d’assassiner son père.';
}
