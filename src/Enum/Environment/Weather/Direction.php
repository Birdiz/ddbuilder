<?php

declare(strict_types=1);

namespace App\Enum\Environment\Weather;

use App\Model\AbstractEnum;

/**
 * Class Direction.
 *
 * @method static Direction NORD()
 * @method static Direction SOUTH()
 * @method static Direction EAST()
 * @method static Direction WEST()
 */
class Direction extends AbstractEnum
{
    private const NORD = 'Nord';
    private const SOUTH = 'Sud';
    private const EAST = 'Est';
    private const WEST = 'Ouest';
}
