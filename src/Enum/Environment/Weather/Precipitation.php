<?php

declare(strict_types=1);

namespace App\Enum\Environment\Weather;

use App\Model\AbstractEnum;

/**
 * Class Precipitation.
 *
 * @method static Precipitation NONE()
 * @method static Precipitation RAIN_LIGHT()
 * @method static Precipitation RAIN_HEAVY()
 * @method static Precipitation SNOW_LIGHT()
 * @method static Precipitation SNOW_HEAVY()
 * @method static Precipitation THUNDER()
 * @method static Precipitation THUNDERSTORM()
 * @method static Precipitation BLIZZARD()
 * @method static Precipitation CLOUDS()
 * @method static Precipitation FOG()
 */
class Precipitation extends AbstractEnum
{
    private const NONE = 'Ciel clair ou légèrement nuageux';
    private const RAIN_LIGHT = 'Pluie fine';
    private const RAIN_HEAVY = 'Pluie drue';
    private const SNOW_LIGHT = 'Quelques flocons de neige';
    private const SNOW_HEAVY = 'Neige épaisse';
    private const THUNDER = 'Orage';
    private const THUNDERSTORM = 'Tempête orageuse';
    private const BLIZZARD = 'Tempête de neige';
    private const CLOUDS = 'Nuageux';
    private const FOG = 'Brouillard';

    public static function getTemperatureFormKey(string $key): int
    {
        return match ($key) {
            static::RAIN_LIGHT()->getKey(), static::RAIN_HEAVY()->getKey() => random_int(-3, 15),
            static::SNOW_LIGHT()->getKey(), static::SNOW_HEAVY()->getKey() => random_int(-5, 0),
            static::THUNDER()->getKey(), static::THUNDERSTORM()->getKey() => random_int(0, 30),
            static::BLIZZARD()->getKey() => random_int(-20, 2),
            default => random_int(-10, 25),
        };
    }

    public static function getPrecipitationIconFromKey(string $key): ?string
    {
        $icons = [
            static::NONE()->getKey() => '<i class="fas fa-cloud-sun fa-2x has-text-danger-dark"></i>',
            static::RAIN_LIGHT()->getKey() => '<i class="fas fa-cloud-rain fa-2x has-text-danger-dark"></i>',
            static::RAIN_HEAVY()->getKey() => '<i class="fas fa-cloud-showers-heavy fa-2x has-text-danger-dark"></i>',
            static::SNOW_LIGHT()->getKey() => '<i class="fas fa-snowflake fa-2x has-text-danger-dark"></i>',
            static::SNOW_HEAVY()->getKey() => '<i class="fas fa-snowflake fa-2x has-text-danger-dark"></i>',
            static::THUNDER()->getKey() => '<i class="fas fa-bolt fa-2x has-text-danger-dark"></i>',
            static::THUNDERSTORM()->getKey() => '<i class="fas fa-poo-storm fa-2x has-text-danger-dark"></i>',
            static::BLIZZARD()->getKey() => '<i class="fas fa-wind fa-2x has-text-danger-dark"></i>',
            static::CLOUDS()->getKey() => '<i class="fas fa-cloud fa-2x has-text-danger-dark"></i>',
            static::FOG()->getKey() => '<i class="fas fa-smog fa-2x has-text-danger-dark"></i>',
        ];

        return $icons[$key];
    }
}
