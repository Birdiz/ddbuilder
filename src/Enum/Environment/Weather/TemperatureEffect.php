<?php

declare(strict_types=1);

namespace App\Enum\Environment\Weather;

use App\Model\AbstractEnum;

/**
 * Class TemperatureEffect.
 *
 * @method static TemperatureEffect NORMAL()
 * @method static TemperatureEffect COLDER()
 * @method static TemperatureEffect HOTTER()
 */
class TemperatureEffect extends AbstractEnum
{
    private const NORMAL = 'Normales pour la saison';
    private const COLDER = '1d4 x 4 degrés plus froid que la normale';
    private const HOTTER = '1d4 x 4 degrés plus chaud que la normale';

    public static function getTemperatureEffectFromValue(string $value): self
    {
        return match ($value) {
            static::COLDER()->getValue() => static::COLDER(),
            static::HOTTER()->getValue() => static::HOTTER(),
            default => static::NORMAL()
        };
    }

    public static function getTemperatureModFormKey(string $key): int
    {
        return match ($key) {
            static::COLDER()->getKey() => -4 * random_int(1, 4),
            static::HOTTER()->getKey() => 4 * random_int(1, 4),
            default => 1,
        };
    }
}
