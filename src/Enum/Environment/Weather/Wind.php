<?php

declare(strict_types=1);

namespace App\Enum\Environment\Weather;

use App\Model\AbstractEnum;

/**
 * Class Wind.
 *
 * @method static Wind NONE()
 * @method static Wind LIGHT()
 * @method static Wind STRONG()
 */
class Wind extends AbstractEnum
{
    private const NONE = 'Pas de vent';
    private const LIGHT = 'Vent léger';
    private const STRONG = 'Vent fort';
}
