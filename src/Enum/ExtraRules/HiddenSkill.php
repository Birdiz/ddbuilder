<?php

namespace App\Enum\ExtraRules;

use App\Model\AbstractEnum;

class HiddenSkill extends AbstractEnum
{
    private const ONE_HANDED_MELEE = [
        'name' => 'Armes de mêlée à une main',
        'description' => '(Épées longues, Marteaux de guerre, Lance, Masse, Hache de guerre, Fléau, Morgenstern, 
        Rapière, Trident, Pique)',
        'skills' => [
            [
                'name' => 'Jet d\'arme',
                'description' => 'Le porteur peut lancer son arme à une distance allant jusqu\'à son score de Force ou 
                jusqu\'à 3 fois avec un désavantage. (Si l\'arme peut déjà être lancée, cette distance est ajoutée en 
                bonus).',
            ],
            [
                'name' => 'Contre',
                'description' => 'Si vous êtes attaqué avec une arme de mêlée contondante, perforante ou tranchante, 
                vous utilisez votre réaction pour ajouter 2 à votre CA. Si l\'attaque rate, vous pouvez attaquer avec 
                votre arme.',
            ],
            [
                'name' => 'Frappe vorpale',
                'description' => 'Par une action, effectuez une attaque au corps à corps avec une arme qui peut infliger 
                un coup critique sur un jet d\'attaque naturelle de 18 ou plus. Les bonus qui permettent de diminuer 
                le seuil de coup critique se cumulent et diminuent le seuil de coup critique comme d\'habitude.',
            ],
            [
                'name' => 'Combo',
                'description' => 'Vous pouvez utiliser une action pour faire une attaque contre un adversaire, si vous 
                touchez, vous pouvez faire une autre attaque avec un -3 pour toucher, si cela touche, vous pouvez 
                faire une autre attaque avec une pénalité cumulative de -3 pour toucher, ce processus continue jusqu\'à 
                ce que vous manquez.',
            ],
        ],
    ];

    private const PROJECTILE = [
        'name' => 'Armes à distance',
        'description' => '(Arc long, Arc court, Arbalète légère, Arbalète lourde, Arbalète de poing, Fronde, 
        Sarbacane)',
        'skills' => [
            [
                'name' => 'Tir dispersé',
                'description' => 'Vous pouvez tirer plusieurs projectiles à la fois. Effectuez une attaque à distance 
                contre chaque cible d\'un groupe dans un cercle de 6 mètres en tant qu\'action. Lancez une fois pour 
                tous.',
            ],
            [
                'name' => 'Tir rapide',
                'description' => 'Vous pouvez tirer une flèche supplémentaire en tant qu\'action bonus',
            ],
            [
                'name' => 'Ricochet',
                'description' => 'Vous pouvez tirer une flèche sur une surface pour ignorer la couverture.',
            ],
            [
                'name' => 'Tir à bout portant',
                'description' => 'Inflige des dégâts doublés aux cibles à moins de 9 mètres.',
            ],
        ],
    ];

    private const LIGHT = [
        'name' => 'Armes légères',
        'description' => '(Épée courte, Poignard, Gourdin, Marteau léger, Faucille, Cimeterre, Rapière, Fouet)',
        'skills' => [
            [
                'name' => 'Tirailleur mobile',
                'description' => 'Lorsque vous effectuez une action de mouvement et que vous vous déplacez d\'au moins 
                3 mètres, vous pouvez effectuer une attaque avec +2 pour toucher.',
            ],
            [
                'name' => 'Visez les genoux !',
                'description' => 'Lorsque vous effectuez une attaque, une fois par tour, vous pouvez essayer de faire 
                trébucher votre cible, la faisant tomber à terre. Ils doivent faire une sauvegarde de Force avec un DD 
                égal à 8 + votre bonus de maîtrise + votre FOR ou DEX (au choix).',
            ],
            [
                'name' => 'Couteau empoisonné',
                'description' => 'Vos armes infligent 1d4 dégâts de poison supplémentaires.',
            ],
            [
                'name' => 'Lame drainante',
                'description' => 'Chaque fois que vous touchez avec une arme, gagnez 1/4 des dégâts sous forme de 
                points de vie temporaires.',
            ],
        ],
    ];

    private const STAFF = [
        'name' => 'Armes d\'Hast',
        'description' => '(Bâton, Glaive, Hallebarde, Lance)',
        'skills' => [
            [
                'name' => 'Saut à la perche',
                'description' => 'En utilisant une action bonus, vous pouvez utiliser votre arme pour sauter à la 
                perche jusqu\'à 3 mètres.',
            ],
            [
                'name' => 'Coup tourné',
                'description' => 'Vous pouvez utiliser une action pour effectuer une attaque contre tous les ennemis à 
                portée. Lancez une fois pour tous.',
            ],
            [
                'name' => 'Briseur de coquille',
                'description' => 'Lorsque vous touchez avec une attaque, vous pouvez réduire la CA de la cible de -1 
                pendant 1 tour. (un ennemi ne peut être pénalisé par cela qu\'une seule fois).',
            ],
            [
                'name' => 'Drain de mana',
                'description' => 'Sur un coup critique, vous gagnez 1 point de mana, qui peut être utilisé pour 
                regagner des emplacements de sorts. Vous pouvez dépenser 1 point de mana pour un emplacement de sort 
                de niveau 1, 2 points de mana pour un emplacement de sort de niveau 2, jusqu\'à 5 points de mana pour 
                un emplacement de sort de niveau 5. Les points de mana sont perdus lors d\'un repos long',
            ],
        ],
    ];

    private const TWO_HANDED_MELEE = [
        'name' => 'Armes de mêlée à deux mains',
        'description' => '(Espadon, Grande hache, Grande massue, Glaive, Hallebarde, Marteau de guerre, Armes 
        polyvalentes lorsqu\'elles sont utilisées comme polyvalentes)',
        'skills' => [
            [
                'name' => 'Coup écrasant',
                'description' => 'Par une action, vous pouvez effectuer une attaque contre une créature, si vous la 
                touchez, elle est envoyée à 1,50 mètre de vous pour chaque point de votre modificateur de force.',
            ],
            [
                'name' => 'Casseur',
                'description' => 'Lorsque vous obtenez un coup critique, l\'adversaire doit effectuer un jet de 
                sauvegarde de Force avec un DD égal à 8 + votre compétence + votre modificateur de force. S\'il échoue, 
                l\'arme qu\'il maniait est cassée.',
            ],
            [
                'name' => 'Frappe au sol',
                'description' => 'Par une action, vous pouvez frapper le sol, obligeant toutes les créatures (touchant 
                le sol) à effectuer un jet de sauvegarde de force avec un DD égal à 8 + votre compétence + votre 
                modificateur de force. En cas d\'échec, elles tombent à terre.',
            ],
            [
                'name' => 'Vicieux',
                'description' => 'Lorsque vous touchez avec une attaque, vous pouvez infliger des dégâts supplémentaires 
                égaux aux dégâts de votre arme (2d6 pour une grande épée, 1d12 pour une grande hache, etc.), mais vous 
                subissez également ces dégâts supplémentaires.',
            ],
        ],
    ];

    private const THROWN = [
        'name' => 'Armes de jet',
        'description' => '(Poignard, hHache, Javelot, Marteau léger, Lance, Fléchette, Filet, Trident)',
        'skills' => [
            [
                'name' => 'Mains habiles',
                'description' => 'Vous pouvez effectuer une attaque supplémentaire avec des armes de jet.',
            ],
            [
                'name' => 'Frappe perçante',
                'description' => 'Lorsque vous obtenez un coup critique, vous infligez 1d6 dégâts supplémentaires 
                (multipliés pour un coup critique, 2d6 dans la plupart des cas)',
            ],
            [
                'name' => 'Lancer d\'adieux',
                'description' => 'En tant qu\'action, effectuez une attaque lancée sur une créature, cette créature ne 
                peut pas effectuer d\'attaques d\'opportunité contre vous pendant 1 tour.',
            ],
            [
                'name' => 'Évantail de couteaux',
                'description' => 'Vous pouvez effectuer une attaque lancée contre des ennemis à moins de 6 mètres égale 
                à votre bonus de compétence, en supposant que vous ayez suffisamment d\'armes de lancer.',
            ],
        ],
    ];
}
