<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\AbstractEnum;

class Archetype extends AbstractEnum
{
    private const CHAMPION = [
        'name' => 'Champion',
        'description' => 'Dans n\'importe quelle bonne troupe de guerriers, il y a toujours un champion - sans égal et 
        sans pareil que se soit de part ses compétences ou ses prouesses guerrières.',
        'fp' => '+3',
        'hp' => '+45',
        'attack' => '+2',
        'special' => 'Augmentation de la capacité (si elle existe) Résistance Légendaire +1',
        'action' => 'Attaque Multiple. Si la créature possède déja cette action, ajouter une attaque.',
        'bonus_action' => 'Si le champion de combat réduit un ennemi à 0 points de vie ou inflige un coup critique en 
        combat, il peut utiliser son action bonus pour effectuer une attaque supplémentaire ce tour-ci.',
        'reaction' => 'Ajouter +3 à sa classe d\'armure contre une attaque une fois par tour.',
    ];

    private const TIRAILLEUR = [
        'name' => 'Tirailleur',
        'description' => 'Il est beaucoup plus difficile de tuer votre ennemi si vous ne pouvez jamais suivre le 
        rythme.',
        'fp' => '+2',
        'hp' => '+25',
        'attack' => '+3',
        'special' => 'Augmentation de la vitesse de déplacement +3m',
        'action' => 'La créature effectue une attaque de mêlée ou d\'arme de jet et peut ensuite se déplacer de 3 
        mètres.',
        'bonus_action' => 'Peut effectuer une Ruée ou Se Desengager par une Action Bonus.',
        'reaction' => 'Quand la créature est toucher par une attaque d\'opportunité, elle peut utiliser sa Réaction 
        pour l\'éviter une fois par tour.',
    ];

    private const SNIPER = [
        'name' => 'Tireur d\'élite',
        'description' => 'L\'attaque la plus dangereuse est celle que vous ne voyez pas arriver.',
        'fp' => '+2',
        'hp' => '+10',
        'attack' => '+5',
        'special' => 'Augmentation des dégâts d\'attaque +5',
        'action' => 'La créature peut effectuer une Attaque à Distance avec une pénalité de -5 au toucher et un bonus 
        de +10 au dégâts.',
        'bonus_action' => 'Visée, les attaques effectuées ne peuvent pas avoir le désavantage et ont de plus 
        un bonus de +5 à toucher, une fois par combat.',
        'reaction' => 'La créature peut se jeter au sol en utilisant sa Réaction.',
    ];

    private const CHAMAN = [
        'name' => 'Chaman',
        'description' => 'Les gobelins qui jouent avec le feu sont infiniment plus dangereux que ceux qui s\'en 
        tiennent aux gourdins. Choisissez un élément (feu, foudre, nécrotique, ...) pour le chaman.',
        'fp' => '+2',
        'hp' => '+10',
        'attack' => '+2',
        'special' => 'DD du jet de Sauvegarde des Sorts: Augmentation de +2, si la créature en possède un',
        'action' => 'Explosion Elémentaire, cible un point à moins de 14m du chaman, toutes les créatures dans un rayon 
        de 4.5m doivent effectuer un jet de sauvegarde de Dextérité. Les créatures affectées subissent des dégâts de 
        l\'élément choisi de Xd8 en cas d\'échec, la moitié en cas de succès. X étant égal à 1 + la moitié 
        du Facteur de Puissance de la créature (arrondi au supérieur).',
        'bonus_action' => 'Trait Elémentaire, jet d\'attaque de sort, portée 18m, en cas de réussite, inflige Xd6 
        dégâts de l\'élément choisi. X étant égal à la moitié du Facteur de Puissance de la créature (arrondi au 
        supérieur).',
        'reaction' => 'Peut utiliser sa réaction pour lancer Absorption des Eléments, à volonté.',
    ];

    private const BODYGUARD = [
        'name' => 'Garde du corps',
        'description' => 'Il y a toujours un gars qui se met en travers de la route.',
        'fp' => '+1',
        'hp' => '+25',
        'attack' => '',
        'special' => 'Augmentation de la CA +1',
        'action' => 'Le garde du corps peut utiliser une action pour tenter de briser une lutte entre un ennemi et un 
        allié qui se trouve à moins de 1.5 m de lui. Le garde du corps a l\'avantage sur ce jet.',
        'bonus_action' => 'Defense - Tant que le garde du corps est à moins de 1.5 m d\'un allié ciblé, les jets 
        d\'attaque contre la cible ont un désavantage jusqu\'au début du prochain tour du garde du corps.',
        'reaction' => 'Quand un ennemi lance un jet d\'attaque contre un allié qui se trouve à moins de 1.5 m du garde 
        du corps, le garde du corps peut utiliser sa réaction pour le forcer à l\'attaquer à la place.',
    ];
}
