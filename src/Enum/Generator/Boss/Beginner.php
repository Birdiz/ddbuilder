<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\Generator\AbstractBossGeneratorEnum;

/**
 * Class Beginner.
 *
 * @method static Beginner BARGHEST()
 * @method static Beginner BUGBEAR_BOSS()
 * @method static Beginner CAPTAIN_BANDIT()
 * @method static Beginner DRAGON_GREEN()
 * @method static Beginner DRAGON_RED()
 * @method static Beginner DRAGON_BLACK()
 * @method static Beginner DRAGON_COPPER()
 * @method static Beginner DRAGON_BRONZE()
 * @method static Beginner DRAGON_GOLD()
 * @method static Beginner DRAGON_SILVER()
 * @method static Beginner DRAGON_BRASS()
 * @method static Beginner DRAGON_BLUE()
 * @method static Beginner DRAGON_WHITE()
 * @method static Beginner HIPPOGRIFF()
 * @method static Beginner GNOLL_PACK_LORD()
 * @method static Beginner GOBLIN_BOSS()
 * @method static Beginner GREEN_HAG()
 * @method static Beginner HOBGOBLIN_CAPTAIN()
 * @method static Beginner MUMMY()
 * @method static Beginner MINOTAUR()
 * @method static Beginner OGRE()
 * @method static Beginner ORC_WAR_CHIEF()
 * @method static Beginner OWLBEAR()
 * @method static Beginner PRIEST()
 * @method static Beginner LIZARD_KING_QUEEN()
 * @method static Beginner SEA_HAG()
 * @method static Beginner SPIDER_GIANT()
 * @method static Beginner SPINED_DEVIL()
 * @method static Beginner TOAD_GIANT()
 * @method static Beginner VETERAN()
 * @method static Beginner KNIGHT()
 * @method static Beginner YUAN_TI_NIGHTMARE_SPEAKER()
 */
class Beginner extends AbstractBossGeneratorEnum
{
    private const BARGHEST = [
        'name' => 'Barghest',
        'fp' => 4,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=barghest',
        'image' => 'https://www.aidedd.org/dnd/images/barghest.jpg',
        'description' => '<p>Un barghest est un démon lupin qui peut prendre la forme d\'un loup ou d\'un 
        gobelin. Dans sa forme naturelle, il ressemble à un hybride gobelin-loup avec de terribles mâchoires et des 
        griffes acérées. Les barghests viennent au monde pour se nourrir de sang et d\'âmes pour devenir plus forts.
        <br/> En tant que jeunes, les barghests sont presque impossibles à distinguer des loups, à l\'exception de leur 
        taille et de leurs griffes. Au fur et à mesure qu\'ils grandissent et deviennent plus forts, leur peau 
        s\'assombrit en rouge bleuâtre et finit par devenir complètement bleue. Un barghest adulte mesure environ 1,8m 
        de long et pèse 90kg. Les yeux d\'un barghest brillent en orange lorsque la créature est excitée.</p>',
    ];

    private const BUGBEAR_BOSS = [
        'name' => 'Chef gobelours',
        'fp' => 1,
        'xp' => 200,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gobelours-chef',
        'image' => 'https://www.aidedd.org/dnd/images/bugbear.jpg',
        'description' => '<p>Les gobelours ressemblent à des gobelins poilus et sauvages mesurant 2,1 mètres 
        de haut. Leurs griffes ne sont pas assez longues et tranchantes pour être utilisées comme armes, de sorte que 
        les gobelours s\'arment souvent avec une variété d\'équipements volés. Le plus souvent, cet équipement est de 
        second qualité et en mauvais état. Beaucoup de gobelours sont chaotiques mauvais, favorisant la furtivité et 
        surprenant leurs adversaires.</p>',
    ];

    private const CAPTAIN_BANDIT = [
        'name' => 'Capitaine bandit',
        'fp' => 2,
        'xp' => 450,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=bandit-capitaine',
        'image' => 'https://www.aidedd.org/dnd/images/bandit-captain.jpg',
        'description' => '<p>Il faut une forte personnalité, un esprit impitoyablement rusé, et une langue 
        acérée pour maintenir un gang de bandits uni. Le capitaine bandit ne manque pas de ces qualités.<br/>
        Non content de gérer une équipe de râleurs égoïstes, le capitaine pirate, une variante du capitaine bandit, 
        dirige et assure la protection d\'un bateau. Pour garder la main sur son équipage, le capitaine doit 
        régulièrement distribuer récompenses et brimades. Plus que les trésors, le capitaine bandit ou pirate est 
        avide de réputation. Un prisonnier qui en appel à sa vanité ou à son ego a plus de chance d\'obtenir un 
        traitement clément que celui qui ne le fait pas ou prétend ne rien savoir de la sulfureuse réputation du 
        capitaine.</p>',
    ];

    private const DRAGON_GREEN = [
        'name' => 'Dragon vert, nouveau-né',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-vert-nouveau-ne',
        'image' => '/build/images/Dragon_Green.webp',
        'description' => '<p>Les dragons verts sont des créatures forestières très territoriales, trompeuses, 
        qui aiment les secrets et les intrigues. Bien que loyaux mauvais, ce sont des ennemis hypocrites et rusés qui 
        aiment le combat. Ils sont le troisième plus puissant des dragons chromatiques. Leur souffle est un cône de gaz 
        vert toxique.</p>',
    ];

    private const DRAGON_RED = [
        'name' => 'Dragon rouge, nouveau-né',
        'fp' => 4,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-rouge-nouveau-ne',
        'image' => '/build/images/Dragon_Red.webp',
        'description' => '<p>Les dragons rouges sont des créatures maléfiques avides et chaotiques, 
        intéressées uniquement par leur propre bien-être, leur vanité et l\'extension de leurs trésors. Ils sont 
        extrêmement confiants, étant le plus grand et le plus puissant des dragons chromatiques. Généralement trouvés 
        dans les régions montagneuses, ils soufflent un cône de feu.</p>',
    ];

    private const DRAGON_BLACK = [
        'name' => 'Dragon noir, nouveau-né',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-noir-nouveau-ne',
        'image' => '/build/images/Dragon_Black.webp',
        'description' => '<p>Les dragons noirs sont les plus vils et les plus cruels de tous les dragons 
        chromatiques. Excellents nageurs qui vivent normalement dans les marécages et les marais, ils préfèrent les 
        attaques d\'embuscade. Ils ont un souffle acide corrosif.</p>',
    ];

    private const DRAGON_COPPER = [
        'name' => 'Dragon de cuivre, nouveau-né',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-cuivre-nouveau-ne',
        'image' => '/build/images/Dragon_Copper.webp',
        'description' => '<p>Les dragons de cuivre sont nés blagueurs, farceurs et énigmatiques. Ils sont 
        chaotiques bons et vivent dans des montagnes rocheuses. Les dragons de cuivre ont un souffle acide mais 
        préfèrent éviter le combat, au lieu de cela, narguer et taquiner les ennemis jusqu\'à ce qu\'ils partent.
        </p>',
    ];

    private const DRAGON_BRONZE = [
        'name' => 'Dragon de bronze, nouveau-né',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-bronze-nouveau-ne',
        'image' => '/build/images/Dragon_Bronze.webp',
        'description' => '<p>Les dragons de bronze sont des dragons curieux avec une apparence féroce et une 
        fascination pour la guerre. Ils cherchent souvent à combattre le mal et rejoignent des armées alignées vers le 
        bien sous une forme humanoïde. Ils sont loyaux bons, vivent dans les zones côtières et ont un souffle 
        électrique.</p>',
    ];

    private const DRAGON_GOLD = [
        'name' => 'Dragon d\'or, nouveau-né',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-or-nouveau-ne',
        'image' => '/build/images/Dragon_Gold.webp',
        'description' => '<p>Les dragons d\'or sont les plus puissants et majestueux des dragons métalliques 
        classiques, ce sont de sages, loyaux bons et ennemis légitimes du mal et de l\'injustice. Les dragons d\'or 
        sont porteurs de connaisances et vivent reclus. Ils préfèrent les repaires de pierre et ont un cône de feu 
        comme souffle.</p>',
    ];

    private const DRAGON_SILVER = [
        'name' => 'Dragon d\'argent, nouveau-né',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-argent-nouveau-ne',
        'image' => '/build/images/Dragon_Silver.webp',
        'description' => '<p>Les dragons d\'argent sont des créatures royales et loyales bonnes qui ont 
        souvent pris une forme humanoïde et vivent parmi les humains et les elfes. Leur souffle est un cône de froid 
        glacial, et ils se reposent normalement dans les montagnes. Ils sont très intelligents et préfèrent ne pas se 
        battre sauf en cas de nécessité.</p>',
    ];

    private const DRAGON_BRASS = [
        'name' => 'Dragon d\'airain, nouveau-né',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-airain-nouveau-ne',
        'image' => '/build/images/Dragon_Brass.webp',
        'description' => '<p>Les dragons d\'airain sont extrêmement bavards et aiment la chaleur intense des 
        déserts. Ils engagent souvent des ennemis et des amis pendant des heures de longues conversations. Ils évitent 
        le combat si possible, mais utilisent leur souffle, un cône de gaz somnifère, pour maîtriser leurs ennemis 
        s\'ils sont menacés. Ce sont les plus faibles des dragons métalliques classiques, mais toujours chaotiques 
        bons.</p>',
    ];

    private const DRAGON_BLUE = [
        'name' => 'Dragon bleu, nouveau-né',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-bleu-nouveau-ne',
        'image' => '/build/images/Dragon_Blue.webp',
        'description' => '<p>Les dragons bleus, ou dragons des tempêtes, sont des chromatiques loyaux mauvais 
        manipulateurs qui sont tristement célèbres pour leur habileté à créer des hallucinations et leur utilisation 
        cruelle des choses. Ils préfèrent les combats aériens, qui leur permettent d\'utiliser leur souffle électrique 
        le plus efficacement possible. Les dragons bleus vivent le plus souvent dans des friches et des déserts arides. 
        Ils sont le deuxième plus puissant des dragons chromatiques.</p>',
    ];

    private const DRAGON_WHITE = [
        'name' => 'Dragon blanc, nouveau-né',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-blanc-nouveau-ne',
        'image' => '/build/images/Dragon_White.webp',
        'description' => '<p>Les dragons blancs, également appelés dragons ou wyvernes de glace, sont les plus 
        petits et les plus faibles des dragons chromatiques classiques. Cependant, ils ne sont en aucun cas 
        inoffensifs. Les dragons blancs sont extrêmement bien adaptés à leur habitat arctique et ont d\'excellentes 
        mémoires. Ils sont cependant plus sauvages que les autres dragons et toujours chaotiques mauvais. Ils soufflent 
        un cône de glace ou de givre.</p>',
    ];

    private const HIPPOGRIFF = [
        'name' => 'Hippogriffe',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=hippogriffe',
        'image' => 'https://www.aidedd.org/dnd/images/hippogriff.jpg',
        'description' => '<p>Une bête dont les origines magiques sont perdues dans l\'histoire, un hippogriffe 
        possède les ailes et les membres antérieurs d\'un aigle, l\'arrière-train d\'un cheval et une tête qui combine 
        les caractéristiques des deux animaux.<br/><br/>
        <b>Montures volantes:</b> Un hippogriffe élevé en captivité peut être formé pour être un fidèle compagnon et 
        une monture. De toutes les créatures pouvant servir de montures volantes, les hippogriffes sont parmi les plus 
        faciles à entraîner et les plus fidèles une fois entraînées correctement.</p>',
    ];

    private const GNOLL_PACK_LORD = [
        'name' => 'Seigneur de meute gnoll',
        'fp' => 2,
        'xp' => 450,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gnoll-seigneur-de-meute',
        'image' => '/build/images/Gnoll_Pack.webp',
        'description' => '<p>L\'alpha d\'une meute de gnolls est le seigneur de meute, gouvernant par la 
        puissance et la ruse. Un seigneur de meute gagne le meilleur des butins, de la nourriture, des bibelots 
        précieux et des objets magiques d\'un pack de gnoll. Il orne son corps de piercings brutaux et de trophées 
        grotesques, teignant sa fourrure de sceaux démoniaques, en espérant que <i>Yeenoghu</i> le rendra invulnérable.
        </p>',
    ];

    private const GOBLIN_BOSS = [
        'name' => 'Chef gobelin',
        'fp' => 1,
        'xp' => 200,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gobelin-chef',
        'image' => 'https://www.aidedd.org/dnd/images/goblin.jpg',
        'description' => '<p>Les gobelins sont gouvernés par les plus forts ou les plus intelligents d\'entre 
        eux. Un chef gobelin peut commander un seul repaire, tandis qu\'un roi ou une reine gobelin (qui n\'est rien de 
        plus qu\'un chef gobelin glorifié) dirige des centaines de gobelins, répartis entre plusieurs repaires pour 
        assurer la survie de la tribu. Les boss gobelins sont facilement évincés, et de nombreuses tribus gobelins 
        sont prises en charge par des chefs de guerre Hobgoblin ou des chefs Gobelours.</p>',
    ];

    private const GREEN_HAG = [
        'name' => 'Guenaude verte',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=guenaude-verte',
        'image' => 'https://www.aidedd.org/dnd/images/green-hag.jpg',
        'description' => '<p>Les misérables et détestables guenaudes vertes habitent dans des forêts 
        mourantes, des marécages solitaires et des landes brumeuses, s\'installant dans des grottes. Les guenaudes 
        vertes adorent manipuler d\'autres créatures pour qu\'elles fassent ce qu\'elles veulent, masquant leurs 
        intentions derrière des couches de déception. Ils attirent les victimes vers eux en imitant des voix appelant à 
        l\'aide, ou chassent les visiteurs indésirables en imitant les cris de bêtes féroces.<br/><br/>
        <b>Obsession de la tragédie:</b> Les guenaudes vertes se délectent des échecs et des tragédies d\'autres 
        créatures. Ils tirent de la joie d\'abaisser les gens et de voir l\'espoir se transformer en désespoir, non 
        seulement pour les individus mais aussi pour des nations entières.</p>',
    ];

    private const SEA_HAG = [
        'name' => 'Guenaude aquatique',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=guenaude-aquatique',
        'image' => 'https://www.aidedd.org/dnd/images/sea-hag.jpg',
        'description' => '<p>Les guenaudes de mer vivent dans des repaires sous-marins lugubres et pollués, 
        entourés de Merrow et d\'autres monstres aquatiques.<br/>
        La beauté pousse une guenaude de mer à des accès de colère. Lorsqu\'elle est confrontée à quelque chose de 
        beau, la guenaude peut simplement l\'attaquer ou la détériorer. Si quelque chose de beau donne de l\'espoir, 
        une guenaude de mer veut qu\'il provoque le désespoir. Si cela inspire du courage, la guenaude de la mer veut 
        qu\'elle provoque la peur.<br/><br/>
        <b>Moche à l\'intérieur et à l\'extérieur:</b> Les guenaudes de mer sont de loin les plus laides de toutes, 
        avec des écailles visqueuses recouvrant leur peau pâle. Les cheveux d\'une guenaude de mer ressemblent à des 
        algues et recouvrent son corps émacié, et ses yeux vitreux semblent aussi sans vie que ceux d\'une poupée. 
        Bien qu\'une guenaude de mer puisse cacher sa vraie forme sous un voile d\'illusion, la guenaude est maudite 
        pour paraître à jamais laide. Sa forme illusoire semble au mieux hagarde.</p>',
    ];

    private const HOBGOBLIN_CAPTAIN = [
        'name' => 'Capitaine Hobgobelin',
        'fp' => 3,
        'xp' => 700,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=hobgobelin-capitaine',
        'image' => 'https://www.aidedd.org/dnd/images/hobgoblin.jpg',
        'description' => '<p>Les cornes de guerre retentissent, les pierres volent des catapultes et le 
        tonnerre de mille pieds bottés résonne à travers le pays alors que les Hobgobelins marchent au combat. À 
        travers les frontières de la civilisation, les colonies et les colons doivent lutter contre ces humanoïdes 
        agressifs, dont la soif de conquête n\'est jamais satisfaite.<br/>
        Les Hobgoblins ont la peau orange foncé ou rouge-orange et les cheveux allant du brun rouge foncé au gris 
        foncé. Des yeux jaunes ou brun foncé scrutent sous leurs sourcils de scarabée, et leurs larges bouches 
        arborent des dents pointues et jaunies. Un hobgobelin mâle peut avoir un grand nez bleu ou rouge, qui 
        symbolise la virilité et le pouvoir chez les gobelins. Les Hobgobelins peuvent vivre aussi longtemps que les 
        humains, bien que leur amour de la guerre et de la bataille signifie que peu le font.</p>',
    ];

    private const MUMMY = [
        'name' => 'Momie',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=momie',
        'image' => 'https://www.aidedd.org/dnd/images/mummy.jpg',
        'description' => '<p>Les momies traînent les pieds dans des temples et des tombes perdus, tuant tous 
        ceux qui perturbent leur repos. Une momie est créée par un prêtre lors d\'un rituel au cours duquel un cadavre, 
        préalablement débarassé de ses organes et enveloppé de tissus, est imprégné d\'énergies nécromantiques. 
        Lorsque les conditions spécifiées lors du rituel sont remplies, la momie s\'anime et cherche alors à punir 
        les intrus.</p>',
    ];

    private const MINOTAUR = [
        'name' => 'Minotaure',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=minotaure',
        'image' => 'https://www.aidedd.org/dnd/images/minotaur.jpg',
        'description' => '<p>Avec leurs fourrures tachées du sang des ennemis qu\'ils ont vaincus, les 
        minotaures sont d\'imposants humanoïdes à tête de taureau dont les rugissements sont des cris de guerre qui 
        terrifient le monde civilisé.</p>',
    ];

    private const OGRE = [
        'name' => 'Ogre',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=ogre',
        'image' => 'https://www.aidedd.org/dnd/images/ogre.jpg',
        'description' => '<p>Les ogres ressemblent à des géants et sont connus pour leur caractère irritable. 
        Lorsque sa rage est titillée, un ogre se déchaîne dans un accès de colère frustrée jusqu\'à ne plus avoir 
        d\'objets ou de créatures à écraser.</p>',
    ];

    private const ORC_WAR_CHIEF = [
        'name' => 'Chef de guerre Orc',
        'fp' => 4,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=orc-chef-de-guerre',
        'image' => 'https://www.aidedd.org/dnd/images/orc.jpg',
        'description' => '<p>Le chef de guerre d\'une tribu orc est son membre le plus fort et le plus rusé. 
        Le règne d\'un chef de guerre ne dure que tant qu\'il suscite la crainte et le respect des autres membres de la 
        tribu, dont la soif de sang doit être régulièrement satisfaite de peur que le chef ne paraisse faible.<br/><br/>
        <b>Scions du massacre:</b> <i>Gruumsh</i> accorde des bénédictions spéciales aux chefs de guerre qui font 
        leurs preuves au combat à maintes reprises, les imprégnant d\'éclats de sa sauvagerie. Un chef de guerre ainsi 
        béni trouve que ses armes s\'enfoncent plus profondément dans ses ennemis, lui permettant d\'infliger plus 
        de carnage.</p>',
    ];

    private const OWLBEAR = [
        'name' => 'Ours-hibou',
        'fp' => 3,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=ours-hibou',
        'image' => 'https://www.aidedd.org/dnd/images/owlbear.jpg',
        'description' => '<p>Croisement monstrueux entre un ours et un hibou géant, l\'ours-hibou est réputé 
        pour sa férocité et son agressivité, qui en font l\'un des prédateurs les plus redoutés de la nature.
        </p>',
    ];

    private const PRIEST = [
        'name' => 'Prêtre',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=pretre',
        'image' => '',
        'description' => '<p>Les prêtres transmettent les enseignements de leurs dieux aux gens du peuple. Ils 
        sont les leaders spirituels des temples et des chapelles et détiennent souvent une position influente au sein 
        de leur communauté. Les prêtres mauvais peuvent travailler ouvertement sous le joug d\'un tyran, ou être les 
        chefs d\'une secte religieuse dissimulée dans l\'ombre d\'une civilisation bonne, supervisant des rites 
        dépravés.<br/>
        Un prêtre dispose généralement d\'un ou plusieurs acolytes qui l\'aident lors des cérémonies.</p>',
    ];

    private const LIZARD_KING_QUEEN = [
        'name' => 'Roi/reine lézard',
        'fp' => 4,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=roi-reine-lezard',
        'image' => '/build/images/Lizardfolk.webp',
        'description' => '<p>Les lézards respectent et craignent la magie avec une admiration religieuse. Les 
        chamans lézards dirigent leurs tribus, supervisant les rites et les cérémonies exécutées pour honorer 
        <i>Semuanya</i>. De temps en temps, cependant, une tribu de lézards produit une figure puissante touchée non 
        pas par Semuanya mais par <i>Sess’inek</i> - un seigneur démon reptilien qui cherche à corrompre et à contrôler 
        les lézards.<br/>
        Les lézards nés à l\'image de <i>Sess’inek</i> sont plus grands et plus rusés que les autres lézards, et sont 
        complètement méchants. Ces rois et reines lézards dominent les tribus des lézards, usurpant l\'autorité d\'un 
        chaman et inspirant une agression inhabituelle parmi leurs sujets.</p>',
    ];

    private const SPIDER_GIANT = [
        'name' => 'Araignée géante',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=araignee-geante',
        'image' => 'https://www.aidedd.org/dnd/images/giant-spider.jpg',
        'description' => '<p>Pour piéger sa proie, une araignée géante tisse des toiles élaborées ou jette des 
        brins 
        de toile collants de son abdomen. Les araignées géantes vivent généralement sous terre, établissant leurs 
        repaires au plafond ou dans des crevasses obscures. Ces endroits sont souvent ornés de cocons qui retiennent 
        d\'anciennes victimes.</p>',
    ];

    private const SPINED_DEVIL = [
        'name' => 'Diable épineux',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=diable-epineux',
        'image' => 'https://www.aidedd.org/dnd/images/spined-devil.jpg',
        'description' => '<p>Plus petits que la plupart des autres démons, les Spinagones agissent comme des 
        messagers et des espions pour les plus grands démons et archidémons. Ce sont les yeux et les oreilles des Neuf 
        Enfers, et même les Fiélons qui méprisent la faiblesse d’un diable épineux le traitent avec un minimum de 
        respect.<br/> Le corps et la queue d\'un diable épineux sont hérissés d\'épines, et il peut lancer les épines 
        de sa queue comme des armes à distance. Les épines s\'enflamment à l\'impact.<br/>
        Lorsqu\'ils ne délivrent pas de messages ou ne recueillent pas de renseignements, les démons épineux servent 
        dans les légions infernales en tant qu\'artillerie volante, compensant leur faiblesse relative en se 
        mobilisant pour submerger leurs ennemis. Bien qu\'ils aient soif de promotion et de pouvoir, les démons 
        épineux sont lâches par nature, et ils se disperseront rapidement si un combat va contre eux.</p>',
    ];

    private const TOAD_GIANT = [
        'name' => 'Crapaud géant',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=crapaud-geante',
        'image' => '/build/images/Frog.webp',
        'description' => '<p>Les crapauds géantes mesurent jusqu\'à 1,8 m de long et pèsent jusqu\'à 110 kg. 
        En raison de cette taille étendue, ils sautent jusqu\'à 3 m de haut et à travers des espaces de 6,1 m de large, 
        même s\'ils sont complètement stationnaires. Dans les premiers temps, certains chercheurs ont noté des 
        spécimens qui pouvaient sauter des hauteurs encore plus grandes, jusqu\'à 9,1 m.</p>',
    ];

    private const VETERAN = [
        'name' => 'Vétéran',
        'fp' => 3,
        'xp' => 700,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=veteran',
        'image' => '',
        'description' => '<p>Les vétérans sont des combattants professionnels qui s\'enrôlent contre rémunération ou 
        pour protéger quelque chose en laquelle ils croient ou qui a de la valeur à leurs yeux. Leurs rangs comportent 
        des soldats retirés après avoir longtemps servis ainsi que des guerriers n\'ayant jamais servi quelqu\'un 
        d\'autre qu\'eux-mêmes.</p>',
    ];

    private const KNIGHT = [
        'name' => 'Chevalier',
        'fp' => 3,
        'xp' => 700,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=chevalier',
        'image' => '',
        'description' => '<p>Les chevaliers sont des guerriers ayant prêté allégeance à un dirigeant, un ordre 
        religieux ou une noble cause. L\'alignement du chevalier détermine à quel point le serment est honoré. Que ce 
        soit en s\'engageant dans une quête ou en patrouillant un royaume, un chevalier voyage souvent avec un 
        entourage qui comprend écuyers et servants, lesquels sont des roturiers.</p>',
    ];

    private const YUAN_TI_NIGHTMARE_SPEAKER = [
        'name' => 'Annonciateur de cauchemars Yuan-ti',
        'fp' => 4,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=yuan-ti-annonciateur-de-cauchemars',
        'image' => 'https://www.aidedd.org/dnd/images/yuan-ti-nightmare-speaker.jpg',
        'description' => '<p>Les annonciateurs de cauchemars sont des yuan-ti malison femelles et prêtres qui 
        ont conclu un pacte avec le dieu serpent <i>Dendar, le Serpent de la nuit</i>.</p>',
    ];

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getMobs(string $bossKey, string $danger): array
    {
        self::convertBossKey($bossKey);

        $mobs = [
            static::TOAD_GIANT()->getKey() => self::getGiantToadMobs($danger),
            static::GOBLIN_BOSS()->getKey() => self::getBossGoblinMobs($danger),
            static::CAPTAIN_BANDIT()->getKey() => self::getCaptainBanditMobs($danger),
            static::GNOLL_PACK_LORD()->getKey() => self::getGnollPackLordMobs($danger),
            static::LIZARD_KING_QUEEN()->getKey() => self::getLizardKingQueenMobs($danger),
            static::MUMMY()->getKey() => self::getMummyMobs($danger),
            static::OGRE()->getKey() => self::getOgreMobs($danger),
            static::ORC_WAR_CHIEF()->getKey() => self::getOrcWarChiefMobs($danger),
            static::PRIEST()->getKey() => self::getPriestMob($danger),
            static::SPIDER_GIANT()->getKey() => self::getGiantSpiderMobs($danger),
            static::SPINED_DEVIL()->getKey() => self::getSpinedDevilMobs($danger),
            static::YUAN_TI_NIGHTMARE_SPEAKER()->getKey() => self::getYuanTiNightmareSpeakerMobs($danger),
            static::VETERAN()->getKey() => self::getVeteranMobs($danger),
            static::KNIGHT()->getKey() => self::getKnightMobs($danger),
        ];

        return array_key_exists($bossKey, $mobs) ? $mobs[$bossKey] : [];
    }

    private static function convertBossKey(string &$bossKey): void
    {
        // The four have the same mobs.
        if (in_array(
            $bossKey,
            [static::BARGHEST()->getKey(), static::BUGBEAR_BOSS()->getKey(), static::HOBGOBLIN_CAPTAIN()->getKey()],
            true
        )) {
            $bossKey = static::GOBLIN_BOSS()->getKey();
        }
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getGiantToadMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::BULLYWUG()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getBossGoblinMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::HOBGOBLIN()->getValue()],
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::GOBLIN()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getCaptainBanditMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::BANDIT()->getValue()],
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::SCOUT()->getValue()],
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::THUG()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getGnollPackLordMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::GNOLL()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getLizardKingQueenMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::LIZARDFOLK_CHAMAN()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getMummyMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::LIZARDFOLK_CHAMAN()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getOgreMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::ORC()->getValue()],
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::HALF_OGRE()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getOrcWarChiefMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::ORC()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getPriestMob(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::ACOLYTE()->getValue()],
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getGiantSpiderMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::SPIDER()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getSpinedDevilMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::QUASIT()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getYuanTiNightmareSpeakerMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::YUAN_TI_PUREBLOOD()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getVeteranMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::BERSERKER()->getValue()],
        ];
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getKnightMobs(string $danger): array
    {
        return [
            ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
            ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::ARCHER()->getValue()],
        ];
    }
}
