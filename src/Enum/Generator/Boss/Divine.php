<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\Generator\AbstractBossGeneratorEnum;

/**
 * Class Divine.
 *
 * @method static Divine ANDROSPHINX()
 * @method static Divine DRAGON_GREEN()
 * @method static Divine DRAGON_RED()
 * @method static Divine DRAGON_BLACK()
 * @method static Divine DRAGON_COPPER()
 * @method static Divine DRAGON_BRONZE()
 * @method static Divine DRAGON_SILVER()
 * @method static Divine DRAGON_GOLD()
 * @method static Divine DRAGON_BRASS()
 * @method static Divine DRAGON_BLUE()
 * @method static Divine DRAGON_WHITE()
 * @method static Divine DRACOLICH()
 * @method static Divine BALOR()
 * @method static Divine DEMILICH()
 * @method static Divine LICH()
 */
class Divine extends AbstractBossGeneratorEnum
{
    private const ANDROSPHINX = [
        'name' => 'Androsphinx',
        'fp' => 17,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=androsphinx',
        'image' => 'https://www.aidedd.org/dnd/images/androsphinx.jpg',
        'description' => '<p>Les androsphinx sont des créatures physiquement très puissantes et ont des 
        rugissements terrifiants et assourdissants qui pourraient être entendus à des kilomètres de distance qui 
        pourraient inspirer la peur chez d\'autres créatures à un point tel qu\'ils deviendraient momentanément 
        paralysés. Ceux qui se tiennent à moins d\'un mètre risquent d\'être assourdis et même de tomber s\'ils sont 
        assez petits.</p>',
    ];

    private const DRAGON_GREEN = [
        'name' => 'Dragon vert, ancien',
        'fp' => 22,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-vert-ancien',
        'image' => '/build/images/Dragon_Green.webp',
        'description' => '<p>Les dragons verts sont des créatures forestières très territoriales, trompeuses, 
        qui aiment les secrets et les intrigues. Bien que loyaux mauvais, ce sont des ennemis hypocrites et rusés qui 
        aiment le combat. Ils sont le troisième plus puissant des dragons chromatiques. Leur souffle est un cône de gaz 
        vert toxique.</p>',
    ];

    private const DRAGON_RED = [
        'name' => 'Dragon rouge, adulte',
        'fp' => 17,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-rouge-adulte',
        'image' => '/build/images/Dragon_Red.webp',
        'description' => '<p>Les dragons rouges sont des créatures maléfiques avides et chaotiques, 
        intéressées uniquement par leur propre bien-être, leur vanité et l\'extension de leurs trésors. Ils sont 
        extrêmement confiants, étant le plus grand et le plus puissant des dragons chromatiques. Généralement trouvés 
        dans les régions montagneuses, ils soufflent un cône de feu.</p>',
    ];

    private const DRAGON_BLACK = [
        'name' => 'Dragon noir, adulte',
        'fp' => 14,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-noir-adulte',
        'image' => '/build/images/Dragon_Black.webp',
        'description' => '<p>Les dragons noirs sont les plus vils et les plus cruels de tous les dragons 
        chromatiques. Excellents nageurs qui vivent normalement dans les marécages et les marais, ils préfèrent les 
        attaques d\'embuscade. Ils ont un souffle acide corrosif.</p>',
    ];

    private const DRAGON_COPPER = [
        'name' => 'Dragon de cuivre, ancien',
        'fp' => 21,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-cuivre-ancien',
        'image' => '/build/images/Dragon_Copper.webp',
        'description' => '<p>Les dragons de cuivre sont nés blagueurs, farceurs et énigmatiques. Ils sont 
        chaotiques bons et vivent dans des montagnes rocheuses. Les dragons de cuivre ont un souffle acide mais 
        préfèrent éviter le combat, au lieu de cela, narguer et taquiner les ennemis jusqu\'à ce qu\'ils partent.
        </p>',
    ];

    private const DRAGON_BRONZE = [
        'name' => 'Dragon de bronze, ancien',
        'fp' => 22,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-bronze-ancien',
        'image' => '/build/images/Dragon_Bronze.webp',
        'description' => '<p>Les dragons de bronze sont des dragons curieux avec une apparence féroce et une 
        fascination pour la guerre. Ils cherchent souvent à combattre le mal et rejoignent des armées alignées vers le 
        bien sous une forme humanoïde. Ils sont loyaux bons, vivent dans les zones côtières et ont un souffle 
        électrique.</p>',
    ];

    private const DRAGON_GOLD = [
        'name' => 'Dragon d\'or, adulte',
        'fp' => 17,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-or-adulte',
        'image' => '/build/images/Dragon_Gold.webp',
        'description' => '<p>Les dragons d\'or sont les plus puissants et majestueux des dragons métalliques 
        classiques, ce sont de sages, loyaux bons et ennemis légitimes du mal et de l\'injustice. Les dragons d\'or 
        sont porteurs de connaisances et vivent reclus. Ils préfèrent les repaires de pierre et ont un cône de feu 
        comme souffle.</p>',
    ];

    private const DRAGON_SILVER = [
        'name' => 'Dragon d\'argent, ancien',
        'fp' => 23,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-argent-ancien',
        'image' => '/build/images/Dragon_Silver.webp',
        'description' => '<p>Les dragons d\'argent sont des créatures royales et loyales bonnes qui ont 
        souvent pris une forme humanoïde et vivent parmi les humains et les elfes. Leur souffle est un cône de froid 
        glacial, et ils se reposent normalement dans les montagnes. Ils sont très intelligents et préfèrent ne pas se 
        battre sauf en cas de nécessité.</p>',
    ];

    private const DRAGON_BRASS = [
        'name' => 'Dragon d\'airain, ancien',
        'fp' => 20,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-airain-ancien',
        'image' => '/build/images/Dragon_Brass.webp',
        'description' => '<p>Les dragons d\'airain sont extrêmement bavards et aiment la chaleur intense des 
        déserts. Ils engagent souvent des ennemis et des amis pendant des heures de longues conversations. Ils évitent 
        le combat si possible, mais utilisent leur souffle, un cône de gaz somnifère, pour maîtriser leurs ennemis 
        s\'ils sont menacés. Ce sont les plus faibles des dragons métalliques classiques, mais toujours chaotiques 
        bons.</p>',
    ];

    private const DRAGON_BLUE = [
        'name' => 'Dragon bleu, ancien',
        'fp' => 23,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-bleu-ancien',
        'image' => '/build/images/Dragon_Blue.webp',
        'description' => '<p>Les dragons bleus, ou dragons des tempêtes, sont des chromatiques loyaux mauvais 
        manipulateurs qui sont tristement célèbres pour leur habileté à créer des hallucinations et leur utilisation 
        cruelle des choses. Ils préfèrent les combats aériens, qui leur permettent d\'utiliser leur souffle électrique 
        le plus efficacement possible. Les dragons bleus vivent le plus souvent dans des friches et des déserts arides. 
        Ils sont le deuxième plus puissant des dragons chromatiques.</p>',
    ];

    private const DRAGON_WHITE = [
        'name' => 'Dragon blanc, ancien',
        'fp' => 20,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-blanc-ancien',
        'image' => '/build/images/Dragon_White.webp',
        'description' => '<p>Les dragons blancs, également appelés dragons ou wyvernes de glace, sont les plus 
        petits et les plus faibles des dragons chromatiques classiques. Cependant, ils ne sont en aucun cas 
        inoffensifs. Les dragons blancs sont extrêmement bien adaptés à leur habitat arctique et ont d\'excellentes 
        mémoires. Ils sont cependant plus sauvages que les autres dragons et toujours chaotiques mauvais. Ils soufflent 
        un cône de glace ou de givre.</p>',
    ];

    private const DRACOLICH = [
        'name' => 'Dracoliche bleue, adulte',
        'fp' => 17,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dracoliche-bleue-adulte',
        'image' => 'https://www.aidedd.org/dnd/images/dracolich.jpg',
        'description' => '<p>Même s\'ils vivent longtemps, tous les dragons doivent finalement mourir. Cette 
        pensée ne convient pas à de nombreux dragons, dont certains se laissent transformer par l\'énergie 
        nécromantique et les rituels anciens en puissants dracoliches morts-vivants. Seuls les dragons les plus 
        narcissiques choisissent cette voie, sachant que ce faisant, ils rompent tous les liens avec leurs proches et 
        les dieux dragons.</p>',
    ];

    private const BALOR = [
        'name' => 'Balor',
        'fp' => 19,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=balor',
        'image' => 'https://www.aidedd.org/dnd/images/demon-balor.jpg',
        'description' => '<p>Figures d\'un mal ancien et terrible, les balors règnent en général sur les 
        armées démoniaques, aspirant à prendre le pouvoir tout en détruisant toutes les créatures qui s\'opposent à 
        eux.<br/><br/>
        Maniant un fouet enflammé et une épée longue qui canalise la puissance de la tempête, les prouesses de 
        combat d’un balor sont alimentées par la haine et la rage. Il canalise cette fureur démoniaque dans ses 
        affres de la mort, tombant dans une explosion de feu qui peut détruire même les ennemis les plus 
        résistants.</p>',
    ];

    private const DEMILICH = [
        'name' => 'Demi-liche',
        'fp' => 18,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=demi-liche',
        'image' => 'https://www.aidedd.org/dnd/images/demilich.jpg',
        'description' => '<p>Une demi-liche est une liche qui n\'est pas parvenue à suffisament alimenter en 
        âmes son phylactère pour empêcher son corps de se décomposer. Dans la plupart des cas, une demi-liche est 
        réduite à un crâne, quelques ossements et un tas de poussière.</p>',
    ];

    private const LICH = [
        'name' => 'Liche',
        'fp' => 21,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=liche',
        'image' => 'https://www.aidedd.org/dnd/images/lich.jpg',
        'description' => '<p>Une liche est le résultat d\'un rituel perpétré par un magicien de haut niveau 
        prêt à tout sacrifier pour obtenir la vie éternelle.</p>',
    ];

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getMobs(string $bossKey, string $danger): array
    {
        if ($bossKey === static::LICH()->getKey()) {
            return [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::CULTIST()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::FANATIC()->getValue()],
            ];
        }

        return [];
    }

    public static function getMaxFP(): int
    {
        return max(array_column(static::toArray(), 'fp'));
    }
}
