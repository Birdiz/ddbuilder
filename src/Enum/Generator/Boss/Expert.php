<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\Generator\AbstractBossGeneratorEnum;

/**
 * Class Expert.
 *
 * @method static Expert ABOLETH()
 * @method static Expert CHAMPION()
 * @method static Expert CHIMERE()
 * @method static Expert DRAGON_GREEN()
 * @method static Expert DRAGON_RED()
 * @method static Expert DRAGON_BLACK()
 * @method static Expert DRAGON_COPPER()
 * @method static Expert DRAGON_BRONZE()
 * @method static Expert DRAGON_GOLD()
 * @method static Expert DRAGON_SILVER()
 * @method static Expert DRAGON_BRASS()
 * @method static Expert DRAGON_BLUE()
 * @method static Expert DRAGON_WHITE()
 * @method static Expert DEVA()
 * @method static Expert BONE_DEVIL()
 * @method static Expert DROW_PRIESTRESS_LOLTH()
 * @method static Expert HAG_ANNIS()
 * @method static Expert HOBGOBLIN_WARLORD()
 * @method static Expert MEDUSE()
 * @method static Expert NAGA_SPIRIT()
 * @method static Expert NECROMANT()
 * @method static Expert WIVERNE()
 * @method static Expert ENCHANTER()
 * @method static Expert MASTER_THIEF()
 * @method static Expert HILL_GIANT()
 */
class Expert extends AbstractBossGeneratorEnum
{
    private const ABOLETH = [
        'name' => 'Aboleth',
        'fp' => 10,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=aboleth',
        'image' => 'https://www.aidedd.org/dnd/images/aboleth.jpg',
        'description' => '<p>Le repaire des Aboleths dans les lacs souterrains ou les profondeurs rocheuses de 
        l\'océan, souvent entouré par les ruines d\'une ancienne ville aboleth déchue. Un aboleth passe la majeure 
        partie de son existence sous l\'eau, faisant parfois surface pour traiter avec des visiteurs ou des fidèles 
        dérangés.<br/><br/>
        Avant la venue des dieux, les aboleths se cachaient dans les océans primordiaux et les lacs souterrains. 
        Ils ont tendu leur esprit et ont pris le contrôle des formes de vie en plein essor du royaume des mortels, 
        faisant de ces créatures leurs esclaves. Leur domination en faisait des dieux. Puis les vrais dieux sont 
        apparus, brisant l\'empire des aboleths et libérant leurs esclaves.<br/><br/>
        Les Aboleths n\'ont jamais oublié.</p>',
    ];

    private const CHAMPION = [
        'name' => 'Champion',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=champion',
        'image' => 'https://5e.tools/img/bestiary/VGM/Champion.jpg',
        'description' => '<p>Les champions sont de puissants guerriers qui ont perfectionné leurs compétences 
        de combat dans les guerres ou les fosses de gladiateurs. Pour les soldats et les autres personnes qui se 
        battent pour gagner leur vie, les champions sont aussi influents que les nobles, et leur présence est courtisée 
        comme un signe de statut parmi les dirigeants.</p>',
    ];

    private const CHIMERE = [
        'name' => 'Chimere',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=chimere',
        'image' => 'https://www.aidedd.org/dnd/images/chimera.jpg',
        'description' => '<p>Une chimère est la vile combinaison d\'une chèvre, d\'un lion et d\'un dragon, et 
        elle arbore les têtes de chacune de ces trois créatures. Elle affectionne d\'arriver en volant pour engloutir 
        sa proie dans les flammes grâce à son souffle avant d\'atterrir et de se jeter au combat.</p>',
    ];

    private const DRAGON_GREEN = [
        'name' => 'Dragon vert, jeune',
        'fp' => 8,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-vert-jeune',
        'image' => '/build/images/Dragon_Green.webp',
        'description' => '<p>Les dragons verts sont des créatures forestières très territoriales, trompeuses, 
        qui aiment les secrets et les intrigues. Bien que loyaux mauvais, ce sont des ennemis hypocrites et rusés qui 
        aiment le combat. Ils sont le troisième plus puissant des dragons chromatiques. Leur souffle est un cône de gaz 
        vert toxique.</p>',
    ];

    private const DRAGON_RED = [
        'name' => 'Dragon rouge, jeune',
        'fp' => 10,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-rouge-jeune',
        'image' => '/build/images/Dragon_Red.webp',
        'description' => '<p>Les dragons rouges sont des créatures maléfiques avides et chaotiques, 
        intéressées uniquement par leur propre bien-être, leur vanité et l\'extension de leurs trésors. Ils sont 
        extrêmement confiants, étant le plus grand et le plus puissant des dragons chromatiques. Généralement trouvés 
        dans les régions montagneuses, ils soufflent un cône de feu.</p>',
    ];

    private const DRAGON_BLACK = [
        'name' => 'Dragon noir, jeune',
        'fp' => 7,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-noir-jeune',
        'image' => '/build/images/Dragon_Black.webp',
        'description' => '<p>Les dragons noirs sont les plus vils et les plus cruels de tous les dragons 
        chromatiques. Excellents nageurs qui vivent normalement dans les marécages et les marais, ils préfèrent les 
        attaques d\'embuscade. Ils ont un souffle acide corrosif.</p>',
    ];

    private const DRAGON_COPPER = [
        'name' => 'Dragon de cuivre, jeune',
        'fp' => 7,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-cuivre-jeune',
        'image' => '/build/images/Dragon_Copper.webp',
        'description' => '<p>Les dragons de cuivre sont nés blagueurs, farceurs et énigmatiques. Ils sont 
        chaotiques bons et vivent dans des montagnes rocheuses. Les dragons de cuivre ont un souffle acide mais 
        préfèrent éviter le combat, au lieu de cela, narguer et taquiner les ennemis jusqu\'à ce qu\'ils partent.
        </p>',
    ];

    private const DRAGON_BRONZE = [
        'name' => 'Dragon de bronze, jeune',
        'fp' => 8,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-bronze-jeune',
        'image' => '/build/images/Dragon_Bronze.webp',
        'description' => '<p>Les dragons de bronze sont des dragons curieux avec une apparence féroce et une 
        fascination pour la guerre. Ils cherchent souvent à combattre le mal et rejoignent des armées alignées vers le 
        bien sous une forme humanoïde. Ils sont loyaux bons, vivent dans les zones côtières et ont un souffle 
        électrique.</p>',
    ];

    private const DRAGON_GOLD = [
        'name' => 'Dragon d\'or, jeune',
        'fp' => 10,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-or-jeune',
        'image' => '/build/images/Dragon_Gold.webp',
        'description' => '<p>Les dragons d\'or sont les plus puissants et majestueux des dragons métalliques 
        classiques, ce sont de sages, loyaux bons et ennemis légitimes du mal et de l\'injustice. Les dragons d\'or 
        sont porteurs de connaisances et vivent reclus. Ils préfèrent les repaires de pierre et ont un cône de feu 
        comme souffle.</p>',
    ];

    private const DRAGON_SILVER = [
        'name' => 'Dragon d\'argent, jeune',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-argent-jeune',
        'image' => '/build/images/Dragon_Silver.webp',
        'description' => '<p>Les dragons d\'argent sont des créatures royales et loyales bonnes qui ont 
        souvent pris une forme humanoïde et vivent parmi les humains et les elfes. Leur souffle est un cône de froid 
        glacial, et ils se reposent normalement dans les montagnes. Ils sont très intelligents et préfèrent ne pas se 
        battre sauf en cas de nécessité.</p>',
    ];

    private const DRAGON_BRASS = [
        'name' => 'Dragon d\'airain, jeune',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-airain-jeune',
        'image' => '/build/images/Dragon_Brass.webp',
        'description' => '<p>Les dragons d\'airain sont extrêmement bavards et aiment la chaleur intense des 
        déserts. Ils engagent souvent des ennemis et des amis pendant des heures de longues conversations. Ils évitent 
        le combat si possible, mais utilisent leur souffle, un cône de gaz somnifère, pour maîtriser leurs ennemis 
        s\'ils sont menacés. Ce sont les plus faibles des dragons métalliques classiques, mais toujours chaotiques 
        bons.</p>',
    ];

    private const DRAGON_BLUE = [
        'name' => 'Dragon bleu, jeune',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-bleu-jeune',
        'image' => '/build/images/Dragon_Blue.webp',
        'description' => '<p>Les dragons bleus, ou dragons des tempêtes, sont des chromatiques loyaux mauvais 
        manipulateurs qui sont tristement célèbres pour leur habileté à créer des hallucinations et leur utilisation 
        cruelle des choses. Ils préfèrent les combats aériens, qui leur permettent d\'utiliser leur souffle électrique 
        le plus efficacement possible. Les dragons bleus vivent le plus souvent dans des friches et des déserts arides. 
        Ils sont le deuxième plus puissant des dragons chromatiques.</p>',
    ];

    private const DRAGON_WHITE = [
        'name' => 'Dragon blanc, jeune',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-blanc-jeune',
        'image' => '/build/images/Dragon_White.webp',
        'description' => '<p>Les dragons blancs, également appelés dragons ou wyvernes de glace, sont les plus 
        petits et les plus faibles des dragons chromatiques classiques. Cependant, ils ne sont en aucun cas 
        inoffensifs. Les dragons blancs sont extrêmement bien adaptés à leur habitat arctique et ont d\'excellentes 
        mémoires. Ils sont cependant plus sauvages que les autres dragons et toujours chaotiques mauvais. Ils soufflent 
        un cône de glace ou de givre.</p>',
    ];

    private const DEVA = [
        'name' => 'Déva',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=deva',
        'image' => 'https://www.aidedd.org/dnd/images/deva.jpg',
        'description' => '<p>Les dévas sont des anges qui agissent en tant que messagers divins ou agents du 
        plan matériel, de Gisombre et de la Féérie et qui peuvent prendre une forme appropriée au royaume dans lequel 
        ils sont envoyés.<br/><br/>
        La légende raconte des anges qui prennent une forme mortelle pendant des années, prêtant aide, espoir et 
        courage aux gens de bon cœur. Un deva peut prendre n\'importe quelle forme, bien qu\'il préfère apparaître 
        aux mortels comme un humanoïde ou un animal inoffensif. Lorsque les circonstances l\'exigent, un deva est une 
        belle créature humanoïde à la peau argentée. Ses cheveux et ses yeux brillent d\'un éclat surnaturel, et de 
        grandes ailes plumeuses se déploient de ses omoplates.</p>',
    ];

    private const BONE_DEVIL = [
        'name' => 'Diable osseux',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=diable-osseux',
        'image' => 'https://www.aidedd.org/dnd/images/bone-devil.jpg',
        'description' => '<p>Poussés par la haine, la convoitise et l\'envie, les démons osseux agissent comme 
        les cruels maîtres d\'œuvre des Neuf enfers. Ils ont mis au travail des démons plus faibles, prenant un plaisir 
        particulier à voir des démons qui les défient rétrogradés. En même temps, ils aspirent à une promotion et 
        sont amèrement envieux de leurs supérieurs, essayant de se faire la faveur même si cela les agace de le 
        faire.</p>',
    ];

    private const DROW_PRIESTRESS_LOLTH = [
        'name' => 'Prêtresse de Lolth Drow',
        'fp' => 8,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=drow-pretresse-de-lolth',
        'image' => 'https://www.aidedd.org/dnd/images/drow.jpg',
        'description' => '<p>Les drows femelles ayant des liens de sang avec une maison noble sont modelées et 
        entraînées dès leur naissance pour devenir prêtresses de Lolth. La reine araignée ne permet pas aux drows 
        mâles de tenir de telles positions.<br/><br/>
        Ces prêtresses exécutent la volonté de la reine araignée et, par conséquent, exercent un pouvoir et une 
        influence considérables dans la société drow. Les mères matrones qui dirigent les maisons drows sont les 
        plus puissantes des prêtresses de Lolth, mais elles doivent constamment équilibrer leur dévotion à la reine 
        araignée avec leur dévotion à leurs familles.</p>',
    ];

    private const HAG_ANNIS = [
        'name' => 'Guenaude annis',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=guenaude-annis',
        'image' => 'https://www.aidedd.org/dnd/images/annis-hag.jpg',
        'description' => '<p>Les guenaudes annis trouvent leur repaire dans les montagnes ou les collines. 
        Bien qu\'elles soient bossues et aux épaules bossues, elles sont les plus grandes et les plus imposantes 
        physiquement de leur genre, mesurant 2m50 de haut.<br/><br/>
        Les guenaudes annis laissent des gages de leur cruauté à la lisière des forêts et d\'autres zones qu\'elles 
        revendiquent. De cette manière, elles provoquent la peur et la paranoïa dans les villages et les colonies 
        voisins. Pour une guenaude annis, rien n\'est plus doux que de transformer une communauté dynamique en un 
        lieu paralysé par la terreur, où les gens ne s\'aventurent jamais la nuit, les étrangers sont confrontés à la 
        suspicion et à la colère, et les parents avertissent leurs enfants de "soyez sages, sinon les annis viendront 
        te chercher."</p>',
    ];

    private const HOBGOBLIN_WARLORD = [
        'name' => 'Seigneur de guerre hobgobelin',
        'fp' => 6,
        'xp' => 2300,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=hobgobelin-seigneur-de-guerre',
        'image' => 'https://www.aidedd.org/dnd/images/hobgoblin.jpg',
        'description' => '<p>Les cornes de guerre retentissent, les pierres volent des catapultes et le 
        tonnerre de mille pieds bottés résonne à travers le pays alors que les Hobgobelins marchent au combat. À 
        travers les frontières de la civilisation, les colonies et les colons doivent lutter contre ces humanoïdes 
        agressifs, dont la soif de conquête n\'est jamais satisfaite.<br/>
        Les Hobgoblins ont la peau orange foncé ou rouge-orange et les cheveux allant du brun rouge foncé au gris 
        foncé. Des yeux jaunes ou brun foncé scrutent sous leurs sourcils de scarabée, et leurs larges bouches 
        arborent des dents pointues et jaunies. Un hobgobelin mâle peut avoir un grand nez bleu ou rouge, qui 
        symbolise la virilité et le pouvoir chez les gobelins. Les Hobgobelins peuvent vivre aussi longtemps que les 
        humains, bien que leur amour de la guerre et de la bataille signifie que peu le font.</p>',
    ];

    private const MEDUSE = [
        'name' => 'Méduse',
        'fp' => 6,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=meduse',
        'image' => 'https://www.aidedd.org/dnd/images/medusa.jpg',
        'description' => '<p>Victime d\'une terrible malédiction, la méduse à la chevelure venimeuse pétrifie 
        tous ceux qui posent le regard sur elle, et les transforme en statues de pierre à la gloire de sa dépravation.
        </p>',
    ];

    private const NAGA_SPIRIT = [
        'name' => 'Naga corrupteur',
        'fp' => 8,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=naga-corrupteur',
        'image' => 'https://www.aidedd.org/dnd/images/spirit-naga.jpg',
        'description' => '<p>Les nagas spirituels vivent dans l\'obscurité et la méchanceté, complotant 
        constamment la vengeance contre les créatures qui leur ont fait du tort - ou qui, selon eux, leur ont fait du 
        tort. Se cachant dans des cavernes et des ruines lugubres, ils consacrent leur temps à développer de nouveaux 
        sorts et à asservir les mortels dont ils s\'entourent. Un naga spirituel aime charmer ses ennemis, les 
        rapprochant afin de pouvoir enfoncer ses crocs venimeux dans leur chair.</p>',
    ];

    private const NECROMANT = [
        'name' => 'Nécromant',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=necromant',
        'image' => '',
        'description' => '<p>Les nécromanciens sont des magiciens spécialisés qui étudient l\'interaction de 
        la vie, de la mort et de l\'au-delà. Certains aiment déterrer des cadavres pour créer des esclaves 
        morts-vivants. Quelques-uns utilisent leurs pouvoirs pour le bien, deviennent chasseurs des morts-vivants et 
        risquent leur vie pour sauver les autres.</p>',
    ];

    private const WIVERNE = [
        'name' => 'Wiverne',
        'fp' => 9,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=wiverne',
        'image' => 'https://www.aidedd.org/dnd/images/wyvern.jpg',
        'description' => '<p>Cousines des grands dragons, les wivernes possèdent deux pattes écailleuses, des 
        ailes de cuir et une queue musclée surmontée d\'un dard empoisonné pouvant tuer une créature en quelques 
        secondes.</p>',
    ];

    private const MASTER_THIEF = [
        'name' => 'Maître voleur',
        'fp' => 5,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=maitre-voleur',
        'image' => '',
        'description' => '',
    ];

    private const HILL_GIANT = [
        'name' => 'Géant des collines',
        'fp' => 5,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=geant-des-collines',
        'image' => 'https://www.aidedd.org/dnd/images/hill-giant.jpg',
        'description' => '<p>Les géants des collines sont des brutes égoïstes et sans-cervelles qui chassent 
        et pillent en permanence afin de trouver de la nourriture. Leur peau est bronzée du fait qu\'ils passent leur 
        vie sous le soleil, et leurs armes sont des arbres déracinés et des rochers arrachés du sol.</p>',
    ];

    private const ENCHANTER = [
        'name' => 'Enchanteur',
        'fp' => 5,
        'xp' => 1800,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=magicien-enchanteur',
        'image' => '',
        'description' => '<p>Les enchanteurs sont des magiciens spécialisés qui savent comment modifier et 
        contrôler les esprits en utilisant la magie. Ils peuvent être aimables et intéressants, utilisant la magie pour 
        manipuler les personnes seulement quand la persuasion conventionnelle a échoué, ou être impolis et exigeants, 
        en utilisant et en s\'appuyant sur des serviteurs charmés et obéissants.</p>',
    ];

    private const FIRE_ELEMENTAL = [
        'name' => 'Élémentaire du feu',
        'fp' => 5,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=elementaire-du-feu',
        'image' => 'https://www.aidedd.org/dnd/images/fire-elemental.jpg',
        'description' => '<p>Un semblant de silhouette humanoïde apparaît au centre de la capricieuse 
        dévastation qu\'est un élémentaire du feu. Où qu\'il passe, il embrase tout ce qui l\'entoure, ne laissant 
        derrière lui que cendre et fumée.</p>',
    ];

    private const WATER_ELEMENTAL = [
        'name' => 'Élémentaire de l\'eau',
        'fp' => 5,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=elementaire-de-l-eau',
        'image' => 'https://www.aidedd.org/dnd/images/water-elemental.jpg',
        'description' => '<p>Êtres natifs du plan Élémentaire de l\'eau et convoqués dans le monde, les 
        élémentaires de l\'eau ressemblent aux crêtes des vagues qui roulent sur le sol. Un élémentaire de l\'eau 
        engloutit toutes les créatures qui se dressent contre lui.</p>',
    ];

    private const AIR_ELEMENTAL = [
        'name' => 'Élémentaire de l\'air',
        'fp' => 5,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=elementaire-de-l-air',
        'image' => 'https://www.aidedd.org/dnd/images/air-elemental.jpg',
        'description' => '<p>Un élémentaire de l\'air est une masse d\'air tourbillonnante prenant la vague 
        apparence d\'un visage. Il peut se transformer en un tourbillon hurlant, créant alors un cyclone destructeur 
        qui éjecte tout sur son passage.</p>',
    ];

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getMobs(string $bossKey, string $danger): array
    {
        return match ($bossKey) {
            static::BONE_DEVIL()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::QUASIT()->getValue()],
            ],
            static::CHAMPION()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
            ],
            static::ENCHANTER()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::ANIMATED_ARMOR()->getValue()],
            ],
            static::DROW_PRIESTRESS_LOLTH()->getKey() => [
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::DROW()->getValue()],
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GOBLIN()->getValue()],
            ],
            static::HILL_GIANT()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GOBLIN()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::WORG()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::HALF_OGRE()->getValue()],
            ],
            static::HOBGOBLIN_WARLORD()->getKey() => [
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::HOBGOBLIN()->getValue()],
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GOBLIN()->getValue()],
            ],
            static::MASTER_THIEF()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::THUG()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::SCOUT()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::SPY()->getValue()],
            ],
            static::NECROMANT()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::CULTIST()->getValue()],
            ],
            default => [],
        };
    }
}
