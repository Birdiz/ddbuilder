<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\Generator\AbstractBossGeneratorEnum;

/**
 * Class Hero.
 *
 * @method static Hero HORNED_DEVIL()
 * @method static Hero BEHIR()
 * @method static Hero ARCHIMAGE()
 * @method static Hero DJINN()
 * @method static Hero DRAGON_GREEN()
 * @method static Hero DRAGON_BLACK()
 * @method static Hero DRAGON_COPPER()
 * @method static Hero DRAGON_BRONZE()
 * @method static Hero DRAGON_SILVER()
 * @method static Hero DRAGON_BRASS()
 * @method static Hero DRAGON_BLUE()
 * @method static Hero DRAGON_WHITE()
 * @method static Hero DRAGON_RED_SHADOW()
 * @method static Hero WARLORD()
 * @method static Hero ELDER_BRAIN()
 * @method static Hero VAMPIRE()
 * @method static Hero DEATH_TYRAN()
 * @method static Hero BEHOLDER()
 * @method static Hero MUMMY_LORD()
 * @method static Hero IRON_GOLEM()
 */
class Hero extends AbstractBossGeneratorEnum
{
    private const HORNED_DEVIL = [
        'name' => 'Diable cornu',
        'fp' => 11,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=diable-cornu',
        'image' => 'https://www.aidedd.org/dnd/images/horned-devil.jpg',
        'description' => '<p>Les démons cornus sont paresseux au point de devenir belliqueux et réticents à se 
        mettre en danger. De plus, ils détestent et craignent toute créature plus forte qu\'eux. Lorsqu\'ils sont 
        suffisamment provoqués ou contrariés, la fureur de ces démons peut être terrifiante.<br/><br/>
        Un malebranche est aussi grand qu\'un ogre et est gainé d\'écailles aussi résistantes que le fer. 
        L\'infanterie volante des légions infernales, les diables à cornes suivent les ordres à la lettre. 
        Leurs énormes ailes et leurs cornes volantes créent une présence intimidante alors qu\'ils tombent du ciel et 
        frappent avec des fourches mortelles et des queues de fouet.</p>',
    ];

    private const BEHIR = [
        'name' => 'Béhir',
        'fp' => 11,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=behir',
        'image' => 'https://www.aidedd.org/dnd/images/behir.jpg',
        'description' => '<p>Le serpentiforme Behir rampe le long des sols et grimpe sur les murs pour 
        atteindre sa proie. Son souffle éclair peut incinérer la plupart des créatures, même si des ennemis plus 
        puissants sont resserrés dans ses enroulements et mangés vivants.<br/><br/>
        La forme monstrueuse d’un behir ressemble à une combinaison de mille-pattes et de crocodile. Sa peau écaillée 
        va du bleu marin au bleu profond, passant au bleu pâle sur sa face inférieure.</p>',
    ];

    private const ARCHIMAGE = [
        'name' => 'Archimage',
        'fp' => 11,
        'xp' => 8400,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=archimage',
        'image' => 'https://www.aidedd.org/dnd/images/archmage.jpg',
        'description' => '<p>Les archimages sont de puissants (et généralement assez âgés) lanceurs de sorts 
        qui consacrent leur vie à l\'étude des arcanes. Les plus bienveillants d\'entre eux conseillent les rois et les 
        reines, tandis que les malveillants règnent en tyrans et cherchent à devenir des liches. Ceux qui ne tendent 
        ni vers le bien, ni vers le mal, s\'enferment dans des tours isolées afin de pratiquer leur magie sans risque 
        d\'être dérangés.<br/>
        Un archimage a généralement un ou plusieurs apprentis, et sa demeure possède de nombreux gardiens et 
        protections magiques destinés à décourager les intrus.</p>',
    ];

    private const DJINN = [
        'name' => 'Djinn',
        'fp' => 11,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=djinn',
        'image' => 'https://www.aidedd.org/dnd/images/djinni.jpg',
        'description' => '<p>Génies fiers et sensuels du plan élémentaire de l\'air, les djinns sont des 
        humanoïdes attrayants, grands et bien musclés, à la peau bleue et aux yeux sombres. Ils s\'habillent de soies 
        aérées et chatoyantes, conçues autant pour le confort que pour faire étalage de leur musculature.</p>',
    ];

    private const DRAGON_RED_SHADOW = [
        'name' => 'Dragon d\'ombre rouge, jeune',
        'fp' => 13,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-ombre-rouge-jeune',
        'image' => 'https://5e.tools/img/bestiary/MM/Shadow%20Dragon.jpg',
        'description' => '<p>Les dragons de l\'ombre sont de véritables dragons qui sont soit nés en Gisombre, 
        soit transformés par des années passées dans ses confins lugubres. Certains dragons de l\'ombre embrassent la 
        Gisombre pour ses paysages sombres et sa désolation. D\'autres cherchent à retourner sur le plan matériel, 
        avides de répandre les ténèbres et le mal du plan de l\'ombre.</p>',
    ];

    private const DRAGON_GREEN = [
        'name' => 'Dragon vert, adulte',
        'fp' => 15,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-vert-adulte',
        'image' => '/build/images/Dragon_Green.webp',
        'description' => '<p>Les dragons verts sont des créatures forestières très territoriales, trompeuses, 
        qui aiment les secrets et les intrigues. Bien que loyaux mauvais, ce sont des ennemis hypocrites et rusés qui 
        aiment le combat. Ils sont le troisième plus puissant des dragons chromatiques. Leur souffle est un cône de gaz 
        vert toxique.</p>',
    ];

    private const DRAGON_BLACK = [
        'name' => 'Dragon noir, adulte',
        'fp' => 14,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-noir-adulte',
        'image' => '/build/images/Dragon_Black.webp',
        'description' => '<p>Les dragons noirs sont les plus vils et les plus cruels de tous les dragons 
        chromatiques. Excellents nageurs qui vivent normalement dans les marécages et les marais, ils préfèrent les 
        attaques d\'embuscade. Ils ont un souffle acide corrosif.</p>',
    ];

    private const DRAGON_COPPER = [
        'name' => 'Dragon de cuivre, adulte',
        'fp' => 14,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-cuivre-adulte',
        'image' => '/build/images/Dragon_Copper.webp',
        'description' => '<p>Les dragons de cuivre sont nés blagueurs, farceurs et énigmatiques. Ils sont 
        chaotiques bons et vivent dans des montagnes rocheuses. Les dragons de cuivre ont un souffle acide mais 
        préfèrent éviter le combat, au lieu de cela, narguer et taquiner les ennemis jusqu\'à ce qu\'ils partent.
        </p>',
    ];

    private const DRAGON_BRONZE = [
        'name' => 'Dragon de bronze, adulte',
        'fp' => 15,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-de-bronze-adulte',
        'image' => '/build/images/Dragon_Bronze.webp',
        'description' => '<p>Les dragons de bronze sont des dragons curieux avec une apparence féroce et une 
        fascination pour la guerre. Ils cherchent souvent à combattre le mal et rejoignent des armées alignées vers le 
        bien sous une forme humanoïde. Ils sont loyaux bons, vivent dans les zones côtières et ont un souffle 
        électrique.</p>',
    ];

    private const DRAGON_SILVER = [
        'name' => 'Dragon d\'argent, adulte',
        'fp' => 16,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-argent-adulte',
        'image' => '/build/images/Dragon_Silver.webp',
        'description' => '<p>Les dragons d\'argent sont des créatures royales et loyales bonnes qui ont 
        souvent pris une forme humanoïde et vivent parmi les humains et les elfes. Leur souffle est un cône de froid 
        glacial, et ils se reposent normalement dans les montagnes. Ils sont très intelligents et préfèrent ne pas se 
        battre sauf en cas de nécessité.</p>',
    ];

    private const DRAGON_BRASS = [
        'name' => 'Dragon d\'airain, adulte',
        'fp' => 13,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-d-airain-adulte',
        'image' => '/build/images/Dragon_Brass.webp',
        'description' => '<p>Les dragons d\'airain sont extrêmement bavards et aiment la chaleur intense des 
        déserts. Ils engagent souvent des ennemis et des amis pendant des heures de longues conversations. Ils évitent 
        le combat si possible, mais utilisent leur souffle, un cône de gaz somnifère, pour maîtriser leurs ennemis 
        s\'ils sont menacés. Ce sont les plus faibles des dragons métalliques classiques, mais toujours chaotiques 
        bons.</p>',
    ];

    private const DRAGON_BLUE = [
        'name' => 'Dragon bleu, adulte',
        'fp' => 16,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-bleu-adulte',
        'image' => '/build/images/Dragon_Blue.webp',
        'description' => '<p>Les dragons bleus, ou dragons des tempêtes, sont des chromatiques loyaux mauvais 
        manipulateurs qui sont tristement célèbres pour leur habileté à créer des hallucinations et leur utilisation 
        cruelle des choses. Ils préfèrent les combats aériens, qui leur permettent d\'utiliser leur souffle électrique 
        le plus efficacement possible. Les dragons bleus vivent le plus souvent dans des friches et des déserts arides. 
        Ils sont le deuxième plus puissant des dragons chromatiques.</p>',
    ];

    private const DRAGON_WHITE = [
        'name' => 'Dragon blanc, adulte',
        'fp' => 13,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=dragon-blanc-adulte',
        'image' => '/build/images/Dragon_White.webp',
        'description' => '<p>Les dragons blancs, également appelés dragons ou wyvernes de glace, sont les plus 
        petits et les plus faibles des dragons chromatiques classiques. Cependant, ils ne sont en aucun cas 
        inoffensifs. Les dragons blancs sont extrêmement bien adaptés à leur habitat arctique et ont d\'excellentes 
        mémoires. Ils sont cependant plus sauvages que les autres dragons et toujours chaotiques mauvais. Ils soufflent 
        un cône de glace ou de givre.</p>',
    ];

    private const WARLORD = [
        'name' => 'Seigneur de guerre',
        'fp' => 12,
        'xp' => 8400,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=seigneur-de-guerre',
        'image' => '',
        'description' => '<p>Les chefs de guerre pourraient être des héros paysans charismatiques qui ont 
        rallié leur peuple pour renverser les tyrans, des commandants de champ de bataille chevronnés qui apportent 
        leur expérience avec eux lorsqu\'ils dirigent leurs alliés au combat, des guerriers célèbres, des figures 
        d\'inspiration et plus encore. Les chefs de guerre comprennent la bataille à tous les niveaux, des tactiques 
        des petites unités à la grande échelle du champ de bataille. Guerriers capables à part entière, ils possèdent 
        un esprit vif et des personnalités fortes, qui les aident à mener leurs compagnons à réussir à vaincre leurs 
        ennemis.</p>',
    ];

    private const ELDER_BRAIN = [
        'name' => 'Cerveau, vénérable',
        'fp' => 14,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=cerveau-venerable',
        'image' => 'https://www.aidedd.org/dnd/images/elder-brain.jpg',
        'description' => '<p>Expression ultime de la domination illithide, un cerveau aîné s\'étale dans une 
        cuve de saumure visqueuse, touchant les pensées de créatures proches et lointaines. Il griffonne sur la toile 
        de leur esprit, réécrit leurs pensées et crée leurs rêves.</p>',
    ];

    private const VAMPIRE = [
        'name' => 'Vampire',
        'fp' => 13,
        'xp' => 10000,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=vampire',
        'image' => 'https://www.aidedd.org/dnd/images/vampire.jpg',
        'description' => '<p>La victime de la morsure d\'un vampire, si elle est enterrée, devient la nuit 
        suivante un vampirien sous son contrôle. Mais lorsque ce vampire meurt ou, plus rarement, s\'il permet à son 
        vampirien de boire son sang, le vampirien se convertit en vampire à part entière, libre de ses actes mais 
        toujours condamné à la nuit éternelle.</p>',
    ];

    private const DEATH_TYRAN = [
        'name' => 'Tyramort',
        'fp' => 14,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=tyramort',
        'image' => 'https://www.aidedd.org/dnd/images/death-tyrant.jpg',
        'description' => '<p>En de rares occasions, l\'esprit endormi d\'un spectateur dérive vers des 
        endroits au-delà de sa folie normale, imaginant une réalité dans laquelle il existe au-delà de la mort. Lorsque 
        de tels rêves prennent racine, un beholder peut se transformer, sa chair se détachant pour laisser un tyran de 
        la mort derrière lui. Ce monstre possède la ruse et une grande partie de la magie qu\'il avait dans la vie, 
        mais il est alimenté par le pouvoir de la non-mort. Un tyran de la mort apparaît comme un crâne massif et nu, 
        avec une pointe de lumière rouge brillant dans son orbite creuse. Avec ses yeux pourris, dix yeux spectraux 
        planent au-dessus de la créature et l\'éblouissent dans toutes les directions.</p>',
    ];

    private const BEHOLDER = [
        'name' => 'Tyrannoeil',
        'fp' => 13,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=tyrannoeil',
        'image' => 'https://www.aidedd.org/dnd/images/beholder.jpg',
        'description' => '<p>Un coup d\'œil sur un beholder suffit pour évaluer sa nature odieuse et d\'un 
        autre monde. Agressifs, haineux et avides, ces aberrations rejettent toutes les autres créatures comme des 
        êtres inférieurs, jouant avec elles ou les détruisant à leur guise.<br/><br/>
        Le corps sphéroïde d\'un beholder lévite à tout moment, et son grand œil bombé se trouve au-dessus d\'une 
        large gueule à pleines dents, tandis que les plus petits yeux qui couronnent son corps se tordent et se 
        tournent pour garder ses ennemis en vue. Lorsqu\'un beholder dort, il ferme son œil central mais laisse ses 
        petits yeux ouverts et alertes.</p>',
    ];

    private const MUMMY_LORD = [
        'name' => 'Seigneur momie',
        'fp' => 15,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=seigneur-momie',
        'image' => 'https://www.aidedd.org/dnd/images/mummy-lord.jpg',
        'description' => '<p>Un seigneur momie veille sur un ancien temple ou une tombe qui est protégé par de 
        petits morts-vivants et truqué de pièges. Caché dans ce temple se trouve le sarcophage où un seigneur momie 
        garde ses plus grands trésors.</p>',
    ];

    private const IRON_GOLEM = [
        'name' => 'Golem de fer',
        'fp' => 16,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=golem-de-fer',
        'image' => 'https://www.aidedd.org/dnd/images/iron-golem.jpg',
        'description' => '<p>Le plus puissant des golems, le golem de fer est un géant massif et imposant en 
        métal lourd. La forme d\'un golem de fer peut être façonnée sous n\'importe quelle forme, bien que la plupart 
        soient conçues pour ressembler à des armures géantes. Son poing peut détruire des créatures d\'un seul coup, et 
        ses pas tremblants secouent la terre sous ses pieds. Les golems de fer brandissent d\'énormes lames pour 
        étendre leur portée, et tous peuvent cracher des nuages de poison mortel.</p>',
    ];

    /**
     * @return array<int, array<string, mixed>>
     */
    public static function getMobs(string $bossKey, string $danger): array
    {
        $mobs = [
            static::HORNED_DEVIL()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::QUASIT()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Beginner::SPINED_DEVIL()->getValue()],
            ],
            static::ARCHIMAGE()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::APPRENTICE_WIZARD()->getValue()],
            ],
            static::ELDER_BRAIN()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::DROW()->getValue()],
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::DUERGAR()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::MIND_FLAYER()->getValue()],
            ],
            static::WARLORD()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::ARCHER()->getValue()],
            ],
            static::VAMPIRE()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::CULTIST()->getValue()],
                ['nb' => static::getNbMobs($danger), 'mob' => Mob::GUARD()->getValue()],
                ['nb' => static::getNbMobs($danger, true), 'mob' => Mob::FANATIC()->getValue()],
            ],
            static::MUMMY_LORD()->getKey() => [
                ['nb' => static::getNbMobs($danger), 'mob' => Beginner::MUMMY()->getValue()],
            ],
        ];

        return array_key_exists($bossKey, $mobs) ? $mobs[$bossKey] : [];
    }
}
