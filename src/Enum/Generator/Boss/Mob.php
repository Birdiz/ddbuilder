<?php

declare(strict_types=1);

namespace App\Enum\Generator\Boss;

use App\Model\AbstractEnum;

/**
 * Class Mob.
 *
 * @method static Mob ACOLYTE()
 * @method static Mob ARCHER()
 * @method static Mob APPRENTICE_WIZARD()
 * @method static Mob BANDIT()
 * @method static Mob BUGBEAR()
 * @method static Mob BULLYWUG()
 * @method static Mob BERSERKER()
 * @method static Mob CULTIST()
 * @method static Mob DROW()
 * @method static Mob DUERGAR()
 * @method static Mob FANATIC()
 * @method static Mob GUARD()
 * @method static Mob GOBLIN()
 * @method static Mob GNOLL()
 * @method static Mob HOBGOBLIN()
 * @method static Mob LIZARDFOLK()
 * @method static Mob LIZARDFOLK_CHAMAN()
 * @method static Mob MIND_FLAYER()
 * @method static Mob ORC()
 * @method static Mob HALF_OGRE()
 * @method static Mob QUASIT()
 * @method static Mob SCOUT()
 * @method static Mob SPIDER()
 * @method static Mob THUG()
 * @method static Mob YUAN_TI_PUREBLOOD()
 * @method static Mob ZOMBIE()
 * @method static Mob ANIMATED_ARMOR()
 * @method static Mob SPY()
 * @method static Mob WORG()
 */
class Mob extends AbstractEnum
{
    private const ACOLYTE = [
        'name' => 'Acolyte',
        'fp' => '1/8',
        'xp' => 25,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=acolyte',
        'image' => '',
    ];

    private const ARCHER = [
        'name' => 'Archer',
        'fp' => 3,
        'xp' => 300,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=archer',
        'image' => '',
    ];

    private const APPRENTICE_WIZARD = [
        'name' => 'Apprenti magicien',
        'fp' => '1/4',
        'xp' => 50,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=apprenti-magicien',
        'image' => '',
    ];

    private const BANDIT = [
        'name' => 'Bandit',
        'fp' => '1/8',
        'xp' => 25,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=bandit',
        'image' => '',
    ];

    private const BUGBEAR = [
        'name' => 'Gobelours',
        'fp' => 1,
        'xp' => 200,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gobelours',
        'image' => 'https://www.aidedd.org/dnd/images/bugbear.jpg',
    ];

    private const BULLYWUG = [
        'name' => 'Brutacien',
        'fp' => '1/4',
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=brutacien',
        'image' => 'https://www.aidedd.org/dnd/images/bullywug.jpg',
    ];

    private const BERSERKER = [
        'name' => 'Berserker',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=berserker',
        'image' => 'https://www.aidedd.org/dnd/images/bullywug.jpg',
    ];

    private const CULTIST = [
        'name' => 'Cultiste',
        'fp' => '1/8',
        'xp' => 25,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=cultiste',
        'image' => '',
    ];

    private const DROW = [
        'name' => 'Drow',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=drow',
        'image' => 'https://www.aidedd.org/dnd/images/drow.jpg',
    ];

    private const DUERGAR = [
        'name' => 'Duergar',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=duergar',
        'image' => 'https://www.aidedd.org/dnd/images/duergar.jpg',
    ];

    private const FANATIC = [
        'name' => 'Fanatique',
        'fp' => 2,
        'xp' => 450,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=fanatique',
        'image' => 'https://www.aidedd.org/dnd/images/cult-fanatic.jpg',
    ];

    private const GUARD = [
        'name' => 'Garde',
        'fp' => '1/8',
        'xp' => 25,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=garde',
        'image' => '',
    ];

    private const GOBLIN = [
        'name' => 'Gobelin',
        'fp' => '1/4',
        'xp' => 50,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gobelin',
        'image' => 'https://www.aidedd.org/dnd/images/goblin.jpg',
    ];

    private const GNOLL = [
        'name' => 'Gnoll',
        'fp' => '1/2',
        'xp' => '100',
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=gnoll',
        'image' => 'https://www.aidedd.org/dnd/images/gnoll.jpg',
    ];

    private const HALF_OGRE = [
        'name' => 'Demi-ogre',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=demi-ogre',
        'image' => 'https://www.aidedd.org/dnd/images/half-ogre.jpg',
    ];

    private const HOBGOBLIN = [
        'name' => 'Hobgobelin',
        'fp' => '1/2',
        'xp' => 100,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=hobgobelin',
        'image' => 'https://www.aidedd.org/dnd/images/hobgoblin.jpg',
    ];

    private const LIZARDFOLK = [
        'name' => 'Homme-lezard',
        'fp' => '1/2',
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=homme-lezard',
        'image' => 'https://www.aidedd.org/dnd/images/lizardfolk.jpg',
    ];

    private const LIZARDFOLK_CHAMAN = [
        'name' => 'Chaman homme-lezard',
        'fp' => 2,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=homme-lezard-chaman',
        'image' => 'https://db4sgowjqfwig.cloudfront.net/campaigns/110358/assets/480731/
Lizardfolk_Shaman.jpg?1436562519',
    ];

    private const MIND_FLAYER = [
        'name' => 'Flagelleur mental',
        'fp' => 7,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=flagelleur-mental',
        'image' => 'https://www.aidedd.org/dnd/images/mind-flayer.jpg',
    ];

    private const ORC = [
        'name' => 'Orc',
        'fp' => '1/2',
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=orc',
        'image' => 'https://www.aidedd.org/dnd/images/orc.jpg',
    ];

    private const QUASIT = [
        'name' => 'Quasit',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=quasit',
        'image' => 'https://www.aidedd.org/dnd/images/quasit.jpg',
    ];

    private const SCOUT = [
        'name' => 'Éclaireur',
        'fp' => '1/2',
        'xp' => 100,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=eclaireur',
        'image' => 'https://www.aidedd.org/dnd/images/scout.jpg',
    ];

    private const SPIDER = [
        'name' => 'Araignée',
        'fp' => 0,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=araignee',
        'image' => 'https://www.aidedd.org/dnd/images/spider.jpg',
    ];

    private const THUG = [
        'name' => 'Malfrat',
        'fp' => '1/2',
        'xp' => 100,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=malfrat',
        'image' => 'https://www.aidedd.org/dnd/images/thug.jpg',
    ];

    private const YUAN_TI_PUREBLOOD = [
        'name' => 'Yuan-ti sang-pur',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=yuan-ti-sang-pur',
        'image' => 'https://www.aidedd.org/dnd/images/yuan-ti-pureblood.jpg',
    ];

    private const ZOMBIE = [
        'name' => 'Zombi',
        'fp' => '1/4',
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=zombi',
        'image' => 'https://www.aidedd.org/dnd/images/zombie.jpg',
    ];

    private const ANIMATED_ARMOR = [
        'name' => 'Armure animée',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=armure-animee',
        'image' => 'https://www.aidedd.org/dnd/images/animated-armor.jpg',
    ];

    private const SPY = [
        'name' => 'Espion',
        'fp' => 1,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=espion',
        'image' => '',
    ];

    private const WORG = [
        'name' => 'Worg',
        'fp' => '1/2',
        'xp' => 100,
        'link' => 'https://www.aidedd.org/dnd/monstres.php?vf=worg',
        'image' => 'https://www.aidedd.org/dnd/images/worg.jpg',
    ];
}
