<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

/**
 * Class AbilityScore.
 *
 * @method static AbilityScore FOR()
 * @method static AbilityScore DEX()
 * @method static AbilityScore CON()
 * @method static AbilityScore INT()
 * @method static AbilityScore SAG()
 * @method static AbilityScore CHA()
 */
class AbilityScore extends AbstractEnum
{
    private const FOR = 'Force';
    private const DEX = 'Dextérité';
    private const CON = 'Constitution';
    private const INT = 'Intelligence';
    private const SAG = 'Sagesse';
    private const CHA = 'Charisme';
}
