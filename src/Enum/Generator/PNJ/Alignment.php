<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

/**
 * @method static Alignment LOYAL_GOOD()
 * @method static Alignment LOYAL_NEUTRAL()
 * @method static Alignment LOYAL_BAD()
 * @method static Alignment NEUTRAL_GOOD()
 * @method static Alignment NEUTRAL()
 * @method static Alignment NEUTRAL_BAD()
 * @method static Alignment CHAOTIQUE_GOOD()
 * @method static Alignment CHAOTIQUE_NEUTRAL()
 * @method static Alignment CHAOTIQUE_BAD()
 */
class Alignment extends AbstractEnum
{
    private const LOYAL_GOOD = 'Loyal Bon';
    private const LOYAL_NEUTRAL = 'Loyal Neutre';
    private const LOYAL_BAD = 'Loyal Mauvais';
    private const NEUTRAL_GOOD = 'Neutre Bon';
    private const NEUTRAL = 'Neutre';
    private const NEUTRAL_BAD = 'Neutre Mauvais';
    private const CHAOTIQUE_GOOD = 'Chaotique Bon';
    private const CHAOTIQUE_NEUTRAL = 'Chaotique Neutre';
    private const CHAOTIQUE_BAD = 'Chaotique Mauvais';

    public static function getAlignmentFromKey(string $key): string
    {
        return static::toArray()[$key];
    }

    /**
     * @return array<string>
     */
    public static function getGoodAlignments(): array
    {
        return [
            static::LOYAL_GOOD()->getKey(),
            static::NEUTRAL_GOOD()->getKey(),
            static::CHAOTIQUE_GOOD()->getKey(),
        ];
    }

    /**
     * @return array<string>
     */
    public static function getNeutralAlignments(): array
    {
        return [
            static::LOYAL_NEUTRAL()->getKey(),
            static::NEUTRAL()->getKey(),
            static::CHAOTIQUE_NEUTRAL()->getKey(),
        ];
    }

    /**
     * @return array<string>
     */
    public static function getBadAlignments(): array
    {
        return [
            static::LOYAL_BAD()->getKey(),
            static::NEUTRAL_BAD()->getKey(),
            static::CHAOTIQUE_BAD()->getKey(),
        ];
    }
}
