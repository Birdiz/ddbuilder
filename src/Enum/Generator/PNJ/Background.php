<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

class Background extends AbstractEnum
{
    private const ACOLYTE = [
        'name' => 'Acolyte',
        'description' => '<p>Le personnage a passé sa vie au service du temple d\'un dieu spécifique ou d\'un panthéon 
        de dieux. Il agit comme intermédiaire entre les royaumes des cieux et le monde des mortels, exécutant 
        des rites sacrés et offrant des sacrifices pour conduire les fidèles face aux divins. Il n\'est pas 
        nécessairement un clerc, car exécuter des rites sacrés religieux n\'est pas la même chose que de canaliser la 
        puissance divine.</p>',
    ];

    private const GUILD_HANDWORKER = [
        'name' => 'Artisan de guilde',
        'description' => '<p>Le personnage est le membre d\'une guilde d\'artisans, compétent dans un domaine 
        particulier et en relation étroite avec d\'autres artisans. Il est au fait du monde des marchés, libéré des 
        contraintes d\'un ordre social féodal grâce à son talent et à sa richesse. Il a appris son savoir-faire en 
        tant qu\'apprenti auprès d\'un maître artisan, sous l\'égide de sa guilde, jusqu\'à ce qu\'il devienne à son 
        tour un maître dans sa branche.</p>',
    ];

    private const ARTIST = [
        'name' => 'Artiste',
        'description' => '<p>Le personnage s\'épanouit lorsqu\'il se trouve devant un public. Il sait comment 
        l\'hypnotiser, comment l\'amuser, comment l\'inspirer. Ses poèmes font fondre les cœurs de ceux qui 
        l\'écoutent, réveillant les colères et les joies, répandant le rire ou la tristesse. Sa musique envahit leur 
        esprit et s\'empare de leurs douleurs. Ses pas de danse hypnotisent, son humour pique au vif. Quelles que 
        soient les techniques qu\'il utilise, l\'art est toute sa vie.</p>',
    ];

    private const CHARLATAN = [
        'name' => 'Charlatan',
        'description' => '<p>Le personnage a toujours eu un bon feeling avec les gens. Il sait comment les accrocher, 
        il peut percevoir leurs plus profonds désirs après quelques minutes de conversations, et avec quelques 
        questions précises il peut lire en eux comme dans un livre ouvert. C\'est un talent très utile, et il sait 
        exactement comment l\'utiliser à son avantage.</p>',
    ];

    private const CRIMINAL = [
        'name' => 'Criminel',
        'description' => '<p>Le personnage est un criminel expérimenté avec un long passif d\'infractions à la loi. Il 
        a passé beaucoup de temps aux côtés d\'autres criminels et a encore des contacts au sein de la pègre. Il est 
        bien plus familier que la plupart des autres personnes avec le monde du meurtre, du vol et de la violence qui 
        règne au sein de la civilisation, et il a survécu jusque là en bafouant les règles et les lois de la société.
        </p>',
    ];

    private const STREET_CHILD = [
        'name' => 'Enfant des rues',
        'description' => '<p>Le personnage a grandi seul dans la rue, orphelin et pauvre. Il n\'avait personne pour le 
        surveiller ni pour subvenir à ses besoins, il a donc tout appris par lui-même. Il s\'est battu férocement pour 
        obtenir sa nourriture et gardait constamment un œil sur d\'autres âmes abandonnées comme lui qui n\'espéraient 
        qu\'un bon moment pour le voler. Il dormait sur des toits ou dans des rues, exposé aux intempéries, et il a 
        vaincu la maladie à de nombreuses reprises, sans aucune médecine ni aucun endroit pour récupérer. Il a survécu 
        en dépit de tous les pronostiques, ce qui l\'a rendu rusé, fort ou rapide, voire un peu tout cela à la fois.
        </p>',
    ];

    private const ERMITE = [
        'name' => 'Ermite',
        'description' => '<p>Le personnage a vécu reclus, soit dans une communauté protégée, comme un monastère, 
        ou entièrement seul, lors d\'un épisode formateur de sa vie. Durant cette période loin de la clameur de la 
        société, il a trouvé le calme, la solitude et peut-être même certaines réponses qu\'il cherchait.</p>',
    ];

    private const HERO = [
        'name' => 'Héros du peuple',
        'description' => '<p>Le personnage vient d\'un milieu social humble, mais est destiné à beaucoup plus. Déjà, 
        les gens de son village natal le considèrent comme leur champion, et son destin l\'appelle à le dresser face 
        aux tyrans et aux monstres qui menacent les gens du commun un peu partout.</p>',
    ];

    private const SEEMAN = [
        'name' => 'Marin',
        'description' => '<p>Le personnage a navigué sur un navire pendant des années. Durant cette période, il a 
        affronté de nombreuses tempêtes, de nombreux monstres des profondeurs, et tous ceux qui voulaient envoyer par 
        le fond son embarcation. Son premier amour est cette ligne inatteignable, l\'horizon, mais le temps est venu 
        pour lui de se frotter à autre chose.</p>',
    ];

    private const NOBLE = [
        'name' => 'Noble',
        'description' => '<p>Le personnage sait ce qu\'est la richesse, le pouvoir et les privilèges. Il porte un 
        titre de noblesse, et sa famille possède des terres, perçoit des impôts, et exerce une influence politique 
        significative. Il pourrait être un aristocrate choyé qui ne connaît pas le travail et l\'inconfort, un ancien 
        marchand qui vient d\'être élevé au rang de noble, ou une fripouille déshéritée avec un attrait pour les 
        titres disproportionnés. Ou il pourrait être un honnête propriétaire terrien qui travaille dur et qui se 
        soucie profondément des gens qui vivent et travaillent sur ses terres, tout à fait conscient de sa 
        responsabilité envers eux.</p>',
    ];

    private const SAGE = [
        'name' => 'Sage',
        'description' => '<p>Le personnage a passé des années à étudier le multivers. Il a écumé les manuscrits, 
        dévoré des parchemins et écouté les plus grands experts sur les sujets qui l\'intéressent. Ses efforts ont 
        fait de lui un maître dans ses domaines d\'études.</p>',
    ];

    private const SAUVAGEON = [
        'name' => 'Sauvageon',
        'description' => '<p>Le personnage a grandi dans les étendues sauvages, loin de toute civilisation et des 
        conforts qu\'offrent les villes et la technologie. Il a été témoin de la migration de troupeaux plus larges 
        que des forêts, il a survécu à des climats plus extrêmes qu\'aucun citadin ne pourrait l\'imaginer, et il a 
        apprécié la solitude d\'être la seule créature pensante à des kilomètres à la ronde. La nature est dans son 
        sang, qu\'il ait été nomade, explorateur, reclus, chasseur ou même pillard. Même dans un endroit dont il ne 
        connait pas les caractéristiques du terrain, il sait de quelle manière appréhender une étendue sauvage.</p>',
    ];

    private const SOLDIER = [
        'name' => 'Soldat',
        'description' => '<p>La guerre a été la vie du personnage aussi loin que remontent ses souvenirs. Il a été 
        formé dès sa jeunesse, a étudié l\'utilisation des armes et des armures, appris des techniques de base de 
        survie, y compris la façon de rester en vie sur un champ de bataille. Il pourrait avoir fait partie de 
        l\'armée d\'un royaume ou d\'une compagnie de mercenaires, ou peut-être avoir été membre d\'une milice 
        locale qui s\'est fait connaître au cours d\'une guerre récente.</p>',
    ];
}
