<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Family;

use App\Model\AbstractEnum;

/**
 * Class Female.
 *
 * @method static Female WIFE()
 * @method static Female MOTHER()
 * @method static Female GRANDMA()
 */
class Female extends AbstractEnum
{
    private const WIFE = 'femme';
    private const GRANDMA = 'grand-mère';
    private const MOTHER = 'mère';
    private const DAUGHTER = 'fille';
    private const TANTE = 'tante';
    private const SISTER = 'soeur';
}
