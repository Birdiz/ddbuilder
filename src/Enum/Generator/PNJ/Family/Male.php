<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Family;

use App\Model\AbstractEnum;

/**
 * Class Male.
 *
 * @method static Male HUSBAND()
 * @method static Male FATHER()
 * @method static Male GRANDPA()
 */
class Male extends AbstractEnum
{
    private const HUSBAND = 'mari';
    private const GRANDPA = 'grand-père';
    private const FATHER = 'père';
    private const SON = 'fils';
    private const UNCLE = 'oncle';
    private const BROTHER = 'frère';
}
