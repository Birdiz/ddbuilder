<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Feature;

use App\Model\AbstractEnum;

class Hairs extends AbstractEnum
{
    private const NOIR = 'noirs';
    private const BRUN = 'bruns';
    private const AUBURN = 'auburns';
    private const CHATAIN = 'châtains';
    private const ROUX = 'roux';
    private const BLOND_VENITIEN = 'blonds vénitien';
    private const BLOND = 'blonds';
    private const GREY = 'gris';
    private const SALT_PEPPER = 'poivre et sel';
    private const BLANC = 'blancs';
    private const RED = 'rouges';
    private const GREEN = 'verts';
    private const PURPLE = 'violets';
    private const BLUE = 'bleus';
    private const YELLOW = 'jaunes';
}
