<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Feature;

use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

class Personality extends AbstractEnum
{
    use ArrayRandTrait;

    private const VERTIGE = 'a le vertige';
    private const PHOBIE_INSECTES = 'a peur des insectes';
    private const PHOBIE_ARAIGNEES = 'a peur des araignées';
    private const PHOBIE_OISEAUX = 'a peur des oiseaux';
    private const PHOBIE_SERPENTS = 'a peur des serpents';
    private const PHOBIE_HIBOURS = 'a peur des ours-hiboux';
    private const PHOBIE_NOIR = 'a peur du noir';
    private const AGRESSIF = 'est aggressif/aggressive';
    private const BRUTAL = 'est brutal(e)';
    private const APPLIQUE = 'est appliqué(e)';
    private const AUTORITAIRE = 'est autoritaire';
    private const AVARE = 'est avare';
    private const BLAGUEUR = 'est blagueur/a le sens de l\'humour';
    private const CALME = 'est calme';
    private const COLERIQUE = 'est colérique';
    private const COURAGEUX = 'est courageux/courageuse';
    private const CRITIQUE = 'est très critique';
    private const CURIEUX = 'est curieux/curieuse';
    private const DESINVOLTE = 'est désinvolte';
    private const DISTANT = 'est distant(e)';
    private const DISTRAIT = 'est distrait(e)';
    private const EGOISTE = 'est égoïste';
    private const EMPATHIQUE = 'est mmpathique';
    private const ENERGIQUE = 'est énergique';
    private const MOBILE = 'ne tient pas en place';
    private const FAINEANT = 'est fainéant(e)/amorphe';
    private const GENEREUX = 'est généreux/généreuse';
    private const SPENDER = 'est dépensier/dépensière';
    private const IMPOLI = 'est impoli(e)';
    private const INCONSCIENT = 'est inconscient(e)';
    private const INTOLERANT = 'est intolérant(e)';
    private const IVROGNE = 'est ivrogne';
    private const JALOUX = 'est jaloux';
    private const JOUEUR = 'est joueur/joueuse';
    private const MALAISE_NATURE = 'est mal à l\'aise dans la nature';
    private const MALAISE_VILLE = 'est mal à l\'aise en ville';
    private const MALHONNETE = 'est malhonnête';
    private const MANIERE = 'est maniéré';
    private const MAUVAIS_PERDANT = 'est mauvais(e) perdant(e)';
    private const MEFIANT = 'est méfiant(e)';
    private const MENTEUR = 'est menteur';
    private const BOIT_JAMAIS = 'ne boit jamais';
    private const OBERSVATEUR = 'est observateur';
    private const OPTIMISTE = 'est optimiste';
    private const PASSIONNE = 'est passionné(e)';
    private const IN_LOVE = 'est amoureux/amoureuse';
    private const PESSIMISTE = 'est pessimiste';
    private const PEUREUX = 'est peureux/peureuse';
    private const COWARD = 'est lâche';
    private const PHILOSOPHE = 'est philosophe';
    private const PROTECTEUR = 'est protecteur/protectrice';
    private const RACISTE = 'est raciste';
    private const REVEUR = 'est rêveur/rêveuse ou souvent perdu(e) dans ses pensées';
    private const SAGE = 'est sage';
    private const SAUVAGE = 'est sauvage';
    private const ANTI_SOCIAL = 'est anti-social(e)';
    private const SERIEUX = 'est sérieux/sérieuse';
    private const SEXISTE = 'est sexiste';
    private const TIMIDE = 'est timide';
    private const VANTARD = 'est vantard(e)';
    private const ABSENT_MINDEND = 'est étourdi(e)';
    private const ADDICT_ALCOOL = 'est alcoolique';
    private const ADDICT_DROGUE = 'est drogué(e)';
    private const ANXIOUS = 'est anxieux/anxieuse';
    private const ARROGANT = 'est arrogant(e)';
    private const OPPOTUNIST = 'est opportuniste';
    private const BIGMOUTH = 'est beau-parleur';
    private const BOLD = 'est culotté(e)';
    private const CHILDISH = 'est puérile';
    private const COMPLEX = 'est complexe';
    private const DISTURB = 'est dérangé(e)';
    private const DYSLECIC = 'est dyslexique';
    private const EGOCENTRIC = 'est égocentrique';
    private const FANATICAL = 'est fanatique';
    private const FLIRT = 'est dragueur/dragueuse';
    private const GLUTTONOUS = 'est glouton(ne)';
    private const TACTLESS = 'est sans tact';
    private const CAPRICIOUS = 'est capricieux/capricieuse';
    private const HUMOURLESS = 'est pince-sans-rire';
    private const IDIOTIC = 'est idiot(e)';
    private const HYPOCRITICAL = 'est hypocrite';
    private const ILLETRATE = 'est illettré(e)';
    private const INCOMPETENT = 'est incompétent(e)';
    private const MASOCHIST = 'est masochiste';
    private const BASHFUL = 'est pudique';
    private const MEDDLESOME = 'est indiscret/indiscrète';
    private const MEGALOMANIAC = 'est mégalo';
    private const NAIVE = 'est naïf/naïve';
    private const NON_VIOLENT = 'est non-violent(e)';
    private const PACIFIST = 'est pacifique';
    private const PROUD = 'est fier/fière';
    private const REMORSELESS = 'est sans remords';
    private const SADIST = 'est sadique';
    private const SARCASTIC = 'est sarcastique';
    private const SCEPTIC = 'est sceptique';
    private const SENILE = 'est sénile';
    private const SPOILED = 'est pourri(e)-gâté(e)';
    private const SUPERSTITIOUS = 'est superstitieux/superstiticieuse';
    private const THEATRICAL = 'est théâtral(e)';
    private const LUCKY = 'est chanceux/chanceuse';
    private const UNLUCKY = 'est malchanceux/malchanceuse';
    private const TRUSTWORTHY = 'est fidèle';
    private const UNTRUSTWORTHY = 'est indigne de confiance';
    private const ZEALOUS = 'est zélé(e)';

    /**
     * @return array<int, Personality>
     */
    public static function getFourFeatures(): array
    {
        return static::getXSimpleItems(4, static::toArray());
    }
}
