<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Feature;

use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

class Physical extends AbstractEnum
{
    use ArrayRandTrait;

    private const POCHES_YEUX = 'a de grandes poches sous les yeux';
    private const GROS_SOURCILS = 'a de gros sourcils';
    private const PAS_SOURCILS = 'n\'a de sourcils';
    private const LONGUES_MOUSTACHES = 'a de longues moustaches';
    private const COURTES_MOUSTACHES = 'a de courtes moustaches';
    private const TRACES_PIQURES = 'a de nombreuses traces de piqûres';
    private const TRACES_BRULURES = 'a des traces de brûlures sur la peau';
    private const TRACES_COUPS = 'a des traces de coups';
    private const NEZ_CASSE = 'a le nez cassé';
    private const NEZ_COULE = 'a le nez qui coule';
    private const CHEVEUX_LONGS = 'a des cheveux très longs';
    private const MAINS_TREMBLENT = 'a les mains qui tremblent';
    private const YEUX_DIFFERENTS = 'a les yeux de couleurs différentes';
    private const MAL_VENTRE = 'a mal au ventre';
    private const CHAUD = 'a toujours chaud';
    private const FROID = 'a toujours froid';
    private const CHEVEUX_LANGUE = 'a un cheveu sur la langue';
    private const DOIGT_COUPE = 'a un doigt coupé';
    private const GRAIN_BEAUTE = 'a un gros grain de beauté';
    private const TATTOO = 'a un tatouage';
    private const TATTOO_VISAGE = 'a un tatouage sur le visage';
    private const TATTOO_FRONT = 'a un tatouage sur le front';
    private const TATTOO_MAIN_DROITE = 'a un tatouage sur la main droite';
    private const TATTOO_MAIN_GAUCHE = 'a un tatouage sur la main gauche';
    private const TATTOO_BRAS_DROITE = 'a un tatouage sur le bras droit';
    private const TATTOO_BRAS_GAUCHE = 'a un tatouage sur la bras gauche';
    private const TATTOO_COU = 'a un tatouage au cou';
    private const TIC_OCULAIRE = 'a un tic oculaire';
    private const BLESSURE = 'a une blessure apparente';
    private const CICATRICE_VISAGE = 'a une grande cicatrice sur le visage';
    private const CICATRICE_FRONT = 'a une grande cicatrice sur le front';
    private const CICATRICE_COU = 'a une cicatrice autour du cou';
    private const CICATRICE_MAINS = 'a des cicatrices d\'acide sur les mains';
    private const CICATRICE_MAIN_DROITE = 'a une cicatrice sur la main droite';
    private const CICATRICE_MAIN_GAUCHE = 'a une cicatrice sur la main gauche';
    private const CICATRICE_BRAS_DROITE = 'a une cicatrice sur le bras droit';
    private const CICATRICE_BRAS_GAUCHE = 'a une cicatrice sur le bras gauche';
    private const BARBE_GLORIEUSE = 'a une longue et glorieuse barbe';
    private const BARBE_COURTE = 'a une courte barbe';
    private const ODEUR = 'a une odeur désagréable';
    private const DENTS = 'a une ou des dents en moins';
    private const TACHE_NAISSANCE = 'a une tache de naissance sur le visage';
    private const TOUX = 'a une toux chronique';
    private const MOUCHES = 'attire les mouches';
    private const BEGAIE = 'begaie';
    private const BOITE = 'boite';
    private const BORGNE = 'est borgne';
    private const CHAUVE = 'est chauve';
    private const DEBRAILLE = 'est débraillé(e)';
    private const GRAND = 'est grand(e) pour sa race';
    private const GROS = 'est gros(se) pour sa race';
    private const MAIGRE = 'est maigre pour sa race';
    private const PARFUME = 'est parfumé(e)';
    private const PETIT = 'est petit(e) pour sa race';
    private const SALE = 'est sale';
    private const VOUTE = 'est voûté(e)';
    private const MACHE = 'mâche toujours quelque chose';
    private const SANS_DENTS = 'n\'a plus de dents';
    private const YEUX_MAUVAIS = 'ne voit pas très bien de loin et/ou de près';
    private const SOURD = 'n\'entend pas bien';
    private const PARLE_BAS = 'parle à voix basse';
    private const PARLE_FORT = 'parle très fort';
    private const BAGUES = 'porte des bagues aux doigts';
    private const OREILLES = 'porte des boucles d\'oreille';
    private const NEZ = 'porte une boucle au nez';
    private const LUMIERE = 'supporte mal la lumière du jour';
    private const TRANSPIRE = 'transpire beaucoup';
    private const MAINS_DOUCES = 'a les mains douces, comme un bourgeois';
    private const MUSCLE = 'a les muscles saillants';
    private const IROQUOISE = 'a une crête iroquoise';
    private const QUEUE_RAT = 'a une queue de rat';
    private const ECAILLES_JAMBES = 'a des écailles sur les jambes';
    private const BIJOUX = 'a des bijoux partout';
    private const BIJOUX_MAINS = 'a des bijoux aux mains';
    private const BIJOUX_OREILLES = 'a des bijoux aux oreilles';
    private const LENT = 'est lent';
    private const GROSSES_LEVRES = 'a des grosses lèvres';
    private const FINES_LEVRES = 'a des lèvres fines';
    private const BEC_LIEVRE = 'a un bec de lièvre';
    private const BRAS_MECANIQUE = 'a un bras mécanique';
    private const JAMBE_MECANIQUE = 'a une jambe mécanique';
    private const OEIL_VERRE = 'a un oeil de verre';
    private const MONOCLE = 'a un monocle';
    private const POIREAU = 'a un gros grain de beauté distrayant';
    private const ACNE = 'a de l\'acné';
    private const DEGARNI = 'a les cheveux dégarnis';
    private const TACHE_VIN = 'a une tache de vin';
    private const SIX_DOIGTS = 'a une tache de vin';
    private const PUPILLES_VERTICALES = 'a les pupilles verticales';
    private const SYMBOLE_RELIGIEUX = 'porte un symbole religieux';
    private const ROTTE = 'rote de temps en temps';
    private const PETE = 'pète sans honte';
    private const GRATTE = 'se gratte souvent';
    private const SOURIT = 'sourit constamment';

    /**
     * @return array<int, Physical>
     */
    public static function getFourFeatures(): array
    {
        return static::getXSimpleItems(4, static::toArray());
    }
}
