<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Feature;

use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;

/**
 * Class Skin.
 *
 * @method static Skin PALE()
 * @method static Skin FAIR()
 * @method static Skin LIGHT()
 * @method static Skin LIGHT_TAN()
 * @method static Skin TAN()
 * @method static Skin DARK_TAN()
 * @method static Skin BROWN()
 * @method static Skin DARK_BROWN()
 * @method static Skin BLACK()
 * @method static Skin GRAY()
 * @method static Skin WHITE()
 * @method static Skin GOLD()
 * @method static Skin SILVER()
 * @method static Skin BRONZE()
 * @method static Skin RED()
 * @method static Skin ORANGE()
 * @method static Skin YELLOW()
 * @method static Skin GREEN()
 * @method static Skin BLUE()
 * @method static Skin PURPLE()
 * @method static Skin PALE_YELLOW()
 * @method static Skin DARK_RED()
 * @method static Skin RED_ORANGE()
 * @method static Skin LIGHT_RED()
 * @method static Skin AMBER()
 * @method static Skin OLIVE()
 * @method static Skin AQUA()
 * @method static Skin PALE_BROWN()
 * @method static Skin PALE_BLUE()
 * @method static Skin PALE_GREEN()
 * @method static Skin PALE_GRAY()
 * @method static Skin DEEP_BLUE()
 * @method static Skin VIOLET_RED()
 * @method static Skin SPRING_GREEN()
 * @method static Skin SEA_GREEN()
 * @method static Skin EMERALD_GREEN()
 */
class Skin extends AbstractEnum
{
    private const PALE = 'pâle';
    private const FAIR = 'très claire';
    private const LIGHT = 'claire';
    private const LIGHT_TAN = 'légèrement bronzée';
    private const TAN = 'bronzée';
    private const DARK_TAN = 'très bronzée';
    private const BROWN = 'marron';
    private const DARK_BROWN = 'marron foncée';
    private const BLACK = 'noire';
    private const GRAY = 'grise';
    private const WHITE = 'blanche';
    private const GOLD = 'dorée';
    private const SILVER = 'argentée';
    private const BRONZE = 'couleur bronze';
    private const RED = 'rouge';
    private const ORANGE = 'orange';
    private const YELLOW = 'jaune';
    private const GREEN = 'verte';
    private const BLUE = 'bleue';
    private const PURPLE = 'violette';
    private const PALE_YELLOW = 'jaune clair';
    private const DARK_RED = 'rouge foncée';
    private const RED_ORANGE = 'rouge orangée';
    private const LIGHT_RED = 'rouge claire';
    private const AMBER = 'ambre';
    private const OLIVE = 'olive';
    private const AQUA = 'bleue d\'eau';
    private const PALE_BROWN = 'marron claire';
    private const PALE_BLUE = 'bleue claire';
    private const PALE_GREEN = 'verte claire';
    private const PALE_GRAY = 'grise claire';
    private const DEEP_BLUE = 'bleue marine';
    private const VIOLET_RED = 'violette rougeacée';
    private const SPRING_GREEN = 'verte printemps';
    private const SEA_GREEN = 'verte bouteille';
    private const EMERALD_GREEN = 'verte émeuraude';

    /**
     * @return array<Skin>
     */
    public static function getHumanSkins(): array
    {
        return [
            static::PALE()->getValue(),
            static::FAIR()->getValue(),
            static::LIGHT()->getValue(),
            static::LIGHT_TAN()->getValue(),
            static::TAN()->getValue(),
            static::DARK_TAN()->getValue(),
            static::BROWN()->getValue(),
            static::DARK_BROWN()->getValue(),
            static::BLACK()->getValue(),
        ];
    }

    /**
     * @return array<Skin>
     */
    public static function getTieflingSkins(): array
    {
        return array_merge(
            static::getHumanSkins(),
            [
                static::RED()->getValue(),
                static::RED_ORANGE()->getValue(),
                static::DARK_RED()->getValue(),
                static::LIGHT_RED()->getValue(),
                static::VIOLET_RED()->getValue(),
                static::ORANGE()->getValue(),
            ]
        );
    }

    public static function getSkinFromRaceName(string $raceName): string
    {
        $humanLikeRaces = [
            Race::HUMAN()->getValue()['name'],
            Race::FOREST_GNOME()->getValue()['name'],
            Race::HALF_ELF()->getValue()['name'],
            Race::HIGH_ELF()->getValue()['name'],
            Race::WOOD_ELF()->getValue()['name'],
            Race::HILL_DWARF()->getValue()['name'],
            Race::MOUNTAIN_DWARF()->getValue()['name'],
            Race::LIGHTFOOT_HALFELING()->getValue()['name'],
            Race::STOOT_HALFELING()->getValue()['name'],
        ];

        if ($raceName === Race::DROW()->getValue()['name']) {
            return static::BLACK()->getValue();
        }
        if ($raceName === Race::TIEFLING()->getValue()['name']) {
            return AbstractEnum::getRandomEnum(static::getTieflingSkins());
        }
        if (in_array($raceName, $humanLikeRaces, true)) {
            return AbstractEnum::getRandomEnum(static::getHumanSkins());
        }

        return AbstractEnum::getRandomEnum(static::values())->getValue();
    }
}
