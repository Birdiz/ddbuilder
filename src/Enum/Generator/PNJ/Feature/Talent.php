<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Feature;

use App\Model\AbstractEnum;

/**
 * Class Talent.
 *
 * @method static Talent INSTRUMENT()
 * @method static Talent LANGUAGES()
 * @method static Talent LUCKY()
 * @method static Talent MEMORY()
 * @method static Talent ANIMALS()
 * @method static Talent CHILDREN()
 * @method static Talent PUZZLES()
 * @method static Talent GAME()
 * @method static Talent IMPERSONATIONS()
 * @method static Talent DRAWS()
 * @method static Talent PAINTS()
 * @method static Talent SINGS()
 * @method static Talent DRINKS()
 * @method static Talent CARPENTER()
 * @method static Talent COOK()
 * @method static Talent DARTS_ROCKS()
 * @method static Talent JUGGLER()
 * @method static Talent ACTOR()
 * @method static Talent DANCER()
 * @method static Talent KNOWS_THIEVES()
 */
class Talent extends AbstractEnum
{
    private const INSTRUMENT = 'joue d\'un instrument de musique';
    private const LANGUAGES = 'parle plusieurs langues couramment';
    private const LUCKY = 'est incroyablement chanceux/chanceuse';
    private const MEMORY = 'a une mémoire parfaite';
    private const ANIMALS = 'est doué(e) avec les animaux';
    private const CHILDREN = 'est doué(e) avec les enfants';
    private const PUZZLES = 'est doué(e) pour résoudre des puzzles';
    private const GAME = 'est doué(e) a un jeu particulier';
    private const IMPERSONATIONS = 'est doué(e) pour les imitations';
    private const DRAWS = 'dessine magnifiquement';
    private const PAINTS = 'peint magnifiquement';
    private const SINGS = 'chante magnifiquement';
    private const DRINKS = 'gagne à tous les jeux de boissons';
    private const CARPENTER = 'est un charpentier/une charpentière né(e)';
    private const COOK = 'est un cuisnier/une cuisinière né(e)';
    private const DARTS_ROCKS = 'est doué(e) pour les flechettes et les ricochets';
    private const JUGGLER = 'est doué(e) au jonglage';
    private const ACTOR = 'est un acteur talentueux/une actrice talentueuse et maître du déguisement';
    private const DANCER = 'est un danseur talentueux/une danseuse talentueuse';
    private const KNOWS_THIEVES = 'sait ce que les voleurs ne savent pas';
    private const MANIPULATIVE = 'manipule les gens avec aisance';
}
