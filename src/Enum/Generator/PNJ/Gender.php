<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

/**
 * Class Gender.
 *
 * @method static Gender FEMALE()
 * @method static Gender MALE()
 */
class Gender extends AbstractEnum
{
    private const FEMALE = 'Femme';
    private const MALE = 'Homme';

    public static function getGenderFromKey(string $genderKey): string
    {
        return $genderKey === static::MALE()->getKey() ? static::MALE()->getValue() : static::FEMALE()->getValue();
    }
}
