<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

class Guild extends AbstractEnum
{
    private const ARCANE = 'guilde des Mages';
    private const SCHOLASTICS = 'guilde des Érudits';
    private const PERFORMERS = 'guilde des Artistes';
    private const ADVENTURER = 'guilde des Aventuriers';
    private const FIGHTERS = 'guilde des Combattants';
    private const SOLDIERS = 'guilde des Soldats';
    private const LABORERS = 'guilde des Travailleurs';
    private const HANDWORKERS = 'guilde des Artisants';
    private const BANKERS = 'guilde des Banquiers';
    private const MERCANTILE = 'guilde des Marchands';
    private const CRIMINALS = 'guilde des Criminels';
    private const THIEVES = 'guilde des Voleurs';
    private const ASSASSINS = 'guilde des Assassins';
}
