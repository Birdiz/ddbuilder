<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Academic.
 *
 * @method static Academic ADVOCATE()
 * @method static Academic ASTROLOGER()
 * @method static Academic CARTOGRAPHER()
 * @method static Academic JUDGE()
 * @method static Academic SCHOLAR()
 * @method static Academic SCRIBE()
 * @method static Academic STUDENT()
 * @method static Academic WRITER()
 */
class Academic extends AbstractEnum
{
    private const ADVOCATE = 'avocat(e)';
    private const ASTROLOGER = 'astrologue';
    private const CARTOGRAPHER = 'cartographe';
    private const JUDGE = 'juge';
    private const SCHOLAR = 'savant(e)';
    private const SCRIBE = 'scribe';
    private const STUDENT = 'étudiant(e)';
    private const WRITER = 'écrivain(e)';
}
