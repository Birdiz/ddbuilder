<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Aristocrat.
 *
 * @method static Aristocrat DUKE()
 * @method static Aristocrat MARQUESS()
 * @method static Aristocrat COUNT()
 * @method static Aristocrat VISCOUNT()
 * @method static Aristocrat BARON()
 * @method static Aristocrat WARCHIEF()
 * @method static Aristocrat CHIEF()
 * @method static Aristocrat ELDER()
 */
class Aristocrat extends AbstractEnum
{
    private const DUKE = 'duc/duchesse';
    private const MARQUESS = 'marquis(e)';
    private const COUNT = 'comte(sse)';
    private const VISCOUNT = 'vicomte(sse)';
    private const BARON = 'baron(esse)';
    private const WARCHIEF = 'chef(e) de guerre';
    private const CHIEF = 'chef(e)';
    private const ELDER = 'aîné(e)';
}
