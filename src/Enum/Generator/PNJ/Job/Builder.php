<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Builder.
 *
 * @method static Builder ARCHITECT()
 * @method static Builder CARPENTER()
 * @method static Builder MASON()
 * @method static Builder ROOFER()
 */
class Builder extends AbstractEnum
{
    private const ARCHITECT = 'architecte';
    private const CARPENTER = 'charpentier/charpentière';
    private const MASON = 'maçon';
    private const ROOFER = 'couvreur/couvreuse';
}
