<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Clergy.
 *
 * @method static Clergy ACOLYTE()
 * @method static Clergy PRIEST()
 * @method static Clergy SHAMAN()
 */
class Clergy extends AbstractEnum
{
    private const ACOLYTE = 'acolyte';
    private const PRIEST = 'prêtre(sse)';
    private const SHAMAN = 'chaman(e)';
}
