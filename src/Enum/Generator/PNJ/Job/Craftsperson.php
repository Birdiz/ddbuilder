<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Craftsperson.
 *
 * @method static Craftsperson ALCHEMIST()
 * @method static Craftsperson ARMORER()
 * @method static Craftsperson BAKER()
 * @method static Craftsperson BASKET_WEAVER()
 * @method static Craftsperson BLACKSMITH()
 * @method static Craftsperson BOOKBINDER()
 * @method static Craftsperson BOWYER()
 * @method static Craftsperson BREWER()
 * @method static Craftsperson BUTCHER()
 * @method static Craftsperson CHANDLER()
 * @method static Craftsperson COBBLER()
 * @method static Craftsperson COOK()
 * @method static Craftsperson COOPER()
 * @method static Craftsperson DYER()
 * @method static Craftsperson ENGRAVER()
 * @method static Craftsperson FURNITURE_MAKER()
 * @method static Craftsperson FURRIER()
 * @method static Craftsperson GLASS_BLOWER()
 * @method static Craftsperson GOLDSMITH()
 * @method static Craftsperson INSTRUMENT_MAKER()
 * @method static Craftsperson INVENTOR()
 * @method static Craftsperson JEWELER()
 * @method static Craftsperson LEATHERWORKER()
 * @method static Craftsperson LOCKSMITH()
 * @method static Craftsperson PAINTER()
 * @method static Craftsperson PAPER_MAKER()
 * @method static Craftsperson POTION_BREWER()
 * @method static Craftsperson POTTER()
 * @method static Craftsperson ROPE_MAKER()
 * @method static Craftsperson RUG_MAKER()
 * @method static Craftsperson SCULPTOR()
 * @method static Craftsperson SHIP_BUILDER()
 * @method static Craftsperson SILVERSMITH()
 * @method static Craftsperson SOAPMAKER()
 * @method static Craftsperson TAILOR()
 * @method static Craftsperson TANNER()
 * @method static Craftsperson TINKER()
 * @method static Craftsperson TOYMAKER()
 * @method static Craftsperson WEAPONSMITH()
 * @method static Craftsperson WEAVER()
 * @method static Craftsperson WHEELWRIGHT()
 * @method static Craftsperson WOODCARVER()
 */
class Craftsperson extends AbstractEnum
{
    private const ALCHEMIST = 'alchimiste';
    private const ARMORER = 'armurier/armurière';
    private const BAKER = 'boulanger/boulangère';
    private const BASKET_WEAVER = 'vannier/vannière';
    private const BLACKSMITH = 'forgeron(ne)';
    private const BOOKBINDER = 'relieur/relieuse';
    private const BOWYER = 'facteur/factrice d\'arcs';
    private const BREWER = 'brasseur/brasseuse';
    private const BUTCHER = 'boucher/bouchère';
    private const CHANDLER = 'fabricant(e) de bougies';
    private const COBBLER = 'cordonnier/cordonnière';
    private const COOK = 'cuisiner/cuisinière';
    private const COOPER = 'tonnelier/tonnelière';
    private const DYER = 'teinturier/teinturière';
    private const ENGRAVER = 'graveur/graveuse';
    private const FURNITURE_MAKER = 'fabriquant(e) de meubles';
    private const FURRIER = 'fourreur/fourreuse';
    private const GLASS_BLOWER = 'souffleur/souffleuse de verre';
    private const GOLDSMITH = 'orfèvre';
    private const INSTRUMENT_MAKER = 'facteur/factrice d\'instruments';
    private const INVENTOR = 'inventeur/inventrice';
    private const JEWELER = 'bijoutier/bioutière';
    private const LEATHERWORKER = 'artisan(te) du cuir';
    private const LOCKSMITH = 'serrurier/serrurière';
    private const PAINTER = 'peintre';
    private const PAPER_MAKER = 'papetier/papetière';
    private const POTION_BREWER = 'brasseur/brasseuse de potions';
    private const POTTER = 'potier/potière';
    private const ROPE_MAKER = 'fabricant(e) de corde';
    private const RUG_MAKER = 'fabricant(e) de tapis';
    private const SCULPTOR = 'sculpteur/sculptrice';
    private const SHIP_BUILDER = 'constructeur/constructrice de navires';
    private const SILVERSMITH = 'orfèvre';
    private const SOAPMAKER = 'savonnier';
    private const TAILOR = 'tailleur/tailleuse';
    private const TANNER = 'tanneur/tanneuse';
    private const TINKER = 'bricoleur/bricoleuse';
    private const TOYMAKER = 'fabricant(e) de jouets';
    private const WEAPONSMITH = 'armurier/armurière';
    private const WEAVER = 'tisserand(e)';
    private const WHEELWRIGHT = 'charron(ne)';
    private const WOODCARVER = 'sculpteur/sculptrice sur bois';
}
