<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Criminal.
 *
 * @method static Criminal BANDIT()
 * @method static Criminal BURGLAR()
 * @method static Criminal PICKPOCKET()
 * @method static Criminal PIRATE()
 */
class Criminal extends AbstractEnum
{
    private const BANDIT = 'bandit';
    private const BURGLAR = 'cambrioleur/cambrioleuse';
    private const PICKPOCKET = 'pickpocket';
    private const PIRATE = 'pirate';
}
