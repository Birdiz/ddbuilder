<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Entertainer.
 *
 * @method static Entertainer ACROBAT()
 * @method static Entertainer JESTER()
 * @method static Entertainer MINSTREL()
 * @method static Entertainer PROSTITUTE()
 * @method static Entertainer STORYTELLER()
 */
class Entertainer extends AbstractEnum
{
    private const ACROBAT = 'acrobate';
    private const JESTER = 'bouffon(ne)';
    private const MINSTREL = 'ménestrel(le)';
    private const PROSTITUTE = 'prostitué(e)';
    private const STORYTELLER = 'conteur/conteuse';
}
