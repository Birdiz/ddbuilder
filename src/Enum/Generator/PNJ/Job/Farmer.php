<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Farmer.
 *
 * @method static Farmer CROP_FARMER()
 * @method static Farmer GATHERER()
 * @method static Farmer HERDER()
 */
class Farmer extends AbstractEnum
{
    private const CROP_FARMER = 'agriculteur/agricultrice';
    private const GATHERER = 'cueilleur/cueilleuse';
    private const HERDER = 'berger/bergère';
}
