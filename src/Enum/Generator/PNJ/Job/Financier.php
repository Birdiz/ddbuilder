<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Financier.
 *
 * @method static Financier BANKER()
 * @method static Financier PAWNBROKER()
 * @method static Financier TAX_COLLECTOR()
 */
class Financier extends AbstractEnum
{
    private const BANKER = 'banquier/banquière';
    private const PAWNBROKER = 'prêteur/prêteuse';
    private const TAX_COLLECTOR = 'collecteur/collectrice des impôts';
}
