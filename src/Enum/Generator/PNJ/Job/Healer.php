<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Healer.
 *
 * @method static Healer HEALER()
 * @method static Healer HERBALIST()
 * @method static Healer MIDWIFE()
 */
class Healer extends AbstractEnum
{
    private const HEALER = 'guérisseur/guérisseuse';
    private const HERBALIST = 'herboriste';
    private const MIDWIFE = 'sage-femme';
}
