<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Hosteler.
 *
 * @method static Hosteler BROTHEL_KEEPER()
 * @method static Hosteler INNKEEPER()
 * @method static Hosteler RESTAURANTIER()
 * @method static Hosteler TAVERN_KEEPER()
 */
class Hosteler extends AbstractEnum
{
    private const BROTHEL_KEEPER = 'gardien(ne) de bordel';
    private const INNKEEPER = 'aubergiste';
    private const RESTAURANTIER = 'restaurateur/restauratrice';
    private const TAVERN_KEEPER = 'tavernier/tavernière';
}
