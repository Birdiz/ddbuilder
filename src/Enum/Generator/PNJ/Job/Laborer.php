<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Laborer.
 *
 * @method static Laborer DAY_LABORER()
 * @method static Laborer MINER()
 * @method static Laborer PORTER()
 * @method static Laborer WOODSELLER()
 */
class Laborer extends AbstractEnum
{
    private const DAY_LABORER = 'travailleur/travailleuse quotidien';
    private const MINER = 'mineur/mineuse';
    private const PORTER = 'porteur/porteuse';
    private const WOODSELLER = 'bûcheron(ne)';
}
