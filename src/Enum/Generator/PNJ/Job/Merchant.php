<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Merchant.
 *
 * @method static Merchant BEER_MERCHANT()
 * @method static Merchant BOOKSELLER()
 * @method static Merchant CARAVANNER()
 * @method static Merchant DAIRY_SELLER()
 * @method static Merchant FISHMONGER()
 * @method static Merchant FLORIST()
 * @method static Merchant GRAIN_MERCHANT()
 * @method static Merchant GROCER()
 * @method static Merchant HABERDASHER()
 * @method static Merchant HAY_MERCHANT()
 * @method static Merchant LIVESTOCK_MERCHANT()
 * @method static Merchant MERCER()
 * @method static Merchant MILITARY_OUTFITTER()
 * @method static Merchant MILLER()
 * @method static Merchant PEDDLER()
 * @method static Merchant SLAVER()
 * @method static Merchant SPICE_MERCHANT()
 * @method static Merchant TOBACCO_MERCHANT()
 * @method static Merchant USED_CLOTHIER()
 * @method static Merchant WINE_MERCHANT()
 * @method static Merchant WOOL_MERCHANT()
 */
class Merchant extends AbstractEnum
{
    private const BEER_MERCHANT = 'marchand(e) de bière';
    private const BOOKSELLER = 'libraire';
    private const CARAVANNER = 'caravanier/caravanière';
    private const DAIRY_SELLER = 'vendeur/vendeuse de produits laitiers';
    private const FISHMONGER = 'poissonnier/poissonnière';
    private const FLORIST = 'fleuriste';
    private const GRAIN_MERCHANT = 'marchand(e) de céréales';
    private const GROCER = 'épicier/épicière';
    private const HABERDASHER = 'mercier';
    private const HAY_MERCHANT = 'marchand(e) de foin';
    private const LIVESTOCK_MERCHANT = 'marchand(e) de bétail';
    private const MERCER = 'marchand(e) de tissu';
    private const MILITARY_OUTFITTER = 'pourvoyeur/pourvoyeuse militaire';
    private const MILLER = 'meunier/meunière';
    private const PEDDLER = 'colporteur/colportrice';
    private const SLAVER = 'négrier/négrière';
    private const SPICE_MERCHANT = 'marchand(e) d\'épices';
    private const TOBACCO_MERCHANT = 'marchand(e) de tabac';
    private const USED_CLOTHIER = 'drapier/drapière usagé';
    private const WINE_MERCHANT = 'marchand(e) de vin';
    private const WOOL_MERCHANT = 'marchand(e) de laine';
}
