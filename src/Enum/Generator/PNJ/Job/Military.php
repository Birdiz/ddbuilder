<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Military.
 *
 * @method static Military BOUNTY_HUNTER()
 * @method static Military CITY_GUARD()
 * @method static Military PRIVATE_GUARD()
 * @method static Military MERCENARY()
 * @method static Military SOLDIER()
 * @method static Military VILLAGE_GUARD()
 * @method static Military KNIGHT()
 */
class Military extends AbstractEnum
{
    private const BOUNTY_HUNTER = 'chasseur/chasseuse de primes';
    private const CITY_GUARD = 'garde de la ville';
    private const PRIVATE_GUARD = 'garde privé';
    private const MERCENARY = 'mercenaire';
    private const SOLDIER = 'soldat';
    private const VILLAGE_GUARD = 'milicien(ne)';
    private const KNIGHT = 'chevalier/chevalière';
    private const CAPTAIN = 'capitaine';
}
