<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Outdoor.
 *
 * @method static Outdoor EXPLORER()
 * @method static Outdoor FISHER()
 * @method static Outdoor HUNTER()
 * @method static Outdoor SAILOR()
 * @method static Outdoor TRAPPER()
 * @method static Outdoor TRAVELER()
 */
class Outdoor extends AbstractEnum
{
    private const EXPLORER = 'explorateur/exploratrice';
    private const FISHER = 'pêcheur/pêcheuse';
    private const HUNTER = 'chasseur/chasseuse';
    private const SAILOR = 'marin';
    private const TRAPPER = 'trappeur/trappeuse';
    private const TRAVELER = 'voyageur/voyageuse';
}
