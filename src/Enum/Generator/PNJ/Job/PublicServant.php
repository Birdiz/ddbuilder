<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class PublicServant.
 *
 * @method static PublicServant DIPLOMAT()
 * @method static PublicServant OFFICIAL()
 * @method static PublicServant POLITICIAN()
 * @method static PublicServant TOWN_CRIER()
 */
class PublicServant extends AbstractEnum
{
    private const DIPLOMAT = 'diplomate';
    private const OFFICIAL = 'officiel(le)';
    private const POLITICIAN = 'politicien(ne)';
    private const TOWN_CRIER = 'crieur/crieuse public';
}
