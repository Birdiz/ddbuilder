<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Servant.
 *
 * @method static Servant BARBER()
 * @method static Servant CARRIAGE_DRIVER()
 * @method static Servant DOMESTIC_SERVANT()
 * @method static Servant GARDENER()
 * @method static Servant GUIDE()
 * @method static Servant INN_SERVER()
 * @method static Servant LAUNDERER()
 * @method static Servant PAGE()
 * @method static Servant RAT_CATCHER()
 * @method static Servant RESTAURANT_SERVER()
 * @method static Servant SLAVE()
 * @method static Servant TAVERN_SERVER()
 */
class Servant extends AbstractEnum
{
    private const BARBER = 'coiffeur/coiffeuse';
    private const CARRIAGE_DRIVER = 'chauffeur/chauffeuse de chariot';
    private const DOMESTIC_SERVANT = 'domestique';
    private const GARDENER = 'jardinier/jardinière';
    private const GUIDE = 'guide';
    private const INN_SERVER = 'serveur/serveuse d\'auberge';
    private const LAUNDERER = 'blanchisseur/blanchisseuse';
    private const PAGE = 'huissier/huissière';
    private const RAT_CATCHER = 'dératiseur/dératiseuse';
    private const RESTAURANT_SERVER = 'serveur/serveuse de restaurant';
    private const SLAVE = 'esclave';
    private const TAVERN_SERVER = 'serveur/serveuse de taverne';
}
