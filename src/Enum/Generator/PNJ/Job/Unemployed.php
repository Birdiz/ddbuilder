<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Job;

use App\Model\AbstractEnum;

/**
 * Class Unemployed.
 *
 * @method static Unemployed BEGGAR()
 * @method static Unemployed CHILD()
 */
class Unemployed extends AbstractEnum
{
    private const BEGGAR = 'mendiant(e)';
    private const CHILD = 'enfant';
}
