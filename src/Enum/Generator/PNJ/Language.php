<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

/**
 * Class Language.
 *
 * @method static Language COMMON()
 * @method static Language ELVISH()
 * @method static Language GIANT()
 * @method static Language GNOMISH()
 * @method static Language GOBELIN()
 * @method static Language HALFELISH()
 * @method static Language DWARFISH()
 * @method static Language ORCISH()
 * @method static Language ABYSSAL()
 * @method static Language CELESTIAL()
 * @method static Language COMMON_UNDERDARK()
 * @method static Language DRACONIC()
 * @method static Language INFERNAL()
 * @method static Language PRIORDIAL()
 * @method static Language DEEP()
 * @method static Language SYLVANIC()
 */
class Language extends AbstractEnum
{
    use ArrayRandTrait;

    private const COMMON = 'Commun';
    private const ELVISH = 'Elfique';
    private const GIANT = 'Géant';
    private const GNOMISH = 'Gnome';
    private const GOBELIN = 'Gobelin';
    private const HALFELISH = 'Halfelin';
    private const DWARFISH = 'Nain';
    private const ORCISH = 'Orque';
    private const ABYSSAL = 'Abyssal';
    private const CELESTIAL = 'Céleste';
    private const COMMON_UNDERDARK = 'Commun d\'Outreterre';
    private const DRACONIC = 'Draconique';
    private const INFERNAL = 'Infernal';
    private const PRIORDIAL = 'Primordial';
    private const DEEP = 'Profond';
    private const SYLVANIC = 'Sylvain';

    /**
     * @param array<int, string> $exceptions
     *
     * @return array<string>
     */
    public static function getLanguage(int $nb, array $exceptions): array
    {
        $baseLanguages = array_filter(
            static::toArray(),
            static function (string $language) use ($exceptions) {
                return false === in_array($language, $exceptions, true);
            }
        );

        return array_merge($exceptions, static::getXComplexeItems($nb, $baseLanguages));
    }
}
