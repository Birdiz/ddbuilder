<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Name;

use App\Model\AbstractEnum;

class Aarakocra extends AbstractEnum
{
    private const PART1 = ['', '', '', '', '', 'c', 'cl', 'cr', 'd', 'g', 'gr', 'h', 'k', 'kh', 'kl', 'kr', 'q', 'qh',
        'ql', 'qr', 'r', 'rh', 's', 'y', 'z', ];
    private const PART2 = ['a', 'e', 'i', 'u', 'a', 'e', 'i', 'u', 'a', 'e', 'i', 'u', 'a', 'e', 'i', 'u', 'a', 'e',
        'i', 'u', 'a', 'e', 'i', 'u', 'a', 'e', 'i', 'u', 'ae', 'aia', 'ee', 'oo', 'ou', 'ua', 'uie', ];
    private const PART3 = ['c', 'cc', 'k', 'kk', 'l', 'll', 'q', 'r', 'rr'];
    private const PART4 = ['a', 'e', 'i', 'a', 'e', 'i', 'a', 'e', 'i', 'a', 'e', 'i', 'a', 'e', 'i', 'aa', 'ea',
        'ee', 'ia', 'ie', ];
    private const PART5 = ['', '', '', '', 'c', 'ck', 'd', 'f', 'g', 'hk', 'k', 'l', 'r', 'rr', 'rc', 'rk', 'rrk',
        's', 'ss', ];

    /**
     * @return string|array<string>
     */
    public static function getName(string $gender, int $nbNames = 1): string|array
    {
        if ($nbNames > 1) {
            $names = [];
            for ($i = 0; $i < $nbNames; ++$i) {
                $names[] = self::getRandomName();
            }

            return $names;
        }

        return self::getRandomName();
    }

    private static function getRandomName(): string
    {
        return self::PART1[random_int(0, count(self::PART1) - 1)].
            self::PART2[random_int(0, count(self::PART2) - 1)].
            self::PART3[random_int(0, count(self::PART3) - 1)].
            self::PART4[random_int(0, count(self::PART4) - 1)].
            self::PART5[random_int(0, count(self::PART5) - 1)];
    }
}
