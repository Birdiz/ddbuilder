<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Name;

use App\Enum\Generator\PNJ\Gender;
use App\Model\AbstractEnum;

class Dragonborn extends AbstractEnum
{
    private const PREFIX_MALE = ['Ali', 'Ar', 'Ba', 'Bal', 'Bel', 'Bha', 'Bren', 'Caer', 'Calu', 'Dur', 'Do', 'Dra',
        'Era', 'Faer', 'Fro', 'Gre', 'Ghe', 'Gora', 'He', 'Hi', 'Ior', 'Jin', 'Jar', 'Kil', 'Kriv', 'Lor', 'Lumi',
        'Mar', 'Mor', 'Med', 'Nar', 'Nes', 'Na', 'Oti', 'Orla', 'Pri', 'Pa', 'Qel', 'Ravo', 'Ras', 'Rho', 'Sa', 'Sha',
        'Sul', 'Taz', 'To', 'Trou', 'Udo', 'Uro', 'Vor', 'Vyu', 'Vrak', 'Wor', 'Wu', 'Wra', 'Wul', 'Xar', 'Yor', 'Zor',
        'Zra', ];
    private const SUFIX_MALE = ['barum', 'bor', 'broth', 'ciar', 'crath', 'daar', 'dhall', 'dorim', 'farn', 'fras',
        'gar', 'ghull', 'grax', 'hadur', 'hazar', 'jhan', 'jurn', 'kax', 'kris', 'kul', 'lasar', 'lin', 'mash', 'morn',
        'naar', 'prax', 'qiroth', 'qrin', 'qull', 'rakas', 'rash', 'rinn', 'roth', 'sashi', 'seth', 'skan', 'trin',
        'turim', 'varax', 'vroth', 'vull', 'warum', 'wunax', 'xan', 'xiros', 'yax', 'ythas', 'zavur', 'zire', 'ziros',
    ];

    private const PREFIX_FEMALE = ['Ari', 'A', 'Bi', 'Bel', 'Cris', 'Ca', 'Drys', 'Da', 'Erli', 'Esh', 'Fae', 'Fen',
        'Gur', 'Gri', 'Hin', 'Ha', 'Irly', 'Irie', 'Jes', 'Jo', 'Ka', 'Kel', 'Ko', 'Lilo', 'Lora', 'Mal', 'Mi', 'Na',
        'Nes', 'Nys', 'Ori', 'O', 'Ophi', 'Phi', 'Per', 'Qi', 'Quil', 'Rai', 'Rashi', 'So', 'Su', 'Tha', 'Ther', 'Uri',
        'Ushi', 'Val', 'Vyra', 'Welsi', 'Wra', 'Xy', 'Xis', 'Ya', 'Yr', 'Zen', 'Zof', ];
    private const SUFIX_FEMALE = ['birith', 'bis', 'bith', 'coria', 'cys', 'dalynn', 'drish', 'drith', 'faeth',
        'fyire', 'gil', 'gissa', 'gwen', 'hime', 'hymm', 'karyn', 'kira', 'larys', 'liann', 'lyassa', 'meila', 'myse',
        'norae', 'nys', 'patys', 'pora', 'qorel', 'qwen', 'rann', 'riel', 'rina', 'rinn', 'rish', 'rith', 'saadi',
        'shann', 'sira', 'thibra', 'thyra', 'vayla', 'vyre', 'vys', 'wophyl', 'wyn', 'xiris', 'xora', 'yassa', 'yries',
        'zita', 'zys', ];

    private const SURNAME_PART_1 = ['', '', '', '', 'c', 'cl', 'cr', 'd', 'dr', 'f', 'g', 'k', 'kl', 'kr', 'l', 'm',
        'my', 'n', 'ny', 'pr', 'sh', 't', 'th', 'v', 'y', ];
    private const SURNAME_PART_2 = ['a', 'e', 'i', 'a', 'e', 'i', 'o', 'u', 'a', 'e', 'i', 'a', 'e', 'i', 'o', 'u',
        'a', 'e', 'i', 'a', 'e', 'i', 'o', 'u', 'aa', 'ia', 'ea', 'ua', 'uu', ];
    private const SURNAME_PART_3 = ['c', 'cc', 'ch', 'lm', 'lk', 'lx', 'ld', 'lr', 'ldr', 'lt', 'lth', 'mb', 'mm',
        'mp', 'mph', 'mr', 'mt', 'nk', 'nx', 'nc', 'p', 'ph', 'r', 'rd', 'rj', 'rn', 'rrh', 'rth', 'st', 'tht', 'x', ];
    private const SURNAME_PART_4 = ['c', 'cm', 'cn', 'd', 'j', 'k', 'km', 'l', 'n', 'nd', 'ndr', 'nk', 'nsht', 'nth',
        'r', 's', 'sht', 'shkm', 'st', 't', 'th', 'x', ];
    private const SURNAME_PART_5 = ['d', 'j', 'l', 'll', 'm', 'n', 'nd', 'rg', 'r', 'rr', 'rd'];
    private const SURNAME_PART_6 = ['c', 'd', 'k', 'l', 'n', 'r', 's', 'sh', 'th'];

    /**
     * @return string|array<string>
     */
    public static function getName(string $gender, int $nbNames = 1): string|array
    {
        if ($nbNames > 1) {
            $names = [];

            for ($i = 0; $i < $nbNames; ++$i) {
                $names[] = self::getGenderedName($gender).self::getSurname();
            }

            return $names;
        }

        return self::getGenderedName($gender).' '.self::getSurname();
    }

    /**
     * @codeCoverageIgnore
     */
    private static function getSurname(): string
    {
        $nameParts = random_int(1, 10);
        $rndPart1 = random_int(0, count(self::SURNAME_PART_1) - 1);
        $rndPart2 = random_int(0, count(self::SURNAME_PART_2) - 1);
        $rndPart4 = random_int(0, count(self::SURNAME_PART_2) - 1);
        $rndPart5 = random_int(0, count(self::SURNAME_PART_6) - 1);

        $rndPart3 = self::getThirdPart(random_int(0, count(self::SURNAME_PART_3) - 1), $rndPart1, $rndPart5);
        if ($nameParts < 4) {
            return self::concatPartsSmallName([$rndPart1, $rndPart2, $rndPart3, $rndPart4, $rndPart5]);
        }

        $rndPart6 = random_int(0, count(self::SURNAME_PART_2) - 1);
        $rndPart7 = self::getSeventhPart(random_int(0, count(self::SURNAME_PART_4) - 1), $rndPart3, $rndPart5);
        if ($nameParts < 7) {
            return self::concatPartsMediumName(
                [$rndPart1, $rndPart2, $rndPart3, $rndPart4, $rndPart7, $rndPart6, $rndPart5]
            );
        }

        $rndPart8 = random_int(0, count(self::SURNAME_PART_6) - 1);
        $rndPart9 = self::getNinethPart(random_int(0, count(self::SURNAME_PART_5) - 1), $rndPart8, $rndPart5);

        return self::concatPartsBigName(
            [$rndPart1, $rndPart2, $rndPart3, $rndPart4, $rndPart7, $rndPart6, $rndPart9, $rndPart8, $rndPart5]
        );
    }

    private static function getFemaleName(): string
    {
        return self::PREFIX_FEMALE[random_int(0, count(self::PREFIX_FEMALE) - 1)].
            self::SUFIX_FEMALE[random_int(0, count(self::SUFIX_FEMALE) - 1)];
    }

    private static function getMaleName(): string
    {
        return self::PREFIX_MALE[random_int(0, count(self::PREFIX_MALE) - 1)].
            self::SUFIX_MALE[random_int(0, count(self::SUFIX_MALE) - 1)];
    }

    private static function getGenderedName(string $gender): string
    {
        return $gender === Gender::MALE()->getValue() ? self::getMaleName() : self::getFemaleName();
    }

    /**
     * @codeCoverageIgnore
     */
    private static function getThirdPart(int $rndPart3, int $rndPart1, int $rndPart5): int
    {
        while (in_array(
            self::SURNAME_PART_3[$rndPart3],
            [self::SURNAME_PART_1[$rndPart1], self::SURNAME_PART_6[$rndPart5]],
            true
        )) {
            $rndPart3 = random_int(0, count(self::SURNAME_PART_3) - 1);
        }

        return $rndPart3;
    }

    /**
     * @param array<int> $parts
     */
    private static function concatPartsSmallName(array $parts): string
    {
        return self::SURNAME_PART_1[$parts[0]].self::SURNAME_PART_2[$parts[1]].
            self::SURNAME_PART_3[$parts[2]].self::SURNAME_PART_2[$parts[3]].self::SURNAME_PART_6[$parts[4]];
    }

    /**
     * @codeCoverageIgnore
     */
    private static function getSeventhPart(int $rndPart7, int $rndPart3, int $rndPart5): int
    {
        while (in_array(
            self::SURNAME_PART_4[$rndPart7],
            [self::SURNAME_PART_3[$rndPart3], self::SURNAME_PART_6[$rndPart5]],
            true
        )) {
            $rndPart7 = random_int(0, count(self::SURNAME_PART_4) - 1);
        }

        return $rndPart7;
    }

    /**
     * @param array<int> $parts
     */
    private static function concatPartsMediumName(array $parts): string
    {
        return self::SURNAME_PART_1[$parts[0]].self::SURNAME_PART_2[$parts[1]].
            self::SURNAME_PART_3[$parts[2]].self::SURNAME_PART_2[$parts[3]].self::SURNAME_PART_4[$parts[4]].
            self::SURNAME_PART_2[$parts[5]].self::SURNAME_PART_6[$parts[6]];
    }

    /**
     * @codeCoverageIgnore
     */
    private static function getNinethPart(int $rndPart9, int $rndPart8, int $rndPart5): int
    {
        while (in_array(
            self::SURNAME_PART_5[$rndPart9],
            [self::SURNAME_PART_6[$rndPart8], self::SURNAME_PART_6[$rndPart5]],
            true
        )) {
            $rndPart9 = random_int(0, count(self::SURNAME_PART_5) - 1);
        }

        return $rndPart9;
    }

    /**
     * @param array<int> $parts
     */
    private static function concatPartsBigName(array $parts): string
    {
        return self::SURNAME_PART_1[$parts[0]].self::SURNAME_PART_2[$parts[1]].
            self::SURNAME_PART_3[$parts[2]].self::SURNAME_PART_2[$parts[3]].self::SURNAME_PART_4[$parts[4]].
            self::SURNAME_PART_2[$parts[5]].self::SURNAME_PART_5[$parts[6]].self::SURNAME_PART_2[$parts[7]]
            .self::SURNAME_PART_6[$parts[8]];
    }
}
