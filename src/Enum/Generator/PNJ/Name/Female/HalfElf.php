<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Name\Female;

use App\Model\AbstractEnum;

class HalfElf extends AbstractEnum
{
    private const AREL_LOMIELLE_BRIMILYE = 'Arel "Lomielle" Brimilyë';
    private const MIGOLLELLA_SIRAL_PIMEL = 'Migollella "Siral" Pimel';
    private const RIGRAKISSA_INIAL_ESOD = 'Rigrakissa "Inial" Esod';
    private const ELALIEL_PESINA_TARINDUL = 'Elaliel "Pesina" Tarindul';
    private const PIRERIA_SERIL_BRARAKAL = 'Pireria "Seril" Brarakal';
    private const LALYE_LOCIONNE_NIMAMEL = 'Lalyë "Locionne" Nimamel';
    private const BRELELINE_ATIS_LEOBE = 'Breleline "Atis" Leobe';
    private const BRELELINE_FILILLE_POCAL = 'Breleline "Filillë" Pocal';
    private const LEODIRIALLE_SIRAL_NASCERT = 'Leodirialle "Siral" Nascert';
    private const LOREL_PAMBONNE_ELASTOND = 'Lorel "Pambonne" Elastond';
    private const LIDIGRIELLE_ANE_RAGRADIL = 'Lidigrielle "Anë" Ragradil';
    private const PRESIONNE_NUMALIEL_THESER = 'Presionne "Numaliel" Theser';
    private const TERE_GRENOGRONNE_CEMILD = 'Tere "Grenogronne" Cemild';
    private const TERIAL_TUMBONNE_FIRCAR = 'Terial "Tumbonne" Fircar';
    private const LORYE_BABINE_ELARD = 'Loryë "Babine" Elard';
    private const LOIELLE_FIRIAL_YROS = 'Loielle "Firial" Yros';
    private const ANEN_TOMBITH_CIRYMEHTEN = 'Anen "Tombith" Cirymehten';
    private const SILIE_BEOBELLE_FINONW = 'Silië "Beobelle" Finonw';
    private const MASIANNE_ELIL_HATACER = 'Masianne "Elil" Hatacer';
    private const EARIEL_MESISSA_CEMEYE = 'Eäriel "Mesissa" Cemëyë';
    private const ISIAL_LATHARDE_ELMACUL = 'Isial "Latharde" Elmacul';
    private const ZAITH_ATIE_NANCEIS = 'Zaith "Atië" Nanceis';
    private const SIREL_LILIE_BRIMAT = 'Sirel "Lilie" Brimat';
    private const ATYE_SIPEIELLE_CIRYTOND = 'Atyë "Sipeielle" Cirytond';
    private const FINE_DACESISSA_ARANDIOR = 'Finë "Dacesissa" Arandior';
    private const SIRE_THOGRINE_AMER = 'Sirë "Thogrine" Amer';
    private const THOSCICE_DORIL_NALEBUS = 'Thoscice "Doril" Nalebus';
    private const TRALUZA_ATILLE_BALATH = 'Traluza "Atillë" Balath';
    private const VALEL_BROGRASITA_SERAR = 'Valel "Brograsita" Serar';
    private const PATIELLE_DORIEL_LATAN = 'Patielle "Doriel" Latan';
    private const GLORE_CAPERTINE_CEMRYEIL = 'Glore "Capertine" Cemryëil';
    private const LUGRYSE_ELAL_PREKIEN = 'Lugryse "Elal" Prekien';
    private const UNOLELLE_PELILLE_BEOSUS = 'Unolelle "Pelillë" Beosus';
    private const RASCONDE_NARIE_HEVIN = 'Rasconde "Narië" Hevin';
    private const LIRABISSE_GLORALIEL_BROMBIR = 'Lirabisse "Gloraliel" Brombir';
    private const POYSE_OTHE_FIVOS = 'Poyse "Othe" Fivos';
    private const EARIAL_SINONONNE_GLORMAC = 'Eärial "Sinononne" Glormac';
    private const CEMIEL_THODELLA_THANDAC = 'Cemiel "Thodella" Thandac';
    private const MACITH_ANALIEL_DRARIDIN = 'Macith "Analiel" Draridin';
    private const NARIEL_METYSSE_ELIEYE = 'Nariel "Metysse" Eliëyë';
    private const PALEN_THELVINA_LINAMB = 'Palen "Thelvina" Linamb';
    private const DRAMICA_ELEN_BUSIN = 'Dramica "Elen" Busin';
    private const SIRE_PORIBIANNE_LOTHONW = 'Sirë "Poribianne" Lothonw';
    private const SIZITA_TARAL_HEPIS = 'Sizita "Taral" Hepis';
    private const ELEN_HIZETELLA_FANARDIL = 'Elen "Hizetella" Fanardil';
    private const MASCICE_PELIE_THOSETH = 'Mascice "Pelië" Thoseth';
    private const GIBYSSE_ARE_MAUDINEN = 'Gibysse "Are" Maudinen';
    private const LALETELLA_CIRYYE_BRELINSCAN = 'Laletella "Ciryyë" Brelinscan';
    private const AMIAL_LUTONDE_FINON = 'Amial "Lutonde" Finon';
    private const SHADISSA_TURALIEL_LODIBUS = 'Shadissa "Turaliel" Lodibus';
    private const ZALINLYSE_FIRIS_NAMUS = 'Zalinlyse "Firis" Namus';
    private const TURYE_ESCIE_PELAMBOND = 'Turyë "Escie" Pelambond';
    private const PELIAL_DRAREDA_THANINDION = 'Pelial "Drareda" Thanindion';
    private const FIRYE_TRADINE_TORENTIEL = 'Firyë "Tradine" Torentiel';
    private const GITOMITA_INE_TRASCIR = 'Gitomita "Inë" Trascir';
    private const NIMIL_ASINA_SILIND = 'Nimil "Asina" Silind';
    private const DISIALLE_FINEN_BERIMOD = 'Disialle "Finen" Berimod';
    private const ITICA_ISIAL_TASCETH = 'Itica "Isial" Tasceth';
    private const BRIMIS_HEDIE_FINAM = 'Brimis "Hedie" Finam';
    private const ONODIE_HIRYE_LADOD = 'Onodie "Hiryë" Ladod';
    private const PIPIALLE_EARALIEL_LODIBUS = 'Pipialle "Eäraliel" Lodibus';
    private const VATARDE_HERALIEL_DRAGAL = 'Vatarde "Heraliel" Dragal';
    private const ANIE_PABRONDE_NIMIL = 'Anië "Pabronde" Nimil';
    private const MEMIELLE_CALIE_PEGUR = 'Memielle "Calië" Pegur';
    private const HENADONDE_HIRE_DRADETH = 'Henadonde "Hire" Dradeth';
    private const OTHEL_KOSCELLA_NUMDAC = 'Othel "Koscella" Numdac';
    private const TERIS_BREDITA_TUR = 'Teris "Bredita" Tur';
    private const BEOSITA_NERIE_THEBRAS = 'Beosita "Nerië" Thebras';
    private const THETINE_FILIE_HINER = 'Thetine "Filië" Hiner';
    private const LILIE_EARIS_BRERTEL = 'Lilie "Eäris" Brertel';
    private const ANALIEL_RITALVYSE_NIMAND = 'Analiel "Ritalvyse" Nimand';
    private const BRIMIL_TRAMICIALLE_ISYEEL = 'Brimil "Tramicialle" Isyëel';
    private const BACEIA_TURYE_PORAN = 'Baceia "Turyë" Poran';
    private const NARIL_BRODISSA_GLORMAC = 'Naril "Brodissa" Glormac';
    private const DROKICE_SIREL_LADEDETH = 'Drokice "Sirel" Ladedeth';
    private const ISEL_GRECARTIANNE_AR = 'Isel "Grecartianne" Ar';
    private const ELE_TUTIONNE_HIRE = 'Ele "Tutionne" Hirë';
    private const NAREN_TADEDYSSE_TARINDUL = 'Naren "Tadedysse" Tarindul';
    private const NAREL_JIMONDE_CIRYADRALIE = 'Narel "Jimonde" Ciryadralië';
    private const AMAL_UNOLELLE_THANIALIL = 'Amal "Unolelle" Thanialil';
    private const ETITH_SIRAL_JISAS = 'Etith "Siral" Jisas';
    private const INIAL_RUNICA_HERANDOR = 'Inial "Runica" Herandor';
    private const ISE_ZARIILLE_HEREIAL = 'Isë "Zariille" Herëial';
    private const HERIE_RATIE_SILONWALIAL = 'Herië "Ratie" Silonwalial';
    private const FILIS_ERETICA_VALEL = 'Filis "Eretica" Valel';
    private const DOMIRCILLE_OTHE_PAMAN = 'Domircille "Othë" Paman';
    private const BRARYSE_FIRIAL_LODIBUS = 'Braryse "Firial" Lodibus';
    private const SHACITH_PALE_MIGRION = 'Shacith "Palë" Migrion';
    private const LALE_HIGRELLE_CIRYMEHTEN = 'Lale "Higrelle" Cirymehten';
    private const FINIE_DAMICIELLE_NIMUNUMIEL = 'Finië "Damicielle" Nimunúmiel';
    private const GRERESARDE_ARTALIEL_BUDAR = 'Greresarde "Artaliel" Budar';
    private const BASIELLE_BRIMYE_THEGATH = 'Basielle "Brimyë" Thegath';
    private const LALIEL_THODELLA_AREALIEL = 'Laliel "Thodella" Arealiel';
    private const TRARTISSE_CALEL_JIBOS = 'Trartisse "Calel" Jibos';
    private const NUMALIEL_RANLOITA_TERAT = 'Numaliel "Ranloita" Terat';
    private const SAUKILLE_AME_RATATH = 'Saukille "Amë" Ratath';
    private const SILIE_RANZESCIALLE_ELANWIL = 'Silië "Ranzescialle" Elanwil';
    private const NERE_ESCELLE_ANASTIL = 'Nerë "Escelle" Anastil';
    private const DAUNELLA_ISAL_MANUS = 'Daunella "Isal" Manus';
    private const THONITA_TUREN_RUNCEN = 'Thonita "Turen" Runcen';
    private const PETELILLE_TARIE_HEMAN = 'Petelille "Tarië" Heman';
    private const REKIALLE_FANIEL_JATOZIL = 'Rekialle "Faniel" Jatozil';
    private const LALE_SHAMELLE_CIRYAMB = 'Lalë "Shamelle" Ciryamb';
    private const BEORSONNE_TEREL_JABRUS = 'Beorsonne "Terel" Jabrus';
    private const SILIEL_DAGYSSE_TARW = 'Siliel "Dagysse" Tarw';
    private const THANIAL_SAUKILLE_CALEIEL = 'Thanial "Saukille" Calëiel';
    private const TURIILLE_NARIAL_BEOLOEL = 'Turiille "Narial" Beoloel';
    private const HIRIEL_RAMIALLE_ELDAR = 'Hiriel "Ramialle" Eldar';
    private const PELEL_GICIALLE_BRIMISIAL = 'Pelel "Gicialle" Brimisial';
    private const DIDIA_SERALIEL_SADOD = 'Didia "Seraliel" Sadod';
    private const DRUZEYSSE_ATIS_DILVAN = 'Druzeysse "Atis" Dilvan';
    private const PIKILLE_ISYE_PELAN = 'Pikille "Isyë" Pelan';
    private const RANNCEBIA_OTHIE_HILEDE = 'Ranncebia "Othië" Hilede';
    private const BRIMEL_BAPADARDE_FILEIAL = 'Brimel "Bapadarde" Fileial';
    private const HEREL_TRANCEONDE_VALEL = 'Herel "Tranceonde" Valel';
    private const PALIE_LEOSISSA_SILONWUL = 'Palië "Leosissa" Silonwul';
    private const AME_DAUDYSE_CALAMB = 'Amë "Daudyse" Calamb';
    private const NUMIL_LOGIE_TERAR = 'Numil "Logie" Terar';
    private const RETHILLE_ATHIE_BANARD = 'Rethille "Athië" Banard';
    private const THELAGRYSE_FANE_SEBRAR = 'Thelagryse "Fanë" Sebrar';
    private const SHASCA_PELIAL_KOMBUS = 'Shasca "Pelial" Kombus';
    private const TADIANNE_FINILLE_ELAS = 'Tadianne "Finillë" Elas';
    private const THANIS_MIIALLE_FARIL = 'Thanis "Miialle" Faril';
    private const MAPESCIELLE_OTHEL_HIBEN = 'Mapescielle "Othel" Hiben';
    private const TURIEL_NAPETYSE_THANW = 'Turiel "Napetyse" Thanw';
    private const FANILLE_BRESCYSSE_PALMACOND = 'Fanillë "Brescysse" Palmacond';
    private const OTHIAL_CALOPINA_FANANDION = 'Othial "Calopina" Fanandion';
    private const YDAGRISSA_PALIEL_FIMBE = 'Ydagrissa "Paliel" Fimbe';
    private const MARCYSSE_NERIS_DRODEL = 'Marcysse "Neris" Drodel';
    private const DROMONDE_CIRYE_DAUDOS = 'Dromonde "Ciryë" Daudos';
    private const LOTHAL_DROTARDE_ELUL = 'Lothal "Drotarde" Elul';
    private const GREGITH_ATHIE_RASAM = 'Gregith "Athië" Rasam';
    private const BRIMYE_MASCICE_TARATIALILLE = 'Brimyë "Mascice" Taratialillë';
    private const EARYE_REDESCILLE_PELANDIL = 'Eäryë "Redescille" Pelandil';
    private const ZARIILLE_ANIAL_JETEMUS = 'Zariille "Anial" Jetemus';
    private const FIRIS_LUTILLE_VALEL = 'Firis "Lutille" Valel';
    private const LEOMISELLA_PELIE_RUNCEN = 'Leomisella "Pelië" Runcen';
    private const PELVIE_ISIEL_RILAL = 'Pelvie "Isiel" Rilal';
    private const ISE_BELILLE_FINIEL = 'Ise "Belille" Finiel';
    private const FIRIL_RANNALELLA_LALARDILAL = 'Firil "Rannalella" Lalardilal';
    private const UNOLELLE_VALIEL_ZADITER = 'Unolelle "Valiel" Zaditer';
    private const DORIE_KOTIALLE_LOTHACIELIE = 'Dorië "Kotialle" Lothacielië';
    private const BERSISSA_THANALIEL_PREIL = 'Bersissa "Thanaliel" Preil';
    private const BREDABIALLE_FINIEL_ITIL = 'Bredabialle "Finiel" Itil';
    private const FANIEL_ZAITH_HERDIN = 'Faniel "Zaith" Herdin';
    private const THEBRIA_TURE_LEORIR = 'Thebria "Turë" Leorir';
    private const EARIS_VACONNE_DORMAC = 'Eäris "Vaconne" Dormac';
    private const FINYE_RANTODILLE_CEMILD = 'Finyë "Rantodille" Cemild';
    private const DRANIONNE_FIRIAL_TOCAL = 'Dranionne "Firial" Tocal';
    private const ARIEL_URISSE_FANARDIELYE = 'Ariel "Urisse" Fanardielyë';
    private const NUMIEL_HICASONNE_GLORILDUR = 'Numiel "Hicasonne" Glorildur';
    private const ELEL_DITYSE_FIRIEL = 'Elel "Dityse" Firiel';
    private const PIBRISSE_ELALIEL_DRAGRASCAL = 'Pibrisse "Elaliel" Dragrascal';
    private const TURIE_BEORSONNE_SIRIR = 'Turië "Beorsonne" Sirir';
    private const THANALIEL_DRASA_ATANATEN = 'Thanaliel "Drasa" Atanaten';
    private const CIRYE_MEBIA_GLORILDUR = 'Cirye "Mebia" Glorildur';
    private const LEOSISSA_AMIL_GRERIPAS = 'Leosissa "Amil" Greripas';
    private const PROLVELLE_ATYE_JEPANIL = 'Prolvelle "Atyë" Jepanil';
    private const NUME_PROSIE_ATHMACEE = 'Nume "Prosie" Athmacëë';
    private const SEDISYSSE_LORIEL_SITAM = 'Sedisysse "Loriel" Sitam';
    private const GIDITONDE_AMIAL_SOCAS = 'Giditonde "Amial" Socas';
    private const TOREL_POLITH_SILONWUL = 'Torel "Polith" Silonwul';
    private const PALYE_MIMBICA_ELIN = 'Palyë "Mimbica" Elin';
    private const DASONDE_FINEN_DRODUS = 'Dasonde "Finen" Drodus';
    private const SIRIONNE_CALAL_MAURER = 'Sirionne "Calal" Maurer';
    private const FIRIL_PREGOBONNE_DORAR = 'Firil "Pregobonne" Dorár';
    private const OTHE_SAURSONDE_CEMT = 'Othë "Saursonde" Cemt';
    private const DORIL_EDIALLE_FILEND = 'Doril "Edialle" Filend';
    private const BREDABIALLE_ATALIEL_JISOS = 'Bredabialle "Ataliel" Jisos';
    private const FILIEL_SIRIANNE_TARYEAL = 'Filiel "Sirianne" Taryëal';
    private const HERIEL_LEOICE_ISIELIEL = 'Heriel "Leoice" Isieliel';
    private const TOREL_BETONDE_FILAND = 'Torel "Betonde" Filand';
    private const ARIL_DOLAZYSE_LORAR = 'Aril "Dolazyse" Lorár';
    private const SIRE_SAUTESILLE_LORRIL = 'Sirë "Sautesille" Lorril';
    private const SOBRYSE_CEME_IGOSAN = 'Sobryse "Cemë" Igosan';
    private const PITOSICA_AMEN_RAKAN = 'Pitosica "Amen" Rakan';
    private const CEMIAL_VADEMINA_TORR = 'Cemial "Vademina" Torr';
    private const PALEL_HELLONDE_TARACILIL = 'Palel "Hellonde" Taracilil';
    private const ISYE_THETINE_CIRYEND = 'Isyë "Thetine" Ciryend';
    private const SAUGORYSE_LALYE_RUNSCAS = 'Saugoryse "Lalyë" Runscas';
    private const ARTEL_KODIONNE_ARTAEN = 'Artel "Kodionne" Artaën';
    private const PALEL_DRUPESIELLE_PALIOND = 'Palel "Drupesielle" Paliond';
    private const ETENITH_ELIS_BREDERT = 'Etenith "Elis" Bredert';
    private const CEME_BEOGRARIA_VALEL = 'Ceme "Beograria" Valel';
    private const BRORYSE_PELAL_JINIS = 'Broryse "Pelal" Jinis';
    private const ATIE_DRORSYSSE_NARADROR = 'Atië "Drorsysse" Naradror';
    private const FANAL_SAULUSINE_TORR = 'Fanal "Saulusine" Torr';
    private const JILICE_FIRIAL_JIDERT = 'Jilice "Firial" Jidert';
    private const DAUMA_FIRAL_DIMBOD = 'Dauma "Firal" Dimbod';
    private const TARIS_PADIANNE_NIMANW = 'Taris "Padianne" Nimanw';
    private const ELIE_RUNRCINE_ATHMACEE = 'Elië "Runrcine" Athmacëë';
    private const VALIAL_HELINRTISSE_AN = 'Valial "Helinrtisse" An';
    private const JIBIE_NERIEL_LATAN = 'Jibie "Neriel" Latan';
    private const VALYE_VADEMINA_OTHRIL = 'Valyë "Vademina" Othril';
    private const ARTYE_RIMIONNE_FANARDIELYE = 'Artyë "Rimionne" Fanardielyë';
    private const EARE_SOVIONNE_NUMANDAEN = 'Eärë "Sovionne" Numándaën';
    private const SILIS_THORIRIELLE_NIM = 'Silis "Thoririelle" Nim';
    private const LINYE_GISISSE_NUMILIE = 'Linyë "Gisisse" Numilië';
    private const TORIE_ZASELLA_ELMEHT = 'Torië "Zasella" Elmeht';
    private const IRARDE_SILE_SEOS = 'Irarde "Sile" Seos';
    private const NARIEL_FISIELLE_DORMAC = 'Nariel "Fisielle" Dormac';
    private const TRADINE_FINE_SEDOS = 'Tradine "Finë" Sedos';
    private const ISCIANNE_NERE_DRALAS = 'Iscianne "Nerë" Dralas';
    private const JIGRISSE_LOTHIE_LODEE = 'Jigrisse "Lothië" Lodee';
    private const PRELUDELLE_ARIE_CARTIS = 'Preludelle "Arië" Cartis';
    private const NERIAL_HEGRIALLE_FINDARALAL = 'Nerial "Hegrialle" Findaralal';
    private const HAELLA_PALIEL_POGORAL = 'Haella "Paliel" Pogoral';
    private const TORIEL_HIZETELLA_NARADROR = 'Toriel "Hizetella" Naradror';
    private const PREGOBONNE_LALIS_KORUS = 'Pregobonne "Lalis" Korus';
    private const PENOSIE_LINIEL_PROCAVUR = 'Penosie "Liniel" Procavur';
    private const NERIS_DIPIELLE_PELIEALIEL = 'Neris "Dipielle" Peliëaliel';
    private const ISALIEL_HAELLA_AMASTIN = 'Isaliel "Haella" Amastin';
    private const LALE_SILLIANNE_FILMACYEIAL = 'Lalë "Sillianne" Filmacyëial';
    private const RUNSILLE_PALYE_JITEMBARD = 'Runsille "Palyë" Jitembard';
    private const GREPELYSSE_CALIS_RIPATATH = 'Grepelysse "Calis" Ripatath';
    private const OLORELLA_FARIS_GRELINGER = 'Olorella "Faris" Grelinger';
    private const VAPAISSA_BRIMYE_JEVIR = 'Vapaissa "Brimyë" Jevir';
    private const FIRYE_DRODISSA_TUR = 'Firyë "Drodissa" Tur';
    private const TURYE_BRETAIE_HIRYEE = 'Turyë "Bretaie" Hiryëë';
    private const LORALIEL_RAMONNE_AMASTIN = 'Loraliel "Ramonne" Amastin';
    private const ADEGRYSE_FIRYE_LUMAN = 'Adegryse "Firyë" Luman';
    private const TORAL_TRAMINE_PELANDIL = 'Toral "Tramine" Pelandil';
    private const BADILLE_ELALIEL_PIMETH = 'Badille "Elaliel" Pimeth';
    private const ARTALIEL_GILUNELLA_SILILALIEL = 'Artaliel "Gilunella" Sililaliel';
    private const THEMIRELLA_LALEL_RUNRAN = 'Themirella "Lalel" Runran';
    private const FANILLE_SHATARTISSE_SERUR = 'Fanillë "Shatartisse" Serur';
    private const YSONDE_LORE_SICAS = 'Ysonde "Lore" Sicas';
    private const TOSITH_HIRIEL_DRUTIL = 'Tosith "Hiriel" Drutil';
    private const LOTHIE_LISIA_CEMILD = 'Lothië "Lisia" Cemild';
    private const SIRE_SAUNCEDILLE_BRIMUL = 'Sirë "Sauncedille" Brimul';
    private const GREPELYSSE_CEMIAL_GREDIROD = 'Grepelysse "Cemial" Gredirod';
    private const PIRETELLA_VALIAL_YLLOR = 'Piretella "Valial" Yllor';
    private const HIRIE_JITHINA_CIRYANDIR = 'Hirië "Jithina" Ciryandir';
    private const MIDIA_CIRYIL_DRARCIL = 'Midia "Ciryil" Drarcil';
    private const DRUPESARDE_ATIS_DROTHETH = 'Drupesarde "Atis" Drotheth';
    private const FANILLE_RANLONNE_FANANATIELIEL = 'Fanillë "Ranlonne" Fananatieliel';
    private const ARAL_SIZIELLE_ELAT = 'Aral "Sizielle" Elat';
    private const ATYE_BRECERILLE_INIL = 'Atyë "Brecerille" Inil';
    private const PORIBIANNE_FILE_LOMBERT = 'Poribianne "File" Lombert';
    private const BRIMIE_CAMITA_ELISEL = 'Brimië "Camita" Elisel';
    private const FILELLE_INE_RAETH = 'Filelle "Inë" Raeth';
    private const ZAITH_ELE_RISAM = 'Zaith "Elë" Risam';
    private const CEMYE_HEBIELLE_CIRYILDIL = 'Cemyë "Hebielle" Ciryildil';
    private const TAREL_PIRERIA_NARAC = 'Tarel "Pireria" Narac';
    private const NARIAL_TRAMICA_FILAMBIL = 'Narial "Tramica" Filambil';
    private const FAREL_BAVIANNE_LOR = 'Farel "Bavianne" Lor';
    private const TUREITA_VALILLE_SAUPEL = 'Tureita "Valillë" Saupel';
    private const ATIE_THONISSA_CALIALIE = 'Atië "Thonissa" Calialië';
    private const OTHILLE_PREPELITA_HERATOND = 'Othillë "Prepelita" Heratond';
    private const TORTARDE_CIRYE_MEROT = 'Tortarde "Cirye" Merot';
    private const RARASELLE_HERILLE_MEION = 'Raraselle "Herillë" Meion';
    private const OTHALIEL_DRAMBELLE_SERANDIN = 'Othaliel "Drambelle" Serandin';
    private const TARILLE_DOMIRTILLE_ISRISIEL = 'Tarillë "Domirtille" Isrisiel';
    private const ULVIONNE_NIMILLE_TUZERTOS = 'Ulvionne "Nimillë" Tuzertos';
    private const SEGOLVELLE_ARE_SOTALIEN = 'Segolvelle "Are" Sotalien';
    private const LAMIELLE_NERIEL_LOEL = 'Lamielle "Neriel" Loel';
    private const EARILLE_PIPIALLE_FILEND = 'Eärillë "Pipialle" Filend';
    private const RICENISSA_ELEL_MEMOT = 'Ricenissa "Elel" Memot';
    private const FIRIE_SERYE_GREDUS = 'Firie "Seryë" Gredus';
    private const NAREL_JIMA_NIMAND = 'Narel "Jima" Nimand';
    private const LINIAL_RUNDIONNE_FINIL = 'Linial "Rundionne" Finil';
    private const HERYE_PIGRAARDE_PALTIN = 'Heryë "Pigraarde" Paltin';
    private const NUMIAL_VAGRINA_FINMEHT = 'Numial "Vagrina" Finmeht';
    private const BALINLONDE_ELE_VABREL = 'Balinlonde "Elë" Vabrel';
    private const ISIAL_RAMONNE_SIRAM = 'Isial "Ramonne" Siram';
    private const DAULLISSA_CALYE_PROTAMAN = 'Daullissa "Calyë" Protaman';
    private const SECATIONNE_PALYE_FIPADAM = 'Secationne "Palyë" Fipadam';
    private const HIZETELLA_TORILLE_LEODATH = 'Hizetella "Torillë" Leodath';
    private const CEMIAL_PODIONNE_PELDIN = 'Cemial "Podionne" Peldin';
    private const LARONNE_INEN_ITAN = 'Laronne "Inen" Itan';
    private const SIPEIELLE_LOTHIE_OCAR = 'Sipeielle "Lothië" Ocar';
    private const SILEN_TUSIE_HIRILLEE = 'Silen "Tusie" Hirillëë';
    private const SAUKELLA_TAREL_ORAL = 'Saukella "Tarel" Oral';
    private const DRALUSCIELLE_TURYE_GRENASIEN = 'Draluscielle "Turyë" Grenasien';
    private const BRIMYE_BROMICE_TARDACER = 'Brimyë "Bromice" Tardacer';
    private const SHARILLE_FILIE_TURIBIL = 'Sharille "Filië" Turibil';
    private const ARYE_SAUNCERCELLE_FARDACIN = 'Aryë "Sauncercelle" Fardacin';
    private const RUNMIE_CALE_SAUGRASATH = 'Runmie "Calë" Saugrasath';
    private const AME_BRETOSCIA_SIRANATIN = 'Amë "Bretoscia" Siranatin';
    private const PIRCELLE_FARALIEL_RUNDOR = 'Pircelle "Faraliel" Rundor';
    private const CEMIAL_TOIALLE_FILAC = 'Cemial "Toialle" Filac';
    private const EARILLE_ETARDE_TARDARIEL = 'Eärillë "Etarde" Tardariel';
    private const HIPADARDE_EARIE_LOEL = 'Hipadarde "Eärië" Loel';
    private const NERYE_MAUGRELLE_ATANATEN = 'Neryë "Maugrelle" Atanaten';
    private const LORSA_LINYE_LADOD = 'Lorsa "Linyë" Ladod';
    private const FINIAL_DOSYSE_THANINDION = 'Finial "Dosyse" Thanindion';
    private const ELYE_MEZONNE_FINON = 'Elyë "Mezonne" Finon';
    private const THANIEL_BEOPYSSE_CIRYANDIR = 'Thaniel "Beopysse" Ciryandir';
    private const SIZITA_ANEL_ZADAL = 'Sizita "Anel" Zadal';
    private const ATIS_PROSCIA_NARIELAL = 'Atis "Proscia" Narielal';
    private const PAZIE_CEMIAL_FILION = 'Pazie "Cemial" Filion';
    private const ESCIE_ARTIAL_SERCAN = 'Escie "Artial" Sercan';
    private const OTHYE_RUNDIONNE_GLORDACIELYE = 'Othyë "Rundionne" Glordacielyë';
    private const HEMIGINA_LINEN_BROCEN = 'Hemigina "Linen" Brocen';
    private const GLOREL_JEZINE_BRIMOR = 'Glorel "Jezine" Brimor';
    private const RESIONNE_TORAL_SISARD = 'Resionne "Toral" Sisard';
    private const ELALIEL_RANGRASIANNE_FILENT = 'Elaliel "Rangrasianne" Filent';
    private const SIREL_BATHONNE_ELAR = 'Sirel "Bathonne" Elár';
    private const HITONDE_FIRIAL_BUMUS = 'Hitonde "Firial" Bumus';
    private const RESIELLE_HIREL_SAUPEL = 'Resielle "Hirel" Saupel';
    private const NIMIEL_URISSE_FIRCER = 'Nimiel "Urisse" Fircer';
    private const GLOREL_DROTARDE_ISENDEL = 'Glorel "Drotarde" Isendel';
    private const SAUNCERCELLE_ELE_PREIL = 'Sauncercelle "Ele" Preil';
    private const DORIS_BRADIVISSE_THANUL = 'Doris "Bradivisse" Thanul';
    private const MIGOLLELLA_ATIE_FILLEN = 'Migollella "Atië" Fillen';
    private const THETHIA_SIRAL_TRADEL = 'Thethia "Siral" Tradel';
    private const SAUGORYSE_ARTIS_GIGODUS = 'Saugoryse "Artis" Gigodus';
    private const TRANCERYSE_VALEL_YDAS = 'Tranceryse "Valel" Ydas';
    private const LEOSISSA_FILALIEL_LEODUR = 'Leosissa "Filaliel" Leodur';
    private const DIDIA_SIRAL_VALIN = 'Didia "Siral" Valin';
}
