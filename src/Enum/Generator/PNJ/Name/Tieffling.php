<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Name;

use App\Enum\Generator\PNJ\Gender;
use App\Model\AbstractEnum;

class Tieffling extends AbstractEnum
{
    private const PREFIX_MALE = ['Aet', 'Ak', 'Am', 'Aran', 'And', 'Ar', 'Ark', 'Bar', 'Car', 'Cas', 'Dam', 'Dhar',
        'Eb', 'Ek', 'Er', 'Gar', 'Gu', 'Gue', 'Hor', 'Ia', 'Ka', 'Kai', 'Kar', 'Kil', 'Kos', 'Ky', 'Loke', 'Mal',
        'Male', 'Mav', 'Me', 'Mor', 'Neph', 'Oz', 'Ral', 'Re', 'Rol', 'Sal', 'Sha', 'Sir', 'Ska', 'The', 'Thy',
        'Thyne', 'Ur', 'Uri', 'Val', 'Xar', 'Zar', 'Zer', 'Zher', 'Zor', ];
    private const SUFIX_MALE = ['adius', 'akas', 'akos', 'char', 'cis', 'cius', 'dos', 'emon', 'ichar', 'il', 'ilius',
        'ira', 'lech', 'lius', 'lyre', 'marir', 'menos', 'meros', 'mir', 'mong', 'mos', 'mus', 'non', 'rai', 'rakas',
        'rakir', 'reus', 'rias', 'ris', 'rius', 'ron', 'ros', 'rus', 'rut', 'shoon', 'thor', 'thos', 'thus', 'us',
        'venom', 'vir', 'vius', 'xes', 'xik', 'xikas', 'xire', 'xius', 'xus', 'zer', 'zire', ];

    private const PREFIX_FEMALE = ['Af', 'Agne', 'Ani', 'Ara', 'Ari', 'Aria', 'Bel', 'Bri', 'Cre', 'Da', 'Di', 'Dim',
        'Dor', 'Ea', 'Fri', 'Gri', 'His', 'In', 'Ini', 'Kal', 'Le', 'Lev', 'Lil', 'Ma', 'Mar', 'Mis', 'Mith', 'Na',
        'Nat', 'Ne', 'Neth', 'Nith', 'Ori', 'Pes', 'Phe', 'Qu', 'Ri', 'Ro', 'Sa', 'Sar', 'Seiri', 'Sha', 'Val', 'Vel',
        'Ya', 'Yora', 'Yu', 'Za', 'Zai', 'Ze', ];
    private const SUFIX_FEMALE = ['bis', 'borys', 'cria', 'cyra', 'dani', 'doris', 'faris', 'firith', 'goria', 'grea',
        'hala', 'hiri', 'karia', 'ki', 'laia', 'lia', 'lies', 'lista', 'lith', 'loth', 'lypsis', 'lyvia', 'maia',
        'meia', 'mine', 'narei', 'nirith', 'nise', 'phi', 'pione', 'punith', 'qine', 'rali', 'rissa', 'seis', 'solis',
        'spira', 'tari', 'tish', 'uphis', 'vari', 'vine', 'wala', 'wure', 'xibis', 'xori', 'yis', 'yola', 'za', 'zes',
        ];

    /**
     * @return string|array<string>
     */
    public static function getName(string $gender, int $nbNames = 1): string|array
    {
        if ($nbNames > 1) {
            $names = [];
            $isMale = $gender === Gender::MALE()->getValue();

            for ($i = 0; $i < $nbNames; ++$i) {
                $names[] = $isMale ? self::getMaleName() : self::getFemaleName();
            }

            return $names;
        }

        return $gender === Gender::MALE()->getValue() ? self::getMaleName() : self::getFemaleName();
    }

    private static function getFemaleName(): string
    {
        return self::PREFIX_FEMALE[random_int(0, count(self::PREFIX_FEMALE) - 1)].
            self::SUFIX_FEMALE[random_int(0, count(self::SUFIX_FEMALE) - 1)];
    }

    private static function getMaleName(): string
    {
        return self::PREFIX_MALE[random_int(0, count(self::PREFIX_MALE) - 1)].
            self::SUFIX_MALE[random_int(0, count(self::SUFIX_MALE) - 1)];
    }
}
