<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

class PNJClass extends AbstractEnum
{
    private const BARBARIAN = 'Barbare';
    private const BARD = 'Barde';
    private const CLERIC = 'Clerc';
    private const DRUID = 'Druide';
    private const FIGHTER = 'Guerrier';
    private const MONK = 'Moine';
    private const PALADIN = 'Paladin';
    private const RANGER = 'Rôdeur';
    private const ROGUE = 'Roublard';
    private const SORCERER = 'Ensorceleur';
    private const WARLOCK = 'Sorcier';
    private const WIZARD = 'Magicien';
}
