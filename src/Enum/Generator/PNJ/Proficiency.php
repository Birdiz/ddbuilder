<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

/**
 * Class Proficiency.
 *
 * @method static Proficiency ACROBATIES()
 * @method static Proficiency ARCANES()
 * @method static Proficiency ATHLETISME()
 * @method static Proficiency DISCRETION()
 * @method static Proficiency DRESSAGE()
 * @method static Proficiency ESCAMOTAGE()
 * @method static Proficiency HISTOIRE()
 * @method static Proficiency INTIMIDATION()
 * @method static Proficiency INVESTIGATION()
 * @method static Proficiency MEDECINE()
 * @method static Proficiency NATURE()
 * @method static Proficiency PERCEPTION()
 * @method static Proficiency PERSUASION()
 * @method static Proficiency RELIGION()
 * @method static Proficiency REPRESENTATION()
 * @method static Proficiency SURVIE()
 * @method static Proficiency TROMPERIE()
 */
class Proficiency extends AbstractEnum
{
    private const ACROBATIES = 'Acrobaties';
    private const ARCANES = 'Arcanes';
    private const ATHLETISME = 'Athlétisme';
    private const DISCRETION = 'Discrétion';
    private const DRESSAGE = 'Dressage';
    private const ESCAMOTAGE = 'Escamotage';
    private const HISTOIRE = 'Histoire';
    private const INTIMIDATION = 'Intimidation';
    private const INVESTIGATION = 'Investigation';
    private const MEDECINE = 'Médecine';
    private const NATURE = 'Nature';
    private const PERCEPTION = 'Perception';
    private const PERSUASION = 'Persuasion';
    private const RELIGION = 'Religion';
    private const REPRESENTATION = 'Représentation';
    private const SURVIE = 'Survie';
    private const TROMPERIE = 'Tromperie';
}
