<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Race;

use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Name\Aarakocra;
use App\Enum\Generator\PNJ\Name\Dragonborn;
use App\Enum\Generator\PNJ\Name\Tieffling;
use App\Model\AbstractNamedEnum;
use App\Util\ArrayRandTrait;

/**
 * Class Race.
 *
 * @method static Race HILL_DWARF()
 * @method static Race MOUNTAIN_DWARF()
 * @method static Race HIGH_ELF()
 * @method static Race WOOD_ELF()
 * @method static Race DROW()
 * @method static Race LIGHTFOOT_HALFELING()
 * @method static Race STOOT_HALFELING()
 * @method static Race HUMAN()
 * @method static Race DRAGONBORN()
 * @method static Race FOREST_GNOME()
 * @method static Race ROCK_GNOME()
 * @method static Race HALF_ELF()
 * @method static Race HALF_ORC()
 * @method static Race TIEFLING()
 * @method static Race AARAKOCRA()
 */
class Race extends AbstractNamedEnum
{
    use ArrayRandTrait;

    private const HILL_DWARF = [
        'name' => 'Nain des collines',
        'traits' => [
            'AS' => ['CON' => 2, 'SAG' => 1],
            'age' => 350,
            'size' => [120, 150],
            'weight' => [60, 120],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Dwarf::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Dwarf::class,
    ];

    private const MOUNTAIN_DWARF = [
        'name' => 'Nain des montagnes',
        'traits' => [
            'AS' => ['FOR' => 2, 'CON' => 2],
            'age' => 350,
            'size' => [120, 150],
            'weight' => [60, 120],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Dwarf::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Dwarf::class,
    ];

    private const HIGH_ELF = [
        'name' => 'Haut elfe',
        'traits' => [
            'AS' => ['DEX' => 2, 'INT' => 1],
            'age' => 750,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Elf::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Elf::class,
    ];

    private const WOOD_ELF = [
        'name' => 'Elfe des bois',
        'traits' => [
            'AS' => ['DEX' => 2, 'SAG' => 1],
            'age' => 750,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Elf::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Elf::class,
    ];

    private const DROW = [
        'name' => 'Elfe noir (drow)',
        'traits' => [
            'AS' => ['DEX' => 2, 'CHA' => 1],
            'age' => 750,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Drow::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Drow::class,
    ];

    private const LIGHTFOOT_HALFELING = [
        'name' => 'Halfelin pas légers',
        'traits' => [
            'AS' => ['DEX' => 2, 'CHA' => 1],
            'age' => 150,
            'size' => [80, 100],
            'weight' => [15, 30],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Halfling::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Halfling::class,
    ];

    private const STOOT_HALFELING = [
        'name' => 'Halfelin robuste',
        'traits' => [
            'AS' => ['DEX' => 2, 'CON' => 1],
            'age' => 150,
            'size' => [80, 100],
            'weight' => [15, 30],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Halfling::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Halfling::class,
    ];

    private const HUMAN = [
        'name' => 'Humain',
        'traits' => [
            'AS' => ['FOR' => 1, 'DEX' => 1, 'CON' => 1, 'INT' => 1, 'SAG' => 1, 'CHA' => 1],
            'age' => 100,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Human::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Human::class,
    ];

    private const DRAGONBORN = [
        'name' => 'Drakéïde',
        'traits' => [
            'AS' => ['FOR' => 2, 'CHA' => 1],
            'age' => 80,
            'size' => [180, 230],
            'weight' => [100, 150],
        ],
        'maleClass' => Dragonborn::class,
        'femaleClass' => Dragonborn::class,
    ];

    private const FOREST_GNOME = [
        'name' => 'Gnome des forêts',
        'traits' => [
            'AS' => ['DEX' => 1, 'INT' => 2],
            'age' => 500,
            'size' => [80, 120],
            'weight' => [15, 30],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Gnome::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Gnome::class,
    ];

    private const ROCK_GNOME = [
        'name' => 'Gnome des rochers',
        'traits' => [
            'AS' => ['CON' => 1, 'INT' => 2],
            'age' => 500,
            'size' => [80, 120],
            'weight' => [15, 30],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\Gnome::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\Gnome::class,
    ];

    private const HALF_ELF = [
        'name' => 'Demi-elfe',
        'traits' => [
            'AS' => ['CHA' => 2, 'RAND' => 2],
            'age' => 180,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\HalfElf::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\HalfElf::class,
    ];

    private const HALF_ORC = [
        'name' => 'Demi-orc',
        'traits' => [
            'AS' => ['FOR' => 2, 'CON' => 1],
            'age' => 75,
            'size' => [150, 180],
            'weight' => [60, 110],
        ],
        'maleClass' => \App\Enum\Generator\PNJ\Name\Male\HalfOrc::class,
        'femaleClass' => \App\Enum\Generator\PNJ\Name\Female\HalfOrc::class,
    ];

    private const TIEFLING = [
        'name' => 'Tieffelin',
        'traits' => [
            'AS' => ['INT' => 1, 'CHA' => 2],
            'age' => 100,
            'size' => [150, 180],
            'weight' => [45, 90],
        ],
        'maleClass' => Tieffling::class,
        'femaleClass' => Tieffling::class,
    ];

    private const AARAKOCRA = [
        'name' => 'Aarakocra',
        'traits' => [
            'AS' => ['DEX' => 2, 'WIS' => 1],
            'age' => 30,
            'size' => [130, 170],
            'weight' => [40, 50],
        ],
        'maleClass' => Aarakocra::class,
        'femaleClass' => Aarakocra::class,
    ];

    /**
     * @return string|array<mixed>
     */
    public static function getName(string $raceName, string $gender): string|array
    {
        $class = $gender === Gender::MALE()->getValue() ? 'maleClass' : 'femaleClass';
        $races = static::getByName();

        if (true === in_array(
                $raceName,
                [
                    static::DRAGONBORN()->getValue()['name'],
                    static::TIEFLING()->getValue()['name'],
                    static::AARAKOCRA()->getValue()['name'],
                ],
                true
            )
        ) {
            return $races[$raceName][$class]::getName($gender);
        }

        return static::getXSimpleItems(1, $races[$raceName][$class]::toArray());
    }

    /**
     * @return array<mixed>
     */
    public static function getMetrics(string $race): array
    {
        return static::getValueFromNameKey(
            $race,
            [
                self::getDwarvesMetrics(),
                self::getDwarvesMetrics(),
                self::getElvesMetrics(),
                self::getElvesMetrics(),
                self::getElvesMetrics(),
                self::getHalflingsMetrics(),
                self::getHalflingsMetrics(),
                self::getHumanMetrics(),
                self::getBigRaceMetrics(),
                self::getGnomesMetrics(),
                self::getGnomesMetrics(),
                self::getHalfelvesMetrics(),
                self::getBigRaceMetrics(),
                self::getHumanMetrics(),
                self::getAarakocrasMetrics(),
            ]
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getDwarvesMetrics(): array
    {
        return [
            'size' => number_format(random_int(120, 150) / 100, 2, 'm', ' '),
            'weight' => random_int(65, 85),
            'age' => random_int(15, 350),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getElvesMetrics(): array
    {
        return [
            'size' => number_format(random_int(150, 180) / 100, 2, 'm', ' '),
            'weight' => random_int(45, 65),
            'age' => random_int(15, 700),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHumanMetrics(): array
    {
        return [
            'size' => number_format(random_int(150, 200) / 100, 2, 'm', ' '),
            'weight' => random_int(55, 120),
            'age' => random_int(15, 80),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBigRaceMetrics(): array
    {
        return [
            'size' => number_format(random_int(180, 220) / 100, 2, 'm', ' '),
            'weight' => random_int(95, 130),
            'age' => random_int(15, 80),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHalfelvesMetrics(): array
    {
        return [
            'size' => number_format(random_int(150, 180) / 100, 2, 'm', ' '),
            'weight' => random_int(50, 90),
            'age' => random_int(15, 180),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGnomesMetrics(): array
    {
        return [
            'size' => number_format(random_int(80, 110) / 100, 2, 'm', ' '),
            'weight' => random_int(20, 40),
            'age' => random_int(15, 350),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHalflingsMetrics(): array
    {
        return [
            'size' => number_format(random_int(80, 110) / 100, 2, 'm', ' '),
            'weight' => random_int(20, 40),
            'age' => random_int(15, 150),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getAarakocrasMetrics(): array
    {
        return [
            'size' => number_format(random_int(130, 170) / 100, 2, 'm', ' '),
            'weight' => random_int(40, 50),
            'age' => random_int(3, 30),
        ];
    }
}
