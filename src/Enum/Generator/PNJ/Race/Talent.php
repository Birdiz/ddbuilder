<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Race;

use App\Enum\Generator\PNJ\Language;
use App\Enum\Generator\PNJ\Proficiency;
use App\Enum\Object\Tools;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

/**
 * Class Talent.
 *
 * @method static Talent ELF_CANTRIP()
 * @method static Talent DROW_MAGIC()
 * @method static Talent BLOW()
 * @method static Talent GNOME_CANTRIP()
 * @method static Talent ANIMAL_FRIENDSHIP()
 * @method static Talent HANDYMAN()
 * @method static Talent THREATENING()
 * @method static Talent INFERNAL_INHERITANCE()
 */
class Talent extends AbstractEnum
{
    use ArrayRandTrait;

    private const ELF_CANTRIP = '{pronoun} connaît un sort mineur de votre choix parmi la liste des sorts mineurs de 
    magicien. L\'Intelligence est sa caractéristique d\'incantation pour ce sort.';

    private const DROW_MAGIC = '{pronoun} connaît le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=lumieres-dansantes" target="_blank">Lumières dansantes</a>. 
    Au niveau 3, {pronoun} peut lancer le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=lueurs-feeriques" target="_blank">Lueurs féeriques</a> une fois 
    avec ce trait et regagne cette capacité lorsqu\'{pronoun} termine un repos long. Au niveau 5, {pronoun} peut
     lancer le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=tenebres" target="_blank">Ténèbres</a> une fois avec ce trait et 
    regagne cette capacité lorsqu\'{pronoun} termine un repos long. Le Charisme est sa caractéristique 
    d\'incantation pour ces sorts.';

    private const BLOW = '{pronoun} peut utiliser son action pour expirer une énergie destructrice. Son ascendance 
    draconique détermine la taille, la forme et le type de dégâts de votre souffle. Lorsqu\'{pronoun} utilise son 
    souffle, toute créature dans la zone de l\'expiration doit faire un jet de sauvegarde, dont le type est 
    déterminé par son ascendance draconique. Le DD de ce jet de sauvegarde est égal à 8 + son modificateur de 
    Constitution + son bonus de maîtrise. En cas d\'échec, la créature subit 2d6 dégâts, et la moitié en cas de 
    réussite. Les dégâts augmentent à 3d6 au niveau 6, 4d6 au niveau 11, et 5d6 au niveau 16. Après avoir utilisé 
    son souffle, {pronoun} doit terminer un repos court ou long pour pouvoir l\'utiliser à nouveau.';

    private const GNOME_CANTRIP = '{pronoun} connaît le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=illusion-mineure" target="_blank">Illusion mineure</a>. 
    L\'Intelligence est sa caractéristique d\'incantation pour ce sort.';

    private const ANIMAL_FRIENDSHIP = 'À l\'aide de sons et de gestes, {pronoun} peut communiquer des idées simples 
    à des bêtes de taille P ou plus petite. Les gnomes des forêts aiment les animaux et traitent de manière 
    bienveillante comme un animal domestique tout animal qu\'ils peuvent trouver (écureuils, blaireaux, lapins, taupes, 
    etc.).';

    private const HANDYMAN = '{pronoun} maîtrise les outils de bricoleur. En utilisant ces outils, {pronoun} 
    peut passer 1 heure et dépenser pour 10 po de matériaux pour construire un mécanisme de taille TP, de CA 5 et 1 pv. 
    Le dispositif cesse de fonctionner après 24 heures (sauf si {pronoun} passe 1 heure à le réparer) ou si 
    {pronoun} utilise une action pour le démonter ; à ce moment, {pronoun} peut récupérer les matériaux 
    qu\'{pronoun} a utilisés pour le créer ; {pronoun} peut avoir jusqu\'à trois de ces dispositifs actifs à la 
    fois. Lorsqu\'{pronoun} crée un mécanisme, {pronoun} choisit l\'une des options suivantes :<br/><br/>
    - <i>Allume feu</i>. Le mécanisme produit une toute petite flamme qui peut être utilisée pour allumer une 
    bougie ou une torche au prix d\'une action.<br/>
    - <i>Boîte à musique</i>. Lorsqu\'on l\'ouvre, la boîte reproduit une chanson (toujours la même) à un volume 
    modéré jusqu\'à la fin du morceau ou avant si la boîte est refermée.<br/>
    - <i>Jouet mécanique</i>. Le jouet représente un animal ou une personne, comme une grenouille, une souris, un 
    oiseau ou un soldat, sur des roulettes. Lorsqu\'il est placé sur le sol, il se déplace de 1,50 mètre chaque 
    tour dans une direction aléatoire et fait des bruits en fonction de la créature qu\'il représente.';

    private const INFERNAL_INHERITANCE = '{pronoun} connaît le sort mineur 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=thaumaturgie" target="_blank">Thaumaturgie</a>. Au niveau 3, 
    {pronoun} peut lancer le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=represailles-infernales" target="_blank">Représailles infernales
    </a> comme un sort de niveau 2 une fois avec ce trait et regagne cette capacité lorsqu\'{pronoun} termine un 
    repos long. Au niveau 5, {pronoun} peut lancer le sort 
    <a href="https://www.aidedd.org/dnd/sorts.php?vf=tenebres" target="_blank">Ténèbres</a> une fois avec ce trait et 
    regagne cette capacité lorsqu\'{pronoun} termine un repos long. Le Charisme est sa caractéristique 
    d\'incantation pour ces sorts.';

    /**
     * @return array<string, mixed>
     */
    public static function getDwarfTalents(): array
    {
        $tool = static::getXSimpleItems(
            1,
            [
                Tools::SMITH()->getValue(),
                Tools::BREWER()->getValue(),
                Tools::MASON()->getValue(),
            ]
        );

        return [
            'Outil maîtrisé' => $tool,
            'Langues' => [
                Language::COMMON()->getValue(),
                Language::DWARFISH()->getValue(),
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getElfTalents(bool $isHigh = true, bool $isDrow = false): array
    {
        $base = [
            'Sens aiguisé' => Proficiency::PERCEPTION()->getValue(),
            'Langues' => [
                Language::COMMON()->getValue(),
                Language::ELVISH()->getValue(),
            ],
        ];

        if ($isDrow) {
            return array_merge(
                $base,
                ['Magie drow' => static::DROW_MAGIC()->getValue()]
            );
        }

        if ($isHigh) {
            return [
                'Langues' => Language::getLanguage(1, $base['Langues']),
                'Sort mineur' => static::ELF_CANTRIP()->getValue(),
            ];
        }

        return $base;
    }

    /**
     * @return array<string, array<int, string>>
     */
    public static function getHalfelingTalents(): array
    {
        return [
            'Langues' => [
                Language::COMMON()->getValue(),
                Language::HALFELISH()->getValue(),
            ],
        ];
    }

    /**
     * @return array<string, array<int, string>>
     */
    public static function getHumanTalent(): array
    {
        return ['Langues' => Language::getLanguage(1, [Language::COMMON()->getValue()])];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getDragonbornTalents(): array
    {
        $languages = [
            'Langues' => [
                Language::COMMON()->getValue(),
                Language::DRACONIC()->getValue(),
            ],
        ];

        $inheritances = self::getDragonbornInheritances();
        $inheritance = array_rand($inheritances);

        return array_merge(
            $languages,
            [
                'Souffle' => static::BLOW()->getValue(),
                "Ascendance draconique - $inheritance" => $inheritances[$inheritance],
            ]
        );
    }

    /**
     * @return array<string, mixed>
     */
    public static function getGnomeTalents(bool $isForest = true): array
    {
        $languages = [
            'Langues' => [
                Language::COMMON()->getValue(),
                Language::GNOMISH()->getValue(),
            ],
        ];

        return $isForest
            ? array_merge(
                $languages,
                [
                    'Illusionniste-né' => static::GNOME_CANTRIP()->getValue(),
                    'Communication avec les petits animaux' => static::ANIMAL_FRIENDSHIP()->getValue(),
                ]
            )
            : array_merge(
                $languages,
                ['Bricoleur' => static::HANDYMAN()->getValue()]
            )
            ;
    }

    /**
     * @return array<string, mixed>
     */
    public static function getHalfElfTalents(): array
    {
        return [
            'Polyvalence' => static::getXSimpleItems(2, Proficiency::toArray()),
            'Langues' => Language::getLanguage(
                1,
                [
                    Language::COMMON()->getValue(),
                    Language::GNOMISH()->getValue(),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getHalfOrcTalents(): array
    {
        return [
            'Menaçant' => Proficiency::INTIMIDATION(),
            'Langues' => Language::getLanguage(
                1,
                [
                    Language::COMMON()->getValue(),
                    Language::ORCISH()->getValue(),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getTiefelingTalents(): array
    {
        return [
            'Ascendance infernale' => static::INFERNAL_INHERITANCE()->getValue(),
            'Langues' => [
                    Language::COMMON()->getValue(),
                    Language::INFERNAL()->getValue(),
                ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getTalentsFromRaceName(string $name): array
    {
        return match ($name) {
            Race::MOUNTAIN_DWARF()->getValue()['name'], Race::HILL_DWARF()->getValue(
            )['name'] => static::getDwarfTalents(),
            Race::HIGH_ELF()->getValue()['name'] => static::getElfTalents(),
            Race::WOOD_ELF()->getValue()['name'] => static::getElfTalents(false),
            Race::DROW()->getValue()['name'] => static::getElfTalents(false, true),
            Race::LIGHTFOOT_HALFELING()->getValue()['name'], Race::STOOT_HALFELING()->getValue(
            )['name'] => static::getHalfelingTalents(),
            Race::HUMAN()->getValue()['name'] => static::getHumanTalent(),
            Race::DRAGONBORN()->getValue()['name'] => static::getDragonbornTalents(),
            Race::FOREST_GNOME()->getValue()['name'] => static::getGnomeTalents(),
            Race::ROCK_GNOME()->getValue()['name'] => static::getGnomeTalents(false),
            Race::HALF_ELF()->getValue()['name'] => static::getHalfElfTalents(),
            Race::HALF_ORC()->getValue()['name'] => static::getHalfOrcTalents(),
            Race::TIEFLING()->getValue()['name'] => static::getTiefelingTalents(),
            default => [],
        };
    }

    /**
     * @return array<string, array<string, string>>
     */
    private static function getDragonbornInheritances(): array
    {
        return [
            'Blanc' => ['Dégâts' => 'Froid', 'Souffle' => 'Cône de 4,50 m (JdS de Con.)'],
            'Bleu' => ['Dégâts' => 'Foudre', 'Souffle' => 'Ligne de 1,50 x 9 m (JdS de Dex.)'],
            'Noir' => ['Dégâts' => 'Acide', 'Souffle' => 'Ligne de 1,50 x 9 m (JdS de Dex.)'],
            'Rouge' => ['Dégâts' => 'Feu', 'Souffle' => 'Cône de 4,50 m (JdS de Con.)'],
            'Vert' => ['Dégâts' => 'Poison', 'Souffle' => 'Cône de 4,50 m (JdS de Con.)'],
            'Airain' => ['Dégâts' => 'Feu', 'Souffle' => 'Ligne de 1,50 x 9 m (JdS de Dex.)'],
            'Argent' => ['Dégâts' => 'Froid', 'Souffle' => 'Cône de 4,50 m (JdS de Con.)'],
            'Bronze' => ['Dégâts' => 'Foudre', 'Souffle' => 'Ligne de 1,50 x 9 m (JdS de Dex.)'],
            'Cuivre' => ['Dégâts' => 'Acide', 'Souffle' => 'Ligne de 1,50 x 9 m (JdS de Dex.)'],
            'Or' => ['Dégâts' => 'Feu', 'Souffle' => 'Cône de 4,50 m (JdS de Con.)'],
        ];
    }
}
