<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

/**
 * Class Relationship.
 *
 * @method static Relationship SINGLE()
 * @method static Relationship MARRIED()
 * @method static Relationship DIVORCED()
 * @method static Relationship WIDOWER()
 * @method static Relationship RELATIONSHIP()
 * @method static Relationship PARAMOUR()
 * @method static Relationship BROKE_UP()
 */
class Relationship extends AbstractEnum
{
    private const SINGLE = 'célibataire';
    private const MARRIED = 'marié(e)';
    private const DIVORCED = 'divorcé(e)';
    private const WIDOWER = 'veuf/veuve';
    private const RELATIONSHIP = 'en relation';
    private const PARAMOUR = 'amant(e) de quelqu\'un';
    private const BROKE_UP = 'récemment séparé(e)';
    private const NOT_INTERESTED = 'inintéressé(e) par une relation';
}
