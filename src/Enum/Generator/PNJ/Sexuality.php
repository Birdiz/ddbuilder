<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ;

use App\Model\AbstractEnum;

class Sexuality extends AbstractEnum
{
    private const STRAIGHT = 'hétérosexuel(le)';
    private const GAY = 'homosexuel(le)';
    private const BI = 'bisexuel(le)';
    private const ASEXUAL = 'asexuel(le)';
    private const SAPIO = 'sapiosexuel(le)';
    private const PAN = 'pansexuel(le)';
    private const NONROMANTIC = 'aromantique';
    private const LITHOROMANTIC = 'lithromantique';
}
