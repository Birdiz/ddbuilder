<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sexuality;

use App\Model\AbstractEnum;

class Attraction extends AbstractEnum
{
    private const STRENGH = 'les attributs physique';
    private const MONEY = 'l\'épaisseur de la bourse';
    private const SOCIAL_STATUS = 'le statut social';
    private const ETIK = 'un intérêt éthique particulier (gentillesse, cupide, etc...)';
    private const KNOWLEDGE = 'les connaissances et savoirs';
    private const SOCIAL_INTERACTION = 'les interactions sociales';
    private const PARTICULARITY = 'un trait qui sort de l\'ordinaire';
    private const FAITH = 'la dévotion';
    private const STRENGH_WILL = 'la force de volonté';
    private const LEADERSHIP = 'le leadership';
    private const HYGIENE = 'le sens de l\'hygiène';
    private const SINCERITY = 'la sincérité';
    private const LESS_AGE = 'la différence d\'age (moins agé)';
    private const MORE_AGE = 'la différence d\'age (plus agé)';
    private const MYSTERY = 'le mystère';
    private const WEIGHT = 'le poids';
}
