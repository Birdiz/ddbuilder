<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sexuality;

use App\Model\AbstractEnum;

class Interest extends AbstractEnum
{
    private const FUN = 's\'amuser et passer à autre chose';
    private const DESCENDANT = 'avoir une descendance';
    private const LONG_TERM = 'une relation sur le long terme';
    private const SHORT_TERM = 'une relation sur le court terme';
    private const MARIAGE = 'le mariage';
    private const PROFIT = 'un bénéfice lié à ce qui l\'attire';
    private const BRAGGING = 'le fait de pouvoir se vanter';
    private const SABOTAGE = 'le fait de pouvoir saboter une autre relation';
    private const ONE_NIGHT = 'une relation d\'une nuit';
    private const NOTHING = 'rien';
    private const LOVERS = 'une union libre';
}
