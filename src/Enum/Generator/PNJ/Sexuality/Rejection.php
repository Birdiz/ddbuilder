<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sexuality;

use App\Model\AbstractEnum;

class Rejection extends AbstractEnum
{
    private const VIOLENCE = 'la violence';
    private const LOW_RANK = 'un statut social bas';
    private const HIGH_RANK = 'un statut social élevé';
    private const STRANGER = 'le fait d\'être un étranger';
    private const ANIMAL = 'les animaux';
    private const WEAKNESS = 'la faiblesse';
    private const MAGIC = 'la magie';
    private const FAITH = 'la foi';
    private const LIE = 'les mensonges';
    private const PUBLIC_RELATION = 'les relations publiques';
    private const LANGAGE = 'un langage spécifique';
    private const UGLY = 'un physique disgracieux';
    private const WEIRD = 'la bizarrerie';
    private const DUMB = 'l\'idiotie';
    private const STINK = 'l\'odeur';
    private const PHYSIC = 'un attribut physique en particulier';
    private const WEIGHT = 'le poids';
    private const BAD_HABITS = 'un comportement social gênant';
    private const MANNERS = 'un comportement personnel gênant';
}
