<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sexuality;

use App\Model\AbstractEnum;

class Search extends AbstractEnum
{
    private const TRUE_LOVE = 'l\'amour véritable';
    private const SEXE = 'le sexe';
    private const PLATONIQUE = 'une relation platonique';
    private const LONG_DISTANCE = 'une relation longue distance';
    private const FUSIONNAL = 'une relation fusionnelle';
    private const ONE_SIDED = 'une relation à sens unique';
}
