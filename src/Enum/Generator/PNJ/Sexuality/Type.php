<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sexuality;

use App\Model\AbstractEnum;

class Type extends AbstractEnum
{
    private const HETEROSEXUEL = 'hétérosexuel(le)';
    private const HOMOSEXUAL = 'homosexuel(le)';
    private const BISEXUEL = 'bisexuel(le)';
    private const OPEN = 'ouvert(e) à tout';
    private const STRICT = 'seulement intéressé(e) par la même espèce';
    private const CURIOUS_CLOSE = 'curieux/curieuse à propos des espèces proches';
    private const CURIOUS_FAR = 'curieux/curieuse à propos des espèces très différentes';
    private const FAN = 'fan d\'une espèce spécifique';
    private const ASEXUAL = 'asexuel(le)';
    private const SAPIO = 'sapiosexuel(le)';
    private const PAN = 'pansexuel(le)';
    private const NONROMANTIC = 'aromantique';
    private const LITHOROMANTIC = 'lithromantique';
}
