<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sidekick;

use App\Model\AbstractEnum;

/**
 * @method static Archetype ARTIFICER()
 * @method static Archetype BARD()
 * @method static Archetype SAVAGE()
 */
class Archetype extends AbstractEnum
{
    private const ARTIFICER = [
        'name' => 'Artificier',
        'description' => 'Humanoïde de taille M, tout alignement, niveau 1',
        'AC' => [
            'score' => 14,
            'type' => '(armure de cuir clouté)',
        ],
        'HP' => [
            'score' => '11',
            'dices' => '(2d8 + 2)',
        ],
        'speed' => '9 m',
        'traits' => [
            'AS' => [
                'FOR' => 10,
                'DEX' => 14,
                'CON' => 13,
                'INT' => 15,
                'WIS' => 10,
                'CHA' => 12,
            ],
            'saving' => 'Constitution +3, Intelligence +4',
            'skills' => 'Arcanes +4, Histoire +4, Perception +2, Escamotage +4',
            'senses' => 'Perception passive 12',
            'languages' => 'commun, plus une de votre choix',
        ],
        'tools' => 'Outils de vouleur, outils de bricoleur ou un type d\'outils d\'artisan de votre choix.',
        'features' => [
            'Imprégation d\'objet' => 'L\'artificier a la capacité d\'imprégner des objets ordinaires avec certaines 
            imprégnations magiques. Les objets magiques créés grâce à cette capacité sont considérés comme des 
            prototypes d\'objets permanents. L\'artificier connaît 4 imprégnations (voir tableau ci-contre).',
            'Incantation' => 'La caractéristique d\'incantation de l\'artificier est l\'Intelligence (jet de 
                sauvegarde contre ses sorts DD 12, +4 au toucher pour les attaques avec un sort). Il a préparé les 
                sorts de magicien suivants :<br/><br/>
                Sorts mineurs (à volonté) : <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=lumiere">Lumière</a>, 
                <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=réparation">Réparation</a><br/>
                Niveau 1 (2 emplacements) : <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=detection-de-la-magie">Détection de la magie</a><br/>',
        ],
        'actions' => [
            'Bâton' => '<i>Attaque au corps à corps avec une arme :</i> +2 au toucher, allonge 1,50 m, une cible. 
            <i>Touché :</i> 3 (1d6) dégâts contondants, ou 4 (1d8) dégâts contondants si utilisé avec les deux mains.',
            'Arbalète légère' => '<i>Attaque à distance avec une arme :</i> +4 au toucher, portée 24/96 m, une cible. 
            <i>Touché :</i> 6 (1d8 + 2) dégâts perforants.',
        ],
    ];

    private const BARD = [
        'name' => 'Barde',
        'description' => 'Humanoïde de taille M, tout alignement, niveau 1',
        'AC' => [
            'score' => 13,
            'type' => '(armure de cuir)',
        ],
        'HP' => [
            'score' => '9',
            'dices' => '(2d8)',
        ],
        'speed' => '9 m',
        'traits' => [
            'AS' => [
                'FOR' => 10,
                'DEX' => 14,
                'CON' => 12,
                'INT' => 13,
                'WIS' => 10,
                'CHA' => 15,
            ],
            'saving' => 'Dextérité +4, Charisme +4',
            'skills' => 'Représentation +4, Arcanes +3, Perspicacité +4',
            'senses' => 'Perception passive 10',
            'languages' => 'commun, plus une de votre choix',
        ],
        'tools' => 'Trois instruments de musique de votre choix.',
        'features' => [
            'Inspiration bardique (3 charges par jour)' => 'Le barde peut inspirer les autres en maniant les mots ou la 
            musique. Pour une action bonus, choisir une créature autre que le barde (rayon de 18 mètres) et qui peut 
            l\'entendre. Cette créature gagne un dé d\'Inspiration bardique (1d6). Une fois dans les 10 minutes 
            suivantes, la créature peut lancer le dé et ajouter le nombre obtenu à un jet de caractéristique, 
            d\'attaque ou de sauvegarde qu\'elle vient de faire. La créature peut attendre de voir le résultat du jet 
            avant de décider d\'appliquer le dé, mais elle doit se décider avant que le MD ne dise si le jet est un 
            succès ou un échec. Une fois le dé d\'Inspiration bardique lancé, il est consommé. Une créature ne peut 
            avoir qu\'un seul dé d\'Inspiration bardique à la fois.',
            'Incantation' => 'La caractéristique d\'incantation du barde est le Charisme (jet de 
                sauvegarde contre ses sorts DD 12, +4 au toucher pour les attaques avec un sort). Il a préparé les 
                sorts de barde suivants :<br/><br/>
                Sorts mineurs (à volonté) : <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=moquerie-cruelle">Moquerie cruelle</a>, 
                <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=prestidigitation">Prestidigitation</a><br/>
                Niveau 1 (2 emplacements) : <a class="has-text-danger-dark link-animation" 
                href="https://www.aidedd.org/dnd/sorts.php?vf=https://www.aidedd.org/dnd/sorts.php?vf=charme-personne">
                Charme personne</a><br/>',
        ],
        'actions' => [
            'Rapière' => '<i>Attaque au corps à corps avec une arme :</i> +2 au toucher, allonge 1,50 m, une cible. 
            <i>Touché :</i> 4 (1d8) dégâts perforants.',
            'Dague' => '<i>Attaque au corps à corps avec une arme :</i> +2 au toucher, allonge 1,50 m, une cible. 
            <i>Touché :</i> 2 (1d4) dégâts perforants.',
        ],
    ];

    private const SAVAGE = [
        'name' => 'Sauvage',
        'description' => 'Humanoïde de taille M, tout alignement, niveau 1',
        'AC' => [
            'score' => 14,
            'type' => '(armure de peau)',
        ],
        'HP' => [
            'score' => '10',
            'dices' => '(2d8 + 2)',
        ],
        'speed' => '9 m',
        'traits' => [
            'AS' => [
                'FOR' => 13,
                'DEX' => 15,
                'CON' => 14,
                'INT' => 10,
                'WIS' => 12,
                'CHA' => 10,
            ],
            'saving' => 'Dextérité +4',
            'skills' => 'Survie +3, Nature +2, Acrobatie +4, Perception +3',
            'senses' => 'Perception passive 13',
            'languages' => 'commun, argot des sauvageons',
        ],
        'tools' => 'Le Sauvage maîtrise le kit d\'herboriste.',
        'features' => [
            'Sens du danger' => 'Le Sauvage a un avantage aux jets de sauvegarde de Dextérité contre les effets qu\'il 
            peut voir, comme les pièges ou les sorts. Pour bénéficier de cet effet il ne doit pas être aveuglé, assourdi 
            ou incapable d\'agir.',
            'Explorateur-né' => 'Choisissez un type de terrain favori : arctique, désert, forêt, littoral, marais, 
            montagne, plaine ou Outreterre. Lorsque le Sauvage fait un jet d’Intelligence ou de Sagesse lié à son 
            terrain favori, son bonus de maîtrise est doublé si il utilise une compétence qu\'il maîtrisez.',
        ],
        'actions' => [
            'Lance (CàC)' => '<i>Attaque au corps à corps avec une arme :</i> +3 au toucher, allonge 1,50 m, une cible. 
            <i>Touché :</i> 5 (1d6 + 2) dégâts perforants, 6 (1d8 + 2) si l\'arme est tenue à deux mains.',
            'Lance (Distance)' => '<i>Attaque à distance avec une arme :</i> +4 au toucher, portée 6/18 m, une cible. 
            <i>Touché :</i> 6 (1d8 + 2) dégâts perforants.',
            'Fronde' => '<i>Attaque à distance avec une arme :</i> +4 au toucher, portée 9/36 m, une cible. 
            <i>Touché :</i> 4 (1d4 + 2) dégâts contondants.',
        ],
    ];
}
