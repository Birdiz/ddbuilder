<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sidekick;

use App\Model\AbstractEnum;

/**
 * @method static Archetype ARTIFICER()
 */
class ExtraInfo extends AbstractEnum
{
    private const ARTIFICER = [
        'Focaliseur arcanique amélioré' => [
            'object' => 'une baguette, un bâton ou un sceptre (nécessite un lien)',
            'description' => 'Tant qu\'elle tient cet objet, une créature gagne un bonus de +1 aux jets d\'attaque avec 
            un sort. De plus, la créature ignore les abris partiels lorsqu\'elle effectue une attaque avec un sort.',
        ],
        'Défense améliorée' => [
            'object' => 'une armure ou un bouclier',
            'description' => 'Une créature reçoit un bonus de +1 à sa Classe d\'Armure lorsqu\'elle porte l\'objet 
            imprégné.',
        ],
        'Arme améliorée' => [
            'object' => 'une arme courante ou de guerre',
            'description' => 'Octroie un bonus de +1 aux jets d\'attaque et de dégâts effectués avec cette arme 
            magique.',
        ],
        'Arme de retour' => [
            'object' => 'une arme courante ou de guerre avec la propriété lancer',
            'description' => 'Octroie un bonus de +1 aux jets d\'attaque et de dégâts effectués avec cette arme magique, 
            qui retourne dans la main de son porteur immédiatement après avoir effectué une attaque à distance.',
        ],
    ];
}
