<?php

declare(strict_types=1);

namespace App\Enum\Generator\PNJ\Sidekick;

use App\Model\AbstractEnum;

/**
 * @method static Archetype ARTIFICER()
 * @method static Archetype BARD()
 * @method static Archetype SAVAGE()
 */
class LevelUp extends AbstractEnum
{
    private const ARTIFICER = [
        2 => [
            'HP' => '16 (3d8 + 3)',
            'traits' => 'Sort (niveau 1) : Soins ou Repli expéditif',
            'spells' => [
                'known_cantrips' => 2,
                'known_spells' => 2,
                'spell_slots' => [
                    1 => 2,
                    2 => '-',
                ],
            ],
        ],
        3 => [
            'HP' => '22 (4d8 + 4)',
            'traits' => 'Sort (niveau 1) : Déguisement ou Sanctuaire',
            'spells' => [
                'known_cantrips' => 2,
                'known_spells' => 3,
                'spell_slots' => [
                    1 => 2,
                    2 => '-',
                ],
            ],
        ],
        4 => [
            'HP' => '27 (5d8 + 5)',
            'traits' => [
                'Sort mineur : Poigne électrique, bouffée de poison ou trait de feu',
                'Amélioration de caractéristique : 2 points à distribuer',
            ],
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 3,
                'spell_slots' => [
                    1 => 2,
                    2 => '-',
                ],
            ],
        ],
        5 => [
            'HP' => '33 (6d8 + 6)',
            'traits' => [
                'Le bonus de maîtrise de l\'artificier augmente de 1',
                'Sort (niveau 2) : Verrou magique, restauration partielle ou arme magique',
            ],
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 4,
                'spell_slots' => [
                    1 => 3,
                    2 => 2,
                ],
            ],
        ],
        6 => [
            'HP' => '38 (7d8 + 7)',
            'traits' => 'Expertise d\'outil : le bonus de maîtrise est doublé pour tout jet de caractéristique qui 
            utilise la maîtrise d\'un outil de l\'artificier',
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 4,
                'spell_slots' => [
                    1 => 3,
                    2 => 2,
                ],
            ],
        ],
    ];

    private const BARD = [
        2 => [
            'HP' => '13 (3d8)',
            'traits' => 'Sort (niveau 1) : Murmures dissonants ou Sommeil',
            'spells' => [
                'known_cantrips' => 2,
                'known_spells' => 2,
                'spell_slots' => [
                    1 => 2,
                    2 => '-',
                ],
            ],
        ],
        3 => [
            'HP' => '18 (4d8)',
            'traits' => [
                'Sort (niveau 1) : Compréhension des langues ou Mot de guérison',
                'Sort mineur : Protection contre les armes ou Coup au but',
            ],
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 3,
                'spell_slots' => [
                    1 => 3,
                    2 => '-',
                ],
            ],
        ],
        4 => [
            'HP' => '22 (5d8)',
            'traits' => 'Amélioration de caractéristique : 2 points à distribuer',
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 3,
                'spell_slots' => [
                    1 => 4,
                    2 => '-',
                ],
            ],
        ],
        5 => [
            'HP' => '27 (6d8)',
            'traits' => [
                'Le bonus de maîtrise du barde augmente de 1',
                'Sort (niveau 2) : Silence, Fracassement ou Discours captivant',
            ],
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 4,
                'spell_slots' => [
                    1 => 4,
                    2 => 2,
                ],
            ],
        ],
        6 => [
            'HP' => '31 (7d8)',
            'traits' => [
                'Inspiration bardique : le dé passe à 1d8',
                'Sort (niveau 1) : Fou rire de Tasha',
            ],
            'spells' => [
                'known_cantrips' => 3,
                'known_spells' => 5,
                'spell_slots' => [
                    1 => 4,
                    2 => 2,
                ],
            ],
        ],
    ];

    private const SAVAGE = [
        2 => [
            'HP' => '16 (3d8 + 4)',
            'traits' => 'Vigilance primitive (2 recharges par jour) : Le Sauvage peut utiliser une action, pour 1 
            minute, il peut sentir si les types de créatures suivants sont présents à 1,5 kilomètre de lui (ou 
            jusqu\'à 9 kilomètres si il est dans son terrain favori) : aberrations, célestes, dragons, élémentaires, 
            fées, démons et morts-vivants. Cette capacité ne révèle pas l\'emplacement ou le nombre des créatures.',
        ],
        3 => [
            'HP' => '23 (4d8 + 6)',
            'traits' => 'Second souffle : Le Sauvage peut utiliser une action bonus à son tour pour regagner des points 
            de vie à raison de 1d10 + son niveau. Une fois qu\'il a utilisé cette capacité, il doit finir un repos court 
            ou long pour pouvoir l\'utiliser à nouveau.',
        ],
        4 => [
            'HP' => '29 (5d8 + 8)',
            'traits' => 'Amélioration de caractéristique : 2 points à distribuer',
        ],
        5 => [
            'HP' => '36 (6d8 + 10)',
            'traits' => 'Le bonus de maîtrise du Sauvage augmente de 1',
        ],
        6 => [
            'HP' => '42 (7d8 + 12)',
            'traits' => 'Attaque supplémentaire : il peut attaquer deux fois, chaque fois qu\'il réalise l\'action 
            Attaquer durant son tour',
        ],
    ];
}
