<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;

class Architecture extends AbstractEnum
{
    private const LONGERE = 'une longère';
    private const COLOMBAGE = 'une maison en colombages';
    private const FERME = 'une ancienne ferme';
    private const VILLA = 'une villa';
    private const BOIS = 'une maison en bois et en torchis';
    private const PIERRE = 'une maison en pierre';
    private const GOTHIQUE = 'une maison gothique';
    private const BAROQUE = 'une maison baroque';
    private const MAITRE = 'une maison de maître';
    private const BALET = 'une maison à balet';
    private const CHAUMIERE = 'une chaumière';
    private const CHALET = 'un chalet';
    private const HOTEL = 'un hôtel reconverti';
}
