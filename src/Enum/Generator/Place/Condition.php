<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;

class Condition extends AbstractEnum
{
    private const NEW = 'neuf/neuve';
    private const OLD = 'vieux/vieille';
    private const VERY_OLD = 'très ancien/ancienne';
    private const RUIN = 'quasiment une ruine';
    private const BUILDING = 'en construction';
    private const RENOVATING = 'en rénovation';
    private const DEMOLITION = 'en train d\'être démoli(e)';
    private const SALES = 'en vente';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [25, 25, 15, 5, 5, 15, 5, 5];
    }
}
