<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;

/**
 * @method static Residence RICH()
 * @method static Residence POOR()
 * @method static Residence MIDDLE()
 * @method static Residence WORKER()
 * @method static Residence ARISTOCRAT()
 * @method static Residence LAND()
 * @method static Residence OUTSIDE()
 * @method static Residence EMPTY()
 */
class District extends AbstractEnum
{
    private const RICH = 'dans un quartier riche';
    private const POOR = 'dans un quartier pauvre';
    private const MIDDLE = 'dans un quartier de classe moyenne';
    private const WORKER = 'dans un quartier travailleur';
    private const ARISTOCRAT = 'dans un quartier aristocratique';
    private const LAND = 'à la campagne';
    private const OUTSIDE = 'hors de la ville';
    private const EMPTY = 'dans un quartier abandonné';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [10, 30, 10, 25, 10, 5, 5, 5];
    }
}
