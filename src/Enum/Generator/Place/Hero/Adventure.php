<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Hero;

use App\Model\AbstractEnum;

class Adventure extends AbstractEnum
{
    private const SKILLED = 'un aventurier expérimenté';
    private const UNSKILLED = 'un aventurier expérimenté';
    private const ENTHUSIASTIC = 'quelqu\'un motivé par l\'exploration';
    private const SOLDIER = 'un soldat';
    private const PRIEST = 'un prêtre';
    private const SAGE = 'quelqu\'un de sage';
    private const REVENGE = 'quelqu\'un qui cherche à se venger';
    private const RAVING = 'quelqu\'un qui a perdu la boule';
    private const CELESTIAL = 'un allié Célèste';
    private const FEY = 'un allié Fée';
    private const MONSTER = 'un monstre déguisé';
    private const VILLAIN = 'un criminel passant pour un allié';
}
