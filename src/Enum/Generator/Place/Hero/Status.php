<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Hero;

use App\Model\AbstractEnum;

class Status extends AbstractEnum
{
    private const RETIRED = 'à la retraite';
    private const RULER = 'le/la dirigeant(e) d\'un localité';
    private const OFFICER = 'officier militaire';
    private const OFFICIAL = 'membre d\'un clergé';
    private const ELDER = 'un(e) ancien(ne) respecté(e)';
    private const FRIEND = 'un(e) vieil/vielle ami(e)';
    private const TEACHER = 'devenu(e) professeur(e)';
    private const PARENT = 'membre de la famille d\'un des joueurs';
    private const COMMONER = 'un(e) villageois(e) déprimé(e)';
    private const MERCHANT = 'un(e) marchant(e) agguerri(e)';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 10, 15, 15, 15, 5, 10, 5, 5, 5];
    }
}
