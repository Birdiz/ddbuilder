<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Name;

use App\Model\AbstractEnum;

class TavernFirstPart extends AbstractEnum
{
    private const SILVER = 'argenté(e)';
    private const GOLDEN = 'en or';
    private const STAGGERING = 'stupéfiant(e)';
    private const LAUGHING = 'rieur/rieuse';
    private const PRANCING = 'cabré(e)';
    private const GILDED = 'doré(e)';
    private const RUNNING = 'courant(e)';
    private const HOWLING = 'hurleur(e)';
    private const SLAUGHTERED = 'massacré(e)';
    private const LEERING = 'reluqueur/reluqueuse';
    private const DRUKEN = 'ivre';
    private const LEAPING = 'sauteur/sauteuse';
    private const ROARING = 'rugisseur/rugisseuse';
    private const FROWNING = 'renfrogné(e)';
    private const LONELY = 'solitaire';
    private const WANDERING = 'voyageur/voyageuse';
    private const MYSTERIOUS = 'mystérieux/mystérieuse';
    private const BARKING = 'aboyeur/aboyeuse';
    private const BLACK = 'noir(e)';
    private const GLEAMING = 'rutilant(e)';
}
