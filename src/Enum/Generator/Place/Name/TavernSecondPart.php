<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Name;

use App\Model\AbstractEnum;

class TavernSecondPart extends AbstractEnum
{
    private const EEL = 'À l\'anguille';
    private const DOLPHIN = 'Au dauphin';
    private const DWARF = 'Au nain';
    private const PEGASUS = 'Le Pégase';
    private const PONY = 'Le poney';
    private const ROSE = 'À la rose';
    private const STAG = 'Au cerf';
    private const WOLF = 'Au loup';
    private const LAMB = 'À l\'agneau';
    private const DEMON = 'Au démon';
    private const GOAT = 'À la chèvre';
    private const SPIRIT = 'À l\'esprit';
    private const HORDE = 'À la horde';
    private const JESTER = 'Au bouffon';
    private const MOUNTAIN = 'À la montagne';
    private const EAGLE = 'À l\'aigle';
    private const SATYR = 'Au Satyre';
    private const DOG = 'Au chien';
    private const SPIDER = 'À l\'araignée';
    private const STAR = 'À l\'étoile';
}
