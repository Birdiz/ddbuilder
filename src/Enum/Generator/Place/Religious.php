<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Enum\Pantheon;
use App\Model\AbstractEnum;

/**
 * Class Religious.
 *
 * @method static Religious TEMPLE_GOOD()
 * @method static Religious TEMPLE_BAD()
 * @method static Religious HOME()
 * @method static Religious SHRINE_ABANDONED()
 * @method static Religious LIBRARY()
 * @method static Religious SHRINE_HIDDEN()
 */
class Religious extends AbstractEnum
{
    private const TEMPLE_GOOD = 'Temple d\'une déité neutre ou bonne';
    private const TEMPLE_BAD = 'Temple d\'une déité mauvaise dirigé par des prêtres mauvais ou charlatans';
    private const HOME = 'Maison des ascètes';
    private const SHRINE_ABANDONED = 'Tombeau abandonné';
    private const LIBRARY = 'Bibliothèque dédiée à l\'étude théologique';
    private const SHRINE_HIDDEN = 'Tombeau caché d\'un fiélon ou d\'une déité mauvaise';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [50, 10, 5, 10, 10, 15];
    }

    /**
     * @return array<Pantheon>
     */
    public static function getGods(string $value): array
    {
        return match ($value) {
            static::TEMPLE_GOOD()->getValue() => Pantheon::getUntilThreeGods(Pantheon::getGoodAndNeutralGods()),
            static::TEMPLE_BAD()->getValue(), static::SHRINE_HIDDEN()->getValue() => Pantheon::getUntilThreeGods(
                Pantheon::getEvilGods()
            ),
            default => [],
        };
    }
}
