<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;
use Exception;

/**
 * Class Residence.
 *
 * @method static Residence SQUAT()
 * @method static Residence HOME_MIDDLE()
 * @method static Residence HOME_UPPER()
 * @method static Residence TENEMENT()
 * @method static Residence ORPHANAGE()
 * @method static Residence DEN()
 * @method static Residence FRONT()
 * @method static Residence MANSION()
 */
class Residence extends AbstractEnum
{
    private const SQUAT = 'un squat abandonné';
    private const HOME_MIDDLE = 'une maison banale';
    private const HOME_UPPER = 'une maison cossue';
    private const TENEMENT = 'un immeuble bondé';
    private const ORPHANAGE = 'un orphelinat';
    private const DEN = 'une tanière d\'esclaves cachés';
    private const FRONT = 'une cache pour un culte secret';
    private const MANSION = 'un somptueux manoir';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [10, 30, 10, 25, 10, 5, 5, 5];
    }

    /**
     * @throws Exception
     */
    public static function getHabitants(string $residenceValue): int
    {
        return match ($residenceValue) {
            static::TENEMENT()->getValue(), static::ORPHANAGE()->getValue() => random_int(20, 50),
            static::HOME_UPPER()->getValue(), static::MANSION()->getValue() => random_int(1, 7),
            default => random_int(2, 10),
        };
    }

    public static function getDistrict(string $residenceValue): string
    {
        return match ($residenceValue) {
            static::HOME_UPPER()->getValue() => District::RICH()->getValue(),
            static::MANSION()->getValue() => District::ARISTOCRAT()->getValue(),
            static::SQUAT()->getValue() => District::EMPTY()->getValue(),
            default => AbstractEnum::getRandomEnum(District::toArray()),
        };
    }
}
