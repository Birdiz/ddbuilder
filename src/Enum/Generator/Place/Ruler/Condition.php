<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Ruler;

use App\Model\AbstractEnum;

class Condition extends AbstractEnum
{
    private const HONEST = 'est honnête';
    private const THIEF = 'est un(e) voleur/voleuse';
    private const STINGY = 'est un(e) rapiat';
    private const PRICE_UPPER = 'fait des prix toujours trop haut';
    private const PRICE_LOWER = 'fait des prix toujours trop bas';
    private const RUDE = 'est malpoli(e)';
    private const NICE = 'est très gentil(le)';
    private const BLIND = 'est aveugle';
    private const DUMB = 'est muet(te)';
    private const SPEAKER = 'parle fort';
    private const SPEAK_TOO_MUCH = 'parle beaucoup trop';
}
