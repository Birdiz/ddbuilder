<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Ruler;

use App\Model\AbstractEnum;

class Status extends AbstractEnum
{
    private const RESPECTED = 'respecté(e), impartial(e) et juste';
    private const TYRANT = 'un(e) tyran craint(e)';
    private const WEAKLING = 'un(e) faible manipulé(e) par tous les autres';
    private const ILLEGITIMATE = 'un(e) dirigeant(e) illégitime, préparant une guerre civile';
    private const RULED = 'conseillé(e) ou contrôlé(e) par un puissant monstre';
    private const MYSTERIOUS = 'mystérieux/mystérieuse, tel(le) une cabale anonyme';
    private const CONTESTED = 'contesté(e), souvent ouvert(e) aux combats';
    private const CABAL = 'une cabale qui a pris le pouvoir ouvertement';
    private const DOLSTISH = 'un(e) nigaud(e) comme on en trouve pas d\'autres';
    private const ON_DEATHBED = 'sur son lit de mort, pour lequel/laquelle les candidats sont en compétion pour le 
    pouvoir';
    private const IRON_WILLED = 'strict(e), dirige avec une main de fer mais respecté';
    private const RELIGIOUS = 'un(e) leader/leadeuse religieux/religieuse';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [25, 15, 5, 5, 5, 5, 5, 5, 5, 5, 10, 10];
    }
}
