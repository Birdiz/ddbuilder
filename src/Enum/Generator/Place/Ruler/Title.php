<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Ruler;

use App\Enum\Generator\Place\Settlement;
use App\Model\AbstractEnum;

/**
 * Class Title.
 *
 * @method static Title EMPEROR()
 * @method static Title KING()
 * @method static Title DUKE()
 * @method static Title PRINCE()
 * @method static Title MARQUESS()
 * @method static Title COUNT()
 * @method static Title VISCOUNT()
 * @method static Title BARON()
 * @method static Title BARONET()
 * @method static Title KNIGHT()
 * @method static Title COMMONER()
 */
class Title extends AbstractEnum
{
    private const EMPEROR = 'Empereur/Impératrice';
    private const KING = 'Roi/Reine';
    private const DUKE = 'Duc/Duchesse';
    private const PRINCE = 'Duc/Duchesse';
    private const MARQUESS = 'Marquis(e)';
    private const COUNT = 'Comte/Comtesse';
    private const VISCOUNT = 'Vicomte/Vicomtesse';
    private const BARON = 'Baron/Baronesse';
    private const BARONET = 'Baronnet/Baronnette';
    private const KNIGHT = 'Chevalier/Chevalière';
    private const COMMONER = 'Roturier/Roturière';

    public static function getTitleForSettlement(string $settlementKey): string
    {
        return match ($settlementKey) {
            Settlement::TOWN()->getKey() => static::getRandomEnum(self::getTownTitles()),
            Settlement::CITY()->getKey() => static::getRandomEnum(self::getCityTitles()),
            default => static::getRandomEnum(self::getVillageTitles()),
        };
    }

    /**
     * @return array<string>
     */
    private static function getTownTitles(): array
    {
        return [
            static::BARONET()->getValue(),
            static::BARON()->getValue(),
            static::VISCOUNT()->getValue(),
            static::COUNT()->getValue(),
            static::MARQUESS()->getValue(),
        ];
    }

    /**
     * @return array<string>
     */
    private static function getCityTitles(): array
    {
        return [
            static::COUNT()->getValue(),
            static::MARQUESS()->getValue(),
            static::PRINCE()->getValue(),
            static::DUKE()->getValue(),
            static::KING()->getValue(),
            static::EMPEROR()->getValue(),
        ];
    }

    /**
     * @return array<string>
     */
    private static function getVillageTitles(): array
    {
        return [
            static::COMMONER()->getValue(),
            static::KNIGHT()->getValue(),
            static::BARONET()->getValue(),
            static::BARON()->getValue(),
        ];
    }
}
