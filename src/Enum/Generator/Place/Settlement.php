<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Enum\Generator\Place\Settlement\Army;
use App\Enum\Generator\Place\Settlement\ArmyCondition;
use App\Enum\Generator\Place\Settlement\DoorCondition;
use App\Enum\Generator\Place\Settlement\Merchandise;
use App\Enum\Generator\Place\Settlement\Wall;
use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;
use App\Util\ArrayTrait;
use App\Util\WeightedRandTrait;

/**
 * Class Settlement.
 *
 * @method static Settlement VILLAGE()
 * @method static Settlement TOWN()
 * @method static Settlement CITY()
 */
class Settlement extends AbstractEnum
{
    use WeightedRandTrait;
    use ArrayRandTrait;
    use ArrayTrait;

    private const VILLAGE = 'Village';
    private const TOWN = 'Bourg';
    private const CITY = 'Ville';

    public static function getValueFromKey(string $key): string
    {
        return static::toArray()[$key];
    }

    public static function getPopulationFromSettlement(string $settlementKey): int
    {
        return match ($settlementKey) {
            static::TOWN()->getKey() => random_int(1001, 6000),
            static::CITY()->getKey() => random_int(6001, 25000),
            default => random_int(10, 1000),
        };
    }

    /**
     * @return array<string, int>
     */
    public static function getRacesFromPopulation(int $population): array
    {
        return static::distributePopulation(
            array_column(Race::toArray(), 'name'),
            $population,
            self::getRacesWeights()
        );
    }

    public static function getHouses(int $population): int
    {
        // Return approximative houses' number considering 1 to 20 habitants/house.
        return (int) floor($population / random_int(1, 20));
    }

    /**
     * @param array<mixed> $enums
     *
     * @return array<mixed>
     */
    public static function getBuildings(
        int $stack,
        array $enums,
        int $population,
        int $populationLimit = 250
    ): array {
        // Considering 1 building for $populationLimit habitants.
        $nbBuildings = ($nbBuildings = (int) floor($population / $populationLimit)) > 0 ? $nbBuildings : 1;

        return $population <= $populationLimit
            ? []
            : static::getMultipleItems(
                $stack,
                $nbBuildings > ($totalEnums = count($enums)) ? $totalEnums : $nbBuildings,
                $enums
            );
    }

    /**
     * @return array<Shop>
     */
    public static function getShops(string $settlementKey, int $population): array
    {
        $stack = match ($settlementKey) {
            static::TOWN()->getKey() => random_int(2, 4),
            static::CITY()->getKey() => random_int(3, 5),
            default => random_int(2, 3),
        };

        return static::getStackedArray(self::getBuildings($stack, Shop::toArray(), $population));
    }

    /**
     * Return a capital in PO.
     */
    public static function getCapital(string $settlementKey, int $population): int
    {
        $capitalPerHabitant = match ($settlementKey) {
            static::TOWN()->getKey() => random_int(50, 100),
            static::CITY()->getKey() => random_int(100, 200),
            default => random_int(10, 50),
        };

        return $population * $capitalPerHabitant;
    }

    /**
     * @return array<Merchandise>
     */
    public static function getMerchandises(string $settlementKey): array
    {
        return match ($settlementKey) {
            static::TOWN()->getKey() => static::getXSimpleItems(random_int(6, 10), Merchandise::toArray()),
            static::CITY()->getKey() => static::getXSimpleItems(random_int(11, 25), Merchandise::toArray()),
            default => static::getXSimpleItems(random_int(3, 5), Merchandise::toArray()),
        };
    }

    /**
     * @return array<string, mixed>
     */
    public static function getProtections(int $population): array
    {
        return [
            'soldiers' => Army::getDefensersForSettlement($population),
            'soldierOrganisation' => static::getRandomizedValue(Army::toArray(), Army::getWeights()),
            'soldierCondition' => ArmyCondition::getRandomEnum(ArmyCondition::toArray()),
            'doors' => random_int(2, 10),
            'doorConditions' => DoorCondition::getRandomEnum(DoorCondition::toArray()),
            'walls' => Wall::getRandomEnum(Wall::toArray()),
        ];
    }

    /**
     * @return array<int>
     */
    private static function getRacesWeights(): array
    {
        return [15, 5, 5, 5, 2, 15, 10, 70, 10, 10, 5, 15, 10, 5];
    }
}
