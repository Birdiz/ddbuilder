<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Contract\WeightedInterface;
use App\Model\AbstractEnum;
use App\Util\PercentageTrait;

class Army extends AbstractEnum implements WeightedInterface
{
    use PercentageTrait;

    private const MILITIA = 'miliciens';
    private const ARMY = 'soldats';
    private const MERCENARY = 'mercenaires';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [60, 35, 5];
    }

    public static function getDefensersForSettlement(int $population): float|int
    {
        return static::percentage($population, random_int(5, 15));
    }
}
