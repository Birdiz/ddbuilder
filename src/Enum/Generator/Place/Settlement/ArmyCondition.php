<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class ArmyCondition extends AbstractEnum
{
    private const ORGANISED = 'organisés et correctement armés, s\'entraînant de temps à autres';
    private const ORGANISED_LAZY = 'organisés et correctement armés, s\'entraînants de temps à autres mais parfaitement 
    fénéants';
    private const UNORGANISED = 'désorganisés, laissés à eux-même et leur bouteille';
    private const TRAINED = 'entraînés quotidiennement, sans pour autant être parfaitement armés';
    private const TRAINED_EQUIPED = 'entraînés quotidiennement et armés jusqu\'aux dents';
    private const AGGRESIVE = 'aggressifs';
    private const FOREIGNER = 'd\'une autre race';
}
