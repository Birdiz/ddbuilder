<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class Calamity extends AbstractEnum
{
    private const VAMPIRE = 'une invasion de vampires suspectée';
    private const CULT = 'un nouveau culte qui cherche des convertis';
    private const FIGURE = 'une figure importante qui est morte (meurtrier suspecté)';
    private const WAR = 'une guerre entre des guildes de voleurs rivales';
    private const PLAGUE = 'une pandémie ou la famine';
    private const CORRUPTION = 'la corruption de ses réprésentants';
    private const MONSTERS = 'des monstres en maraudage';
    private const WIZARD = 'un puissant mage qui a emménagé en ville';
    private const DEPRESSION = 'une répression économique';
    private const FLOODING = 'des innondations';
    private const UNDEAD = 'les morts qui remuent dans le cimetière';
    private const PROPHECY = 'une prophétie de fin du monde';
    private const BRINK_WAR = 'une guerre risque d\'éclater';
    private const STRIFE = 'un conflit interne qui mène peu à peu au chaos';
    private const ENEMIES = 'un siège par des ennemis';
    private const SCANDAL = 'un scandal qui menace les familles les plus puissantes';
    private const DUNGEON = 'un dongeon qui a été découvert et les aventuriers arrivent en masse';
    private const RELIGIOUS = 'une secte qui se bat pour le pouvoir';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 10, 5, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
