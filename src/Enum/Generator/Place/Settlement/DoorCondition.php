<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class DoorCondition extends AbstractEnum
{
    private const GUARDED = 'lourdement gardées et surveillées';
    private const UNGUARDED = 'malheureusment sans garde et l\'air y passe autant que les malfrats';
    private const PATROL = 'surveillées par une patrouille qui fait son tour de ronde';
    private const CORRUPTION = 'gardées par des gardes corrompus';
    private const NORMAL = 'gardées par quelques gardes';
}
