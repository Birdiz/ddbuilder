<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Enum\Generator\Place\Settlement;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

class Feature extends AbstractEnum
{
    use ArrayRandTrait;

    private const CANALS = 'des canaux sont à la place des rues.';
    private const MONUMENT = 'une statue ou un monument massif';
    private const TEMPLE = 'un grand temple';
    private const FORTRESS = 'une forteresse imposante';
    private const VERDANT = 'des parcs verdoyants ou un verger';
    private const RIVERS = 'des rivières divisent la ville';
    private const TRADE = 'un centre de commerce important';
    private const HEADQUARTERS = 'un quartier-général d\'un famille ou d\'une guilde puissante';
    private const WEALTHY = 'la population est relativement riche';
    private const DESTITUTE = 'le fantôme d\'un(e) déshérité(e)';
    private const SMELL = 'une odeur d\'une puanteur terrible (tanneries, égoûts, ...)';
    private const SPECIFIC_GOOD = 'un centre d\'un commerce d\'une ressource particulière';
    private const BATTLES = 'le site de multiples combats';
    private const MYTHIC = 'le site d\'un événement magique ou mythique';
    private const LIBRARY = 'une importante bibliothèque ou un centre d\'archives';
    private const BANNED_GODS = 'le culte de tous les dieux bannis';
    private const REPUTATION = 'une réputation sinistre';
    private const ACADEMY = 'une bibliothèque ou une académie notable';
    private const TOMB = 'un site important d\'une tombe ou d\'un cimetière';
    private const RUINS = 'les bâtiments ont été construits sur d\'anciennes ruines';

    public static function getFeatureForSettlement(string $settlementKey): mixed
    {
        return match ($settlementKey) {
            Settlement::TOWN()->getKey() => static::getXSimpleItems(random_int(1, 3), static::toArray()),
            Settlement::CITY()->getKey() => static::getXSimpleItems(random_int(2, 5), static::toArray()),
            default => static::getRandomEnum(static::toArray()),
        };
    }
}
