<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class KnownFor extends AbstractEnum
{
    private const CUISINE = 'sa cuisine délicieuse';
    private const RUDE = 'ses habitants malpolis';
    private const GREEDY = 'ses marchands cupides';
    private const ARTISTS = 'ses artistes et ses écrivains';
    private const HERO = 'son héro/sauveur';
    private const FLOWERS = 'ses fleurs';
    private const HORDES = 'ses hordes de mendiants';
    private const WARRIORS = 'ses rudes guerriers';
    private const MAGIC = 'sa magie noire';
    private const DECADANCE = 'sa décadence';
    private const PIETY = 'sa piété';
    private const GAMBLING = 'ses jeux d\'argent';
    private const GODLESSNESS = 'son absence de dieux';
    private const EDUCATION = 'son éducation';
    private const WINES = 'ses vins';
    private const FASHION = 'sa mode';
    private const POLITIC = 'son intrique politique';
    private const GUILDS = 'ses guildes puissantes';
    private const DRINKS = 'ses boissons fortes';
    private const PATRIOTISM = 'son patriotisme';
}
