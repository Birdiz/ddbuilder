<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class Merchandise extends AbstractEnum
{
    private const PORC = 'du porc';
    private const WINE = 'du vin';
    private const IVORY = 'de l\'ivoire';
    private const PRECIOUS_WOOD = 'du bois précieux';
    private const PERFUM = 'du parfum';
    private const CLOTHES = 'des vêtements d\'apparat';
    private const VEGETABLES = 'des légumes';
    private const GLAS = 'du verre';
    private const COFFEE = 'du café';
    private const BOOKS = 'des livres';
    private const CHEESE = 'du fromage';
    private const WEAPONS = 'des armes de qualité supérieure';
    private const JEWELERY = 'des bijoux';
    private const IRON = 'du fer';
    private const BEEF = 'du boeuf';
    private const CARAVAN_COMMODITY = 'des fournitures de caravanes';
    private const FURR = 'des fourrures';
    private const GEM = 'des gemmes';
    private const LEATHER = 'des produits en cuir';
    private const TEE = 'du thé';
    private const CATTLE = 'du bétail';
    private const ART = 'des objets d\'art';
    private const SILK = 'de la soie';
    private const INCENT = 'de l\'encens';
    private const CARPET = 'des tapis';
    private const BEAD = 'des perles';
    private const BEER = 'de la bière';
    private const HORSE = 'des chevaux';
}
