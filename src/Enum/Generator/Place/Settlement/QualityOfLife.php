<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Enum\Generator\Place\Settlement;
use App\Model\AbstractEnum;
use App\Util\PercentageTrait;
use Exception;

/**
 * Class QualityOfLife.
 *
 * @method static QualityOfLife LOW()
 * @method static QualityOfLife MEDIUM()
 * @method static QualityOfLife HIGH()
 */
class QualityOfLife extends AbstractEnum
{
    use PercentageTrait;

    private const LOW = 'faible';
    private const MEDIUM = 'moyenne';
    private const HIGH = 'élevée';

    /**
     * @throws Exception
     */
    public static function getQualityOfLifeForSettlement(string $settlementKey, int $capital, int $population): string
    {
        // Average factors depending on getCapital random_int parameter.
        $basis = match ($settlementKey) {
            Settlement::TOWN()->getKey() => $population * 75,
            Settlement::CITY()->getKey() => $population * 100,
            default => $population * 30,
        };

        $delta = static::percentage($basis, 20);
        if ($capital < $basis - $delta) {
            return static::LOW()->getValue();
        }
        if ($capital > $basis + $delta) {
            return static::HIGH()->getValue();
        }

        return static::MEDIUM()->getValue();
    }
}
