<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class RaceRelation extends AbstractEnum
{
    private const HARMONY = 'l\'harmonie entre toutes et tous';
    private const TENSION = 'par contre une tension ou une rivalité entre races';
    private const CONQUERORS = 'la race majoritaire conquérante';
    private const MINORITY_RULERS = 'la race minoritaire dirigeante';
    private const MINORITY_REFUGEES = 'les races minoritaires réfugiées, accueillies ici tant bien que mal';
    private const MAJORITY_OPPRESSES = 'la race majoritaire qui oppresse les races minoritaires';
    private const MINORITY_OPPRESSES = 'la race minoritaire qui oppresse les races majoritaires';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [50, 20, 10, 5, 5, 5, 5];
    }
}
