<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place\Settlement;

use App\Model\AbstractEnum;

class Wall extends AbstractEnum
{
    private const WOOD = 'en bois';
    private const WOOD_FORTIFIED = 'en bois fortifiés';
    private const STONE = 'en pierre';
    private const STONE_FORTIFIED = 'en pierre fortifiés';
    private const WOOD_STONE = 'en bois et pierre';
    private const WOOD_STONE_FORTIFIED = 'en bois et pierre fortifiés';
}
