<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;

class Shop extends AbstractEnum
{
    private const PAWNSHOP = 'Prêteur sur gage';
    private const HERBALIST = 'Apothicaire';
    private const GROCERY = 'Épicerie';
    private const DISPENSARY = 'Dispensaire';
    private const FRUITS_VEGETABLES = 'Vendeur de fruits et légumes';
    private const DRIED_MEATS = 'Vendeur de viandes séchées';
    private const POTTERY = 'Céramiste';
    private const UNDERTAKER = 'Pompes funèbres';
    private const BOOKS = 'Librairie';
    private const MONEY_LENDER = 'Prêteur';
    private const WEAPONS_ARMORS = 'Vendeur d\'armes et/ou d\'armures';
    private const CHANDLER = 'Vendeur de cires et de bougies';
    private const BUTCHER = 'Boucher';
    private const SMITHY = 'Forgeron';
    private const BLACKSMITH = 'Maréchal-ferrant';
    private const CARPENTER = 'Charpentier';
    private const WOODWORK = 'Menuisier';
    private const WEAVER = 'Tisserand';
    private const JEWELER = 'Bijoutier';
    private const BAKER = 'Boulanger';
    private const MAPMAKER = 'Cartographe';
    private const TAILOR = 'Couturier';
    private const ROPEMAKER = 'Fabricant de cordages';
    private const MASON = 'Maçon';
    private const SCRIBE = 'Scribe';
    private const MAGICSTALL = 'Échoppe de magie';
    private const CLOGGMAKER = 'Boquetier';
    private const COOPER = 'Tonnelier';
    private const WHEELWRIGHT = 'Charron';
    private const SHOEMAKER = 'Cordonnier';
    private const FISHMONGER = 'Poissonnier';
    private const NOTARY = 'Notaire';
    private const BASKETMAKER = 'Vannier';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [];
    }
}
