<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Enum\Generator\Place\Name\TavernFirstPart;
use App\Enum\Generator\Place\Name\TavernSecondPart;
use App\Model\AbstractEnum;

class Tavern extends AbstractEnum
{
    private const BAR = 'est un bar calme et discret';
    private const DIVE = 'est un tripot bruyant';
    private const HANGOUT = 'est une niche d\'une guilde de voleur';
    private const GATHERING_PLACE = 'est le lieu de rencontre d\'une société secrète';
    private const CLUB = 'est un club dînatoire de classe supérieure';
    private const DEN = 'est le repaire à jeux d\'argent';
    private const CATERS = 's\'adresse à une race ou une guilde spécifique';
    private const MEMBERS_ONLY = 'n\'accepte que les membres du club uniquement';
    private const BROTHEL = 'est un bordel';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [25, 20, 5, 5, 10, 10, 10, 5, 10];
    }

    public static function getName(): string
    {
        return TavernFirstPart::getRandomEnum(TavernSecondPart::toArray())
            .' '.
            TavernSecondPart::getRandomEnum(TavernFirstPart::toArray())
            ;
    }
}
