<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Model\AbstractEnum;

/**
 * Class Type.
 *
 * @method static Type VILLAGE()
 * @method static Type TOWN()
 * @method static Type CITY()
 * @method static Type SETTLEMENT()
 * @method static Type RESIDENCE()
 * @method static Type RELIGIOUS()
 * @method static Type TAVERN()
 * @method static Type WAREHOUSE()
 * @method static Type SHOP()
 */
class Type extends AbstractEnum
{
    private const VILLAGE = 'Village';
    private const TOWN = 'Bourg';
    private const CITY = 'Ville';
    private const RESIDENCE = 'Résidence';
    private const RELIGIOUS = 'Religieux';
    private const TAVERN = 'Auberge';
    private const WAREHOUSE = 'Entrepôt';
    private const SHOP = 'Boutique';
}
