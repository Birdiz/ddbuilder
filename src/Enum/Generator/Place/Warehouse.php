<?php

declare(strict_types=1);

namespace App\Enum\Generator\Place;

use App\Enum\Generator\Place\Settlement\Merchandise;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

class Warehouse extends AbstractEnum
{
    use ArrayRandTrait;

    private const EMPTY = 'Vide ou abandonné';
    private const RICH = 'Biens riches';
    private const CHEAP = 'Biens bon marché';
    private const BULK = 'Biens en masse';
    private const ANIMALS = 'Animaux vivants';
    private const WEAPONS = 'Armes et/ou armures';
    private const EXOTIC = 'Biens provenant de l\'étranger';
    private const DEN = 'Repaire secret de contrebande';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [20, 10, 20, 20, 5, 10, 10, 5];
    }

    /**
     * @param array<Merchandise> $merchandises
     *
     * @return array<mixed>
     */
    public static function getWarehousesForSettlement(array $merchandises): array
    {
        // Considering 1 warehouse can handle 3 merchandises.
        return static::getMultipleItems(random_int(1, (int) floor(count($merchandises) / 3)), 1, static::toArray());
    }
}
