<?php

declare(strict_types=1);

namespace App\Enum\Generator\Travel\Encounter;

use App\Model\AbstractEnum;

class Merchant extends AbstractEnum
{
    private const ARMOR_MERCHANT = 'une caravane de marchands vendant des armures';
    private const WEAPONS_MERCHANT = 'une caravane de marchands vendant des armes';
    private const UTILITIES_MERCHANT = 'une caravane de marchands vendant du matériel d\'aventurier';
    private const ARMOR_WEAPONS_MERCHANT = 'une caravane de marchands vendant des armes et des armures';
    private const SPICES_MERCHANT = 'une caravane de marchands vendant des épices';
    private const MAGICAL_BASIC_MERCHANT = 'une caravane de marchands vendant des objets magiques rudimentaires';
    private const MAGICAL_MERCHANT = 'une caravane de marchands vendant des objets magiques';
    private const WEIRD_MERCHANT = 'une caravane de marchands vendant des objets étranges';
    private const GUARDED_MERCHANT = 'une caravane de marchands à la cargaison guardée';
    private const WOUNDED_MERCHANT = 'une caravane de marchands blessés';
    private const FOOD_MERCHANT = 'une caravane de marchands vendant des denrées alimentaires';
    private const EMPTY_MERCHANT = 'une caravane de marchands avec une cargaison à vide';
    private const DISTRESS_MERCHANT = 'une caravane de marchands faisant appel à l\'aide';
}
