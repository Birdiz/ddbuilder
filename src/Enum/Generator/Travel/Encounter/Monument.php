<?php

namespace App\Enum\Generator\Travel\Encounter;

use App\Model\AbstractEnum;

class Monument extends AbstractEnum
{
    private const SEALED_BURIAL_MOUND = 'un tertre scellé';
    private const SEALED_BURIAL_MOUND_VEGETATION = 'un tertre scellé recouvert de végétation';
    private const SEALED_BURIAL_MOUND_GUARDED = 'un tertre scellé gardé par des squelettes';
    private const SEALED_BURIAL_MOUND_PROTECTED = 'un tertre scellé gardé par une tribu barbare';
    private const SEALED_BURIAL_PYRAMID = 'une pyramide scellée';
    private const SEALED_BURIAL_PYRAMID_VEGETATION = 'une pyramide scellée recouverte de végétation';
    private const SEALED_BURIAL_PYRAMID_GUARDED = 'une pyramide scellée gardée par des scorpions géants';
    private const SEALED_BURIAL_PYRAMID_PROTECTED = 'une pyramide scellée gardée par une tribu d\'hommes-lézards';
    private const PLUDERED_BURIAL_MOUND = 'un tertre pillé';
    private const PLUDERED_BURIAL_MOUND_VEGETATION = 'un tertre pillé recouvert de végétation';
    private const PLUDERED_BURIAL_PYRAMID = 'une pyramide pillée';
    private const PLUDERED_BURIAL_PYRAMID_VEGETATION = 'une pyramide pillée recouverte de végétation';
    private const FACES_CARVED = 'des visages sculptés dans la roche';
    private const GIANT_STATUES_CARVED = 'des statues géantes sculptées dans la roche';
    private const GIANT_STATUES_CARVED_GUADED = 'des statues géantes sculptées dans la roche gardées par une tribu 
    gnome';
    private const GIANT_STATUES_CARVED_ANCIENT = 'des statues géantes sculptées dans la roche représentant un peuple de 
    l\'ancien monde';
    private const OBELISK_WARNING = 'un obélisque intact gravé d\'un message d\'avertisement';
    private const OBELISK_DEATH = 'un obélisque intact gravé d\'un message de mort';
    private const OBELISK_LORE_HISTORY = 'un obélisque intact gravé d\'un passage historique';
    private const OBELISK_LORE_RELIGIOUS = 'un obélisque intact gravé d\'un passage religieux';
    private const OBELISK_LORE_MYSTIQUE = 'un obélisque intact gravé d\'un passage mystique';
    private const OBELISK_LORE_LANGUAGE = 'un obélisque intact gravé d\'un passage en langue inconnue';
    private const OBELISK_DEDICATION = 'un obélisque intact dédié à quelqu\'un';
    private const OBELISK_DEDICATION_KING = 'un obélisque intact dédié à un roi des temps anciens';
    private const OBELISK_RELIGIOUS = 'un obélisque intact gravé d\'une iconographie religieuse';
    private const OBELISK_RUINE = 'un obélisque en ruine';
    private const OBELISK_TOPPLED = 'un obélisque renversé';
    private const STATUE_PERSON = 'une statue représentant une personne';
    private const STATUE_KNIGHT = 'une statue représentant un chevalier';
    private const STATUE_DEITY = 'une statue représentant une divinité';
    private const STATUE_FIEND = 'une statue représentant un fiélon';
    private const STATUE_PERSON_RUINED = 'une statue en ruine représentant une personne';
    private const STATUE_KNIGHT_RUINED = 'une statue en ruine représentant un chevalier';
    private const STATUE_DEITY_RUINED = 'une statue en ruine représentant une divinité';
    private const STATUE_FIEND_RUINED = 'une statue en ruine représentant un fiélon';
    private const STATUE_PERSON_TOPPLED = 'une statue renversée représentant une personne';
    private const STATUE_KNIGHT_TOPPLED = 'une statue renversée représentant un chevalier';
    private const STATUE_DEITY_TOPPLED = 'une statue renversée représentant une divinité';
    private const STATUE_FIEND_TOPPLED = 'une statue renversée représentant un fiélon';
    private const GREAT_WALL = 'une muraille intacte avec des tours de garde espacées d\'un kilomètre entre chaque';
    private const GREAT_WALL_RUINED = 'une muraille en ruine';
    private const GREAT_WALL_RUINED_ANCIENT = 'une muraille en ruine datant des anciens temps';
    private const GREAT_WALL_ARCH = 'une arche dans une muraille';
    private const GREAT_WALL_ARCH_ELVES = 'une arche elfique dans une muraille';
    private const FOUNTAIN = 'une fontaine';
    private const FOUNTAIN_SPOILED = 'une fontaine rejetant de l\'eau croupie';
    private const FOUNTAIN_BLOOD = 'une fontaine rejetant du sang';
    private const STANDING_STONES = 'un cercle parfait de pierres dressées';
    private const STANDING_STONES_GUARDED = 'un cercle parfait de pierres dressées gardé par une tribu d\'elfes';
    private const STANDING_STONES_RUINED = 'un cercle de pierres dressées en ruine';
    private const STANDING_STONES_TOPPLED = 'un cercle de pierres renversées';
    private const TOTEM = 'un totem';
    private const TOTEM_RELIGIOUS = 'un totem dédié à une divinité';
    private const TOTEM_EVIL = 'un totem dédié à un félon';
}
