<?php

namespace App\Enum\Generator\Travel\Encounter;

use App\Model\AbstractEnum;

class WeirdLocale extends AbstractEnum
{
    private const DEAD_MAGIC_ZONE = 'une zone morte de magie';
    private const WILD_MAGIC_ZONE = 'une zone de magie instable';
    private const TALKING_STONES = 'des rochers sculptés en forme de visages qui parlent';
    private const TALKING_PAINTED_STONES = 'des rochers sculptés et paints en forme de visages qui parlent';
    private const MYSTICAL_CAVE = 'une cave crytsalline mystique qui répond aux questions';
    private const ANCIENT_TREE = 'un viel arbre avec un esprit scellé en lui';
    private const ANCIENT_TREE_PROTECTED = 'un viel arbre avec un esprit scellé en lui protégé par des Boiseux';
    private const BATTLEFIELD_FOG = 'un ancien champ de bataille où un brouillard se lève parfois en formes humanoïdes';
    private const PORTAL = 'un portail permanent vers un autre plan';
    private const WILD_PORTAL = 'un portail instable vers un autre plan';
    private const WISHING_WELL = 'un puit à souhaits';
    private const HEAL_WELL = 'une fontaine mystique';
    private const GIANT_CRYSTAL = 'un immense éclat de cristal jaillissant du sol';
    private const GIANT_CRYSTAL_PROTECTED = 'un immense éclat de cristal jaillissant du sol protégé par des 
    élémentaires';
    private const WRECKED_SHIP = 'un bâteau échoué en pleine terre';
    private const WRECKED_SHIP_CHEST = 'un bâteau échoué en pleine terre avec un coffre';
    private const WRECKED_SHIP_CORPSES = 'un bâteau échoué en pleine terre avec des corps étendus à côté';
    private const HAUNTED_HILL = 'une colline hantée';
    private const HAUNTED_HILL_HOUSE = 'une colline avec une maison dite hantée';
    private const HAUNTED_HILL_HOUSE_GRAVEYARD = 'une colline avec une maison et un cimetière dits hantés';
    private const RIVER_FERRY = 'un traversier au capitaine squelettique sur une large rivière';
    private const PETRIFIED_SOLDIERS = 'un champ de soldats pétrifiés';
    private const PETRIFIED_FOREST = 'une forêt d\'arbres pétrifiés';
    private const AWAKENED_FOREST = 'une forêt d\'arbres animés';
    private const CANYON_DRAGON = 'un canyon contenant un cimetière de dragons';
    private const CANYON_ABOLETH = 'un canyon contenant un cimetière d\'aboleths';
    private const CANYON_ORC = 'un canyon contenant un cimetière d\'orcs';
    private const FLOATING_TOWER = 'une tour flottante sur un morceau de roche en lévitation';
    private const FLOATING_TOWER_WIZZARD = 'une tour de magicien flottante sur un morceau de roche en lévitation';
}
