<?php

namespace App\Enum\Generator\Travel;

use App\Model\AbstractNamedEnum;

/**
 * @method static Pace SLOW()
 * @method static Pace NORMAL()
 * @method static Pace FAST()
 */
class Pace extends AbstractNamedEnum
{
    private const SLOW = [
        'name' => 'Lent',
        'description' => 'Les déplacements lents sont préférés lors d\'un transport de cargaison, pour éviter les 
        dangers ou si la capacité de déplacement est réduite.',
        'pace' => 29,
        'danger_multiplier' => 0.5,
        'foraging_bonus' => 2,
        'lost_probability' => 5,
        'lost_day_multiplier' => 1,
    ];

    private const NORMAL = [
        'name' => 'Normal',
        'description' => 'Les déplacements normaux sont le lot de tous les voyageurs voulant atteindre leur destination
        sans fatigue mais de bon train.',
        'pace' => 38,
        'danger_multiplier' => 1,
        'foraging_bonus' => 1,
        'lost_probability' => 20,
        'lost_day_multiplier' => 2,
    ];

    private const FAST = [
        'name' => 'Rapide',
        'description' => 'Les déplacements rapide permettent d\'atteindre de longues distances rapidement au détriment 
        de la sécurité ou des rations...',
        'pace' => 48,
        'danger_multiplier' => 2,
        'foraging_bonus' => 0,
        'lost_probability' => 30,
        'lost_day_multiplier' => 3,
    ];

    public static function getRandomDelta(): int
    {
        $random = random_int(0, 10);

        if (6 >= $random) {
            return 0;
        }

        return 9 <= $random ? random_int(0, 10) : -random_int(0, 10);
    }

    public static function getPaceEffect(int $pace, int $km): string
    {
        return match (true) {
            $pace > $km => 'Les joueurs ont eu des difficultés aujourd\'hui.',
            $pace < $km => 'Les joueurs ont donné un coup de collier pour avancer un peu plus !',
            default => 'Les joueurs avancent à leur allure, sans motivation ni contrainte.'
        };
    }
}
