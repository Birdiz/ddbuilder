<?php

declare(strict_types=1);

namespace App\Enum;

use App\Model\AbstractEnum;

/**
 * Class Hoard.
 *
 * @method static Hoard NOTHING()
 * @method static Hoard ARTT1()
 * @method static Hoard ARTT2()
 * @method static Hoard ARTT3()
 * @method static Hoard ARTT4()
 * @method static Hoard ARTT5()
 * @method static Hoard GEMT1()
 * @method static Hoard GEMT2()
 * @method static Hoard GEMT3()
 * @method static Hoard GEMT4()
 * @method static Hoard GEMT5()
 * @method static Hoard GEMT6()
 * @method static Hoard TABLEA()
 * @method static Hoard TABLEB()
 * @method static Hoard TABLEC()
 * @method static Hoard TABLED()
 * @method static Hoard TABLEE()
 * @method static Hoard TABLEF()
 * @method static Hoard TABLEG()
 * @method static Hoard TABLEH()
 * @method static Hoard TABLEI()
 */
class Hoard extends AbstractEnum
{
    private const NOTHING = 'NOTHING';

    private const ARTT1 = 'ARTT1';
    private const ARTT2 = 'ARTT2';
    private const ARTT3 = 'ARTT3';
    private const ARTT4 = 'ARTT4';
    private const ARTT5 = 'ARTT5';

    private const GEMT1 = 'GEMT1';
    private const GEMT2 = 'GEMT2';
    private const GEMT3 = 'GEMT3';
    private const GEMT4 = 'GEMT4';
    private const GEMT5 = 'GEMT5';
    private const GEMT6 = 'GEMT6';

    private const TABLEA = 'TABLEA';
    private const TABLEB = 'TABLEB';
    private const TABLEC = 'TABLEC';
    private const TABLED = 'TABLED';
    private const TABLEE = 'TABLEE';
    private const TABLEF = 'TABLEF';
    private const TABLEG = 'TABLEG';
    private const TABLEH = 'TABLEH';
    private const TABLEI = 'TABLEI';
}
