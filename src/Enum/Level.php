<?php

declare(strict_types=1);

namespace App\Enum;

use App\Model\AbstractEnum;

/**
 * Class Level.
 *
 * @method static Level ONE()
 * @method static Level TWO()
 * @method static Level THREE()
 * @method static Level FOUR()
 * @method static Level FIVE()
 * @method static Level SIX()
 * @method static Level SEVEN()
 * @method static Level EIGHT()
 * @method static Level NINE()
 * @method static Level TEN()
 * @method static Level ELEVEN()
 * @method static Level TWELVE()
 * @method static Level THIRTEEN()
 * @method static Level FOURTEEN()
 * @method static Level FIFTEEN()
 * @method static Level SIXTEEN()
 * @method static Level SEVENTEEN()
 * @method static Level EIGHTTEEN()
 * @method static Level NINETEEN()
 * @method static Level TWENTY()
 */
class Level extends AbstractEnum
{
    private const ONE = 1;
    private const TWO = 2;
    private const THREE = 3;
    private const FOUR = 4;
    private const FIVE = 5;
    private const SIX = 6;
    private const SEVEN = 7;
    private const EIGHT = 8;
    private const NINE = 9;
    private const TEN = 10;
    private const ELEVEN = 11;
    private const TWELVE = 12;
    private const THIRTEEN = 13;
    private const FOURTEEN = 14;
    private const FIFTEEN = 15;
    private const SIXTEEN = 16;
    private const SEVENTEEN = 17;
    private const EIGHTTEEN = 18;
    private const NINETEEN = 19;
    private const TWENTY = 20;

    public static function getValueFromKey(string $level): int
    {
        return static::toArray()[$level];
    }
}
