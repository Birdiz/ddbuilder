<?php

declare(strict_types=1);

namespace App\Enum\MagicTradingPost\Craft;

use App\Model\AbstractEnum;

class Condition extends AbstractEnum
{
    private const FORMULA = '<b>Une formule :</b> Les personnages ont besoin d\'une formule décrivant la construction 
    de l\'élément. Cela peut être quelque chose que les personnages ont développé dans le jeu (avec l\'approbation du 
    MJ) ou une formule établie d\'une source réputée (ou non).';

    private const SPELLCASTER = '<b>Capacités de lanceur de sorts :</b> Un personnage doit avoir des emplacements de 
    sorts et être capable de lancer un sort qu\'un objet produira afin d\'imprégner un objet de cette magie. Cela 
    inclut avoir et dépenser les composants matériels requis par le sort. Les objets magiques qui ne produisent pas de 
    sorts nécessitent toujours des lanceurs de sorts pour créer l\'objet (comme une arme +1).';

    private const LEVEL = '<b>Exigences de niveau de personnage :</b> Selon la rareté de l\'objet, un personnage doit
    atteindre un niveau minimum pour fabriquer cet objet. Un tableau illustrant cela est fourni ci-dessous.';

    private const MATERIAL = '<b>Matériaux, outils et emplacement :</b> Un MJ peut décider si un personnage a besoin
    d\'outils ou de fournitures particuliers, ou si un objet ne peut être créé qu\'à un certain endroit. Exemple : la 
    langue de la flamme nécessite d\'être forgée avec de la lave. Cela devrait être décrit dans la formule.';
}
