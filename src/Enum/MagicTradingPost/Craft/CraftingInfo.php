<?php

declare(strict_types=1);

namespace App\Enum\MagicTradingPost\Craft;

use App\Model\AbstractEnum;

class CraftingInfo extends AbstractEnum
{
    private const COMMON = [
        'rarity' => 'Commun',
        'cost' => '100 PO',
        'level' => '3',
        'duration' => '4',
    ];

    private const UNCOMMON = [
        'rarity' => 'Peu commun',
        'cost' => '500 PO',
        'level' => '3',
        'duration' => '20',
    ];

    private const RARE = [
        'rarity' => 'Rare',
        'cost' => '5 000 PO',
        'level' => '6',
        'duration' => '200',
    ];

    private const VERY_RARE = [
        'rarity' => 'Très rare',
        'cost' => '50 000 PO',
        'level' => '11',
        'duration' => '2 000 (~5,5 ans)',
    ];

    private const LEGENDARY = [
        'rarity' => 'Légendaire',
        'cost' => '500 000 PO',
        'level' => '17',
        'duration' => '20 000 (~55 ans)',
    ];
}
