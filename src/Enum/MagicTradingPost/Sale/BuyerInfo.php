<?php

declare(strict_types=1);

namespace App\Enum\MagicTradingPost\Sale;

use App\Model\AbstractEnum;

class BuyerInfo extends AbstractEnum
{
    private const TENTH = [
        'result' => '20 ou moins',
        'description' => 'Un acheteur offrant un dixième du prix de base',
    ];

    private const QUARTER = [
        'result' => '21 - 40',
        'description' => 'Un acheteur offrant un quart du prix de base et un acheteur louches offrant la moitié du 
        prix de base',
    ];

    private const HALF = [
        'result' => '41 - 80',
        'description' => 'Un acheteur offrant la moitié du prix de base et un acheteur louche offrant le prix de base 
        complet',
    ];

    private const FULL = [
        'result' => '81 - 90',
        'description' => 'Un acheteur offrant le prix de base complet',
    ];

    private const ONE_AND_HALF = [
        'result' => '91 ou plus',
        'description' => 'Un acheteur louche offrant une fois et demie le prix de base, sans poser de questions',
    ];
}
