<?php

declare(strict_types=1);

namespace App\Enum\MagicTradingPost\Sale;

use App\Model\AbstractEnum;

class SaleInfo extends AbstractEnum
{
    private const COMMON = [
        'rarity' => 'Commun',
        'price' => '100 PO',
        'duration' => '1d4',
        'modificator' => '+10',
    ];

    private const UNCOMMON = [
        'rarity' => 'Peu commun',
        'price' => '500 PO',
        'duration' => '1d6',
        'modificator' => '+0',
    ];

    private const RARE = [
        'rarity' => 'Rare',
        'price' => '5 000 PO',
        'duration' => '1d8',
        'modificator' => '-10',
    ];

    private const VERY_RARE = [
        'rarity' => 'Très rare',
        'price' => '50 000 PO',
        'duration' => '1d10',
        'modificator' => '-20',
    ];
}
