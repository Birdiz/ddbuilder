<?php

declare(strict_types=1);

namespace App\Enum\Object;

use App\Model\AbstractEnum;

/**
 * Class AC.
 *
 * @method static AC SOFT()
 * @method static AC VERY_FRAGILE()
 * @method static AC FRAGILE()
 * @method static AC MEDIUM()
 * @method static AC HARD()
 * @method static AC VERY_HARD()
 * @method static AC NEARLY_INDESTRUCTIBLE()
 */
class AC extends AbstractEnum
{
    private const SOFT = 'CA11';
    private const VERY_FRAGILE = 'CA13';
    private const FRAGILE = 'CA15';
    private const MEDIUM = 'CA17';
    private const HARD = 'CA19';
    private const VERY_HARD = 'CA21';
    private const NEARLY_INDESTRUCTIBLE = 'CA23';
}
