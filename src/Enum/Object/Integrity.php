<?php

declare(strict_types=1);

namespace App\Enum\Object;

use App\Model\AbstractEnum;

/**
 * Class Integrity.
 *
 * @method static Integrity FRAGILE()
 * @method static Integrity NORMAL()
 * @method static Integrity RESILIENT()
 */
class Integrity extends AbstractEnum
{
    private const FRAGILE = [
        'label' => 'Fragile',
        'multiplier' => 0.5,
    ];

    private const NORMAL = [
        'label' => 'Normal(e)',
        'multiplier' => 1,
    ];

    private const RESILIENT = [
        'label' => 'Résistant(e)',
        'multiplier' => 2,
    ];

    public static function getMultiplierFormKey(string $key): float
    {
        return match ($key) {
            static::FRAGILE()->getKey() => static::FRAGILE()->getValue()['multiplier'],
            static::RESILIENT()->getKey() => static::RESILIENT()->getValue()['multiplier'],
            default => static::NORMAL()->getValue()['multiplier'],
        };
    }
}
