<?php

declare(strict_types=1);

namespace App\Enum\Object;

use App\Model\AbstractEnum;

/**
 * Class Material.
 *
 * @method static Material GLASS()
 * @method static Material ICE()
 * @method static Material CLOTH()
 * @method static Material PAPER()
 * @method static Material WOOD()
 * @method static Material BONE()
 * @method static Material STONE()
 * @method static Material METAL()
 * @method static Material ADAMANTINE()
 * @method static Material UNKNOW()
 */
class Material extends AbstractEnum
{
    private const GLASS = 'Verre';
    private const ICE = 'Glace';
    private const CLOTH = 'Textile';
    private const PAPER = 'Papier';
    private const WOOD = 'Bois';
    private const BONE = 'Os';
    private const STONE = 'Pierre';
    private const METAL = 'Métal';
    private const ADAMANTINE = 'Diamant';
    private const UNKNOW = 'Inconnu';

    public static function getDieFromValue(string $value): int
    {
        return match ($value) {
            static::GLASS()->getValue(), static::ICE()->getValue(), static::CLOTH()->getValue(), static::PAPER(
            )->getValue() => 4,
            static::WOOD()->getValue(), static::BONE()->getValue() => 6,
            static::METAL()->getValue() => 10,
            static::ADAMANTINE()->getValue() => 12,
            default => 8,
        };
    }
}
