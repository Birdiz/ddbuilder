<?php

declare(strict_types=1);

namespace App\Enum\Object;

use App\Model\AbstractEnum;

/**
 * Class Size.
 *
 * @method static Size TINY()
 * @method static Size SMALL()
 * @method static Size MEDIUM()
 * @method static Size BIG()
 * @method static Size LARGE()
 * @method static Size HUGE()
 */
class Size extends AbstractEnum
{
    private const TINY = [
        'label' => 'Très petit(e)',
        'multiplier' => 1,
    ];

    private const SMALL = [
        'label' => 'Petit(e)',
        'multiplier' => 2,
    ];

    private const MEDIUM = [
        'label' => 'Moyen(ne)',
        'multiplier' => 3,
    ];

    private const BIG = [
        'label' => 'Grand(e)',
        'multiplier' => 4,
    ];

    private const LARGE = [
        'label' => 'Très grand(e)',
        'multiplier' => 5,
    ];

    private const HUGE = [
        'label' => 'Géant(e)',
        'multiplier' => 6,
    ];

    public static function getMultiplierFormKey(string $key): int
    {
        return match ($key) {
            static::TINY()->getKey() => static::TINY()->getValue()['multiplier'],
            static::SMALL()->getKey() => static::SMALL()->getValue()['multiplier'],
            static::BIG()->getKey() => static::BIG()->getValue()['multiplier'],
            static::LARGE()->getKey() => static::LARGE()->getValue()['multiplier'],
            static::HUGE()->getKey() => static::HUGE()->getValue()['multiplier'],
            default => static::MEDIUM()->getValue()['multiplier'],
        };
    }
}
