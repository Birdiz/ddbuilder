<?php

declare(strict_types=1);

namespace App\Enum\Object;

use App\Model\AbstractEnum;

/**
 * Class Tools.
 *
 * @method static Tools SMITH()
 * @method static Tools BREWER()
 * @method static Tools MASON()
 */
class Tools extends AbstractEnum
{
    private const SMITH = 'Outils de forgeron';
    private const BREWER = 'Outils de brassage';
    private const MASON = 'Outils de maçon';
}
