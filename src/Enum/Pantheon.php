<?php

declare(strict_types=1);

namespace App\Enum;

use App\Enum\Generator\PNJ\Alignment;
use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

/**
 * Class Pantheon.
 *
 * @method static Pantheon AURILE()
 * @method static Pantheon AZOUTH()
 * @method static Pantheon BAINE()
 * @method static Pantheon BESHABA()
 * @method static Pantheon BHAAL()
 * @method static Pantheon CHAUNTEA()
 * @method static Pantheon CYRIC()
 * @method static Pantheon DENEIR()
 * @method static Pantheon ELDATH()
 * @method static Pantheon GOND()
 * @method static Pantheon HEAUME()
 * @method static Pantheon ILMATAR()
 * @method static Pantheon KELEMVOR()
 * @method static Pantheon LATHANDRE()
 * @method static Pantheon LAIRA()
 * @method static Pantheon LLIIRA()
 * @method static Pantheon LOVIATAR()
 * @method static Pantheon MAILIKKI()
 * @method static Pantheon MALAR()
 * @method static Pantheon MASK()
 * @method static Pantheon MILIL()
 * @method static Pantheon MYRKUL()
 * @method static Pantheon MYSTRA()
 * @method static Pantheon OGHMA()
 * @method static Pantheon SAVRAS()
 * @method static Pantheon SELUNE()
 * @method static Pantheon SHAR()
 * @method static Pantheon SUNIE()
 * @method static Pantheon SYLVANUS()
 * @method static Pantheon TALONA()
 * @method static Pantheon TALOS()
 * @method static Pantheon TEMPUS()
 * @method static Pantheon TORM()
 * @method static Pantheon TYMORA()
 * @method static Pantheon TYR()
 * @method static Pantheon UMBERLIE()
 * @method static Pantheon WAUKYNE()
 * @method static Pantheon BAHAMUT()
 * @method static Pantheon BLIDDOOLPOOLP()
 * @method static Pantheon CORELLON_LARETHIAN()
 * @method static Pantheon EADRO()
 * @method static Pantheon GARL_BRILLEDOR()
 * @method static Pantheon GROLANTOR()
 * @method static Pantheon GRUUMSH()
 * @method static Pantheon HRUGGEK()
 * @method static Pantheon KURTULMAK()
 * @method static Pantheon LAOGZED()
 * @method static Pantheon LOLTH()
 * @method static Pantheon MAGLUBIYET()
 * @method static Pantheon MORADIN()
 * @method static Pantheon RILLIFANE_RALLATHIL()
 * @method static Pantheon SASHALAS_PROFONDEURS()
 * @method static Pantheon SEHANINE_ARCHELUNE()
 * @method static Pantheon SEKOLAH()
 * @method static Pantheon SEMUANYA()
 * @method static Pantheon SKERRIT()
 * @method static Pantheon SKORAEUS()
 * @method static Pantheon SURTUR()
 * @method static Pantheon THRYM()
 * @method static Pantheon TIAMAT()
 * @method static Pantheon YONDALLA()
 */
class Pantheon extends AbstractEnum
{
    use ArrayRandTrait;

    private const AURILE = [
        'name' => 'Aurile, déesse de l\'hiver',
        'alignment' => 'Neutre Mauvaise',
        'domains' => 'Nature, Tempête',
    ];

    private const AZOUTH = [
        'name' => 'Azouth, dieu des magiciens',
        'alignment' => 'Loyal Neutre',
        'domains' => 'Savoir',
    ];

    private const BAINE = [
        'name' => 'Baine, dieu de la tyrannie',
        'alignment' => 'Loyal Mauvais',
        'domains' => 'Guerre',
    ];

    private const BESHABA = [
        'name' => 'Beshaba, déesse de la malchance',
        'alignment' => 'Chaotique Mauvaise',
        'domains' => 'Duperie',
    ];

    private const BHAAL = [
        'name' => 'Bhaal, dieu du meurtre',
        'alignment' => 'Neutre Mauvais',
        'domains' => 'Mort',
    ];

    private const CHAUNTEA = [
        'name' => 'Chauntéa, déesse de l\'agriculture',
        'alignment' => 'Neutre Bonne',
        'domains' => 'Vie',
    ];

    private const CYRIC = [
        'name' => 'Cyric, dieu du mensonge',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Duperie',
    ];

    private const DENEIR = [
        'name' => 'Déneïr, dieu de l\'écriture',
        'alignment' => 'Neutre Bon',
        'domains' => 'Savoir',
    ];

    private const ELDATH = [
        'name' => 'Eldath, déesse de la paix',
        'alignment' => 'Neutre Bonne',
        'domains' => 'Nature, Vie',
    ];

    private const GOND = [
        'name' => 'Gond, dieu de l\'artisanat',
        'alignment' => 'Neutre',
        'domains' => 'Savoir',
    ];

    private const HEAUME = [
        'name' => 'Heaume, dieu de la protection',
        'alignment' => 'Loyal Neutre',
        'domains' => 'Lumière, Vie',
    ];

    private const ILMATAR = [
        'name' => 'Ilmater, dieu de l\'endurance',
        'alignment' => 'Loyal Bon',
        'domains' => 'Vie',
    ];

    private const KELEMVOR = [
        'name' => 'Kélemvor, dieu des morts',
        'alignment' => 'Loyal Neutre',
        'domains' => 'Mort',
    ];

    private const LATHANDRE = [
        'name' => 'Lathandre, dieu de la naissance et du renouveau',
        'alignment' => 'Neutre Bonne',
        'domains' => 'Lumière, Vie',
    ];

    private const LAIRA = [
        'name' => 'Leira, déesse de l\'illusion',
        'alignment' => 'Chaotique Neutre',
        'domains' => 'Duperie',
    ];

    private const LLIIRA = [
        'name' => 'Lliira, déesse de la joie',
        'alignment' => 'Chaotique Bonne',
        'domains' => 'Vie',
    ];

    private const LOVIATAR = [
        'name' => 'Loviatar, déesse de la souffrance',
        'alignment' => 'Loyale Mauvaise',
        'domains' => 'Mort',
    ];

    private const MAILIKKI = [
        'name' => 'Mailikki, déesse des forêts',
        'alignment' => 'Neutre Bonne',
        'domains' => 'Nature',
    ];

    private const MALAR = [
        'name' => 'Malar, dieu de la chasse',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Nature',
    ];

    private const MASK = [
        'name' => 'Mask, dieu des voleurs',
        'alignment' => 'Chaotique Neutre',
        'domains' => 'Duperie',
    ];

    private const MILIL = [
        'name' => 'Milil, dieu de la poésie et des chants',
        'alignment' => 'Neutre Bon',
        'domains' => 'Lumière',
    ];

    private const MYRKUL = [
        'name' => 'Myrkul, dieu de la mort',
        'alignment' => 'Neutre Mauvais',
        'domains' => 'Mort',
    ];

    private const MYSTRA = [
        'name' => 'Mystra, déesse de la magie',
        'alignment' => 'Neutre Bonne',
        'domains' => 'Savoir',
    ];

    private const OGHMA = [
        'name' => 'Oghma, dieu de la connaissance',
        'alignment' => 'Neutre',
        'domains' => 'Savoir',
    ];

    private const SAVRAS = [
        'name' => 'Savras, dieu de la divination et du destin',
        'alignment' => 'Loyal Neutre',
        'domains' => 'Savoir',
    ];

    private const SELUNE = [
        'name' => 'Séluné, déesse de la lune',
        'alignment' => 'Chaotique Bonne',
        'domains' => 'Savoir, Vie',
    ];

    private const SHAR = [
        'name' => 'Shar, déesse des ténèbres et de la perte',
        'alignment' => 'Neutre Mauvaise',
        'domains' => 'Duperie, Mort',
    ];

    private const SUNIE = [
        'name' => 'Sunie, déesse de l\'amour et de la beauté',
        'alignment' => 'Neutre',
        'domains' => 'Lumière, Vie',
    ];

    private const SYLVANUS = [
        'name' => 'Sylvanus, dieu de la nature sauvage',
        'alignment' => 'Neutre',
        'domains' => 'Nature',
    ];

    private const TALONA = [
        'name' => 'Talona, déesse de la maladie et du poison',
        'alignment' => 'Chaotique Mauvaise',
        'domains' => 'Mort',
    ];

    private const TALOS = [
        'name' => 'Talos, dieu des tempêtes',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Tempête',
    ];

    private const TEMPUS = [
        'name' => 'Tempus, dieu de la guerre',
        'alignment' => 'Neutre',
        'domains' => 'Guerre',
    ];

    private const TORM = [
        'name' => 'Torm, dieu du courage et du sacrifice de soi',
        'alignment' => 'Loyal Bon',
        'domains' => 'Guerre',
    ];

    private const TYMORA = [
        'name' => 'Tymora, déesse de la chance',
        'alignment' => 'Chaotique Bonne',
        'domains' => 'Duperie',
    ];

    private const TYR = [
        'name' => 'Tyr, dieu de la justice',
        'alignment' => 'Loyal Bon',
        'domains' => 'Guerre',
    ];

    private const UMBERLIE = [
        'name' => 'Umberlie, déesse des mers',
        'alignment' => 'Chaotique Mauvaise',
        'domains' => 'Tempête',
    ];

    private const WAUKYNE = [
        'name' => 'Waukyne, déesse du commerce',
        'alignment' => 'Neutre',
        'domains' => 'Duperie, Savoir',
    ];

    private const BAHAMUT = [
        'name' => 'Bahamut, dieu du Bien des dragons',
        'alignment' => 'Loyal Bon',
        'domains' => 'Guerre, Vie',
    ];

    private const BLIDDOOLPOOLP = [
        'name' => 'Blibdoolpoolp, déesse kuo-toa',
        'alignment' => 'Neutre Mauvaise',
        'domains' => 'Mort',
    ];

    private const CORELLON_LARETHIAN = [
        'name' => 'Corellon Larethian, dieu elfe de l\'art et de la magie',
        'alignment' => 'Chaotique Bon',
        'domains' => 'Lumière',
    ];

    private const EADRO = [
        'name' => 'Eadro, dieu de la mer des hommes-poissons',
        'alignment' => 'Neutre',
        'domains' => 'Nature, Tempête',
    ];

    private const GARL_BRILLEDOR = [
        'name' => 'Garl Brilledor, dieu gnome de la duperie et des ruses',
        'alignment' => 'Loyal Bon',
        'domains' => 'Duperie',
    ];

    private const GROLANTOR = [
        'name' => 'Grolantor, dieu de la guerre des géants des collines',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Guerre',
    ];

    private const GRUUMSH = [
        'name' => 'Gruumsh, dieu orque des tempêtes et de la guerre',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Guerre, Tempête',
    ];

    private const HRUGGEK = [
        'name' => 'Hruggek, dieu gobelours de la violence',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Guerre',
    ];

    private const KURTULMAK = [
        'name' => 'Kurtulmak, dieu kobold de la guerre et des mines',
        'alignment' => 'Loyal Mauvais',
        'domains' => 'Guerre',
    ];

    private const LAOGZED = [
        'name' => 'Laogzed, dieu troglodyte de la faim',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Mort',
    ];

    private const LOLTH = [
        'name' => 'Lolth, déesse drow des araignées',
        'alignment' => 'Chaotique Mauvaise',
        'domains' => 'Duperie',
    ];

    private const MAGLUBIYET = [
        'name' => 'Maglubiyet, dieu gobelin de la guerre',
        'alignment' => 'Loyal Mauvais',
        'domains' => 'Guerre',
    ];

    private const MORADIN = [
        'name' => 'Moradin, dieu nain de la création',
        'alignment' => 'Loyal Bon',
        'domains' => 'Savoir',
    ];

    private const RILLIFANE_RALLATHIL = [
        'name' => 'Rillifane Rallathil, dieu elfe de la nature',
        'alignment' => 'Chaotique Bon',
        'domains' => 'Nature',
    ];

    private const SASHALAS_PROFONDEURS = [
        'name' => 'Sashalas des Profondeurs, dieu elfe de la mer',
        'alignment' => 'Chaotique Bon',
        'domains' => 'Nature, Tempête',
    ];

    private const SEHANINE_ARCHELUNE = [
        'name' => 'Sehanine Archelune, déesse elfe de la lune',
        'alignment' => 'Chaotique Bonne',
        'domains' => 'Savoir',
    ];

    private const SEKOLAH = [
        'name' => 'Sekolah, dieu de la chasse des sahuagins',
        'alignment' => 'Loyal Mauvais',
        'domains' => 'Nature, Tempête',
    ];

    private const SEMUANYA = [
        'name' => 'Semuanya, dieu de la survie des hommes-lézards',
        'alignment' => 'Neutre',
        'domains' => 'Vie',
    ];

    private const SKERRIT = [
        'name' => 'Skerrit, dieu de la nature des centaures et des satyres',
        'alignment' => 'Neutre',
        'domains' => 'Nature',
    ];

    private const SKORAEUS = [
        'name' => 'Skoraeus Os-de-pierre, dieu des géants des pierres et de l\'art',
        'alignment' => 'Neutre',
        'domains' => 'Savoir',
    ];

    private const SURTUR = [
        'name' => 'Surtur, dieu des géants du feu et de l\'artisanat',
        'alignment' => 'Loyal Mauvais',
        'domains' => 'Guerre, Savoir',
    ];

    private const THRYM = [
        'name' => 'Thrym, dieu des géants du givre et de la force',
        'alignment' => 'Chaotique Mauvais',
        'domains' => 'Guerre',
    ];

    private const TIAMAT = [
        'name' => 'Tiamat, déesse du Mal des dragons',
        'alignment' => 'Loyale Mauvaise',
        'domains' => 'Duperie',
    ];

    private const YONDALLA = [
        'name' => 'Yondalla, déesse halfeline de la fertilité et de la protection',
        'alignment' => 'Loyale Bonne',
        'domains' => 'Vie',
    ];

    /**
     * @return array<int, mixed>
     */
    public static function getGodsByRaceName(string $raceName): array
    {
        $commonGods = self::getCommonGods();
        $elvesGods = self::getElvesGods();

        return match ($raceName) {
            Race::HILL_DWARF()->getValue()['name'], Race::MOUNTAIN_DWARF()->getValue()['name'] => array_merge(
                $commonGods,
                [static::MORADIN()->getValue()]
            ),
            Race::HIGH_ELF()->getValue()['name'], Race::WOOD_ELF()->getValue()['name'], Race::HALF_ELF()->getValue(
            )['name'] => array_merge($commonGods, $elvesGods),
            Race::LIGHTFOOT_HALFELING()->getValue()['name'], Race::STOOT_HALFELING()->getValue()['name'] => array_merge(
                $commonGods,
                [static::YONDALLA()->getValue()]
            ),
            Race::DRAGONBORN()->getValue()['name'] => array_merge(
                $commonGods,
                [
                    static::TIAMAT()->getValue(),
                    static::BAHAMUT()->getValue(),
                ]
            ),
            Race::FOREST_GNOME()->getValue()['name'], Race::ROCK_GNOME()->getValue()['name'] => array_merge(
                $commonGods,
                [static::GARL_BRILLEDOR()->getValue()]
            ),
            Race::HALF_ORC()->getValue()['name'] => array_merge($commonGods, [static::THRYM()->getValue()]),
            default => $commonGods,
        };
    }

    /**
     * @return array<int, mixed>
     */
    public static function getGoodAndNeutralGods(): array
    {
        return array_merge(
            self::getGoodCommonGods(),
            self::getNeutralCommonGods(),
            [
                static::BAHAMUT()->getValue(),
                static::CORELLON_LARETHIAN()->getValue(),
                static::EADRO()->getValue(),
                static::GARL_BRILLEDOR()->getValue(),
                static::MORADIN()->getValue(),
                static::RILLIFANE_RALLATHIL()->getValue(),
                static::SASHALAS_PROFONDEURS()->getValue(),
                static::SEHANINE_ARCHELUNE()->getValue(),
                static::SEMUANYA()->getValue(),
                static::SKERRIT()->getValue(),
                static::SKORAEUS()->getValue(),
                static::YONDALLA()->getValue(),
            ]
        );
    }

    /**
     * @return array<int, mixed>
     */
    public static function getEvilGods(): array
    {
        return array_merge(
            self::getEvilCommonGods(),
            [
                static::BLIDDOOLPOOLP()->getValue(),
                static::GROLANTOR()->getValue(),
                static::GRUUMSH()->getValue(),
                static::HRUGGEK()->getValue(),
                static::KURTULMAK()->getValue(),
                static::LAOGZED()->getValue(),
                static::LOLTH()->getValue(),
                static::MAGLUBIYET()->getValue(),
                static::SEKOLAH()->getValue(),
                static::SURTUR()->getValue(),
                static::THRYM()->getValue(),
                static::TIAMAT()->getValue(),
            ]
        );
    }

    /**
     * @param array<int, mixed> $gods
     *
     * @return array<int, mixed>
     */
    public static function getUntilThreeGods(array $gods, ?string $alignement = null): array
    {
        if (null !== $alignement) {
            return self::getUntilThreeGodsByAlignment($alignement);
        }

        return static::getXComplexeItems(3, $gods);
    }

    /**
     * @return array<int, mixed>
     */
    private static function getCommonGods(): array
    {
        return array_merge(self::getEvilCommonGods(), self::getGoodCommonGods(), self::getNeutralCommonGods());
    }

    /**
     * @return array<Pantheon>
     */
    private static function getElvesGods(): array
    {
        return [
            static::CORELLON_LARETHIAN(),
            static::RILLIFANE_RALLATHIL(),
            static::SASHALAS_PROFONDEURS(),
            static::SEHANINE_ARCHELUNE(),
        ];
    }

    /**
     * @return array<int, mixed>
     */
    private static function getEvilCommonGods(): array
    {
        return [
            static::AURILE()->getValue(),
            static::BAINE()->getValue(),
            static::BESHABA()->getValue(),
            static::BHAAL()->getValue(),
            static::CYRIC()->getValue(),
            static::LOVIATAR()->getValue(),
            static::MALAR()->getValue(),
            static::MYRKUL()->getValue(),
            static::SHAR()->getValue(),
            static::TALONA()->getValue(),
            static::TALOS()->getValue(),
            static::UMBERLIE()->getValue(),
        ];
    }

    /**
     * @return array<int, mixed>
     */
    private static function getGoodCommonGods(): array
    {
        return [
            static::CHAUNTEA()->getValue(),
            static::DENEIR()->getValue(),
            static::ELDATH()->getValue(),
            static::ILMATAR()->getValue(),
            static::LATHANDRE()->getValue(),
            static::LLIIRA()->getValue(),
            static::MAILIKKI()->getValue(),
            static::MILIL()->getValue(),
            static::MYSTRA()->getValue(),
            static::SELUNE()->getValue(),
            static::TORM()->getValue(),
            static::TYMORA()->getValue(),
            static::TYR()->getValue(),
        ];
    }

    /**
     * @return array<int, mixed>
     */
    private static function getNeutralCommonGods(): array
    {
        return [
            static::AZOUTH()->getValue(),
            static::GOND()->getValue(),
            static::HEAUME()->getValue(),
            static::KELEMVOR()->getValue(),
            static::LAIRA()->getValue(),
            static::MASK()->getValue(),
            static::OGHMA()->getValue(),
            static::SAVRAS()->getValue(),
            static::SUNIE()->getValue(),
            static::SYLVANUS()->getValue(),
            static::TEMPUS()->getValue(),
            static::WAUKYNE()->getValue(),
        ];
    }

    /**
     * @return array<int, mixed>
     */
    private static function getUntilThreeGodsByAlignment(string $alignement): array
    {
        return match (true) {
            in_array($alignement, Alignment::getGoodAlignments()) => static::getXComplexeItems(
                3,
                self::getGoodCommonGods()
            ),
            in_array($alignement, Alignment::getNeutralAlignments()) => static::getXComplexeItems(
                3,
                self::getNeutralCommonGods()
            ),
            in_array($alignement, Alignment::getBadAlignments()) => static::getXComplexeItems(
                3,
                self::getEvilCommonGods()
            ),
            default => []
        };
    }
}
