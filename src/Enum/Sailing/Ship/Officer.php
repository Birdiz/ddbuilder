<?php

declare(strict_types=1);

namespace App\Enum\Sailing\Ship;

use App\Model\AbstractEnum;

/**
 * Class Officer.
 *
 * @method static Officer CAPTAIN()
 * @method static Officer FIRST_MATE()
 * @method static Officer BOSUN()
 * @method static Officer QUARTERMASTER()
 * @method static Officer SURGEON()
 * @method static Officer COOK()
 */
class Officer extends AbstractEnum
{
    private const CAPTAIN = [
        'name' => 'Capitaine',
        'description' => 'Le capitaine donne des ordres. Les meilleurs capitaines ont des scores élevés d\'Intelligence 
        et de Charisme, ainsi qu\'une maîtrise des véhicules nautiques et des compétences d\'Intimidation et de 
        Persuasion.',
    ];

    private const FIRST_MATE = [
        'name' => 'Officier de pont',
        'description' => 'Ce spécialiste maintient le moral de l’équipage en assurant une supervision étroite, des 
        encouragements et de la discipline. Un officier de pont bénéficie d\'un score de Charisme élevé, ainsi que 
        d\'une maîtrise des compétences d\'Intimidation et de Persuasion.',
    ];

    private const BOSUN = [
        'name' => 'Maître d\'équipage',
        'description' => 'Le maître d\'équipage (ou second maître) fournit des conseils techniques au capitaine et à 
        l\'équipage et dirige les efforts de réparation et d\'entretien. Un bon maître d\'équipage a un score de Force 
        élevé, ainsi que la maîtrise des outils de menuisier et de l\'Athlétisme.',
    ];

    private const QUARTERMASTER = [
        'name' => 'Quartier-maître',
        'description' => 'Le quartier-maître trace le cap du navire en se fondant sur la connaissance des cartes 
        maritimes et sur une étude des conditions météorologiques et maritimes. Un quartier-maître fiable a tendance à 
        avoir un score de Sagesse élevé, ainsi qu\'une maîtrise des outils de navigation et de la compétence Nature.',
    ];

    private const SURGEON = [
        'name' => 'Chirurgien',
        'description' => 'Le chirurgien du navire s\'occupe des blessés, empêche les maladies de se propager dans tout 
        le navire et supervise l’assainissement. Un chirurgien compétent bénéficie d\'un score d\'Intelligence élevé, 
        ainsi que d\'une maîtrise des kits d\'herboristerie et de la compétence Médecine.',
    ];

    private const COOK = [
        'name' => 'Cuisinier',
        'description' => 'Le cuisinier d\'un navire travaille avec les ingrédients limités à bord d\'un navire pour 
        préparer les repas. Un cuisinier qualifié maintient le moral de l’équipage au top de sa forme, tandis qu’une 
        mauvaise gestion baisse la performance de l’équipage tout entier. Un cuisinier talentueux a un score de 
        Constitution élevé, ainsi que des compétences avec les kit de brasseur et les ustensiles de cuisine.',
    ];
}
