<?php

declare(strict_types=1);

namespace App\Enum\Sailing\Ship;

use App\Model\AbstractNamedEnum;

/**
 * Class Ship.
 *
 * @method static Ship AIRSHIP()
 * @method static Ship GALLEY()
 * @method static Ship KEELBOAT()
 * @method static Ship LONGSHIP()
 * @method static Ship ROWBOAT()
 * @method static Ship SAILING_SHIP()
 * @method static Ship WARSHIP()
 */
class Ship extends AbstractNamedEnum
{
    private const AIRSHIP = [
        'name' => 'Dirigeable',
        'price' => '20 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '25m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 20,
            'passengers' => 10,
        ],
        'cargo_capacity' => '1T',
        'speed' => '15 km/h',
        'hull' => [
            'AC' => 13,
            'HP' => 300,
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '16',
            'HP' => 50,
            'description' => 'Si la barre est détruite, le dirigeable ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Moteur élémentaire',
                'AC' => '18',
                'HP' => 100,
                'damage' => '–6m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(air) pouvoir élémentaire',
                    'speed' => '25m',
                    'description' => 'Si le moteur est détruit, le dirigeable s\'écrase immédiatement',
                ],
            ],
        ],
        'weapons' => [
            [
                'name' => 'Baliste (4)',
                'AC' => '15',
                'HP' => 50,
                'attack' => 'Attaque à distance: +6 au touché, portée 36/146 m, une cible. Touché: 16 (3d10) dégâts 
                perforants',
            ],
        ],
        'action' => 'À son tour, le dirigeable peut utiliser sa barre pour se déplacer à l\'aide de son moteur 
        élémentaire. Il peut également tirer de ses balistes. S\'il a la moitié de son équipage ou moins, il ne peut 
        tirer que deux des balistes.',
    ];

    private const LONGSHIP = [
        'name' => 'Drakkar',
        'price' => '10 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '20m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 40,
            'passengers' => 100,
        ],
        'cargo_capacity' => '10T',
        'speed' => '8 km/h',
        'hull' => [
            'AC' => 15,
            'HP' => 300,
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '16',
            'HP' => 50,
            'description' => 'Si la barre est détruite, le drakkar ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voiles',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–3m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '11m',
                    'description' => '5m en naviguant face au vent; 15m en naviguant avec le vent',
                ],
            ],
            [
                'name' => 'Rames',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–1.5m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) rame',
                    'speed' => '10m',
                    'description' => null,
                ],
            ],
        ],
        'action' => 'À son tour, le drakkar peut se déplacer grâce à sa barre. S\'il a la moitié de son équipage ou 
        moins, il se déplace à la moitié de sa vitesse.',
    ];

    private const KEELBOAT = [
        'name' => 'Quillard',
        'price' => '3 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '18m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 3,
            'passengers' => 4,
        ],
        'cargo_capacity' => '0.5T',
        'speed' => '4.5 km/h',
        'hull' => [
            'AC' => 15,
            'HP' => 100,
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '12',
            'HP' => 50,
            'description' => 'Si la barre est détruite, le quillard ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voiles',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–3m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '11m',
                    'description' => '5m en naviguant face au vent; 15m en naviguant avec le vent',
                ],
            ],
            [
                'name' => 'Rames',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–1.5m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) rame',
                    'speed' => '10m',
                    'description' => null,
                ],
            ],
        ],
        'weapons' => [
            [
                'name' => 'Baliste',
                'AC' => '15',
                'HP' => 50,
                'attack' => 'Attaque à distance: +6 au touché, portée 36/146 m, une cible. Touché: 16 (3d10) dégâts 
                perforants',
            ],
        ],
        'action' => 'À son tour, le quillard peut se déplacer grâce à sa barre. Il peut également tirer de sa baliste. 
        S\'il a la moitié de son équipage ou moins, il se déplace à la moitié de sa vitesse et ne peut pas tirer de sa 
        baliste.',
    ];

    private const SAILING_SHIP = [
        'name' => 'Voilier',
        'price' => '10 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '24m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 30,
            'passengers' => 20,
        ],
        'cargo_capacity' => '100T',
        'speed' => '8 km/h',
        'hull' => [
            'AC' => 15,
            'HP' => '300 (seuil minimum de dégâts de 15)',
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '18',
            'HP' => 50,
            'description' => 'Si la barre est détruite, le drakkar ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voiles',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–1.5m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '14m',
                    'description' => '5m en naviguant face au vent; 15m en naviguant avec le vent',
                ],
            ],
        ],
        'weapons' => [
            [
                'name' => 'Baliste',
                'AC' => '15',
                'HP' => 50,
                'attack' => 'Attaque à distance: +6 au touché, portée 36/146 m, une cible. Touché: 16 (3d10) dégâts 
                perforants',
            ],
            [
                'name' => 'Mangonneau',
                'AC' => '15',
                'HP' => 100,
                'attack' => 'Attaque à distance: +5 au touché, portée 60/240 m, une cible. Touché: 27 (5d10) dégâts 
                contondants',
            ],
        ],
        'action' => 'À son tour, le voilier peut se déplacer grâce à sa barre. Il peut aussi tier de sa balise et de 
        son mangonneau. S\'il a la moitié de son équipage ou moins, il se déplace à la moitié de sa vitesse et ne peut 
        tirer qu\'avec une de ses armes à la fois.',
    ];

    private const WARSHIP = [
        'name' => 'Navire de guerre',
        'price' => '25 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '30m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 40,
            'passengers' => 60,
        ],
        'cargo_capacity' => '200T',
        'speed' => '6.5 km/h',
        'hull' => [
            'AC' => 15,
            'HP' => '500 (seuil minimum de dégâts de 20)',
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '18',
            'HP' => 50,
            'description' => 'Si la barre est détruite, le navire de guerre ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voiles',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–3m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '11m',
                    'description' => '5m en naviguant face au vent; 13m en naviguant avec le vent',
                ],
            ],
            [
                'name' => 'Rames',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–1.5m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) rame',
                    'speed' => '6m avec 80 rameurs ou plus, 3m avec 40 rameurs ou plus, 1.5m avec 10 rameurs ou plus',
                    'description' => null,
                ],
            ],
        ],
        'weapons' => [
            [
                'name' => 'Baliste (2)',
                'AC' => '15',
                'HP' => 50,
                'attack' => 'Attaque à distance: +6 au touché, portée 36/146 m, une cible. Touché: 16 (3d10) dégâts 
                perforants',
            ],
            [
                'name' => 'Mangonneau (2)',
                'AC' => '15',
                'HP' => 100,
                'attack' => 'Attaque à distance: +5 au touché, portée 60/240 m, une cible. Touché: 27 (5d10) dégâts 
                contondants',
            ],
            [
                'name' => 'Éperon',
                'AC' => '20',
                'HP' => '100 (seuil minimum de dégâts de 10)',
                'attack' => 'Le navire de guerre a un avantage sur tous les jets de sauvegarde relatifs à l\'écrasement 
                lorsqu\'il s\'écrase sur une créature ou un objet, et tout dommage qu\'il subit du crash est à la 
                place appliqué au bélier. Ces avantages ne s\'appliquent pas si un véhicule s\'écrase la coque',
            ],
        ],
        'action' => 'À son tour, le navire de guerre peut se déplacer grâce à sa barre. Il peut également tirer de ses 
        balistes et de ses mangonneaux. S\'il a la moitié de son équipage ou moins, il se déplace à la moitié de sa 
        vitesse et ne peut tirer qu\'avec la moitié de ses armes.',
    ];

    private const GALLEY = [
        'name' => 'Galère',
        'price' => '30 000 PO',
        'metrics' => [
            'size' => 'gigantesque',
            'length' => '40m',
            'width' => '6m',
        ],
        'creature_capacity' => [
            'crew' => 80,
            'passengers' => 40,
        ],
        'cargo_capacity' => '150T',
        'speed' => '6 km/h',
        'hull' => [
            'AC' => 15,
            'HP' => 500,
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '16',
            'HP' => 50,
            'description' => 'Si la barre est détruite, la galère ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voiles',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–3m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '11m',
                    'description' => '5m en naviguant face au vent; 15m en naviguant avec le vent',
                ],
            ],
            [
                'name' => 'Rames',
                'AC' => '12',
                'HP' => 100,
                'damage' => '–1.5m de vitesse pour 25 dégâts subis',
                'locomotion' => [
                    'type' => '(eau) rame',
                    'speed' => '10m',
                    'description' => null,
                ],
            ],
        ],
        'weapons' => [
            [
                'name' => 'Baliste (4)',
                'AC' => '15',
                'HP' => 50,
                'attack' => 'Attaque à distance: +6 au touché, portée 36/146 m, une cible. Touché: 16 (3d10) dégâts 
                perforants',
            ],
            [
                'name' => 'Mangonneau (2)',
                'AC' => '15',
                'HP' => 100,
                'attack' => 'Attaque à distance: +5 au touché, portée 60/240 m, une cible. Touché: 27 (5d10) dégâts 
                contondants',
            ],
            [
                'name' => 'Éperon',
                'AC' => '20',
                'HP' => '100 (seuil minimum de dégâts de 10)',
                'attack' => 'La galère a un avantage sur tous les jets de sauvegarde relatifs à l\'écrasement 
                lorsqu\'elle s\'écrase sur une créature ou un objet, et tout dommage qu\'elle subit du crash est à la 
                place appliqué au bélier. Ces avantages ne s\'appliquent pas si un véhicule s\'écrase la coque',
            ],
        ],
        'action' => 'À son tour, la galère peut se déplacer grâce à sa barre. Elle peut également tirer de ses balistes 
        et de ses mangonneaux. Si elle a la moitié de son équipage ou moins, elle se déplace à la moitié de sa vitesse 
        et ne peut tirer qu\'avec la moitié de ses armes.',
    ];

    private const ROWBOAT = [
        'name' => 'Canot',
        'price' => '50 PO',
        'metrics' => [
            'size' => 'grand',
            'length' => '3m',
            'width' => '1.5m',
        ],
        'creature_capacity' => [
            'crew' => 2,
            'passengers' => 2,
        ],
        'cargo_capacity' => '0.25T',
        'speed' => '4.5 km/h',
        'hull' => [
            'AC' => 11,
            'HP' => 50,
        ],
        'control' => [
            'name' => 'Gouvernail',
            'AC' => '12',
            'HP' => 25,
            'description' => 'Si la barre est détruite, le canot ne peut pas tourner',
        ],
        'movements' => [
            [
                'name' => 'Voile',
                'AC' => '12',
                'HP' => 25,
                'damage' => null,
                'locomotion' => [
                    'type' => '(eau) voile',
                    'speed' => '4.5m',
                    'description' => 'Si le canot n\'a plus de voile, il s\'arrête',
                ],
            ],
            [
                'name' => 'Rames',
                'AC' => '12',
                'HP' => 25,
                'damage' => null,
                'locomotion' => [
                    'type' => '(eau) rame',
                    'speed' => '4.5m',
                    'description' => null,
                ],
            ],
        ],
        'action' => 'À son tour, le canot peut se déplacer grâce à sa barre.',
    ];

    /**
     * @return array<string, mixed>
     */
    public static function getShip(string $name): array
    {
        return match ($name) {
            static::GALLEY()->getValue()['name'] => static::GALLEY()->getValue(),
            static::KEELBOAT()->getValue()['name'] => static::KEELBOAT()->getValue(),
            static::LONGSHIP()->getValue()['name'] => static::LONGSHIP()->getValue(),
            static::ROWBOAT()->getValue()['name'] => static::ROWBOAT()->getValue(),
            static::SAILING_SHIP()->getValue()['name'] => static::SAILING_SHIP()->getValue(),
            static::WARSHIP()->getValue()['name'] => static::WARSHIP()->getValue(),
            default => static::AIRSHIP()->getValue(),
        };
    }
}
