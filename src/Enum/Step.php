<?php

declare(strict_types=1);

namespace App\Enum;

use App\Model\AbstractEnum;

/**
 * Class Step.
 *
 * @method static Step BEGINNER()
 * @method static Step EXPERTS()
 * @method static Step HEROS()
 * @method static Step GOD_LIKE()
 */
class Step extends AbstractEnum
{
    private const BEGINNER = 'Débutant';
    private const EXPERTS = 'Expert';
    private const HEROS = 'Héros';
    private const GOD_LIKE = 'Divin';
}
