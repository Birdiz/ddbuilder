<?php

declare(strict_types=1);

namespace App\Enum;

use App\Model\AbstractEnum;

/**
 * Class Stuff.
 *
 * @method static Stuff LOW()
 * @method static Stuff MEDIUM()
 * @method static Stuff HIGH()
 */
class Stuff extends AbstractEnum
{
    private const LOW = 'Faible';
    private const MEDIUM = 'Moyen';
    private const HIGH = 'Haut';
}
