<?php

declare(strict_types=1);

namespace App\Enum\Tavern;

use App\Model\AbstractEnum;

class Ambiance extends AbstractEnum
{
    private const CALME_EMPTY = 'L\'endroit est calme, et pour cause : l\'établissement est complètement vide. Pas un 
    chat, pas un rat, même pas de patron.';
    private const CALME = 'L\'endroit est calme, le patron s\'occupe de quelques tables et vous salue en vous voyant.';
    private const CALME_FULL = 'L\'endroit est calme, c\'est très étrange, compte tenu du fait que c\'est bondé. Les 
    gens autour de vous ont leurs nez rivés sur leurs affaires ou ne discutent même pas à leur voisin de table.';
    private const ALMOST_EMPTY = 'Pas un rat, le comptoir est vide et le patron ne vous regarde même pas entrer.';
    private const WEIRD = 'Vous passez la porte et remarquez que la population n\'est pas des plus communes. La 
    bougeoisie se mêle au bas peuple, des poivrots causent avec des marchands. Où êtes-vous tombés !?';
    private const TENSE = 'Vous entrez dans l\'établissement et remarquez que trop tard qu\'il est rempli de malfrats et
    autres bandits. Vous vous sentez piégé.';
    private const COSY = 'Vous entrez dans l\'établissement et êtes frappés par la beauté du lieu: tout semble soigné
    bien rangé, les clients sont de bonnes familles et l\'âtre vous réchauffe le coeur.';
    private const SIMPLE = 'Vous passez le pas de porte et arrivez dans un établissement des plus pauvres. Sans être
    peuplé de mauvaises gens, le lieu n\'est pas des plus exceptionnels.';
    private const RICHE = 'Vous êtes à peine passer à travers le sas d\'entrée qui vous sentez des effluves d\'odeurs
    très agréables. L\'air est chaud et doux, les gens sont richement habillés et discutent tout bas. Vous êtes 
    acceuillis par un serveur très poli.';
    private const MODEST = 'L\'intérieur est sobre, bien rangé et légèrement décoré. La population est mixte et discute
     bruyamment.';
    private const FIGHT = 'Vous passez le pas de porte et esquiver de justesse une chope de bière qui s\'écrase contre
    le mur derrière vous. Des cris vous alertent et vous indiquent qu\'une rixe est en cours.';
    private const UNWELCOME = 'À peine rentrés, les clients se retournent sur vous et vous dévisagent longuement en 
    chuchotant. Le patron ne vous accueille même pas.';
    private const WHOREHOUSE = 'Vous êtes tombés dans une taverne un peu spéciale : Le patron est une femme, les tables
    sont arrangées autour d\'une estrade, les clients sont quasiment tous des hommes et c\'est lorsque vous voyez une
    danseuse agiter ses seins nus sur la scène que vous comprenez que vous êtes arrivés dans un bordel.';
    private const CONCERT = 'Vous entrez dans une taverne bien animée ! Un groupe joue de la musique, plusieurs gens 
    dansent et l\'ambiance bat son comble !';
    private const RING = 'La taverne où vous vous trouvez est aménagée autour d\'un ring en plein centre. Vous remarquez
     un tableau en ardoise avec un nom tout en haut suivi de plusieurs bâtonnets notés à la craie blanche. Pour le reste
     c\'est une taverne plutôt classique.';
}
