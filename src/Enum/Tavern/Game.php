<?php

declare(strict_types=1);

namespace App\Enum\Tavern;

use App\Model\AbstractEnum;

/**
 * Class Game.
 *
 * @method static Game KILL_HYDRA()
 * @method static Game TREASURE()
 * @method static Game PRINCESS_HEART()
 * @method static Game WRESTLING()
 * @method static Game POKER()
 * @method static Game ROULETTE()
 * @method static Game DRINKING_GAME()
 */
class Game extends AbstractEnum
{
    private const KILL_HYDRA = [
        'name' => 'Tuer l\'hydre',
        'rules' => '<p>Le but du jeu est de tuer l\'hydre, le gagnant récupérant toutes les mises.<br/>
        Tous les joueurs misent une même somme d\'argent.<br/>
        Ensuite, chacun jette 2d6 pour déterminer l\'ordre de jeu, le plus haut score jouant en premier. 
        En cas d\'égalité, tous rejettent les dés. Une fois l\'ordre de jeu initial établi, 
        un joueur peut toutefois rajouter une mise complète pour faire rejeter les dés à tous.<br/>
        Lorsque plus personne ne mise, le « combat » peut commencer et l\'ordre de jeu ne peut alors plus être modifié.
        L\'hydre possède 11 têtes par joueurs (soit 22 têtes à deux, 33 têtes à trois, etc). 
        À chaque tour, un joueur peut lancer 1d6, 2d6 ou 3d6. Le résultat correspond au nombre de têtes
        qui sont coupées. Celui qui coupe la dernière tête tue l\'hydre et rafle la mise.</p>',
        'variants' => [
            'Un joueur ne peut lancer le même nombre de dés que le joueur précédent.',
            'Un même joueur ne peut lancer qu\'une seule fois 1d6, une seule fois 2d6 et une seule fois 3d6 
            (3 jets maximum donc).',
        ],
        'add-on' => '<p>Un joueur qui maîtrise les dés peut une et une seule fois par partie ajouter son bonus
         de maîtrise à n\'importe quel dé.</p>',
    ];

    private const TREASURE = [
        'name' => 'Trésor',
        'rules' => '<p>Le jeu consiste à se partager un trésor.<br/> 
        Chaque joueur mise 7 pièces (de cuivre, d\'argent, d\'or, peu importe).<br/>
        Ensuite, tous les joueurs lancent en même temps 1d6, 
        et chacun ramasse le nombre de pièces que lui indique son dé.<br/>
        Quand il n\'y a plus assez de pièces pour que tout le monde puisse les ramasser, 
        c\'est celui qui a fait le plus grand score lors de ce dernier tour qui récupère tout ce qui reste.
        S\'il y a égalité, la somme est partagée entre les ex aequo.</p>',
        'variants' => [
            'Le trésor du Dragon : la mise est de 13 pièces et on lance 2d6 à chaque fois.',
        ],
        'add-on' => '<p>Un joueur qui maîtrise les dés peut appliquer au premier lancer de chaque partie son 
        bonus de maîtrise comme résultat du dé (ou des deux dés si vous jouez avec la variante). 
        Par exemple, s\'il a un bonus de maîtrise de +3 et obtient 1 au dé, on considère qu\'il a obtenu 3.</p>',
    ];

    private const PRINCESS_HEART = [
        'name' => 'Le coeur de la princesse',
        'rules' => '<p>Le jeu consiste à conquérir le cœur d\'une princesse en lui présentant le plus grand collier.
        <br/>Tous les joueurs misent une même somme d\'argent. Tous les joueurs lancent simultanément 5d6.<br/>
        Chaque joueur peut relancer une première fois le nombre de dés qu\'il souhaite. Chaque joueur peut relancer une
        deuxième et dernière fois le nombre de dés qu\'il souhaite.<br/>
        On compare alors les suites obtenues (suite de nombre consécutif, comme 1-2-3-4-5). 
        La plus longue suite gagne la mise. Si deux suites sont aussi longues, celle dont les numéros sont les plus 
        hauts est gagnante (4,5,6 gagne à 2,3,4 par exemple). En cas d\'égalité totale, 
        seuls les ex aequo refont une manche pour se départager.</p>',
        'variants' => [
            'Le cœur de la reine : si le gagnant emporte la partie avec une suite complète (5 chiffres), 
            la mise initiale doit être doublée par tous les joueurs, et le gagnant jette un sixième dé. 
            Si avec ce dernier lancer il obtient une suite totale (6 chiffres), 
            il fait tripler la mise de ses adversaires. Si on joue à cette variante, 
            il faut bien entendu s\'assurer en début de partie que tous les joueurs possèdent bien trois fois la mise
            de départ.',
        ],
        'add-on' => '<p>Un joueur qui maîtrise les dés peut relancer quand il le souhaite mais simultanément un 
        nombre de dés inférieur ou égal à son bonus de maîtrise, avec l\'obligation de prendre les nouveaux résultats.
        </p>',
    ];

    private const WRESTLING = [
        'name' => 'Bras de fer',
        'rules' => '<p>Les joueurs s\'engagent dans une lutte au bras de fer avec des opposants de la taverne. 
        Chaque participant mise une somme sur la table et le gagnant remporte la totalité. Le résultat de la 
        confrontation se détermine en faisant des jets de Force opposés, le score le plus haut remportant la
        manche. Pour remporter le défi, il faut gagner deux manches consécutives. En cas d\'égalité, personne ne prend 
        l\'avantage et on rejette les dés, la manche continue.<br/>
        Les adversaires devant tous être d\'un bon niveau, lancer 4d6 et garder les trois meilleurs scores pour 
        déterminer la valeur de Force de l\'adversaire.
        Spécificités selon la race de l\'adversaire :<br/></p>
        <ul>
            <li>Un <b>humain</b> ou un <b>demi-elfe</b> ont +1 en Force.</li>
            <li>Un <b>nain</b>, un <b>demi-orque</b> ou un <b>drakéide</b> ont +2 en Force.</li>
            <li>Un <b>demi-orque</b> qui gagne la première manche d\'un défi a un désavantage au premier jet de Force 
            de la manche suivante (excès de confiance), ou l\'avantage s\'il perd la première manche 
            (piqué au vif).</li>
            <li>Un <b>halfelin</b> peut relancer tous les résultats de 1 au jet de Force.</li>
            <li>Une fois pour chaque adversaire différent, un <b>tieffelin</b> peut tenter de déstabiliser son 
            adversaire en réussissant un jet opposé de Charisme, ou un gnome tenter de déconcentrer son adversaire 
            (par des mimiques ou des blagues) en réussissant un jet opposé d\'Intelligence. En cas de réussite, 
            le PNJ obtient l\'avantage au jet de Force suivant (la tentative doit être tentée avant de lancer les
            dés de Force).</li>
            <li>Les <b>elfes</b> ne se prêtent généralement pas à ce genre de concours stupides !</li>
        </ul>',
        'variants' => [],
        'add-on' => '',
    ];

    private const POKER = [
        'name' => 'Poker Dice ou Dice Poker',
        'rules' => '<p>Chaque adversaire mise le même montant et lance 5d6. Ensuite chacun peut choisir d\'augmenter la 
        mise avec le même montant.<br/>
        Ils choisissent de relancer 0, 1, 2, 3, 4 ou 5 de leurs dés. Le résultat du deuxième lancer détermine le 
        gagnant.</p>',
        'variants' => [],
        'add-on' => '',
    ];

    private const ROULETTE = [
        'name' => 'Roulette naine',
        'rules' => '<p>6 chopes à bière sont placées sur une table, l\'une d\'elles contient des traces de pisse.<br/>
        Deux adversaires choisissent et boivent un à tour de rôle. Le joueur qui prend la bière de pisse perd.<br/>
        Aucun reniflement autorisé.</p>',
        'variants' => [],
        'add-on' => '',
    ];

    private const DRINKING_GAME = [
        'name' => 'Jeu de boisson',
        'rules' => '<p>Les concurrents peuvent demander entre 4 types de boissons différentes. Ces quatre boissons sont 
        commandées en fonction de la quantité d\'alcool qu\'elles contiennent. Plus ils sont forts (ou puissant en 
        alcool), plus le DD du jet de sauvegarde Constitution qu\'ils devront faire pour éviter de se saouler sera 
        élevé. Essayez de donner à chacun d\'eux un nom différent pour que cela paraisse plus réaliste. A titre 
        d\'exemple, vous pouvez utiliser ce format:</p>
        <ul>
            <li>«Cœur-léger» (DC10), vaut 1 point</li>
            <li>«Sang de boeuf» (DC14), vaut 2 points</li>
            <li>«Cœur de glace» (DD17), vaut 3 points</li>
            <li>«Carnage» (DC20), vaut 4 points</li>
        </ul>
        <p>L\'objectif est d\'atteindre 20 points sans tomber inconscient ni vomir. En fonction de la boisson que vous 
        choisissez, le nombre de points que vous obtenez pour faire le jet de sauvegarde. Chaque fois que vous échouez 
        à une sauvegarde, vous gagnez une pénalité qui vous rendra plus difficile de gagner, tout en vous laissant une 
        longueur d\'avance sur la perte de connaissance. Même si vous échouez à la sauvegarde, les points vous sont 
        toujours accordés.</p>
        <ul>
            <li>1er échec : -2 aux jets de sauvegarde.</li>
            <li>2ème échec : -2 aux jets de sauvegarde, désavantage en jets de sauvegarde et condition empoisonnée.</li>
            <li>3e échec : état inconscient ou le buveur vomit.</li>
        </ul>',
        'variants' => [],
        'add-on' => 'Ceux qui ont l\'avantage dans les jets de sauvegarde contre l\'empoisonnement ont également 
        l\'avantage dans ces DD. Si le personnage est un buveur habituel (un pirate par exemple), vous pouvez en tant 
        que MJ choisir de donner un bonus à ses sauvegardes allant de +1 à +3.',
    ];

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function getRandomGame(): array
    {
        return static::getRandomEnum(static::toArray());
    }
}
