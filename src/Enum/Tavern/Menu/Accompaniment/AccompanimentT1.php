<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\Accompaniment;

use App\Model\AbstractWeightedEnum;

class AccompanimentT1 extends AbstractWeightedEnum
{
    private const BOUILLIE_ORGE = 'bouillie d’orge';
    private const BOUILLIE_SEIGLE = 'bouillie de seigle';
    private const BOUILLIE_AVOINE = 'bouillie d\'avoine';
    private const BOUILLIE_SARRASIN = 'bouillie de sarrasin';
    private const PAIN_SEIGLE = 'pain de seigle rassis';
    private const PAIN_ORGE = 'pain de orge rassis';
    private const PAIN_AVOINE = 'pain d\'avoine rassis';
    private const PAIN_SARRASIN = 'pain de sarrasin rassis';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 15, 15, 15, 10, 10, 10, 10];
    }
}
