<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\Accompaniment;

use App\Model\AbstractWeightedEnum;

class AccompanimentT2 extends AbstractWeightedEnum
{
    private const BOUILLIE_ORGE = 'bouillie d’orge';
    private const BOUILLIE_SEIGLE = 'bouillie de seigle';
    private const BOUILLIE_SARRASIN = 'bouillie de sarrasin';
    private const BOUILLIE_MILLET = 'bouillie de millet';
    private const BOUILLIE_AVOINE = 'bouillie d\'avoine';
    private const PAIN_SEIGLE = 'pain de seigle';
    private const PAIN_ORGE = 'pain de orge';
    private const PAIN_SARRASIN = 'pain de sarrasin';
    private const PAIN_AVOINE = 'pain d\'avoine';
    private const PAIN_FROMENT = 'pain de froment (blé)';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 20, 15, 10, 15, 10];
    }
}
