<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\Accompaniment;

use App\Model\AbstractWeightedEnum;

class AccompanimentT3 extends AbstractWeightedEnum
{
    private const PAIN_SEIGLE = 'pain de seigle';
    private const PAIN_ORGE = 'pain de orge';
    private const PAIN_SARRASIN = 'pain de sarrasin';
    private const PAIN_AVOINE = 'pain d\'avoine';
    private const PAIN_FROMENT = 'pain de froment (blé)';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [15, 15, 15, 15, 40];
    }
}
