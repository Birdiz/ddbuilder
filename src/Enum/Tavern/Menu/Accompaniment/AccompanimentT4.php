<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\Accompaniment;

use App\Model\AbstractWeightedEnum;

class AccompanimentT4 extends AbstractWeightedEnum
{
    private const CHAMPIGNONS = 'champignons';
    private const OIGNONS = 'oignons';
    private const LENTILLES = 'lentilles';
    private const PUREE_CELERI = 'purée de céléris';
    private const POIS_CHICHES = 'pois chiches';
    private const PUREE_NAVET = 'purée de navets';
    private const PUREE_POIS = 'purée de pois';
    private const EPINARDS = 'épinards';
    private const POIREAUX = 'poireaux';
    private const CAROTTES_BOUILLIES = 'carottes bouillies';
    private const FEVES = 'f&eagrave;';
    private const CHOUX_BRAISES = 'choux braisés';
    private const ECHALOTES = 'échalotes';
    private const CRESSON = 'cresson';
    private const PANAIS = 'panais';
    private const BETTERAVES_BOUILLIES = 'betteraves bouillies';
    private const TOPINAMBOUR = 'topinambour';
    private const CHOUX_FLEURS = 'choux fleurs';
    private const ARTICHAUTS = 'artichauts';
    private const SALSIFIS = 'salsifis';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
