<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\Accompaniment;

use App\Model\AbstractWeightedEnum;

class AccompanimentT5 extends AbstractWeightedEnum
{
    private const SAUCE_VIN_BLANC_ORANGE = 'sauce au vin blanc et à l\'orange';
    private const SAUCE_VIN_ROUGE_PRUNES = 'sauce au vin rouge et aux prunes';
    private const SAUCE_TRUFFES = 'sauce aux truffes';
    private const SAUCE_MIEL_AMANDES = 'sauce au miel et aux amandes';
    private const SAUCE_BEURRE = 'sauce au beurre persillé';
    private const SAUCE_MOUTARDE = 'sauce à la moutarde et à la coriandre';
    private const SAUCE_CITRON = 'sauce au citron et au gingembre';
    private const SAUCE_SAFRAN = 'sauce au safran et à la cannelle';
    private const SAUCE_CREME = 'sauce à la crème et à la ciboulette';
    private const BLETTES = 'blettes au beurre';
    private const LARDONS = 'lardons et échalottes confites';
    private const OLIVES = 'olives, persil et marjolaine';
    private const OIGNONS = 'oignons gratinés';
    private const PUREE_POIS_LARD = 'purée de pois frite au lard';
    private const POIREAUX = 'poireaux au beurre';
    private const TROMPETTES = 'trompettes de la mort';
    private const CHATAIGNES = 'chataîgnes et morilles';
    private const CEPES = 'cèpes à la crème';
    private const CREME_SELSIFIS = 'crème de salsifis';
    private const FEVES = 'fèves rissolées au romarin';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
