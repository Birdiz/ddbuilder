<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Broth.
 *
 * @method static Broth LENTILLES()
 * @method static Broth POIS()
 * @method static Broth FEVES()
 * @method static Broth POIS_CHICHES()
 * @method static Broth NAVETS()
 * @method static Broth CHAMPIGNONS()
 * @method static Broth OIGNONS()
 * @method static Broth CELERIS()
 * @method static Broth POIREAUX()
 * @method static Broth CHOUX()
 * @method static Broth BLETTES()
 * @method static Broth CRESSONS()
 * @method static Broth ORTIES()
 * @method static Broth CAROTTES()
 * @method static Broth BETTERAVES()
 * @method static Broth SALSIFIS()
 */
class Broth extends AbstractWeightedEnum
{
    private const LENTILLES = 'lentilles';
    private const POIS = 'pois';
    private const FEVES = 'fèves';
    private const POIS_CHICHES = 'pois chiches';
    private const NAVETS = 'navets';
    private const CHAMPIGNONS = 'champignons';
    private const OIGNONS = 'oignons';
    private const CELERIS = 'céléris';
    private const POIREAUX = 'poireaux';
    private const CHOUX = 'choux';
    private const BLETTES = 'blettes';
    private const CRESSONS = 'cressons';
    private const ORTIES = 'orties';
    private const CAROTTES = 'carottes';
    private const BETTERAVES = 'betteraves';
    private const SALSIFIS = 'salsifis';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 15, 5, 5, 5, 5, 10, 5, 5, 5, 5, 5, 5];
    }
}
