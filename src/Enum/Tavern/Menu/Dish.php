<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Dish.
 *
 * @method static Dish COCHON_ROTI()
 * @method static Dish POULET_ROTI()
 * @method static Dish PERDRIX_ROTIE()
 * @method static Dish OMELETTE_LARD()
 * @method static Dish TOURTE_VEAU()
 * @method static Dish GIGOT_MOUTON()
 * @method static Dish CABRIS_ROTI()
 * @method static Dish SANGLIER_BOUILLI()
 * @method static Dish BOEUF_BOUILLI()
 * @method static Dish SAUCISSES_PORC()
 * @method static Dish ANGUILLE_ROTIE()
 * @method static Dish TRIPES_BROCHET()
 * @method static Dish CARPE()
 * @method static Dish SARDINES_GRILLEES()
 * @method static Dish TOURTE_CRABE()
 * @method static Dish HARENG_GRILLE()
 * @method static Dish BROCHETTES_POULPE()
 * @method static Dish GRATIN_MORUE()
 * @method static Dish GRATIN_MAQUEREAU()
 * @method static Dish ESTURGEON()
 */
class Dish extends AbstractWeightedEnum
{
    private const COCHON_ROTI = 'cochon roti';
    private const POULET_ROTI = 'poulet roti';
    private const PERDRIX_ROTIE = 'perdrix rotie';
    private const OMELETTE_LARD = 'omelette au lard';
    private const TOURTE_VEAU = 'tourte au veau';
    private const GIGOT_MOUTON = 'gigot de mouton';
    private const CABRIS_ROTI = 'cabris roti';
    private const SANGLIER_BOUILLI = 'sanglier bouilli';
    private const BOEUF_BOUILLI = 'boeuf bouilli';
    private const SAUCISSES_PORC = 'saucisses de porc';
    private const ANGUILLE_ROTIE = 'anguille rotie';
    private const TRIPES_BROCHET = 'tripes de brochet';
    private const CARPE = 'carpe';
    private const SARDINES_GRILLEES = 'sardines grillées';
    private const TOURTE_CRABE = 'tourte au crabe';
    private const HARENG_GRILLE = 'hareng grillé';
    private const BROCHETTES_POULPE = 'brochettes de poulpe';
    private const GRATIN_MORUE = 'gratin de morue';
    private const GRATIN_MAQUEREAU = 'gratin de maquereau';
    private const ESTURGEON = 'esturgeon';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
