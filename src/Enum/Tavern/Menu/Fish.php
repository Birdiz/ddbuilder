<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Fish.
 *
 * @method static Fish BROCHET_MIJOTE()
 * @method static Fish BROCHET()
 * @method static Fish BROCHET_TRIPES()
 * @method static Fish CARPE()
 * @method static Fish ESTURGEON_SAUCISSES()
 * @method static Fish HUITRES()
 * @method static Fish ESTURGEON_PATE()
 * @method static Fish CRABES()
 * @method static Fish HARENG()
 * @method static Fish HOMARD()
 * @method static Fish LANGOUSTES()
 * @method static Fish ANGUILLES()
 * @method static Fish SAUMON()
 * @method static Fish TRUITE()
 * @method static Fish TURBOT()
 * @method static Fish SOLE()
 * @method static Fish MORUE()
 * @method static Fish MAQUEREAU()
 * @method static Fish POULPE()
 * @method static Fish LOTTE()
 */
class Fish extends AbstractWeightedEnum
{
    private const BROCHET_MIJOTE = 'brochet mijoté';
    private const BROCHET = 'brochet';
    private const BROCHET_TRIPES = 'tripes de brochet';
    private const CARPE = 'hachis de carpe';
    private const ESTURGEON_SAUCISSES = 'saucisses d\'esturgeon';
    private const HUITRES = 'pâté d\'huîtres en croûte';
    private const ESTURGEON_PATE = 'pâté d\'esturgeon';
    private const CRABES = 'crabes en potée';
    private const HARENG = 'hareng';
    private const HOMARD = 'homard bouilli';
    private const LANGOUSTES = 'langoustes flambées';
    private const ANGUILLES = 'anguilles frites';
    private const SAUMON = 'saumon grillé';
    private const TRUITE = 'truite';
    private const TURBOT = 'turbot';
    private const SOLE = 'sole panée';
    private const MORUE = 'morue gratinée';
    private const MAQUEREAU = 'maquereau farci';
    private const POULPE = 'ragoût de poulpes';
    private const LOTTE = 'lotte risolée';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
