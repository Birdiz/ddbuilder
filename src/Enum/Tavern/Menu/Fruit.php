<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Fruit.
 *
 * @method static Fruit POIRES()
 * @method static Fruit POMMES()
 * @method static Fruit FIGUES()
 * @method static Fruit CERISES()
 * @method static Fruit RAISINS()
 * @method static Fruit ABRICOTS_PECHES()
 * @method static Fruit GRENADES()
 * @method static Fruit FRAMBOISES_MURES()
 * @method static Fruit MYRTILLES()
 * @method static Fruit MELON()
 */
class Fruit extends AbstractWeightedEnum
{
    private const POIRES = 'poires';
    private const POMMES = 'pommes';
    private const FIGUES = 'figues';
    private const CERISES = 'cerises';
    private const RAISINS = 'raisins';
    private const ABRICOTS_PECHES = 'abricots, pêches';
    private const GRENADES = 'grenades';
    private const FRAMBOISES_MURES = 'framboises, mûres';
    private const MYRTILLES = 'myrtilles';
    private const MELON = 'melon';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [10, 10, 10, 10, 10, 10, 10, 10, 10, 10];
    }
}
