<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Hotpot.
 *
 * @method static Hotpot LENTILLES()
 * @method static Hotpot POIS()
 * @method static Hotpot FEVES()
 * @method static Hotpot POIS_CHICHES()
 * @method static Hotpot OIGNONS()
 * @method static Hotpot POIREAUX()
 * @method static Hotpot CHOUX()
 * @method static Hotpot BLETTES()
 * @method static Hotpot CRESSONS()
 * @method static Hotpot ORTIES()
 * @method static Hotpot SALSIFIS()
 * @method static Hotpot CHAMPIGNONS()
 */
class Hotpot extends AbstractWeightedEnum
{
    private const LENTILLES = 'lentilles';
    private const POIS = 'pois';
    private const FEVES = 'fèves';
    private const POIS_CHICHES = 'pois chiches';
    private const OIGNONS = 'oignons';
    private const POIREAUX = 'poireaux';
    private const CHOUX = 'choux';
    private const BLETTES = 'blettes';
    private const CRESSONS = 'cressons';
    private const ORTIES = 'orties';
    private const SALSIFIS = 'salsifis';
    private const CHAMPIGNONS = 'champignons';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 5, 5];
    }

    public static function getSecondIngredientValue(string $firstIngredientValue): ?string
    {
        while (true) {
            $secondIgradientValue = static::getRandomEnum(static::toArray());
            if ($secondIgradientValue !== $firstIngredientValue) {
                break;
            }
        }

        return $secondIgradientValue;
    }
}
