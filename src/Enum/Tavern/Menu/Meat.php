<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Meat.
 *
 * @method static Meat COCHON_ROTI()
 * @method static Meat VEAU_FARCI()
 * @method static Meat RAGOUT_PORC()
 * @method static Meat OMELETTE_LARD()
 * @method static Meat POULET_ROTI()
 * @method static Meat FAISAN_ROTI()
 * @method static Meat PERDRIX_ROTIE()
 * @method static Meat BECASSE_ROTIE()
 * @method static Meat PIGEON_ROTI()
 * @method static Meat PORCELET_FARCI()
 * @method static Meat MOUTON_ROTI()
 * @method static Meat CABRIS_ROTI()
 * @method static Meat SANGLIER_ROTI()
 * @method static Meat CERF_ROTI()
 * @method static Meat TOURTE_VEAU()
 * @method static Meat BOEUF_ROTI()
 * @method static Meat CHEVREUIL_FARCI()
 * @method static Meat OIE_FARCI()
 * @method static Meat LAPIN()
 * @method static Meat CANARD()
 */
class Meat extends AbstractWeightedEnum
{
    private const COCHON_ROTI = 'cochon roti';
    private const VEAU_FARCI = 'cuisse de veau farcie';
    private const RAGOUT_PORC = 'ragoût de porc';
    private const OMELETTE_LARD = 'omelette au lard fumé';
    private const POULET_ROTI = 'poulet roti';
    private const FAISAN_ROTI = 'faisan roti';
    private const PERDRIX_ROTIE = 'perdrix rotie';
    private const BECASSE_ROTIE = 'bécasse rotie';
    private const PIGEON_ROTI = 'pigeon roti';
    private const PORCELET_FARCI = 'porcelet farci';
    private const MOUTON_ROTI = 'mouton roti';
    private const CABRIS_ROTI = 'cabris roti';
    private const SANGLIER_ROTI = 'sanglier roti';
    private const CERF_ROTI = 'cerf roti';
    private const TOURTE_VEAU = 'tourte au veau';
    private const BOEUF_ROTI = 'quartier de boeuf roti';
    private const CHEVREUIL_FARCI = 'chevreuil farci';
    private const OIE_FARCI = 'oie farcie';
    private const LAPIN = 'lapin';
    private const CANARD = 'canard confit';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
