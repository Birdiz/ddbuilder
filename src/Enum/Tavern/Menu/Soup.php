<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Soup.
 *
 * @method static Soup NAVETS()
 * @method static Soup ALGUES()
 * @method static Soup FEUILLES_SAUVAGES()
 * @method static Soup RUTABAGAS()
 * @method static Soup ORTIES()
 * @method static Soup CRESSON()
 * @method static Soup CHAMPIGNONS()
 * @method static Soup CAROTTES()
 * @method static Soup BETTERAVES()
 * @method static Soup PANAIS()
 * @method static Soup OIGNONS()
 * @method static Soup AIL()
 */
class Soup extends AbstractWeightedEnum
{
    private const NAVETS = 'aux navets';
    private const ALGUES = 'aux algues';
    private const FEUILLES_SAUVAGES = 'aux feuilles sauvages';
    private const RUTABAGAS = 'aux rutabagas';
    private const ORTIES = 'aux orties';
    private const CRESSON = 'au cresson';
    private const CHAMPIGNONS = 'aux champignons';
    private const CAROTTES = 'aux carottes';
    private const BETTERAVES = 'aux betteraves';
    private const PANAIS = 'aux panais';
    private const OIGNONS = 'aux oignons';
    private const AIL = 'à l\'ail';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [25, 20, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
