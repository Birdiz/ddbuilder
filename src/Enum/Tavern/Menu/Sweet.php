<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu;

use App\Model\AbstractWeightedEnum;

/**
 * Class Sweet.
 *
 * @method static Sweet FRUITS()
 * @method static Sweet GATEAU()
 * @method static Sweet FLAN()
 * @method static Sweet POIRES()
 * @method static Sweet BEIGNETS()
 * @method static Sweet TARTE_POMMES()
 * @method static Sweet GAUFRES()
 * @method static Sweet TARTE_PRUNES()
 * @method static Sweet BISCUITS()
 * @method static Sweet GINGEMBRE()
 */
class Sweet extends AbstractWeightedEnum
{
    private const FRUITS = 'fruits confits';
    private const GATEAU = 'gâteau au miel';
    private const FLAN = 'flan aux œufs';
    private const POIRES = 'poires rôties à la cannelle';
    private const BEIGNETS = 'beignets';
    private const TARTE_POMMES = 'tarte aux pommes et coings';
    private const GAUFRES = 'gaufres au sucre et à la cannelle';
    private const TARTE_PRUNES = 'tarte aux prunes';
    private const BISCUITS = 'biscuits aux épices';
    private const GINGEMBRE = 'gingembre confit';

    /**
     * {@inheritDoc}
     */
    public static function getWeights(): array
    {
        return [10, 10, 10, 10, 10, 10, 10, 10, 10, 10];
    }
}
