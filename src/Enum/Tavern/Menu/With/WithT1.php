<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\With;

use App\Model\AbstractWeightedEnum;

/**
 * Class WithT1.
 *
 * @method static WithT1 NOTHING()
 * @method static WithT1 QUEUES_POISSON()
 * @method static WithT1 GRAISSE_BALEINE()
 * @method static WithT1 CHATAIGNES()
 * @method static WithT1 RESTES_LAPIN()
 * @method static WithT1 RESTES_GRIVE()
 * @method static WithT1 RESTES_CERF()
 * @method static WithT1 RESTES_OISEAUX()
 * @method static WithT1 GRAISSE_PORC()
 * @method static WithT1 LARD()
 */
class WithT1 extends AbstractWeightedEnum
{
    private const NOTHING = 'rien';
    private const QUEUES_POISSON = 'queues de poisson';
    private const GRAISSE_BALEINE = 'graisse de baleine';
    private const CHATAIGNES = 'châtaignes';
    private const RESTES_LAPIN = 'restes de lapin ou de lièvre';
    private const RESTES_GRIVE = 'restes de grive ou de merle';
    private const RESTES_CERF = 'restes de cerf ou de sanglier';
    private const RESTES_OISEAUX = 'restes d’oiseaux sauvages (bécasse, pigeon)';
    private const GRAISSE_PORC = 'graisse de porc';
    private const LARD = 'lard rance';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [55, 5, 5, 5, 5, 5, 5, 5, 5, 5];
    }
}
