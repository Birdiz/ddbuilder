<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\With;

use App\Model\AbstractWeightedEnum;

/**
 * Class WithT2.
 *
 * @method static WithT2 BROTH()
 * @method static WithT2 POULET()
 * @method static WithT2 OEUFS()
 * @method static WithT2 PORC()
 * @method static WithT2 POISSON()
 * @method static WithT2 MOLLUSQUES()
 * @method static WithT2 LAPIN()
 * @method static WithT2 OISEAU()
 */
class WithT2 extends AbstractWeightedEnum
{
    private const BROTH = 'bouillon';
    private const POULET = 'carcasse de poulet';
    private const OEUFS = 'oeufs';
    private const PORC = 'porc (lard, jambon)';
    private const POISSON = 'poisson (hareng, anguille)';
    private const MOLLUSQUES = 'mollusques (huîtres, palourdes, moules)';
    private const LAPIN = 'lapin ou lièvre';
    private const OISEAU = 'oiseau (pigeon, caille, bécasse, perdrix, grive, merle)';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [45, 15, 10, 10, 5, 5, 5, 5];
    }
}
