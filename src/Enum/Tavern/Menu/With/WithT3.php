<?php

declare(strict_types=1);

namespace App\Enum\Tavern\Menu\With;

use App\Model\AbstractWeightedEnum;

/**
 * Class WithT3.
 *
 * @method static WithT3 FROMAGE()
 * @method static WithT3 BEURRE()
 * @method static WithT3 OEUFS()
 * @method static WithT3 PORC()
 * @method static WithT3 POULET()
 * @method static WithT3 OISEAU()
 * @method static WithT3 POISSON()
 * @method static WithT3 MOLLUSQUES()
 * @method static WithT3 LAPIN()
 */
class WithT3 extends AbstractWeightedEnum
{
    private const FROMAGE = 'fromage';
    private const BEURRE = 'beurre';
    private const OEUFS = 'oeufs';
    private const PORC = 'porc (lard, jambon)';
    private const POULET = 'carcasse de poulet';
    private const OISEAU = 'oiseau (pigeon, caille, bécasse, perdrix, grive, merle)';
    private const POISSON = 'poisson (hareng, anguille)';
    private const MOLLUSQUES = 'mollusques (huîtres, palourdes, moules)';
    private const LAPIN = 'lapin ou lièvre';

    /**
     * @return array<int>
     */
    public static function getWeights(): array
    {
        return [25, 15, 15, 15, 10, 5, 5, 5, 5];
    }
}
