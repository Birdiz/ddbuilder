<?php

declare(strict_types=1);

namespace App\Enum\Tavern;

use App\Model\AbstractEnum;

class OverHeard extends AbstractEnum
{
    private const DRAGON = 'Dragon? Quel dragon?';
    private const RABBIT = 'Cette dame là-bas, je ne lui parlerais pas, une fois, elle m\'a transformé en lapin.';
    private const GHOST = 'Honnêtement, j\'ai vu cet étrange fantôme, et j\'ai failli craquer mon pantalon!';
    private const ERMITE = 'Oui, tout le monde connaît cette vieille ermite, elle vit seul. Oh? Tu n\'as jamais entendu 
    parler d\'elle, laisse-moi te raconter.';
    private const TURTLES = 'Une fois, ce type se promène et me dit qu\'il a été bloqué sur une île! Et qu\'il  
    chevauchait des tortues de mer ou quelque chose comme ça, je me suis dit il en avait eu quelques-uns de trop.';
    private const LIGHTS = 'La chose la plus étrange, les lumières dans le ciel nocturne. Pour la plupart, elles 
    ressemble à un beau reflet, quelque chose que les dieux ont imaginé, mais l\'autre nuit, je jure que j\'ai vu 
    quelque chose qui bouge là-haut.';
    private const SOUP = 'La soupe d\'orge à la taverne locale guérira à peu près n\'importe quelle maladie, j\'avais 
    une verrue sur mon pied de la taille de mon gros orteil! C\'était parti le jour d\'après.';
    private const PRIESTESS = 'Cette vieille prêtresse que j\'ai vue marcher la nuit et se parler à elle-même.';
    private const MYTH = 'Non, c\'est juste un mythe que la plupart les cimetières sont hantés. Vous savez qu\'est-ce 
    qui est vraiment hanté? Ma vie amoureuse !';
    private const HONESTY = 'Normalement, je ne vous dirais pas ceci, mais vous semblez être des gens décents.';
    private const ROGUE = 'Il y avait un voleur qui traînait le marché ces derniers temps, j\'étais celui qui l\'a 
    découvert. Attraper en flagrant délit, et j\'lui ai dit de me donner la moitié !';
    private const SPRITE = 'Une fois, j\'ai dansé avec un esprit follet, au moins je pensais que c\'était un esprit 
    follet... Ils sont assez gros, n\'est-ce pas ?';
    private const LEPRECHAUN = 'Je jure devant les dieux! Il y avait un petit lutin derrière la taverne en train de 
    pisser. J\'ai dit "Hey copain!"';
    private const HORSE = 'Ma vieille jument, elle a été là depuis des années, mon grand-père me l\'a donné, c\'était 
    sa jument! Peux-tu croire ça ?';
    private const GAMBLER = 'Je ne joue jamais. Même si...';
    private const PRINTS = 'J\'ai vu des traces géantes l\'autre jour, juste devant ma maison !';
    private const GUARDS = 'Gardez un œil sur ces gardes, ils vous défonceront !';
    private const FISH = 'Attrapé le plus gros poisson l\'autre jour, pas de mensonge, c\'était gros comme ça...';
    private const PROPHECY = 'J\'ai dit "tu sais quoi, je ne crois vos prophéties!" Et tout ce qu\'ils ont fait, c\'est 
    de me dégager, et s\'éloigner. Peux-tu croire ça ?';
    private const HELPER = 'Je ne laisserais tomber personne, j\'ai passé ma vie à garder les gens en vie.';
    private const WORKER = 'Nous recherchons toujours ouvriers agricoles, tout ce que nous obtenons, c\'est des hommes 
    de main par ici, que se passe-t-il avec ça ?';
    private const ADVENTURERS = 'Je pourrais faire avec un peu moins d\'aventurier dans cette ville, ils entret et 
    boivent toute la bonne bière !';
    private const BLACKSMITH = 'Ce forgeron fait une armure d\'or! Enfin je pense que c\'était de l\'or, de toutes 
    façons, c\'était vraiment brillant !';
    private const IMAGINATION = 'Vous savez ce qui est bizarre, c\'est comme s\'il y a un sentiment d\'autre chose, 
    comme si nous étions tous dans l\'imagination d\'un génie fou, ou c\'est juste moi ?';
    private const MERROW = 'Jamais vu une sirène, j\'ai vu une chèvre nager une fois cependant.';
    private const CONFIDENT = 'Êtes-vous si sûr de tout le monde de votre groupe ?';
    private const PURGE = 'Honnêtement, cette ville a besoin d\'une purge, ou quelque chose comme ça, peut-être qu\'il 
    y a une potion que nous pouvons obtenir contre ces types politiques beudonnants ?';
    private const WORK = 'La seule façon de gagner votre vie ici est d\'aller au boulot, traînant partout dans le monde 
    à l\'aventure, ce n\'est pas du travail ! Labourer les champs est un travail.';
    private const SUPERSITIONS = 'Je n\'ai pas de superstitions idiotes à propos de mes dés de jeu.';
    private const HUNTING = 'J\'étais à la chasse l\'autre jour, j\'suis tombé sur un tas de cerfs au sud de la ville.';
    private const BEANS = 'J\'ai vu un mec sur la route, il essayait de me vendre un sac de haricots, j\'lui ai dit 
    d\'aller dans l\'autre sens, j\'ai pas besoin de haricots! J\'en ai tout le champ.';
    private const PRINCESS = 'Pourquoi est-ce que la princesse est toujours kidnappée ? Pourquoi tu n\'entends jamais 
    parler de prince en jambe étant kidnappé ? Tu sais si j\'étais apte, je pourrais essayer ça. Réécrire un peu 
    l\'histoire.';
    private const BOOK = 'J\'ai entendu parler de ce vieux livre, eh bien mon voisin parlait ce vieux livre. Sonnait un 
    peu intéressant, mais je ne sais pas lire. Avec runes bizarres dedans.';
    private const WEREWOLVES = 'Peut-être que ne pige pas bien, tu me dis que tu as vu une bande de loups-garous 
    l\'autre nuit, et le garde ne veut pas faire quoi que ce soit à ce sujet. Et maintenant tu dis que tu veux sortir 
    et les tuer ?';
    private const CHEESE = 'Je voudrais un casse-croûte, quelque chose avec du fromage.';
    private const HARDWORK = 'Je dois me lever tôt demain, beaucoup à faire. Creuser des fossés, et destombes.';
    private const GUILDS = 'J\'ai tendance à me demander si toutes ces guildes ténébreuses existent vraiment. Vous avez 
    entendu parler des assassins et des voleurs vraiment organisés ?';
    private const MARMELADE = 'C\'est vraiment une question de détails et de confiture de framboise.';
    private const TROLLS = 'Ce vieux mythe sur les trolls de la pierre du soleil est faux';
    private const SNAKES = 'Je ne plaisante pas, elle avait la tête pleine de serpents !';
    private const FIRE = 'Tu as déjà allumé un feu et jeté de l\'huile dessus ? C\'est hilarant !';
    private const CHILDREN = 'Non, je n\'ai pas vu vos enfants, vous devriez vraiment sortir et les trouver.';
    private const NICE = 'Et j\'ai dit "ouais, c\'est l\'un des gentils", hahahah.';
    private const STRANGE_CAT = 'Alors là, je me suis occupé de mes affaires et tout d\'un coup un chat étrange 
    est apparu dans un arbre.';
    private const BABARIAN = 'Ne sous-estimez jamais un barbare, ou ma femme en la matière !';
    private const ARMOR = 'Vous devriez probablement dépenser, il est temps de réparer votre armure, ça tient à peine 
    debout';
    private const RIVER = 'Il y a cette rivière qui coule des montagnes, incroyable, goûtez l\'eau, mais elle est 
    verte !';
    private const ORTHOGRAPH = 'Non, je n\'ai jamais entendu parler de ce dieu, êtes-vous sûr d\'avoir le bon 
    orthographe ?';
    private const UNVISIBLE = 'Vous auriez dû voir son visage quand je lui ai dit qu\'il était invisible. Ce chapelier 
    fou a couru de l\'autre côté de la rue pour voler des tomates.';
    private const GOBLIN = 'Je n\'ai jamais rencontré de gobelin avec un sens de l\'humour.';
    private const TRAPS = 'Je jure, chaque fois que je rencontre un aventurier, il me parle de tomber dans des pièges 
    à fosse, qui a conçu toutes ces choses ? Pourquoi continuez-vous de tomber dedans ? Vous penseriez qu\'une fois 
    suffit, non ?';
    private const ILLUSION = 'Non, tant pis... je pense que je viens de voir une illusion. Attends une seconde...';
    private const WILDERNESS = 'Vous savez ce qui est vraiment stupide ? Essayer de dormir dans la région sauvage.';
    private const BEDBUGS = 'C\'était le pire des cas de punaises de lit que j’ai déjà eu !';
    private const MUSTARD = 'Pardonne-moi ? tu sais si il y a de la moutarde grise à cette taverne ?';
    private const BEARD = 'Pourquoi tous les sorciers ont-ils la barbe de toutes façons ? Est-ce une sorte de truc de 
    club rituel que je n\'ai pas compris ? Et pourquoi des tours au milieu de nulle part ?';
    private const VAMPIRE = 'Bien sûr, vous avez vu un vampire, bien sûr.';
    private const HOODED = 'Ne trouvez-vous pas étrange qu\'il y ait toujours un homme assis dans le coin avec sa 
    capuche en essayant de ne pas attirer l\'attention sur lui-même ?';
    private const PIPES = 'Vous allez devoir lui parler, ce putain de barde jouait encore de la cornemuse !';
    private const CONFUSION = 'Je pense que vous me confondez avec quelqu\'un qui s\'en soucie.';
    private const EXPERIMENT = 'Ce fut une expérience religieuse incroyablement édifiante !';
    private const DRUNK = 'Pourquoi sautez-vous sur un pied ? Oh peu importe, je pense que j\'en ai eu un peu trop. 
    Que puis-je faire pour vous ?';
    private const UNCLE = 'Mon oncle a serré un ours dans ses bras, une fois.';
    private const LADY = 'Cette dame qui sert les boissons, quoi que vous fassiez, ne l\'appelez pas son prénom.';
    private const LAST_TIME = 'C\'est la dernière fois que je bois dans cet établissement !';
    private const BIG_MAN = 'Hé toi gros tas, tu veux aire un bras de fer ?';
    private const KISSING = 'Tenez ma bière pendant que j\'embrasse votre elfe.';
    private const LIZARDFOLK = 'Tout le monde sait qu\'il n\'y a pas de telles choses comme des hommes-lézards, c\'est 
    pour effrayer les enfants.';
    private const KING = 'Je ne dis pas que je suis le roi, je dis juste, avez-vous nous déjà vus ensemble dans la même 
    pièce en même temps ?';
    private const ORCS = 'Entendre la danse des Orques quand ils battent les aventuriers, est-ce vrai ?';
    private const TAVERN = 'Alors là, nous étions assis au Dragon Vert, nos pieds sur un tabouret, à écouter de la 
    bonne musique...';
    private const LIES = 'La plupart des histoires que vous entendrez ici sont peu probables et certaines des 
    mensonges.';
    private const FLAMMES = 'Si jamais vous êtes en feu, arrêtez tout, laissez vous tomber et rouler. Eh bien, si son 
    feu est magique, alors je n\'en ai aucune idée.';
    private const GOD = 'Le dieu doit être vraiment fou !';
    private const KNITTING = 'J\'aime tricoter, et toi ?';
    private const WHISKY = 'Avez-vous déjà bu du whisky avec un nain ? Moi, oui ! Et j\'ai toujours eu la gueule de 
    bois.';
    private const STORY = 'Il y a cette longue histoire sur une épée et une pierre, je ne suis pas sûr de la fin.';
    private const CONTRARY = 'Certains jours, vous êtes le chevalier, d\'autres jours le dragon.';
    private const NEVER_SAID = 'Je n\'ai jamais dit ça une seule fois. J\'ai dit...';
    private const CARAVANE = 'Mais bien sûr, il y a un marchand à la recherche d\'un gardien de caravane, pourquoi 
    demandez-vous ?';
    private const SCAR = 'Vous voyez cette cicatrice ici ? J\'ai eu ça quand je fuyais un loup-garou.';
    private const DARKVISION = 'N\'oubliez pas de toujours avoir quelqu\'un comme un elfe ou un nain dans votre groupe, 
    ces types peuvent voir dans l\'obscurité !';
    private const RATIONS = 'Il y a un agriculteur local qui vend d\'assez bonnes rations, si vous y allez en 
    randonnée.';
    private const SLEEP = 'Meilleur sommeil de tous les temps ! Non, ce n\'était pas dans cette auberge, je dis juste 
    que j\'ai vraiment bien dormi hier soir.';
    private const MINOTAUR = 'J\'ai entendu dire que certains marchands lâchent de l\'or pour une corne de Minotaure.';
    private const GET_OUT = 'Faites comme si je ne vous avais jamais rencontré avant. Allez-vous en.';
    private const RUINES = 'Je pensais jeter un œil autour des vieilles ruines. Qu\'est-ce que tu dis, tu veux venir ?';
    private const ARROW = 'AHHHHHHK! Quelqu\'un peut-il retirer cette flèche de mon épaule???';
}
