<?php

declare(strict_types=1);

namespace App\Enum\Tavern;

use App\Model\AbstractEnum;

/**
 * Class Service.
 *
 * @method static Service WORKER_UNQUALIFIED()
 * @method static Service WORKER_QUALIFIED()
 * @method static Service MESSENGER()
 * @method static Service TRANSPORT_INTRA()
 * @method static Service TRANSPORT_EXTRA()
 * @method static Service TRANSPORT_BOAT()
 * @method static Service PRIVATE_GUARD()
 * @method static Service PROSTITUTE()
 * @method static Service HUNTING_PARTY()
 * @method static Service GATHERING_PARTY()
 */
class Service extends AbstractEnum
{
    private const WORKER_UNQUALIFIED = [
        'name' => 'Embaucher un travailleur non qualifié',
        'price' => '2 PA/j',
    ];

    private const WORKER_QUALIFIED = [
        'name' => 'Embaucher un travailleur qualifié',
        'price' => '2 PO/j',
    ];

    private const MESSENGER = [
        'name' => 'Embaucher un messager',
        'price' => '2 PC/km',
    ];

    private const TRANSPORT_INTRA = [
        'name' => 'Transport en ville',
        'price' => '1 PC/j',
    ];

    private const TRANSPORT_EXTRA = [
        'name' => 'Transport entre deux villes',
        'price' => '2 PC/1,5km',
    ];

    private const TRANSPORT_BOAT = [
        'name' => 'Transport en bâteau',
        'price' => '1 PA/1,5km',
    ];

    private const PRIVATE_GUARD = [
        'name' => 'Garde personnel',
        'price' => '5 PA/j',
    ];

    private const PROSTITUTE = [
        'name' => 'Prostitué(e)',
        'price' => '1 PO/j minimum',
    ];

    private const HUNTING_PARTY = [
        'name' => 'Groupe de chasse',
        'price' => '1 PO/j/pnj',
    ];

    private const GATHERING_PARTY = [
        'name' => 'Groupe de cueilleur',
        'price' => '5 PA/j/pnj',
    ];
}
