<?php

declare(strict_types=1);

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class BugType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                self::getEmailParams()
            )
            ->add(
                'message',
                TextareaType::class,
                self::getMessageParams()
            )
            ->add(
                'miellat',
                TextType::class,
                self::getMiellatParams()
            )
            ->add('Envoyer', SubmitType::class)
        ;
    }

    /**
     * @return array<string, mixed>
     */
    private static function getMiellatParams(): array
    {
        return [
            'required' => false,
            'attr' => [
                'class' => 'input is-hidden',
            ],
            'label_attr' => [
                'class' => 'label is-hidden',
            ],
            'row_attr' => [
                'class' => 'field is-hidden',
            ],
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    private static function getMessageParams(): array
    {
        return [
            'attr' => [
                'class' => 'textarea',
                'placeholder' => 'Le lien vers l\'accueil ne fonctionne plus !',
            ],
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    private static function getEmailParams(): array
    {
        return [
            'attr' => [
                'class' => 'input',
                'placeholder' => 'ddbuilder@email.fr',
            ],
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }
}
