<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Environment\Disease;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class DiseaseType extends AbstractType
{
    public const DISEASE = 'Maladie';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::DISEASE,
                ChoiceType::class,
                [
                    'choices' => Disease::getNames(),
                    'label' => 'Choisir une maladie *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
