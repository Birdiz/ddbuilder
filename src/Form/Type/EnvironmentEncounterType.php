<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Environment\Encounter\Environment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class EnvironmentEncounterType extends AbstractType
{
    public const ENVIRONMENT = 'Environment';

    /**
     * @codeCoverageIgnore
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::ENVIRONMENT,
                ChoiceType::class,
                [
                    'choices' => array_flip(Environment::toArray()),
                    'label' => 'Type de terrain *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
