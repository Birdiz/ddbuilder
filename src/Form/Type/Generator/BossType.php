<?php

declare(strict_types=1);

namespace App\Form\Type\Generator;

use App\Enum\Danger;
use App\Enum\Level;
use App\Enum\Stuff;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class BossType extends AbstractType
{
    public const LEVEL = 'Niveau';
    public const DANGER = 'Danger';
    public const STUFF = 'Stuff';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::LEVEL,
                ChoiceType::class,
                self::getLevelParams()
            )
            ->add(
                static::DANGER,
                ChoiceType::class,
                self::getDangerParams()
            )
            ->add(
                static::STUFF,
                ChoiceType::class,
                self::getStuffParams()
            )
            ->add('OK', SubmitType::class)
        ;
    }

    /**
     * @return array<string, mixed>
     */
    private static function getStuffParams(): array
    {
        return [
            'choices' => array_flip(Stuff::toArray()),
            'label' => 'Équipement *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getDangerParams(): array
    {
        return [
            'choices' => array_flip(Danger::toArray()),
            'label' => 'Difficulté *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getLevelParams(): array
    {
        return [
            'choices' => array_flip(Level::toArray()),
            'label' => 'Niveau général des joueurs *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }
}
