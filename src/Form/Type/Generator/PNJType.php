<?php

declare(strict_types=1);

namespace App\Form\Type\Generator;

use App\Enum\Generator\PNJ\Alignment;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Job\Academic;
use App\Enum\Generator\PNJ\Job\Aristocrat;
use App\Enum\Generator\PNJ\Job\Builder;
use App\Enum\Generator\PNJ\Job\Clergy;
use App\Enum\Generator\PNJ\Job\Craftsperson;
use App\Enum\Generator\PNJ\Job\Criminal;
use App\Enum\Generator\PNJ\Job\Entertainer;
use App\Enum\Generator\PNJ\Job\Farmer;
use App\Enum\Generator\PNJ\Job\Financier;
use App\Enum\Generator\PNJ\Job\Healer;
use App\Enum\Generator\PNJ\Job\Hosteler;
use App\Enum\Generator\PNJ\Job\Laborer;
use App\Enum\Generator\PNJ\Job\Merchant;
use App\Enum\Generator\PNJ\Job\Military;
use App\Enum\Generator\PNJ\Job\Outdoor;
use App\Enum\Generator\PNJ\Job\PublicServant;
use App\Enum\Generator\PNJ\Job\Servant;
use App\Enum\Generator\PNJ\Job\Unemployed;
use App\Enum\Generator\PNJ\Race\Race;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;

class PNJType extends AbstractType
{
    public const RACE = 'Race';
    public const GENDER = 'Gender';
    public const JOB = 'Job';
    public const ALIGNMENT = 'Alignment';
    public const FAMILY_MEMBERS = 'Family_Members';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::RACE,
                ChoiceType::class,
                self::getRaceParams()
            )
            ->add(
                static::GENDER,
                ChoiceType::class,
                self::getGenderParams()
            )
            ->add(
                static::ALIGNMENT,
                ChoiceType::class,
                self::getAlignmentParams()
            )
            ->add(
                static::JOB,
                ChoiceType::class,
                self::getJobParams()
            )
            ->add(
                static::FAMILY_MEMBERS,
                IntegerType::class,
                self::getFamilyMembersParams()
            )
            ->add('OK', SubmitType::class)
        ;
    }

    /**
     * @return array<string, mixed>
     */
    private static function getFamilyMembersParams(): array
    {
        return [
            'label' => 'Famille (max. 5)',
            'attr' => [
                'class' => 'input',
            ],
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'data' => 3,
            'required' => false,
            'constraints' => [
                new Positive(),
                new LessThanOrEqual(5),
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getJobParams(): array
    {
        return [
            'choices' => [
                'Académique' => array_flip(Academic::toArray()),
                'Aristocrate' => array_flip(Aristocrat::toArray()),
                'Constructeur' => array_flip(Builder::toArray()),
                'Religieux' => array_flip(Clergy::toArray()),
                'Artisan' => array_flip(Craftsperson::toArray()),
                'Criminel' => array_flip(Criminal::toArray()),
                'Artiste' => array_flip(Entertainer::toArray()),
                'Agriculteur' => array_flip(Farmer::toArray()),
                'Financier' => array_flip(Financier::toArray()),
                'Guérisseur' => array_flip(Healer::toArray()),
                'Hôtellerie' => array_flip(Hosteler::toArray()),
                'Ouvrier' => array_flip(Laborer::toArray()),
                'Marchand' => array_flip(Merchant::toArray()),
                'Militaire' => array_flip(Military::toArray()),
                'Extérieur' => array_flip(Outdoor::toArray()),
                'Fonctionnaire' => array_flip(PublicServant::toArray()),
                'Serviteur' => array_flip(Servant::toArray()),
                'Sans emploi' => array_flip(Unemployed::toArray()),
            ],
            'label' => 'Métier',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'required' => false,
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getAlignmentParams(): array
    {
        return [
            'choices' => array_flip(Alignment::toArray()),
            'label' => 'Alignement',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'required' => false,
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGenderParams(): array
    {
        return [
            'choices' => array_flip(Gender::toArray()),
            'label' => 'Civilité *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getRaceParams(): array
    {
        return [
            'choices' => Race::getNames(),
            'label' => 'Race *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }
}
