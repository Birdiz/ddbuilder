<?php

declare(strict_types=1);

namespace App\Form\Type\Generator;

use App\Enum\Generator\Place\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PlaceType extends AbstractType
{
    public const TYPE = 'Type';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::TYPE,
                ChoiceType::class,
                [
                    'choices' => array_flip(Type::toArray()),
                    'label' => 'Type de lieu *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
