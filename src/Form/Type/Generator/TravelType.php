<?php

declare(strict_types=1);

namespace App\Form\Type\Generator;

use App\Enum\Generator\Travel\Pace;
use App\Enum\Level;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;

class TravelType extends AbstractType
{
    public const PACE = 'Pace';
    public const DAYS = 'Days';
    public const PLAYERS = 'Players';
    public const LEVEL = 'Level';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::PACE,
                ChoiceType::class,
                self::getPaceParams()
            )
            ->add(
                static::DAYS,
                IntegerType::class,
                self::getDaysParams()
            )
            ->add(
                static::PLAYERS,
                IntegerType::class,
                self::getPlayersParams()
            )
            ->add(
                static::LEVEL,
                ChoiceType::class,
                self::getLevelParams()
            )
            ->add('OK', SubmitType::class)
        ;
    }

    /**
     * @return array<string, mixed>
     */
    private static function getLevelParams(): array
    {
        return [
            'choices' => array_flip(Level::toArray()),
            'label' => 'Niveau général des joueurs *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getPaceParams(): array
    {
        return [
            'choices' => Pace::getNames(),
            'label' => 'Vitesse de voyage *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getDaysParams(): array
    {
        return [
            'attr' => [
                'class' => 'input',
            ],
            'data' => 1,
            'label' => 'Durée en jour (max. 31) *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'constraints' => [
                new Positive(),
                new LessThanOrEqual(31),
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getPlayersParams(): array
    {
        return [
            'attr' => [
                'class' => 'input',
            ],
            'data' => 3,
            'label' => 'Nombre de joueurs *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'constraints' => [
                new Positive(),
            ],
        ];
    }
}
