<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Environment\Madness;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class MadnessType extends AbstractType
{
    public const MADNESS = 'Folie';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::MADNESS,
                ChoiceType::class,
                [
                    'choices' => Madness::getNames(),
                    'label' => 'Choisir une folie *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
