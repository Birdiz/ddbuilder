<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class MagicItemType extends AbstractType
{
    public const ITEM = 'item';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $list = self::getList();

        $builder
            ->add(
                static::ITEM,
                ChoiceType::class,
                [
                    'choices' => $list,
                    'label' => 'Choisir un objet *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                    'required' => false,
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }

    /**
     * @return array<string, string>
     */
    private static function getList(): array
    {
        $list = array_merge(
            array_column(TableA::toArray(), 'name', 'name'),
            array_column(TableB::toArray(), 'name', 'name'),
            array_column(TableC::toArray(), 'name', 'name'),
            array_column(TableD::toArray(), 'name', 'name'),
            array_column(TableE::toArray(), 'name', 'name'),
            array_column(TableF::toArray(), 'name', 'name'),
            array_column(TableG::toArray(), 'name', 'name'),
            array_column(TableH::toArray(), 'name', 'name'),
            array_column(TableI::toArray(), 'name', 'name')
        );

        asort($list);

        return $list;
    }
}
