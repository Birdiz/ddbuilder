<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Environment\Month;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class MonthType extends AbstractType
{
    public const MONTH = 'Mois';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::MONTH,
                ChoiceType::class,
                [
                    'choices' => array_flip(Month::toArray()),
                    'label_attr' => [
                        'style' => 'display:none;',
                    ],
                    'row_attr' => [
                        'class' => 'field is-horizontal',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
