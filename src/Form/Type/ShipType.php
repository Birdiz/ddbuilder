<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Sailing\Ship\Ship;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class ShipType extends AbstractType
{
    public const NAME = 'Nom';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::NAME,
                ChoiceType::class,
                [
                    'choices' => Ship::getNames(),
                    'label' => 'Type de bâtiment *',
                    'label_attr' => [
                        'class' => 'label',
                    ],
                    'row_attr' => [
                        'class' => 'field',
                    ],
                ]
            )
            ->add('OK', SubmitType::class)
        ;
    }
}
