<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Enum\Dungeon\Treasure\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\Positive;

class TreasureType extends AbstractType
{
    public const TYPE = 'Type';
    public const CHEST = 'Coffres';
    public const OPTIONS = 'Options';

    public const NONE = 'Aucune';
    public const HOARD = 'Butin';
    public const COINS = 'Pièces';
    public const GEMMES_ART = 'Gemmes & Art';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                static::TYPE,
                ChoiceType::class,
                self::getTypeParams()
            )
            ->add(
                static::CHEST,
                IntegerType::class,
                self::getChestParams()
            )
            ->add(
                static::OPTIONS,
                ChoiceType::class,
                self::getOptionsParams()
            )
            ->add('OK', SubmitType::class)
        ;
    }

    /**
     * @return array<string, mixed>
     */
    private static function getTypeParams(): array
    {
        return [
            'choices' => array_flip(Type::toArray()),
            'label' => 'Palier des joueurs *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getChestParams(): array
    {
        return [
            'attr' => [
                'class' => 'input',
            ],
            'data' => 1,
            'label' => 'Coffre(s) (max. 6) *',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'constraints' => [
                new Positive(),
                new LessThanOrEqual(6),
            ],
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getOptionsParams(): array
    {
        return [
            'label' => 'Options :',
            'label_attr' => [
                'class' => 'label',
            ],
            'row_attr' => [
                'class' => 'field',
            ],
            'choices' => [
                static::NONE => '',
                static::HOARD => static::HOARD,
                static::COINS => static::COINS,
                static::GEMMES_ART => static::GEMMES_ART,
            ],
            'choice_attr' => [
                static::NONE => ['class' => 'radio'],
                static::HOARD => ['class' => 'radio'],
                static::COINS => ['class' => 'radio'],
                static::GEMMES_ART => ['class' => 'radio'],
            ],
            'expanded' => true,
            'required' => false,
        ];
    }
}
