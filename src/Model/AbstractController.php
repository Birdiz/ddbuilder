<?php

declare(strict_types=1);

namespace App\Model;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyAbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractController extends SymfonyAbstractController
{
    /**
     * @param string|array<string> $input
     *
     * @return array<string>|string|null
     */
    public static function getEnumKeyFromInputName(
        Request $request,
        FormInterface $form,
        array|string $input
    ): array|string|null {
        $form->handleRequest($request);
        if (true === $form->isSubmitted() && true === $form->isValid()) {
            $data = $form->getData();

            if (true === is_array($input)) {
                $keys = [];
                foreach ($input as $item) {
                    $keys[$item] = $data[$item] ?? null;
                }

                return $keys;
            }

            return $data[$input] ?? null;
        }

        return null;
    }
}
