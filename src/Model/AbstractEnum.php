<?php

declare(strict_types=1);

namespace App\Model;

use MyCLabs\Enum\Enum;

abstract class AbstractEnum extends Enum
{
    /**
     * @param array<mixed> $enums
     */
    public static function getRandomEnum(array $enums, bool $withKey = false): mixed
    {
        $values = array_values($enums);
        $valuesLength = count($values);
        $idx = 1 === $valuesLength ? 0 : random_int(0, $valuesLength - 1);
        $value = $values[$idx];

        if (true === $withKey) {
            $keys = array_keys($enums);
        }

        return true === $withKey ? ['key' => $keys[$idx], 'value' => $value] : $value;
    }
}
