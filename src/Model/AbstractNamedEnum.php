<?php

declare(strict_types=1);

namespace App\Model;

abstract class AbstractNamedEnum extends AbstractEnum
{
    /**
     * @return array<string>
     */
    public static function getNames(): array
    {
        return array_column(static::toArray(), 'name', 'name');
    }

    /**
     * @return array<string, mixed>
     */
    public static function getByName(): array
    {
        return array_combine(
            array_column(static::toArray(), 'name'),
            static::toArray()
        );
    }

    /**
     * @return array<string, mixed>
     */
    public static function getFromName(string $raceName): array
    {
        return static::getByName()[$raceName];
    }

    /**
     * @param array<mixed> $values
     *
     * @return array<mixed>|null
     */
    public static function getValueFromNameKey(?string $name, array $values): ?array
    {
        if (false === is_string($name)) {
            return null;
        }

        $value = array_combine(
            array_column(static::toArray(), 'name'),
            $values,
        );

        return $value[$name];
    }
}
