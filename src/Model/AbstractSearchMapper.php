<?php

declare(strict_types=1);

namespace App\Model;

use App\Contract\Searcher\SearchMapperInterface;

abstract class AbstractSearchMapper implements SearchMapperInterface
{
    public const ADVENTURE = 'adventure';
    public const ALCHEMY = 'alchemy';
    public const ARSENAL = 'arsenal';
    public const DM_SCREEN = 'dm_screen';
    public const DUNGEON = 'dungeon';
    public const ENVIRONMENT = 'environment';
    public const EXTRA_RULES = 'extra_rules';
    public const PNJ = 'generator_pnj';
    public const BOSS = 'generator_boss';
    public const PLACE = 'generator_place';
    public const TRAVEL = 'generator_travel';
    public const MAGIC_TRADING_POST = 'magic_trading_post';
    public const SAILING = 'sailing';
    public const SIDEKICK = 'sidekick';
    public const TAVERN = 'tavern';

    /**
     * @return array<string, array<int, string>>
     */
    abstract public static function values(): array;

    /**
     * @return array<string, array<int, string>>
     */
    abstract public static function paths(): array;

    /**
     * @param array<string>|null $realPaths
     *
     * @return array<string>|null
     */
    public function map(?array $realPaths): ?array
    {
        if (null === $realPaths) {
            return null;
        }

        $routeNames = [];
        foreach (static::paths() as $routeName => $paths) {
            foreach ($paths as $path) {
                if (in_array($path, $realPaths, true)) {
                    $routeNames[$routeName] = $routeName;
                }
            }
        }

        return $routeNames;
    }
}
