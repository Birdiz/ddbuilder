<?php

declare(strict_types=1);

namespace App\Model;

use App\Contract\WeightedInterface;

abstract class AbstractWeightedEnum extends AbstractEnum implements WeightedInterface
{
    /**
     * @return array<int>
     */
    abstract public static function getWeights(): array;
}
