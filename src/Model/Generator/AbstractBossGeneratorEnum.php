<?php

declare(strict_types=1);

namespace App\Model\Generator;

use App\Enum\Danger;
use App\Model\AbstractEnum;

abstract class AbstractBossGeneratorEnum extends AbstractEnum
{
    /**
     * @return array<int, array<string, mixed>>
     */
    abstract public static function getMobs(string $bossKey, string $danger): array;

    /**
     * @return array<string, mixed>
     */
    public static function getBoss(int $fp): array
    {
        $bosses = array_filter(
            self::toArray(),
            static function (array $boss) use ($fp) {
                return $fp === $boss['fp'];
            }
        );

        return static::getRandomEnum($bosses, true);
    }

    protected static function getNbMobs(string $danger, bool $isHalved = false): float
    {
        $numbersByDanger = [
            Danger::EASY()->getKey() => 1,
            Danger::MEDIUM()->getKey() => random_int(1, 3),
            Danger::HARD()->getKey() => random_int(2, 5),
            Danger::MORTAL()->getKey() => random_int(3, 7),
        ];

        return true === $isHalved ? round($numbersByDanger[$danger] / 2) : $numbersByDanger[$danger];
    }
}
