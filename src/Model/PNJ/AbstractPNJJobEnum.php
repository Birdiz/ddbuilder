<?php

declare(strict_types=1);

namespace App\Model\PNJ;

use App\Enum\Generator\PNJ\Job\Academic;
use App\Enum\Generator\PNJ\Job\Aristocrat;
use App\Enum\Generator\PNJ\Job\Builder;
use App\Enum\Generator\PNJ\Job\Clergy;
use App\Enum\Generator\PNJ\Job\Craftsperson;
use App\Enum\Generator\PNJ\Job\Criminal;
use App\Enum\Generator\PNJ\Job\Entertainer;
use App\Enum\Generator\PNJ\Job\Farmer;
use App\Enum\Generator\PNJ\Job\Financier;
use App\Enum\Generator\PNJ\Job\Healer;
use App\Enum\Generator\PNJ\Job\Hosteler;
use App\Enum\Generator\PNJ\Job\Laborer;
use App\Enum\Generator\PNJ\Job\Merchant;
use App\Enum\Generator\PNJ\Job\Military;
use App\Enum\Generator\PNJ\Job\Outdoor;
use App\Enum\Generator\PNJ\Job\PublicServant;
use App\Enum\Generator\PNJ\Job\Servant;
use App\Enum\Generator\PNJ\Job\Unemployed;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;

abstract class AbstractPNJJobEnum extends AbstractEnum
{
    use ArrayRandTrait;

    /**
     * @return array<string, string>
     */
    public static function getAllJobs(): array
    {
        return array_merge(
            Academic::toArray(),
            Aristocrat::toArray(),
            Builder::toArray(),
            Clergy::toArray(),
            Craftsperson::toArray(),
            Criminal::toArray(),
            Entertainer::toArray(),
            Farmer::toArray(),
            Financier::toArray(),
            Healer::toArray(),
            Hosteler::toArray(),
            Laborer::toArray(),
            Merchant::toArray(),
            Military::toArray(),
            Outdoor::toArray(),
            PublicServant::toArray(),
            Servant::toArray(),
            Unemployed::toArray()
        );
    }

    public static function getJobName(?string $key): ?string
    {
        if (false === is_string($key)) {
            return null;
        }

        return static::getAllJobs()[$key] ?? null;
    }

    public static function getRandomJob(): string
    {
        return static::getXSimpleItems(1, static::getAllJobs());
    }
}
