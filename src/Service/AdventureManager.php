<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Adventure\Event;
use App\Enum\Adventure\Plane\Layer;
use App\Enum\Adventure\Plane\Plane;
use App\Enum\Adventure\PlotHook;
use App\Enum\Adventure\Prophecy;
use App\Enum\Dungeon\Chase\Escape;
use App\Enum\Dungeon\Chase\Urban;
use App\Enum\Dungeon\Chase\Wilderness;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Util\ArrayRandTrait;
use App\Util\WeightedRandTrait;

class AdventureManager extends AbstractManager
{
    use WeightedRandTrait;
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getRandomInfo(): array
    {
        return [
            'event' => AbstractEnum::getRandomEnum(Event::toArray()),
            'plane' => self::getRandomPlaneInfo(),
            'prophecy' => AbstractEnum::getRandomEnum(Prophecy::toArray()),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRandomChase(): array
    {
        return [
            'environment' => static::getXSimpleItems(1, Escape::getRandomEnum(Escape::toArray())),
            'urban' => static::getRandomizedValue(Urban::toArray(), Urban::getWeigths()),
            'wilderness' => static::getRandomizedValue(Wilderness::toArray(), Wilderness::getWeigths()),
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getRandomPlotHook(): array
    {
        return AbstractEnum::getRandomEnum(PlotHook::toArray());
    }

    /**
     * @return array<string, string>
     */
    private static function getRandomPlaneInfo(): array
    {
        $plane = static::getRandomizedValue(Plane::toArray(), Plane::getWeights());

        return [
            'plane' => $plane,
            'subplane' => Layer::getLayer($plane),
        ];
    }
}
