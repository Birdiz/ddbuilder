<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Arsenal\Ammunition;
use App\Enum\Arsenal\ExtendedArmor\Craftmanship as CraftmanshipArmor;
use App\Enum\Arsenal\ExtendedArmor\HeavyArmor;
use App\Enum\Arsenal\ExtendedArmor\LightArmor;
use App\Enum\Arsenal\ExtendedArmor\MediumArmor;
use App\Enum\Arsenal\ExtendedWeapon\Craftmanship as CraftmanshipWeapon;
use App\Enum\Arsenal\ExtendedWeapon\MartialMelee;
use App\Enum\Arsenal\ExtendedWeapon\MartialRanged;
use App\Enum\Arsenal\ExtendedWeapon\Shield;
use App\Enum\Arsenal\ExtendedWeapon\SimpleMelee;
use App\Enum\Arsenal\ExtendedWeapon\SimpleRanged;
use App\Enum\Arsenal\Firearm\Ammunition as FirearmAmmunition;
use App\Enum\Arsenal\Firearm\MagicAmmunition;
use App\Enum\Arsenal\Firearm\MartialFirearm;
use App\Enum\Arsenal\Firearm\SimpleFirearm;
use App\Enum\Arsenal\Silvering;
use App\Model\AbstractManager;

class ArsenalManager extends AbstractManager
{
    /**
     * @return array<string, array<string, string>>
     */
    public static function getExtendedWeapons(): array
    {
        return [
            'Armes courantes de corps à corps' => SimpleMelee::toArray(),
            'Armes courantes à distance' => SimpleRanged::toArray(),
            'Armes de guerre de corps à corps' => MartialMelee::toArray(),
            'Armes de guerre à distance' => MartialRanged::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getExtendedShields(): array
    {
        return Shield::toArray();
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public static function getFirearms(): array
    {
        return [
            'simple_firearms' => SimpleFirearm::toArray(),
            'martial_firearms' => MartialFirearm::toArray(),
            'ammunitions' => FirearmAmmunition::toArray(),
            'magical_ammunitions' => MagicAmmunition::getPricedMagicAmmunitions(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getAdditionalRules(): array
    {
        return [
            'craftmanship_armor' => CraftmanshipArmor::toArray(),
            'craftmanship_weapon' => CraftmanshipWeapon::toArray(),
            'silvering' => Silvering::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getExtendedArmors(): array
    {
        return [
            'Armures légères' => LightArmor::toArray(),
            'Armures intermédiaires' => MediumArmor::toArray(),
            'Armures lourdes' => HeavyArmor::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getExtendedAmmunitions(): array
    {
        return Ammunition::toArray();
    }
}
