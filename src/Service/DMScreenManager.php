<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\DMScreen\Building\Building;
use App\Enum\DMScreen\Building\Maintenance;
use App\Enum\DMScreen\Damage;
use App\Enum\DMScreen\Lifestyle;
use App\Enum\DMScreen\Vehicule\Boat;
use App\Enum\DMScreen\Vehicule\Mounting\Equipment;
use App\Enum\DMScreen\Vehicule\Mounting\Mount;
use App\Model\AbstractManager;
use App\Util\ArrayTrait;

class DMScreenManager extends AbstractManager
{
    use ArrayTrait;

    /**
     * @return array<string, array<string, string>>
     */
    public static function getDamageInfo(): array
    {
        return Damage::formatDamage();
    }

    /**
     * @return array<string, mixed>
     */
    public static function getVehiculeInfo(): array
    {
        $mounts = Mount::toArray();
        $equipments = Equipment::toArray();
        $boats = Boat::toArray();

        static::sortBy($mounts, 'name');
        static::sortBy($equipments, 'name');
        static::sortBy($boats, 'name');

        return [
            'mounting' => [
                'mount' => $mounts,
                'equipment' => $equipments,
            ],
            'boats' => $boats,
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getBuildingInfo(): array
    {
        $buildings = Building::toArray();
        $maintenances = Maintenance::toArray();

        static::sortBy($buildings, 'name');
        static::sortBy($maintenances, 'name');

        return [
            'building' => $buildings,
            'maintenance' => $maintenances,
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getLifestyles(): array
    {
        $lifestyles = Lifestyle::toArray();
        foreach (array_keys($lifestyles) as $key) {
            $lifestyles[$key]['services'] = Lifestyle::getServices($key);
        }

        return $lifestyles;
    }
}
