<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Dungeon\Adventure;
use App\Enum\Dungeon\Air;
use App\Enum\Dungeon\Door;
use App\Enum\Dungeon\Noise;
use App\Enum\Dungeon\Odor;
use App\Enum\Dungeon\Puzzle;
use App\Enum\Dungeon\Riddle;
use App\Enum\Dungeon\Trap\Effect;
use App\Enum\Dungeon\Trap\Trap;
use App\Enum\Dungeon\Trap\Trigger;
use App\Enum\Dungeon\Treasure\Type;
use App\Enum\Object\Size;
use App\Form\Type\TreasureType;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Service\Helper\Dungeon\TrapPriceHelper;
use App\Util\HitPointTrait;
use App\Util\WeightedRandTrait;

class DungeonManager extends AbstractManager
{
    use HitPointTrait;
    use WeightedRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getRandomTrap(): array
    {
        $effect = AbstractEnum::getRandomEnum(Effect::toArray());

        return [
            'trap' => AbstractEnum::getRandomEnum(Trap::toArray()),
            'trigger' => AbstractEnum::getRandomEnum(Trigger::toArray()),
            'price' => TrapPriceHelper::getRandomTrapPrice($effect['name']),
            'effect' => $effect,
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRandomInfo(): array
    {
        return [
            'door' => self::getRandomDoor(),
            'air' => AbstractEnum::getRandomEnum(Air::toArray()),
            'odor' => AbstractEnum::getRandomEnum(Odor::toArray()),
            'noise' => AbstractEnum::getRandomEnum(Noise::toArray()),
            'adventure' => AbstractEnum::getRandomEnum(Adventure::toArray()),
            'riddle' => AbstractEnum::getRandomEnum(Riddle::toArray()),
            'puzzle' => AbstractEnum::getRandomEnum(Puzzle::toArray()),
        ];
    }

    /**
     * @param array<string|int>|null $keys
     *
     * @return array<int, array<string, mixed>>|null
     */
    public static function getRandomTreasure(?array $keys): ?array
    {
        if (null === $keys) {
            return null;
        }

        return match ($keys[TreasureType::OPTIONS]) {
            TreasureType::HOARD => Type::getHoardFromKey($keys[TreasureType::TYPE], $keys[TreasureType::CHEST]),
            TreasureType::COINS => Type::getTreasureFromKey(
                $keys[TreasureType::TYPE],
                $keys[TreasureType::CHEST],
                true
            ),
            TreasureType::GEMMES_ART => Type::getGemsAndArtFromKey(
                $keys[TreasureType::TYPE],
                $keys[TreasureType::CHEST]
            ),
            default => Type::getTreasureFromKey($keys[TreasureType::TYPE], $keys[TreasureType::CHEST])
        };
    }

    /**
     * @return array<int, mixed>
     */
    private static function getRandomDoor(): array
    {
        $doorValue = AbstractEnum::getRandomEnum(Door::toArray());
        $doorHitPoints = static::getHitPointsForObject(
            Door::getIntegrityFromValue($doorValue),
            Size::BIG(),
            Door::getMaterialFromValue($doorValue)
        );

        return [
            $doorValue,
            [
                Door::getACFromValue($doorValue),
                $doorHitPoints,
            ],
        ];
    }
}
