<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Environment\Adventure;
use App\Enum\Environment\Disease;
use App\Enum\Environment\Encounter\Encounter;
use App\Enum\Environment\Foraging;
use App\Enum\Environment\Madness;
use App\Enum\Environment\Metal;
use App\Enum\Environment\Month;
use App\Enum\Environment\Rumor;
use App\Enum\Environment\Weather\Direction;
use App\Enum\Environment\Weather\Precipitation;
use App\Enum\Environment\Weather\TemperatureEffect;
use App\Enum\Environment\Weather\Wind;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Util\DiceTrait;
use App\Util\ForecastTrait;
use App\Util\WeightedRandTrait;

class EnvironmentManager extends AbstractManager
{
    use ForecastTrait;
    use WeightedRandTrait;
    use DiceTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getRandomForecast(?string $monthKey = null): array
    {
        $precipitations = Precipitation::toArray();

        if (null !== $monthKey && \array_key_exists($monthKey, Month::values())) {
            $precipitationValue = static::getRandomizedValue(
                $precipitations,
                Month::getPrecipitationWeigthsByMonthKey($monthKey)
            );
        } else {
            $precipitationValue = AbstractEnum::getRandomEnum($precipitations);
        }

        $precipitationKey = array_flip($precipitations)[$precipitationValue];

        $temperatureEffectValue = AbstractEnum::getRandomEnum(TemperatureEffect::toArray());
        $temperature = static::getTemperature(
            TemperatureEffect::getTemperatureEffectFromValue($temperatureEffectValue),
            $precipitationKey
        );

        return [
            'icon' => Precipitation::getPrecipitationIconFromKey($precipitationKey),
            'precipitation' => $precipitationValue,
            'wind' => AbstractEnum::getRandomEnum(Wind::toArray()),
            'direction' => AbstractEnum::getRandomEnum(Direction::toArray()),
            'temperature' => $temperature,
            'month' => Month::getValueFromKey($monthKey),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRandomInfo(): array
    {
        $encounter = AbstractEnum::getRandomEnum(Encounter::toArray());
        $encounter['result'] = static::compute($encounter['dices'], $encounter['dice'], $encounter['mod']);

        return [
            'wind' => AbstractEnum::getRandomEnum(Wind::toArray()),
            'precipitation' => AbstractEnum::getRandomEnum(Precipitation::toArray()),
            'temperature' => AbstractEnum::getRandomEnum(TemperatureEffect::toArray()),
            'foraging' => static::getRandomForaging(true),
            'adventure' => AbstractEnum::getRandomEnum(Adventure::toArray()),
            'encounter' => $encounter,
            'rumor' => AbstractEnum::getRandomEnum(Rumor::toArray()),
        ];
    }

    /**
     * @return array<int, mixed>|null
     */
    public static function getDisease(?string $diseaseKey): ?array
    {
        return Disease::getValueFromNameKey($diseaseKey, Disease::toArray());
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getMetals(): array
    {
        return Metal::toArray();
    }

    /**
     * @return array<int, mixed>|null
     */
    public static function getMadness(?string $madnessKey): ?array
    {
        return Madness::getValueFromNameKey($madnessKey, Madness::toArray());
    }

    /**
     * @return array<int, mixed>
     */
    public static function getRandomForaging(bool $stringify = false): array
    {
        $foragingValue = AbstractEnum::getRandomEnum(Foraging::toArray());

        return [$foragingValue, Foraging::getDDFromValue($foragingValue, $stringify)];
    }
}
