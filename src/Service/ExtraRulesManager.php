<?php

namespace App\Service;

use App\Enum\ExtraRules\HiddenSkill;
use App\Model\AbstractManager;

class ExtraRulesManager extends AbstractManager
{
    /**
     * @return array<string, string>
     */
    public static function getHiddenSkills(): array
    {
        return HiddenSkill::toArray();
    }
}
