<?php

declare(strict_types=1);

namespace App\Service\Generator;

use App\Enum\Generator\Boss\Archetype;
use App\Enum\Level;
use App\Form\Type\Generator\BossType;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Model\Generator\AbstractBossGeneratorEnum;
use App\Service\Helper\Generator\Boss\BountyHelper;
use App\Service\Helper\Generator\Boss\FPHelper;
use App\Service\Helper\Generator\Boss\RoomHelper;
use App\Util\FamilyTrait;
use App\Util\WeightedRandTrait;

class BossManager extends AbstractManager
{
    use WeightedRandTrait;
    use FamilyTrait;

    /**
     * @param array<string>|null $keys
     *
     * @return array<string, mixed>|null
     */
    public static function getRandomBoss(?array $keys): ?array
    {
        if (null === $keys) {
            return null;
        }

        $level = Level::getValueFromKey($keys[BossType::LEVEL]);
        $danger = $keys[BossType::DANGER];

        $fp = FPHelper::getFP(
            $level,
            $danger,
            $keys[BossType::STUFF]
        );

        return self::getRandomBossSet((int) $fp, $level, $danger);
    }

    /**
     * @return array<string, mixed>
     */
    private static function getRandomBossSet(int $fp, int $level, string $danger): array
    {
        /** @var AbstractBossGeneratorEnum $class */
        $class = FPHelper::getBossClassFromFP($fp);
        $boss = $class::getBoss($fp);
        $mobs = $class::getMobs($boss['key'], $danger);

        return [
            'boss' => $boss['value'],
            'archetype' => AbstractEnum::getRandomEnum(Archetype::toArray()),
            'bounty' => BountyHelper::getBounty($fp, $level, $danger),
            'mobs' => $mobs,
            'rooms' => RoomHelper::getRooms(array_column($mobs, 'mob'), $danger),
        ];
    }
}
