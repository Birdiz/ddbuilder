<?php

declare(strict_types=1);

namespace App\Service\Generator;

use App\Enum\Generator\PNJ\Alignment;
use App\Enum\Generator\PNJ\Background;
use App\Enum\Generator\PNJ\Feature\Hairs;
use App\Enum\Generator\PNJ\Feature\Personality;
use App\Enum\Generator\PNJ\Feature\Physical;
use App\Enum\Generator\PNJ\Feature\Talent as FeatureTalent;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Guild;
use App\Enum\Generator\PNJ\Race\Race;
use App\Enum\Generator\PNJ\Relationship;
use App\Enum\Generator\PNJ\Sexuality\Attraction;
use App\Enum\Generator\PNJ\Sexuality\Interest;
use App\Enum\Generator\PNJ\Sexuality\Rejection;
use App\Enum\Generator\PNJ\Sexuality\Search;
use App\Enum\Generator\PNJ\Sexuality\Type;
use App\Form\Type\Generator\PNJType;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Service\Helper\PNJ\JobHelper;
use App\Service\Helper\PNJ\RaceHelper;

class PNJManager extends AbstractManager
{
    /**
     * @param array<string|int>|null $keys
     *
     * @return array<string, mixed>|null
     */
    public static function getRandomPNJ(?array $keys): ?array
    {
        if (null === $keys) {
            return null;
        }

        $gender = Gender::getGenderFromKey($keys[PNJType::GENDER]);
        $relationship = AbstractEnum::getRandomEnum(Relationship::toArray());
        $alignment = $keys[PNJType::ALIGNMENT] ?? AbstractEnum::getRandomEnum(Alignment::keys());

        return array_merge(
            RaceHelper::getRaceRelatedInfo(
                Race::getFromName($keys[PNJType::RACE]),
                $gender,
                $relationship,
                $alignment,
                $keys[PNJType::FAMILY_MEMBERS]
            ),
            [
                'gender' => $gender,
                'job' => JobHelper::getJob($keys[PNJType::JOB]),
                'background' => AbstractEnum::getRandomEnum(Background::toArray()),
                'hairs' => AbstractEnum::getRandomEnum(Hairs::toArray()),
                'sexuality' => self::getSexuality(),
                'relationship' => $relationship,
                'talent' => AbstractEnum::getRandomEnum(FeatureTalent::toArray()),
                'personality' => Personality::getFourFeatures(),
                'physical' => Physical::getFourFeatures(),
                'alignment' => Alignment::getAlignmentFromKey($alignment),
                'guild' => AbstractEnum::getRandomEnum(Guild::toArray()),
            ]
        );
    }

    /**
     * @return array<string, string>
     */
    private static function getSexuality(): array
    {
        return [
            'type' => AbstractEnum::getRandomEnum(Type::toArray()),
            'attraction' => AbstractEnum::getRandomEnum(Attraction::toArray()),
            'rejection' => AbstractEnum::getRandomEnum(Rejection::toArray()),
            'interest' => AbstractEnum::getRandomEnum(Interest::toArray()),
            'search' => AbstractEnum::getRandomEnum(Search::toArray()),
        ];
    }
}
