<?php

declare(strict_types=1);

namespace App\Service\Generator;

use App\Enum\Generator\Place\Type;
use App\Model\AbstractManager;
use App\Service\Helper\Generator\Place\PlaceSetHelper;
use App\Service\Helper\Generator\Place\SettlementHelper;

class PlaceManager extends AbstractManager
{
    /**
     * @return array<string, mixed>|null
     */
    public static function getRandomPlace(?string $key): ?array
    {
        if (null === $key) {
            return null;
        }

        if (in_array($key, [Type::VILLAGE()->getKey(), Type::TOWN()->getKey(), Type::CITY()->getKey()], true)) {
            return SettlementHelper::getSettlementFromType($key);
        }

        return PlaceSetHelper::getRandomPlaceSet($key);
    }
}
