<?php

declare(strict_types=1);

namespace App\Service\Generator\Travel;

use App\Contract\Generator\Travel\EncounterRegistryInterface;
use App\Contract\Generator\Travel\EncounterStrategyInterface;

class EncounterRegistry implements EncounterRegistryInterface
{
    /** @var EncounterStrategyInterface[] */
    private array $registry;

    /**
     * @param iterable<mixed> $strategies
     */
    public function __construct(iterable $strategies)
    {
        foreach ($strategies as $strategy) {
            $this->registry[$strategy->getType()] = $strategy;
        }
    }

    public function getStrategy(string $type): EncounterStrategyInterface
    {
        if (false === array_key_exists($type, $this->registry)) {
            throw new \LogicException('Encounter Strategy not implemented.');
        }

        return $this->registry[$type];
    }
}
