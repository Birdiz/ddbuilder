<?php

declare(strict_types=1);

namespace App\Service\Generator\Travel;

use App\Contract\Generator\Travel\EncounterStrategyInterface;
use App\Enum\Level;
use App\Service\Helper\Generator\Boss\MobHelper;

class FightStrategy implements EncounterStrategyInterface
{
    public const FIGHT = 'fight';

    public function getType(): string
    {
        return self::FIGHT;
    }

    public function process(?string $levelKey = null, ?int $nbPlayers = null): string|array
    {
        if (null === $levelKey || null === $nbPlayers) {
            return [];
        }

        return MobHelper::getRandomMobs(Level::getValueFromKey($levelKey), $nbPlayers);
    }
}
