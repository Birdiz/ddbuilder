<?php

declare(strict_types=1);

namespace App\Service\Generator\Travel;

use App\Contract\Generator\Travel\EncounterStrategyInterface;
use App\Enum\Generator\Travel\Encounter\Merchant;
use App\Model\AbstractEnum;

class MerchantStrategy implements EncounterStrategyInterface
{
    public const MERCHANT = 'merchant';

    public function getType(): string
    {
        return self::MERCHANT;
    }

    public function process(?string $levelKey = null, ?int $nbPlayers = null): string|array
    {
        return AbstractEnum::getRandomEnum(Merchant::toArray());
    }
}
