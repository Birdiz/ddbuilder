<?php

declare(strict_types=1);

namespace App\Service\Generator\Travel;

use App\Contract\Generator\Travel\EncounterStrategyInterface;
use App\Enum\Generator\Travel\Encounter\Monument;
use App\Model\AbstractEnum;

class MonumentStrategy implements EncounterStrategyInterface
{
    public const MONUMENT = 'monument';

    public function getType(): string
    {
        return self::MONUMENT;
    }

    public function process(?string $levelKey = null, ?int $nbPlayers = null): string|array
    {
        return AbstractEnum::getRandomEnum(Monument::toArray());
    }
}
