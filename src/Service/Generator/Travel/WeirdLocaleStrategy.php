<?php

declare(strict_types=1);

namespace App\Service\Generator\Travel;

use App\Contract\Generator\Travel\EncounterStrategyInterface;
use App\Enum\Generator\Travel\Encounter\WeirdLocale;
use App\Model\AbstractEnum;

class WeirdLocaleStrategy implements EncounterStrategyInterface
{
    public const WEIRD_LOCALE = 'weird_locale';

    public function getType(): string
    {
        return self::WEIRD_LOCALE;
    }

    public function process(?string $levelKey = null, ?int $nbPlayers = null): string|array
    {
        return AbstractEnum::getRandomEnum(WeirdLocale::toArray());
    }
}
