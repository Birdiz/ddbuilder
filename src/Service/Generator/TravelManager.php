<?php

declare(strict_types=1);

namespace App\Service\Generator;

use App\Enum\Environment\Food;
use App\Enum\Environment\Foraging;
use App\Enum\Generator\Travel\Pace;
use App\Form\Type\Generator\TravelType;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Service\EnvironmentManager;
use App\Service\Generator\Travel\EncounterRegistry;
use App\Service\Generator\Travel\FightStrategy;
use App\Service\Generator\Travel\MerchantStrategy;
use App\Service\Generator\Travel\MonumentStrategy;
use App\Service\Generator\Travel\WeirdLocaleStrategy;
use App\Util\WeightedRandTrait;

class TravelManager extends AbstractManager
{
    use WeightedRandTrait;

    public function __construct(private readonly EncounterRegistry $encounterRegistry)
    {
    }

    /**
     * @param array<string|int>|null $keys
     *
     * @return array<int, array<string, mixed>>|null
     */
    public function generateTravel(?array $keys): ?array
    {
        if (null === $keys) {
            return null;
        }

        $pace = Pace::getFromName($keys[TravelType::PACE]);
        $days = $keys[TravelType::DAYS];

        $travel = [];
        for ($i = 1; $i <= $days; ++$i) {
            $lost = self::getLostEvent($keys[TravelType::DAYS], $pace, $keys[TravelType::PLAYERS]);

            if ([] !== $lost) {
                $travel[$i] = $lost;
                $i += $lost['days'] - 1;
                $days += $lost['days'];
            } else {
                $travel[$i] = $this->getDay($pace, $keys);
            }
        }

        return $travel;
    }

    /**
     * @param array<string, mixed> $pace
     *
     * @return array<string, mixed>
     */
    private static function getFood(array $pace): array
    {
        $foraging = EnvironmentManager::getRandomForaging();

        return [
            'rations' => $foraging[1] <= random_int(1, 20) + $pace['foraging_bonus']
                ? random_int(1, Foraging::getRationFromValue($foraging[0]))
                : 0,
            'forage' => AbstractEnum::getRandomEnum(Food::toArray()),
        ];
    }

    /**
     * @return array<string, mixed>|string
     */
    private function getEncounter(string $name, string $levelKey, int $nbPlayers): array|string
    {
        $weights = match ($name) {
            Pace::SLOW()->getValue()['name'] => [10, 20, 35, 35],
            Pace::FAST()->getValue()['name'] => [40, 10, 25, 25],
            default => [25, 25, 25, 25]
        };

        $encounterType = static::getRandomizedValue(
            [
                FightStrategy::FIGHT,
                MerchantStrategy::MERCHANT,
                MonumentStrategy::MONUMENT,
                WeirdLocaleStrategy::WEIRD_LOCALE,
            ],
            $weights
        );

        return $this->encounterRegistry->getStrategy($encounterType)->process($levelKey, $nbPlayers);
    }

    /**
     * @param array<string, mixed> $pace
     * @param array<int|string>    $keys
     *
     * @return array<string, mixed>
     */
    private function getDay(array $pace, array $keys): array
    {
        // TODO add game or activity during night time
        $paceValue = $pace['pace'];
        $km = self::getKm($paceValue);
        $nbPlayers = $keys[TravelType::PLAYERS];

        return [
            'forecast' => EnvironmentManager::getRandomForecast(),
            'consume' => $nbPlayers,
            'rations' => self::getFood($pace)['rations'],
            'forage' => self::getFood($pace)['forage'],
            'encounter' => $this->getEncounter($pace['name'], $keys[TravelType::LEVEL], $nbPlayers),
            'is_trap' => 1 === random_int(0, 1),
            'km' => $km,
            'pace_effect' => Pace::getPaceEffect($paceValue, $km),
        ];
    }

    /**
     * @param array<string, mixed> $pace
     *
     * @return array<string, mixed>
     */
    private static function getLostEvent(int $days, array $pace, int $players): array
    {
        $lost = [];

        if (3 < $days && $pace['lost_probability'] >= random_int(0, 100)) {
            $days = random_int(1, 3) * $pace['lost_day_multiplier'];
            $rations = 0;
            $km = 0;

            for ($i = 1; $i <= $days; ++$i) {
                $rations += self::getFood($pace)['rations'] - $players;
                $km += self::getKm($pace['pace']);
            }

            $lost = [
                'days' => $days,
                'rations' => $rations,
                'lost_km' => $km,
            ];
        }

        return $lost;
    }

    private static function getKm(int $pace): int
    {
        return $pace + Pace::getRandomDelta();
    }
}
