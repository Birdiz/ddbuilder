<?php

namespace App\Service\Helper\Dungeon;

use App\Enum\Dungeon\Trap\Effect;

class TrapPriceHelper
{
    public static function getRandomTrapPrice(string $dangerosity): int
    {
        $price = random_int(1, 10);

        $price *= match ($dangerosity) {
            Effect::DANGEROUS()->getValue()['name'] => 2,
            Effect::DEADLY()->getValue()['name'] => 3,
            default => 1
        };

        return $price;
    }
}
