<?php

namespace App\Service\Helper\Generator\Boss;

use App\Util\StringTrait;

class BountyHelper
{
    use StringTrait;

    public static function getBounty(float $fp, int $level, string $danger): string
    {
        return static::normalizeCoins(self::getBasePrice($fp) * FPHelper::getMultiplicatorFromDanger($danger) * $level);
    }

    private static function getBasePrice(float $fp): int
    {
        if (4 < $fp && 7 >= $fp) {
            return random_int(100, 500); // Expert
        }
        if (7 < $fp && 15 >= $fp) {
            return random_int(500, 1000); // Hero
        }
        if (15 < $fp) {
            return random_int(1000, 2000); // Divine
        }

        return random_int(10, 100); // Beginner
    }
}
