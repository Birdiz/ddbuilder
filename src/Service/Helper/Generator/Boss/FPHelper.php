<?php

namespace App\Service\Helper\Generator\Boss;

use App\Enum\Danger;
use App\Enum\Generator\Boss\Beginner;
use App\Enum\Generator\Boss\Divine;
use App\Enum\Generator\Boss\Expert;
use App\Enum\Generator\Boss\Hero;
use App\Enum\Stuff;

class FPHelper
{
    public const LEVEL_TO_XP = [
        1 => 50,
        2 => 100,
        3 => 150,
        4 => 250,
        5 => 500,
        6 => 600,
        7 => 750,
        8 => 900,
        9 => 1100,
        10 => 1200,
        11 => 1600,
        12 => 2000,
        13 => 2200,
        14 => 2500,
        15 => 2800,
        16 => 3200,
        17 => 3900,
        18 => 4200,
        19 => 4900,
        20 => 5700,
    ];

    public static function getBossClassFromFP(float $fp): string
    {
        if (4 < $fp && 10 >= $fp) {
            return Expert::class;
        }
        if (10 < $fp && 16 >= $fp) {
            return Hero::class;
        }
        if (16 < $fp) {
            return Divine::class;
        }

        return Beginner::class;
    }

    public static function getFP(int $level, string $danger, string $stuff): float
    {
        $maxFP = Divine::getMaxFP();
        $fp = round($level * static::getMultiplicatorFromDanger($danger) + self::getModificatorFromStuff($stuff));

        return min($fp, $maxFP);
    }

    public static function getMultiplicatorFromDanger(string $danger): float
    {
        $dangers = [
            Danger::EASY()->getKey() => 0.5,
            Danger::MEDIUM()->getKey() => 1,
            Danger::HARD()->getKey() => 1.25,
            Danger::MORTAL()->getKey() => 1.5,
        ];

        return $dangers[$danger];
    }

    private static function getModificatorFromStuff(string $stuff): float
    {
        $stuffs = [
            Stuff::LOW()->getKey() => 0,
            Stuff::MEDIUM()->getKey() => 1,
            Stuff::HIGH()->getKey() => 2,
        ];

        return $stuffs[$stuff];
    }
}
