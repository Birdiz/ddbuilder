<?php

namespace App\Service\Helper\Generator\Boss;

use App\Enum\Generator\Boss\Beginner;
use App\Enum\Generator\Boss\Expert;
use App\Enum\Generator\Boss\Hero;
use App\Enum\Generator\Boss\Mob;
use App\Util\ArrayRandTrait;

class MobHelper
{
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getRandomMobs(int $level, int $nbPlayers): array
    {
        $baseXP = FPHelper::LEVEL_TO_XP[$level] * $nbPlayers;
        $set = self::getSetFromLevel($level);

        if ([] === $set) {
            return $set;
        }

        $setMobs = $set['mobs'];
        $hasChief = false;
        $mobs = [];

        while (0 <= $baseXP) {
            if ([] === $setMobs) {
                break;
            }

            if (false === $hasChief) {
                self::checkChief($set, $mobs, $baseXP, $hasChief);
            }

            self::addMob($mobs, $setMobs, $baseXP);
            self::checkBaseXP($baseXP, $mobs);
        }

        return $mobs;
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSetFromLevel(int $level): array
    {
        if (4 < $level && 10 >= $level) {
            return static::arrayRand([
                self::getHobgoblinSet(),
                self::getEnchanterSet(),
            ]);
        }
        if (10 < $level && 16 >= $level) {
            return static::arrayRand([
                self::getArchimageSet(),
                self::getWarlordSet(),
                self::getVampireSet(),
            ]);
        }
        if (16 < $level) {
            return [];
        }

        return static::arrayRand([
            self::getBanditSet(),
            self::getGoblinSet(),
            self::getBugbearSet(),
            self::getGnollSet(),
        ]);
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBanditSet(): array
    {
        return [
            'mobs' => [
                Mob::BANDIT()->getValue(),
                Mob::THUG()->getValue(),
                Mob::SCOUT()->getValue(),
            ],
            'chief' => Beginner::CAPTAIN_BANDIT()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGoblinSet(): array
    {
        return [
            'mobs' => [
                Mob::GOBLIN()->getValue(),
                Mob::HOBGOBLIN()->getValue(),
                Mob::BUGBEAR()->getValue(),
            ],
            'chief' => Beginner::GOBLIN_BOSS()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBugbearSet(): array
    {
        return [
            'mobs' => [
                Mob::GOBLIN()->getValue(),
                Mob::WORG()->getValue(),
                Mob::BUGBEAR()->getValue(),
            ],
            'chief' => Beginner::BUGBEAR_BOSS()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGnollSet(): array
    {
        return [
            'mobs' => [
                Mob::GNOLL()->getValue(),
                Mob::WORG()->getValue(),
            ],
            'chief' => Beginner::GNOLL_PACK_LORD()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHobgoblinSet(): array
    {
        return [
            'mobs' => [
                Mob::GOBLIN()->getValue(),
                Mob::HOBGOBLIN()->getValue(),
                Mob::WORG()->getValue(),
            ],
            'chief' => Expert::HOBGOBLIN_WARLORD()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getEnchanterSet(): array
    {
        return [
            'mobs' => [
                Mob::GUARD()->getValue(),
                Mob::CULTIST()->getValue(),
                Mob::ACOLYTE()->getValue(),
            ],
            'chief' => Expert::ENCHANTER()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getArchimageSet(): array
    {
        return [
            'mobs' => [
                Mob::ACOLYTE()->getValue(),
                Mob::APPRENTICE_WIZARD()->getValue(),
                Beginner::VETERAN()->getValue(),
            ],
            'chief' => Hero::ARCHIMAGE()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getWarlordSet(): array
    {
        return [
            'mobs' => [
                Mob::ARCHER()->getValue(),
                Beginner::VETERAN()->getValue(),
                Beginner::KNIGHT()->getValue(),
            ],
            'chief' => Hero::WARLORD()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getVampireSet(): array
    {
        return [
            'mobs' => [
                Mob::FANATIC()->getValue(),
                Beginner::VETERAN()->getValue(),
                Beginner::KNIGHT()->getValue(),
            ],
            'chief' => Hero::VAMPIRE()->getValue(),
        ];
    }

    /**
     * Check if baseXP is negative, if it's the case, then remove some mobs from last key in $mobs.
     *
     * @param array<string, mixed> $mobs
     */
    private static function checkBaseXP(int &$baseXP, array &$mobs): void
    {
        $lastMob = end($mobs);
        while (0 > $baseXP) {
            if (1 === $lastMob['nb']) {
                break;
            }

            --$lastMob['nb'];
            $baseXP += $lastMob['mob']['xp'];
        }

        $mobs[array_key_last($mobs)] = $lastMob;
    }

    /**
     * @param array<string, mixed>             $set
     * @param array<int, array<string, mixed>> $mobs
     */
    private static function checkChief(array $set, array &$mobs, int &$baseXP, bool &$hasChief): void
    {
        if (true === ($set['chief']['xp'] <= $baseXP / 2)) {
            $mobs[] = [
                'nb' => 1,
                'mob' => $set['chief'],
            ];

            $baseXP -= $set['chief']['xp'];
            $hasChief = true;
        }
    }

    /**
     * @param array<int, array<string, mixed>> $mobs
     * @param array<string, mixed>             $setMobs
     */
    private static function addMob(array &$mobs, array &$setMobs, int &$baseXP): void
    {
        $nb = random_int(1, 5);
        $mobIdx = array_rand($setMobs);
        $mob = $setMobs[$mobIdx];

        $mobs[] = [
            'nb' => $nb,
            'mob' => $mob,
        ];

        $baseXP -= $nb * $mob['xp'];

        unset($setMobs[$mobIdx]);
    }
}
