<?php

namespace App\Service\Helper\Generator\Boss;

use App\Enum\Danger;
use App\Service\DungeonManager;

class RoomHelper
{
    /**
     * @param array<mixed> $mobs
     *
     * @return array<int, array<string, mixed>>
     */
    public static function getRooms(array $mobs, string $danger): array
    {
        $rooms = [];
        [$nbRooms, $maxMobs, $traps] = self::getRoomInfo($danger);

        for ($i = 1; $i <= $nbRooms; ++$i) {
            // $traps is a bank of traps, remove each item already used.
            if ($i === random_int(1, $nbRooms) && [] !== $traps) {
                $trapIdx = array_rand($traps);
                $rooms[$i - 1]['trap'] = $traps[$trapIdx];
                unset($traps[$trapIdx]);
            }

            if ([] !== $mobs) {
                foreach ($mobs as $mob) {
                    if (0 >= $maxMobs) {
                        continue;
                    }

                    $nbMobs = random_int(1, $maxMobs);
                    $rooms[$i - 1]['mobs'][] = [
                        'nb' => $nbMobs,
                        'mob' => $mob,
                    ];
                    $maxMobs -= $nbMobs;
                }
            }
        }

        return array_values($rooms); // re-index
    }

    /**
     * @return array<mixed>
     */
    private static function getRoomInfo(string $danger): array
    {
        return match ($danger) {
            Danger::HARD()->getKey() => [
                random_int(2, 4),
                4,
                [
                    DungeonManager::getRandomTrap(),
                    DungeonManager::getRandomTrap(),
                ],
            ],
            Danger::MORTAL()->getKey() => [
                random_int(4, 5),
                8,
                [
                    DungeonManager::getRandomTrap(),
                    DungeonManager::getRandomTrap(),
                    DungeonManager::getRandomTrap(),
                ],
            ],
            default => [
                random_int(1, 2),
                2,
                [
                    DungeonManager::getRandomTrap(),
                ],
            ],
        };
    }
}
