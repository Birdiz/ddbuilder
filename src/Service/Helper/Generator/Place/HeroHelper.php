<?php

namespace App\Service\Helper\Generator\Place;

use App\Enum\Generator\Place\Hero\Adventure;
use App\Enum\Generator\Place\Hero\Status as HeroStatus;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\PNJClass;
use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;
use App\Util\WeightedRandTrait;

class HeroHelper
{
    use WeightedRandTrait;

    /**
     * @param array<string, int> $races
     *
     * @return array<string, mixed>
     *
     * @codeCoverageIgnore
     */
    public static function getHero(array $races): array
    {
        // One chance over 5 to get a local hero
        if (1 !== random_int(1, 5)) {
            return [];
        }

        $heroRace = array_rand($races);
        $heroGender = AbstractEnum::getRandomEnum(Gender::toArray());

        return [
            'hero' => Race::getName($heroRace, $heroGender),
            'heroRace' => $heroRace,
            'heroGender' => $heroGender,
            'heroClass' => AbstractEnum::getRandomEnum(PNJClass::toArray()),
            'heroAdventure' => AbstractEnum::getRandomEnum(Adventure::toArray()),
            'heroStatus' => static::getRandomizedValue(HeroStatus::toArray(), HeroStatus::getWeights()),
        ];
    }
}
