<?php

namespace App\Service\Helper\Generator\Place;

use App\Enum\DMScreen\Lifestyle;
use App\Enum\Generator\Place\Architecture;
use App\Enum\Generator\Place\Condition;
use App\Enum\Generator\Place\Religious;
use App\Enum\Generator\Place\Residence;
use App\Enum\Generator\Place\Ruler\Condition as RulerCondition;
use App\Enum\Generator\Place\Settlement\Merchandise;
use App\Enum\Generator\Place\Shop;
use App\Enum\Generator\Place\Tavern;
use App\Enum\Generator\Place\Type;
use App\Enum\Generator\Place\Warehouse;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Race\Race;
use App\Enum\Generator\Travel\Encounter\Monument;
use App\Enum\Generator\Travel\Encounter\WeirdLocale;
use App\Enum\Tavern\Game;
use App\Enum\Tavern\Service;
use App\Model\AbstractEnum;
use App\Util\ArrayRandTrait;
use App\Util\WeightedRandTrait;

class PlaceSetHelper
{
    use WeightedRandTrait;
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    private static function buildingCommonInfo(string $type): array
    {
        $rulerGender = AbstractEnum::getRandomEnum(Gender::toArray());
        $rulerRace = AbstractEnum::getRandomEnum(Race::toArray())['name'];

        return [
            'type' => $type,
            'guards' => random_int(0, 10),
            'condition' => static::getRandomizedValue(Condition::toArray(), Condition::getWeights()),
            'rulerGender' => $rulerGender,
            'rulerRace' => $rulerRace,
            'rulerName' => Race::getName($rulerRace, $rulerGender),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRandomPlaceSet(string $type): array
    {
        $building = self::buildingCommonInfo($type);
        $buildings = [
            Type::RESIDENCE()->getKey() => self::getResidence($building),
            Type::RELIGIOUS()->getKey() => self::getReligious($building),
            Type::TAVERN()->getKey() => self::getTavern($building),
            Type::WAREHOUSE()->getKey() => self::getWarehouse($building),
            Type::SHOP()->getKey() => self::getShop($building),
        ];

        return array_key_exists($type, $buildings) ? $buildings[$type] : [];
    }

    /**
     * @param array<string, mixed> $building
     *
     * @return array<string, mixed>
     */
    private static function getResidence(array &$building): array
    {
        $residence = static::getRandomizedValue(Residence::toArray(), Residence::getWeights());

        $building['residence'] = $residence;
        $building['habitants'] = Residence::getHabitants($residence);
        $building['district'] = Residence::getDistrict($residence);
        $building['architecture'] = AbstractEnum::getRandomEnum(Architecture::toArray());

        return $building;
    }

    /**
     * @param array<string, mixed> $building
     *
     * @return array<string, mixed>
     */
    private static function getReligious(array &$building): array
    {
        $religiousPlace = static::getRandomizedValue(Religious::toArray(), Religious::getWeights());

        $building['religious'] = $religiousPlace;
        $building['gods'] = Religious::getGods($religiousPlace);
        $building['acolytes'] = random_int(1, 10);
        $building['encounter'] = AbstractEnum::getRandomEnum(array_merge(Monument::toArray(), WeirdLocale::toArray()));

        return $building;
    }

    /**
     * @param array<string, mixed> $building
     *
     * @return array<string, mixed>
     */
    private static function getTavern(array &$building): array
    {
        $winnerGender = AbstractEnum::getRandomEnum(Gender::toArray());
        $winnerRace = AbstractEnum::getRandomEnum(Race::toArray())['name'];

        $building['name'] = Tavern::getName();
        $building['tavern'] = static::getRandomizedValue(Tavern::toArray(), Tavern::getWeights());
        $building['waiters'] = random_int(0, 5);
        $building['comfort'] = static::getRandomizedValue(
            Lifestyle::toArray(),
            Lifestyle::getWeights()
        )['name'];
        $building['services'] = static::getXComplexeItems(random_int(0, 3), Service::toArray());
        $building['rooms'] = random_int(0, 10);
        $building['winner'] = [
            'gender' => $winnerGender,
            'race' => $winnerRace,
            'name' => Race::getName($winnerRace, $winnerGender),
        ];
        $building['game'] = AbstractEnum::getRandomEnum(Game::toArray())['name'];

        return $building;
    }

    /**
     * @param array<string, mixed> $building
     *
     * @return array<string, mixed>
     */
    private static function getWarehouse(array &$building): array
    {
        $building['warehouse'] = static::getRandomizedValue(Warehouse::toArray(), Warehouse::getWeights());
        $building['doors'] = random_int(1, 5);
        $building['merchandises'] = static::getXSimpleItems(random_int(1, 5), Merchandise::toArray());
        $building['size'] = random_int(50, 500);

        return $building;
    }

    /**
     * @param array<string, mixed> $building
     *
     * @return array<string, mixed>
     */
    private static function getShop(array &$building): array
    {
        $building['shop'] = AbstractEnum::getRandomEnum(Shop::toArray());
        $building['rulerCondition'] = AbstractEnum::getRandomEnum(RulerCondition::toArray());
        $building['waitingTime'] = random_int(1, 45);
        $building['recommendable'] = 1 === random_int(1, 2);
        $building['merchandises'] = static::getXSimpleItems(
            random_int(1, 5),
            array_merge(Merchandise::toArray(), ['MAGIC' => 'des objets magiques'])
        );

        return $building;
    }
}
