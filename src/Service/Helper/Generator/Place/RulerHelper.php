<?php

namespace App\Service\Helper\Generator\Place;

use App\Enum\Generator\Place\Ruler\Status;
use App\Enum\Generator\Place\Ruler\Title;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;
use App\Util\FamilyTrait;
use App\Util\WeightedRandTrait;

class RulerHelper
{
    use WeightedRandTrait;
    use FamilyTrait;

    /**
     * @param array<string, int> $races
     *
     * @return array<string, mixed>
     */
    public static function getRuler(array $races, string $type): array
    {
        $race = array_rand($races);
        $gender = AbstractEnum::getRandomEnum(Gender::toArray());

        return [
            'ruler' => Race::getName($race, $gender),
            'rulerRace' => $race,
            'rulerGender' => $gender,
            'rulerTitle' => Title::getTitleForSettlement($type),
            'rulerStatus' => static::getRandomizedValue(Status::toArray(), Status::getWeights()),
            'rulerFamily' => self::getRulerFamily($race),
        ];
    }

    /**
     * @return array<string, mixed>|null
     *
     * @codeCoverageIgnore
     */
    private static function getRulerFamily(string $rulerRace): ?array
    {
        if (2 === random_int(1, 2)) {
            return static::getFamily($rulerRace, random_int(2, 5));
        }

        return null;
    }
}
