<?php

namespace App\Service\Helper\Generator\Place;

use App\Enum\Adventure\Event;
use App\Enum\Generator\Place\Name\Settlement as SettlementName;
use App\Enum\Generator\Place\Religious;
use App\Enum\Generator\Place\Settlement;
use App\Enum\Generator\Place\Settlement\Calamity;
use App\Enum\Generator\Place\Settlement\Feature;
use App\Enum\Generator\Place\Settlement\KnownFor;
use App\Enum\Generator\Place\Settlement\QualityOfLife;
use App\Enum\Generator\Place\Settlement\RaceRelation;
use App\Enum\Generator\Place\Tavern;
use App\Enum\Generator\Place\Warehouse;
use App\Util\ArrayRandTrait;
use App\Util\WeightedRandTrait;

class SettlementHelper
{
    use WeightedRandTrait;
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getSettlementFromType(string $type): array
    {
        $population = Settlement::getPopulationFromSettlement($type);
        $races = Settlement::getRacesFromPopulation($population);
        $capital = Settlement::getCapital($type, $population);
        $merchandises = Settlement::getMerchandises($type);

        return array_merge(
            [
                'type' => Settlement::getValueFromKey($type),
                'name' => SettlementName::getRandomEnum(SettlementName::toArray()),
                'knownFor' => KnownFor::getRandomEnum(KnownFor::toArray()),
                'features' => Feature::getFeatureForSettlement($type),
                'calamity' => static::getRandomizedValue(Calamity::toArray(), Calamity::getWeights()),
                'events' => static::getXSimpleItems(random_int(2, 5), Event::toArray()),
                'population' => $population,
                'houses' => Settlement::getHouses($population),
                'races' => $races,
                'relations' => static::getRandomizedValue(RaceRelation::toArray(), RaceRelation::getWeights()),
                'capital' => $capital,
                'comfort' => QualityOfLife::getQualityOfLifeForSettlement($type, $capital, $population),
                'merchandises' => $merchandises,
                'warehouses' => Warehouse::getWarehousesForSettlement($merchandises),
                'shops' => Settlement::getShops($type, $population),
                'taverns' => self::getTavernsForSettlement($population),
                'temples' => self::getTemplesForSettlement($population),
                'protections' => Settlement::getProtections($population),
            ],
            RulerHelper::getRuler($races, $type),
            HeroHelper::getHero($races)
        );
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getTavernsForSettlement(int $population): array
    {
        $taverns = [];

        foreach (Settlement::getBuildings(1, Tavern::toArray(), $population, 400) as $tavern) {
            $taverns[] = [
                'name' => Tavern::getName(),
                'type' => $tavern,
            ];
        }

        return $taverns;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    private static function getTemplesForSettlement(int $population): array
    {
        $religiousPlaces = [];

        foreach (Settlement::getBuildings(
            1,
            Religious::toArray(),
            $population,
            500
        ) as $religious) {
            $religiousPlaces[] = [
                'religious' => $religious,
                'gods' => Religious::getGods($religious),
            ];
        }

        return $religiousPlaces;
    }
}
