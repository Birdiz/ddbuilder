<?php

declare(strict_types=1);

namespace App\Service\Helper\Mapper;

use App\Enum\Adventure\Event;
use App\Enum\Adventure\Plane\Layer;
use App\Enum\Adventure\Plane\Plane;
use App\Enum\Adventure\PlotHook;
use App\Enum\Adventure\Prophecy;
use App\Enum\Arsenal\Ammunition;
use App\Enum\Arsenal\ExtendedArmor\Craftmanship as CraftmanshipArmor;
use App\Enum\Arsenal\ExtendedArmor\HeavyArmor;
use App\Enum\Arsenal\ExtendedArmor\LightArmor;
use App\Enum\Arsenal\ExtendedArmor\MediumArmor;
use App\Enum\Arsenal\ExtendedWeapon\Craftmanship as CraftmanshipWeapon;
use App\Enum\Arsenal\ExtendedWeapon\MartialMelee;
use App\Enum\Arsenal\ExtendedWeapon\MartialRanged;
use App\Enum\Arsenal\ExtendedWeapon\Shield;
use App\Enum\Arsenal\ExtendedWeapon\SimpleMelee;
use App\Enum\Arsenal\ExtendedWeapon\SimpleRanged;
use App\Enum\Arsenal\Firearm\Ammunition as FirearmAmmunition;
use App\Enum\Arsenal\Firearm\MagicAmmunition;
use App\Enum\Arsenal\Firearm\MartialFirearm;
use App\Enum\Arsenal\Firearm\SimpleFirearm;
use App\Enum\Arsenal\Silvering;
use App\Enum\Danger;
use App\Enum\DMScreen\Building\Building;
use App\Enum\DMScreen\Building\Maintenance;
use App\Enum\DMScreen\Damage;
use App\Enum\DMScreen\Lifestyle;
use App\Enum\DMScreen\Vehicule\Boat;
use App\Enum\DMScreen\Vehicule\Mounting\Equipment;
use App\Enum\DMScreen\Vehicule\Mounting\Mount;
use App\Enum\Dungeon\Air;
use App\Enum\Dungeon\Chase\Escape;
use App\Enum\Dungeon\Chase\Urban;
use App\Enum\Dungeon\Chase\Wilderness;
use App\Enum\Dungeon\Door;
use App\Enum\Dungeon\Noise;
use App\Enum\Dungeon\Odor;
use App\Enum\Dungeon\Puzzle;
use App\Enum\Dungeon\Riddle;
use App\Enum\Dungeon\Trap\Effect;
use App\Enum\Dungeon\Trap\Trap;
use App\Enum\Dungeon\Trap\Trigger;
use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use App\Enum\Environment\Adventure;
use App\Enum\Environment\Disease;
use App\Enum\Environment\Encounter\Encounter;
use App\Enum\Environment\Food;
use App\Enum\Environment\Foraging;
use App\Enum\Environment\Madness;
use App\Enum\Environment\Metal;
use App\Enum\Environment\Month;
use App\Enum\Environment\Rumor;
use App\Enum\Environment\Weather\Direction;
use App\Enum\Environment\Weather\Precipitation;
use App\Enum\Environment\Weather\TemperatureEffect;
use App\Enum\Environment\Weather\Wind;
use App\Enum\ExtraRules\HiddenSkill;
use App\Enum\Generator\Boss\Beginner;
use App\Enum\Generator\Boss\Divine;
use App\Enum\Generator\Boss\Expert;
use App\Enum\Generator\Boss\Hero;
use App\Enum\Generator\Boss\Mob;
use App\Enum\Generator\Place\Hero\Status as HeroStatus;
use App\Enum\Generator\Place\Religious;
use App\Enum\Generator\Place\Residence;
use App\Enum\Generator\Place\Ruler\Status;
use App\Enum\Generator\Place\Ruler\Title;
use App\Enum\Generator\Place\Settlement;
use App\Enum\Generator\Place\Settlement\Army;
use App\Enum\Generator\Place\Settlement\ArmyCondition;
use App\Enum\Generator\Place\Settlement\Calamity;
use App\Enum\Generator\Place\Settlement\DoorCondition;
use App\Enum\Generator\Place\Settlement\Feature;
use App\Enum\Generator\Place\Settlement\KnownFor;
use App\Enum\Generator\Place\Settlement\Merchandise;
use App\Enum\Generator\Place\Settlement\QualityOfLife;
use App\Enum\Generator\Place\Settlement\RaceRelation;
use App\Enum\Generator\Place\Settlement\Wall;
use App\Enum\Generator\Place\Shop;
use App\Enum\Generator\Place\Tavern;
use App\Enum\Generator\Place\Type;
use App\Enum\Generator\Place\Warehouse;
use App\Enum\Generator\PNJ\Alignment;
use App\Enum\Generator\PNJ\Background;
use App\Enum\Generator\PNJ\Feature\Hairs;
use App\Enum\Generator\PNJ\Feature\Personality;
use App\Enum\Generator\PNJ\Feature\Physical;
use App\Enum\Generator\PNJ\Feature\Skin;
use App\Enum\Generator\PNJ\Feature\Talent as FeatureTalent;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Guild;
use App\Enum\Generator\PNJ\Job\Academic;
use App\Enum\Generator\PNJ\Job\Aristocrat;
use App\Enum\Generator\PNJ\Job\Builder;
use App\Enum\Generator\PNJ\Job\Clergy;
use App\Enum\Generator\PNJ\Job\Craftsperson;
use App\Enum\Generator\PNJ\Job\Criminal;
use App\Enum\Generator\PNJ\Job\Entertainer;
use App\Enum\Generator\PNJ\Job\Farmer;
use App\Enum\Generator\PNJ\Job\Financier;
use App\Enum\Generator\PNJ\Job\Healer;
use App\Enum\Generator\PNJ\Job\Hosteler;
use App\Enum\Generator\PNJ\Job\Laborer;
use App\Enum\Generator\PNJ\Job\Merchant;
use App\Enum\Generator\PNJ\Job\Military;
use App\Enum\Generator\PNJ\Job\Outdoor;
use App\Enum\Generator\PNJ\Job\PublicServant;
use App\Enum\Generator\PNJ\Job\Servant;
use App\Enum\Generator\PNJ\Job\Unemployed;
use App\Enum\Generator\PNJ\Proficiency;
use App\Enum\Generator\PNJ\Race\Race;
use App\Enum\Generator\PNJ\Race\Talent as RaceTalent;
use App\Enum\Generator\PNJ\Relationship;
use App\Enum\Generator\PNJ\Sexuality;
use App\Enum\Generator\PNJ\Sidekick\Archetype;
use App\Enum\Generator\PNJ\Sidekick\ExtraInfo;
use App\Enum\Generator\PNJ\Sidekick\LevelUp;
use App\Enum\Generator\Travel\Encounter\Monument;
use App\Enum\Generator\Travel\Encounter\WeirdLocale;
use App\Enum\Generator\Travel\Pace;
use App\Enum\Level;
use App\Enum\MagicTradingPost\Craft\Condition;
use App\Enum\MagicTradingPost\Craft\CraftingInfo;
use App\Enum\MagicTradingPost\Sale\BuyerInfo;
use App\Enum\MagicTradingPost\Sale\SaleInfo;
use App\Enum\Object\Size;
use App\Enum\Pantheon;
use App\Enum\Sailing\Ship\Officer;
use App\Enum\Sailing\Ship\Ship;
use App\Enum\Stuff;
use App\Enum\Tavern\Ambiance;
use App\Enum\Tavern\Game;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT1;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT2;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT3;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT4;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT5;
use App\Enum\Tavern\Menu\Broth;
use App\Enum\Tavern\Menu\Dish;
use App\Enum\Tavern\Menu\Fish;
use App\Enum\Tavern\Menu\Fruit;
use App\Enum\Tavern\Menu\Hotpot;
use App\Enum\Tavern\Menu\Meat;
use App\Enum\Tavern\Menu\Soup;
use App\Enum\Tavern\Menu\Sweet;
use App\Enum\Tavern\Menu\With\WithT1;
use App\Enum\Tavern\Menu\With\WithT2;
use App\Enum\Tavern\Menu\With\WithT3;
use App\Enum\Tavern\OverHeard;
use App\Enum\Tavern\Service;
use App\Model\AbstractSearchMapper;

class PageEnumMapper extends AbstractSearchMapper
{
    private const ADVENTURE_ENUMS = [
        Escape::class,
        Event::class,
        Layer::class,
        Plane::class,
        PlotHook::class,
        Prophecy::class,
        Urban::class,
        Wilderness::class,
    ];
    private const ALCHEMY_ENUMS = [];
    private const ARSENAL_ENUMS = [
        SimpleMelee::class,
        SimpleRanged::class,
        MartialMelee::class,
        MartialRanged::class,
        Shield::class,
        SimpleFirearm::class,
        MartialFirearm::class,
        FirearmAmmunition::class,
        MagicAmmunition::class,
        CraftmanshipArmor::class,
        CraftmanshipWeapon::class,
        Silvering::class,
        LightArmor::class,
        MediumArmor::class,
        HeavyArmor::class,
        Ammunition::class,
    ];
    private const DM_SCREEN_ENUMS = [
        Damage::class,
        Mount::class,
        Equipment::class,
        Boat::class,
        Building::class,
        Maintenance::class,
        Lifestyle::class,
    ];
    private const DUNGEON_ENUMS = [
        Effect::class,
        Trap::class,
        Trigger::class,
        Air::class,
        Odor::class,
        Noise::class,
        Adventure::class,
        Riddle::class,
        Puzzle::class,
        Door::class,
        Size::class,
    ];
    private const ENVIRONMENT_ENUMS = [
        Precipitation::class,
        Month::class,
        TemperatureEffect::class,
        Wind::class,
        Direction::class,
        Encounter::class,
        Adventure::class,
        Rumor::class,
        Disease::class,
        Metal::class,
        Madness::class,
        Metal::class,
        Foraging::class,
    ];
    private const EXTRA_RULES_ENUMS = [
        HiddenSkill::class,
    ];
    private const PNJ_ENUMS = [
        Academic::class,
        Aristocrat::class,
        Builder::class,
        Clergy::class,
        Craftsperson::class,
        Criminal::class,
        Entertainer::class,
        Farmer::class,
        Financier::class,
        Healer::class,
        Hosteler::class,
        Laborer::class,
        Merchant::class,
        Military::class,
        Outdoor::class,
        PublicServant::class,
        Servant::class,
        Unemployed::class,
        Background::class,
        Hairs::class,
        Sexuality::class,
        Race::class,
        Skin::class,
        RaceTalent::class,
        Pantheon::class,
        Relationship::class,
        Gender::class,
        FeatureTalent::class,
        Personality::class,
        Physical::class,
        Guild::class,
        Alignment::class,
        Proficiency::class,
    ];
    private const BOSS_ENUMS = [
        Archetype::class,
        Level::class,
        Expert::class,
        Hero::class,
        Divine::class,
        Beginner::class,
        Danger::class,
        Stuff::class,
        Mob::class,
    ];
    private const PLACE_ENUMS = [
        Type::class,
        Settlement::class,
        Merchandise::class,
        KnownFor::class,
        Feature::class,
        Calamity::class,
        Event::class,
        RaceRelation::class,
        QualityOfLife::class,
        Warehouse::class,
        Shop::class,
        Tavern::class,
        Religious::class,
        Army::class,
        ArmyCondition::class,
        DoorCondition::class,
        Wall::class,
        Title::class,
        Status::class,
        Adventure::class,
        HeroStatus::class,
        Residence::class,
        Monument::class,
        WeirdLocale::class,
        Lifestyle::class,
        Service::class,
        Game::class,
    ];
    private const TRAVEL_ENUMS = [
        Pace::class,
        Foraging::class,
        Food::class,
        Mob::class,
        Expert::class,
        Hero::class,
        Divine::class,
        Beginner::class,
        Merchant::class,
        WeirdLocale::class,
    ];
    private const MAGIC_TRADING_POST_ENUMS = [
        Condition::class,
        CraftingInfo::class,
        SaleInfo::class,
        BuyerInfo::class,
        TableA::class,
        TableB::class,
        TableC::class,
        TableD::class,
        TableF::class,
        TableG::class,
        TableH::class,
        TableI::class,
        TableE::class,
    ];
    private const SAILING_ENUMS = [
        Ship::class,
        Officer::class,
    ];
    private const SIDEKICK_ENUMS = [
        Archetype::class,
        LevelUp::class,
        ExtraInfo::class,
    ];
    private const TAVERN_ENUMS = [
        Soup::class,
        WithT1::class,
        AccompanimentT1::class,
        WithT2::class,
        Broth::class,
        AccompanimentT2::class,
        Hotpot::class,
        WithT3::class,
        AccompanimentT3::class,
        Dish::class,
        AccompanimentT4::class,
        Meat::class,
        Fish::class,
        Sweet::class,
        Fruit::class,
        AccompanimentT5::class,
        Game::class,
        Lifestyle::class,
        Ambiance::class,
        OverHeard::class,
    ];

    /**
     * @return array<string, array<int, string>>
     */
    public static function values(): array
    {
        return [
            static::ADVENTURE => self::ADVENTURE_ENUMS,
            static::ALCHEMY => self::ALCHEMY_ENUMS,
            static::ARSENAL => self::ARSENAL_ENUMS,
            static::DM_SCREEN => self::DM_SCREEN_ENUMS,
            static::DUNGEON => self::DUNGEON_ENUMS,
            static::ENVIRONMENT => self::ENVIRONMENT_ENUMS,
            static::EXTRA_RULES => self::EXTRA_RULES_ENUMS,
            static::PNJ => self::PNJ_ENUMS,
            static::BOSS => self::BOSS_ENUMS,
            static::PLACE => self::PLACE_ENUMS,
            static::TRAVEL => self::TRAVEL_ENUMS,
            static::MAGIC_TRADING_POST => self::MAGIC_TRADING_POST_ENUMS,
            static::SAILING => self::SAILING_ENUMS,
            static::SIDEKICK => self::SIDEKICK_ENUMS,
            static::TAVERN => self::TAVERN_ENUMS,
        ];
    }

    /**
     * @return array<string, array<int, string>>
     */
    public static function paths(): array
    {
        $paths = [];
        foreach (static::values() as $routeName => $classes) {
            $paths[$routeName] = array_map(
                static fn (string $class) => (new \ReflectionClass($class))->getShortName().'.php',
                $classes
            );
        }

        return $paths;
    }
}
