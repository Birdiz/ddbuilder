<?php

declare(strict_types=1);

namespace App\Service\Helper\Mapper;

use App\Form\Type\DiseaseType;
use App\Form\Type\Generator\BossType;
use App\Form\Type\Generator\PlaceType;
use App\Form\Type\Generator\PNJType;
use App\Form\Type\Generator\TravelType;
use App\Form\Type\MadnessType;
use App\Form\Type\MagicItemType;
use App\Form\Type\MonthType;
use App\Form\Type\ShipType;
use App\Form\Type\TreasureType;
use App\Model\AbstractSearchMapper;

class PageFormMapper extends AbstractSearchMapper
{
    private const ADVENTURE_FORMS = [];
    private const ALCHEMY_FORMS = [];
    private const ARSENAL_FORMS = [];
    private const DM_SCREEN_FORMS = [];
    private const DUNGEON_FORMS = [
        TreasureType::class,
    ];
    private const ENVIRONMENT_FORMS = [
        DiseaseType::class,
        MadnessType::class,
        MonthType::class,
    ];
    private const EXTRA_RULES_FORMS = [];
    private const PNJ_FORMS = [
        PNJType::class,
    ];
    private const BOSS_FORMS = [
        BossType::class,
    ];
    private const PLACE_FORMS = [
        PlaceType::class,
    ];
    private const TRAVEL_FORMS = [
        TravelType::class,
    ];
    private const MAGIC_TRADING_POST_FORMS = [
        MagicItemType::class,
    ];
    private const SAILING_FORMS = [
        ShipType::class,
    ];
    private const SIDEKICK_FORMS = [];
    private const TAVERN_FORMS = [];

    /**
     * @return array<string, array<int, string>>
     */
    public static function values(): array
    {
        return [
            static::ADVENTURE => self::ADVENTURE_FORMS,
            static::ALCHEMY => self::ALCHEMY_FORMS,
            static::ARSENAL => self::ARSENAL_FORMS,
            static::DM_SCREEN => self::DM_SCREEN_FORMS,
            static::DUNGEON => self::DUNGEON_FORMS,
            static::ENVIRONMENT => self::ENVIRONMENT_FORMS,
            static::EXTRA_RULES => self::EXTRA_RULES_FORMS,
            static::PNJ => self::PNJ_FORMS,
            static::BOSS => self::BOSS_FORMS,
            static::PLACE => self::PLACE_FORMS,
            static::TRAVEL => self::TRAVEL_FORMS,
            static::MAGIC_TRADING_POST => self::MAGIC_TRADING_POST_FORMS,
            static::SAILING => self::SAILING_FORMS,
            static::SIDEKICK => self::SIDEKICK_FORMS,
            static::TAVERN => self::TAVERN_FORMS,
        ];
    }

    /**
     * @return array<string, array<int, string>>
     */
    public static function paths(): array
    {
        $paths = [];
        foreach (static::values() as $routeName => $classes) {
            $paths[$routeName] = array_map(
                static fn (string $class) => (new \ReflectionClass($class))->getShortName().'.php',
                $classes
            );
        }

        return $paths;
    }
}
