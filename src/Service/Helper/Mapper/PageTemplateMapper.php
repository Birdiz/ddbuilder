<?php

declare(strict_types=1);

namespace App\Service\Helper\Mapper;

use App\Model\AbstractSearchMapper;

class PageTemplateMapper extends AbstractSearchMapper
{
    private const ADVENTURE_TEMPLATES = [
        '_chase.html.twig',
        'adventure.html.twig',
    ];
    private const ALCHEMY_TEMPLATES = ['alchemy.html.twig'];
    private const ARSENAL_TEMPLATES = [
        '_additional_rules.html.twig',
        '_ammunitions.html.twig',
        '_extended_ammunitions.html.twig',
        '_extended_armors.html.twig',
        '_extended_shields.html.twig',
        '_extended_weapons.html.twig',
        '_firearms.html.twig',
        'arsenal.html.twig',
    ];
    private const DM_SCREEN_TEMPLATES = [
        '_building.html.twig',
        '_lifestyle.html.twig',
        '_maintenance.html.twig',
        '_mounting.html.twig',
        'dm_screen.html.twig',
    ];
    private const DUNGEON_TEMPLATES = [
        '_trap.html.twig',
        '_treasure.html.twig',
        'dungeon.html.twig',
    ];
    private const ENVIRONMENT_TEMPLATES = [
        '_forecast.html.twig',
        '_madness_effects.html.twig',
        '_metals.html.twig',
        'environment.html.twig',
    ];
    private const EXTRA_RULES_TEMPLATES = [
        'extra_rules.html.twig',
    ];
    private const PNJ_TEMPLATES = [
        '_pnj.html.twig',
        'pnj.html.twig',
    ];
    private const BOSS_TEMPLATES = [
        '_archetype.html.twig',
        '_boss.html.twig',
        '_mob.html.twig',
        '_room.html.twig',
        'boss.html.twig',
    ];
    private const PLACE_TEMPLATES = [
        '_religious.html.twig',
        '_residence.html.twig',
        '_settlement.html.twig',
        '_shop.html.twig',
        '_tavern.html.twig',
        '_warehouse.html.twig',
        'place.html.twig',
    ];
    private const TRAVEL_TEMPLATES = [
        '_travel.html.twig',
        'travel.html.twig',
    ];
    private const MAGIC_TRADING_POST_TEMPLATES = [
        '_item.html.twig',
        '_sell_buy_info.html.twig',
        'magic_trading_post.html.twig',
    ];
    private const SAILING_TEMPLATES = [
        '_officers.html.twig',
        '_ships.html.twig',
        'sailing.html.twig',
    ];
    private const SIDEKICK_TEMPLATES = [
        '_card.html.twig',
        '_extra_info.html.twig',
        '_level_up.html.twig',
        'sidekick.html.twig',
    ];
    private const TAVERN_TEMPLATES = [
        '_game.html.twig',
        '_meal.html.twig',
        'tavern.html.twig',
    ];

    /**
     * @return array<string, array<int, string>>
     */
    public static function values(): array
    {
        return [
            static::ADVENTURE => self::ADVENTURE_TEMPLATES,
            static::ALCHEMY => self::ALCHEMY_TEMPLATES,
            static::ARSENAL => self::ARSENAL_TEMPLATES,
            static::DM_SCREEN => self::DM_SCREEN_TEMPLATES,
            static::DUNGEON => self::DUNGEON_TEMPLATES,
            static::ENVIRONMENT => self::ENVIRONMENT_TEMPLATES,
            static::EXTRA_RULES => self::EXTRA_RULES_TEMPLATES,
            static::PNJ => self::PNJ_TEMPLATES,
            static::BOSS => self::BOSS_TEMPLATES,
            static::PLACE => self::PLACE_TEMPLATES,
            static::TRAVEL => self::TRAVEL_TEMPLATES,
            static::MAGIC_TRADING_POST => self::MAGIC_TRADING_POST_TEMPLATES,
            static::SAILING => self::SAILING_TEMPLATES,
            static::SIDEKICK => self::SIDEKICK_TEMPLATES,
            static::TAVERN => self::TAVERN_TEMPLATES,
        ];
    }

    /**
     * @return array<string, array<int, string>>
     */
    public static function paths(): array
    {
        return static::values();
    }
}
