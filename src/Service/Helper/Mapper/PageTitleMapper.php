<?php

declare(strict_types=1);

namespace App\Service\Helper\Mapper;

use App\Model\AbstractSearchMapper;

class PageTitleMapper
{
    private const ADVENTURE = 'AVENTURE';
    private const ALCHEMY = 'ALCHIMIE';
    private const ARSENAL = 'ARSENAL';
    private const DM_SCREEN = 'ÉCRAN DU MAÎTRE';
    private const DUNGEON = 'DONJON';
    private const ENVIRONMENT = 'ENVIRONNEMENT';
    private const EXTRA_RULES = 'RÈGLES ADDITIONNELLES';
    private const PNJ = 'PERSONNAGE';
    private const BOSS = 'BOSS';
    private const PLACE = 'LIEU';
    private const TRAVEL = 'VOYAGE';
    private const MAGIC_TRADING_POST = 'COMPTOIR MAGIQUE';
    private const SAILING = 'NAVIGATION';
    private const SIDEKICK = 'COMPAGNONS';
    private const TAVERN = 'AUBERGE';

    /**
     * @return array<string, string>
     */
    public function map(): array
    {
        return [
            AbstractSearchMapper::ADVENTURE => self::ADVENTURE,
            AbstractSearchMapper::ALCHEMY => self::ALCHEMY,
            AbstractSearchMapper::ARSENAL => self::ARSENAL,
            AbstractSearchMapper::DM_SCREEN => self::DM_SCREEN,
            AbstractSearchMapper::DUNGEON => self::DUNGEON,
            AbstractSearchMapper::ENVIRONMENT => self::ENVIRONMENT,
            AbstractSearchMapper::EXTRA_RULES => self::EXTRA_RULES,
            AbstractSearchMapper::PNJ => self::PNJ,
            AbstractSearchMapper::BOSS => self::BOSS,
            AbstractSearchMapper::PLACE => self::PLACE,
            AbstractSearchMapper::TRAVEL => self::TRAVEL,
            AbstractSearchMapper::MAGIC_TRADING_POST => self::MAGIC_TRADING_POST,
            AbstractSearchMapper::SAILING => self::SAILING,
            AbstractSearchMapper::SIDEKICK => self::SIDEKICK,
            AbstractSearchMapper::TAVERN => self::TAVERN,
        ];
    }
}
