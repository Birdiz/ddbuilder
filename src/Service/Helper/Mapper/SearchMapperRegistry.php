<?php

declare(strict_types=1);

namespace App\Service\Helper\Mapper;

use App\Contract\Searcher\SearchMapperInterface;
use App\Contract\Searcher\SearchMapperRegistryInterface;

class SearchMapperRegistry implements SearchMapperRegistryInterface
{
    /** @var SearchMapperInterface[] */
    private array $registry;

    /**
     * @param iterable<mixed> $mappers
     */
    public function __construct(iterable $mappers)
    {
        foreach ($mappers as $mapper) {
            $this->registry[] = $mapper;
        }
    }

    /**
     * @param array<string>|null $paths
     *
     * @return array<string>
     */
    public function map(?array $paths): array
    {
        $results = [];

        foreach ($this->registry as $mapper) {
            $result = $mapper->map($paths);

            if (null === $result) {
                continue;
            }

            $results += $result;
        }

        return $results;
    }
}
