<?php

namespace App\Service\Helper\PNJ;

use App\Util\ArrayRandTrait;

class ASHelper
{
    use ArrayRandTrait;

    private const AS = [15, 14, 13, 12, 10, 8];

    /**
     * @param array<string, mixed> $configs
     *
     * @return array<string, int>
     */
    public static function computeAS(array $configs): array
    {
        $AS = self::AS;
        self::shuffle($AS);

        $randomAS = [
            'FOR' => $AS[0],
            'DEX' => $AS[1],
            'CON' => $AS[2],
            'INT' => $AS[3],
            'SAG' => $AS[4],
            'CHA' => $AS[5],
        ];

        foreach ($configs as $key => $config) {
            if (\array_key_exists($key, $randomAS)) {
                $randomAS[$key] += $config;
            }
        }

        return $randomAS;
    }
}
