<?php

namespace App\Service\Helper\PNJ;

use App\Enum\Generator\PNJ\PNJClass;
use App\Model\AbstractEnum;
use App\Model\PNJ\AbstractPNJJobEnum;

class JobHelper
{
    public static function getJob(?string $jobKey): string
    {
        $job = AbstractPNJJobEnum::getJobName($jobKey);

        if (null === $job) {
            if (0 === random_int(0, 1)) {
                $job = AbstractPNJJobEnum::getRandomJob();
            } else {
                $job = AbstractEnum::getRandomEnum(PNJClass::toArray());
            }
        }

        return $job;
    }
}
