<?php

namespace App\Service\Helper\PNJ;

use App\Enum\Generator\PNJ\Feature\Skin;
use App\Enum\Generator\PNJ\Race\Race;
use App\Enum\Generator\PNJ\Race\Talent as RaceTalent;
use App\Enum\Pantheon;
use App\Util\ArrayRandTrait;
use App\Util\FamilyTrait;

class RaceHelper
{
    use FamilyTrait;
    use ArrayRandTrait;

    /**
     * @param array<string, mixed> $race
     *
     * @return array<string, mixed>
     */
    public static function getRaceRelatedInfo(
        array $race,
        string $gender,
        string $relationship,
        string $alignment,
        ?int $familyMembers
    ): array {
        $raceName = $race['name'];
        $race['traits'] = TraitHelper::computeTraits($race['traits']);

        return [
            'race' => $race,
            'image' => RaceImageHelper::fetchRaceImage($raceName),
            'name' => Race::getName($raceName, $gender),
            'skin' => Skin::getSkinFromRaceName($raceName),
            'metrics' => Race::getMetrics($raceName),
            'passives' => RaceTalent::getTalentsFromRaceName($raceName),
            'family' => null === $familyMembers || 0 === $familyMembers
                ? []
                : static::getFamily($raceName, $familyMembers, $relationship),
            'gods' => Pantheon::getUntilThreeGods(Pantheon::getGodsByRaceName($raceName), $alignment),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRandomRace(): array
    {
        return static::arrayRand(Race::toArray());
    }

    /**
     * @param array<string, mixed> $race
     *
     * @return array<string, string>
     */
    public static function getRaceRelatedMinimalInfo(array $race, string $gender): array
    {
        $raceName = $race['name'];

        return [
            'race_name' => $raceName,
            'gender' => $gender,
            'name' => Race::getName($raceName, $gender),
        ];
    }
}
