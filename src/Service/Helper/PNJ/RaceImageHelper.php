<?php

namespace App\Service\Helper\PNJ;

use App\Contract\MediaFetchableInterface;
use App\Enum\Generator\PNJ\Race\Race;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class RaceImageHelper implements MediaFetchableInterface
{
    public static function getMediaPath(): string
    {
        return static::MEDIA_PATH_PNJ;
    }

    /** @codeCoverageIgnore */
    public static function fetchRaceImage(string $raceName): string
    {
        $package = new PathPackage(static::getMediaPath(), new EmptyVersionStrategy());
        $images = array_combine(
            array_column(Race::toArray(), 'name'),
            [
                $package->getUrl('Hill_Dwarf.webp'),
                $package->getUrl('Mountain_Dwarf.png'),
                $package->getUrl('High_Elf.png'),
                $package->getUrl('Wood_Elves.webp'),
                $package->getUrl('Drow.png'),
                $package->getUrl('Lightfoot_Halfling.png'),
                $package->getUrl('Stout_Halfling.webp'),
                $package->getUrl('Human.webp'),
                $package->getUrl('Dragonborn.png'),
                $package->getUrl('Gnomes.webp'),
                $package->getUrl('Gnomes.webp'),
                $package->getUrl('Half_Elf.png'),
                $package->getUrl('Half_Orc.png'),
                $package->getUrl('Tiefling.webp'),
                $package->getUrl('Aarakocra.webp'),
            ]
        );

        return $images[$raceName];
    }
}
