<?php

namespace App\Service\Helper\PNJ;

class TraitHelper
{
    /**
     * @param array<string, mixed> $configs
     *
     * @return array<string, mixed>
     */
    public static function computeTraits(array $configs): array
    {
        $traits = ['AS' => [], 'age' => 0, 'size' => 0, 'weight' => 0];

        foreach ($configs as $key => $config) {
            switch ($key) {
                case 'AS':
                    $traits['AS'] = ASHelper::computeAS($config);
                    break;
                case 'age':
                    $traits['age'] = random_int(10, $config);
                    break;
                case 'size':
                    $traits['size'] = random_int($config[0], $config[1]);
                    break;
                case 'weight':
                    $traits['weight'] = random_int($config[0], $config[1]);
                    break;
            }
        }

        return $traits;
    }
}
