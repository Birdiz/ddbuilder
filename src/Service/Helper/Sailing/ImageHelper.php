<?php

namespace App\Service\Helper\Sailing;

use App\Contract\MediaFetchableInterface;
use App\Enum\Sailing\Ship\Ship;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

class ImageHelper implements MediaFetchableInterface
{
    public static function getMediaPath(): string
    {
        return static::MEDIA_PATH_SHIPS;
    }

    /**
     * @param array<string, mixed> $ships
     *
     * @codeCoverageIgnore
     */
    public static function fetchShipImage(array &$ships): void
    {
        $package = new PathPackage(static::getMediaPath(), new EmptyVersionStrategy());

        foreach ($ships as $idx => $ship) {
            $ships[$idx]['image'] = match ($ship['name']) {
                Ship::AIRSHIP()->getValue()['name'] => $package->getUrl('Airship.webp'),
                Ship::GALLEY()->getValue()['name'] => $package->getUrl('Galley.jpeg'),
                Ship::KEELBOAT()->getValue()['name'] => $package->getUrl('Keelboat.jpg'),
                Ship::LONGSHIP()->getValue()['name'] => $package->getUrl('Longship.webp'),
                Ship::ROWBOAT()->getValue()['name'] => $package->getUrl('Rowboat.jpg'),
                Ship::SAILING_SHIP()->getValue()['name'] => $package->getUrl('SailingShip.jpg'),
                Ship::WARSHIP()->getValue()['name'] => $package->getUrl('Warship.jpg'),
                default => null
            };
        }
    }
}
