<?php

namespace App\Service\Helper\Tavern;

use App\Enum\Tavern\Menu\Fish;
use App\Enum\Tavern\Menu\Fruit;
use App\Enum\Tavern\Menu\Meat;
use App\Enum\Tavern\Menu\Sweet;
use App\Util\WeightedRandTrait;

class PlateHelper
{
    use WeightedRandTrait;

    public static function getDish(bool $isAristocratic): string
    {
        return sprintf(
            '%s <i>%s</i> %s',
            static::getRandomizedValue(Meat::toArray(), Meat::getWeights()),
            $isAristocratic ? 'ET' : 'OU',
            static::getRandomizedValue(Fish::toArray(), Fish::getWeights())
        );
    }

    public static function getDessert(bool $isAristocratic): string
    {
        return sprintf(
            '%s <i>%s</i> %s',
            static::getRandomizedValue(Sweet::toArray(), Sweet::getWeights()),
            $isAristocratic ? 'ET' : 'OU',
            static::getRandomizedValue(Fruit::toArray(), Fruit::getWeights())
        );
    }
}
