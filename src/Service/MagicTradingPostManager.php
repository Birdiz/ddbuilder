<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use App\Enum\Generator\PNJ\Gender;
use App\Enum\MagicTradingPost\Craft\Condition;
use App\Enum\MagicTradingPost\Craft\CraftingInfo;
use App\Enum\MagicTradingPost\Sale\BuyerInfo;
use App\Enum\MagicTradingPost\Sale\SaleInfo;
use App\Model\AbstractManager;
use App\Service\Helper\PNJ\RaceHelper;
use App\Util\ArrayRandTrait;
use App\Util\WeightedRandTrait;

class MagicTradingPostManager extends AbstractManager
{
    use WeightedRandTrait;
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getCraftInfo(): array
    {
        return [
            'conditions' => Condition::toArray(),
            'crafting' => CraftingInfo::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getSaleInfo(): array
    {
        return [
            'sale' => SaleInfo::toArray(),
            'buyer' => BuyerInfo::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getRandomSale(): array
    {
        $items = [
            TableA::class => static::getMultipleItemsWeigthed(5, TableA::toArray(), TableA::getWeights()),
            TableB::class => static::getMultipleItemsWeigthed(2, TableB::toArray(), TableB::getWeights()),
            TableC::class => static::getMultipleItemsWeigthed(2, TableC::toArray(), TableC::getWeights()),
            TableD::class => static::getMultipleItemsWeigthed(1, TableD::toArray(), TableD::getWeights()),
            TableF::class => static::getMultipleItemsWeigthed(3, TableF::toArray(), TableF::getWeights()),
            TableG::class => static::getMultipleItemsWeigthed(2, TableG::toArray(), TableG::getWeights()),
            TableH::class => static::getMultipleItemsWeigthed(1, TableH::toArray(), TableH::getWeights()),
        ];

        self::shuffleAssoc($items);

        return $items;
    }

    /**
     * @return array<string, string|array<string, string>>|null
     */
    public static function getItem(?string $keys): ?array
    {
        foreach (self::getItems() as $table => $list) {
            $key = array_search(
                $keys,
                array_combine(
                    array_keys($list),
                    array_column($list, 'name')
                ),
                true
            );

            if (false !== $key) {
                return array_merge(
                    $list[$key],
                    ['table-name' => $table]
                );
            }
        }

        return null;
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    private static function getItems(): array
    {
        return [
            TableA::class => TableA::toArray(),
            TableB::class => TableB::toArray(),
            TableC::class => TableC::toArray(),
            TableD::class => TableD::toArray(),
            TableE::class => TableE::toArray(),
            TableF::class => TableF::toArray(),
            TableG::class => TableG::toArray(),
            TableH::class => TableH::toArray(),
            TableI::class => TableI::toArray(),
        ];
    }

    /**
     * @return array<string, array<string, string>|bool>>
     */
    public static function getRandomMerchant(): array
    {
        return [
            'race_info' => RaceHelper::getRaceRelatedMinimalInfo(
                RaceHelper::getRandomRace(),
                0 === random_int(0, 1) ? Gender::MALE()->getValue() : Gender::FEMALE()->getValue()
            ),
            'is_loyal' => 0 === random_int(0, 1),
        ];
    }
}
