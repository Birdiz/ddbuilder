<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Sailing\Ship\Officer;
use App\Enum\Sailing\Ship\Ship;
use App\Model\AbstractManager;
use App\Service\Helper\Sailing\ImageHelper;

class SailingManager extends AbstractManager
{
    /**
     * @return array<string, mixed>
     */
    public static function getShips(): array
    {
        $ships = Ship::toArray();

        ImageHelper::fetchShipImage($ships);

        return $ships;
    }

    /**
     * @return array<string, string>
     */
    public static function getOfficers(): array
    {
        return Officer::toArray();
    }

    /**
     * @return array<string, int|string>|null
     */
    public static function getRandomCrew(?string $key): ?array
    {
        if (null === $key || false === in_array($key, array_column(Ship::toArray(), 'name'), true)) {
            return null;
        }

        $ship = Ship::getShip($key);
        $crew_capacity = $ship['creature_capacity']['crew'];
        $crew = random_int((int) ceil($crew_capacity / 2), $crew_capacity);

        return [
            'ship' => $key,
            'captain' => $crew > 10 ? 1 : 0,
            'first-mate' => $crew > 40 ? (int) ceil($crew / 50) : 0,
            'bosun' => $crew > 20 ? (int) ceil($crew / 40) : 0,
            'quartermaster' => $crew > 20 ? 1 : 0,
            'surgeon' => $crew > 15 ? (int) ceil($crew / 30) : 0,
            'cook' => $crew > 10 ? (int) ceil($crew / 20) : 0,
            'crew' => $crew,
        ];
    }
}
