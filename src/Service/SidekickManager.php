<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\Generator\PNJ\Sidekick\Archetype;
use App\Enum\Generator\PNJ\Sidekick\ExtraInfo;
use App\Enum\Generator\PNJ\Sidekick\LevelUp;
use App\Model\AbstractManager;

class SidekickManager extends AbstractManager
{
    /**
     * @return array<string, mixed>
     */
    public static function getArtificer(): array
    {
        return [
            'artificer' => Archetype::ARTIFICER()->getValue(),
            'level_up' => LevelUp::ARTIFICER()->getValue(),
            'extra_info' => ExtraInfo::ARTIFICER()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getBard(): array
    {
        return [
            'bard' => Archetype::BARD()->getValue(),
            'level_up' => LevelUp::BARD()->getValue(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSavage(): array
    {
        return [
            'savage' => Archetype::SAVAGE()->getValue(),
            'level_up' => LevelUp::SAVAGE()->getValue(),
        ];
    }
}
