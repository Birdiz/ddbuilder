<?php

declare(strict_types=1);

namespace App\Service;

use App\Enum\DMScreen\Lifestyle;
use App\Enum\Tavern\Ambiance;
use App\Enum\Tavern\Game;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT1;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT2;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT3;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT4;
use App\Enum\Tavern\Menu\Accompaniment\AccompanimentT5;
use App\Enum\Tavern\Menu\Broth;
use App\Enum\Tavern\Menu\Dish;
use App\Enum\Tavern\Menu\Hotpot;
use App\Enum\Tavern\Menu\Soup;
use App\Enum\Tavern\Menu\With\WithT1;
use App\Enum\Tavern\Menu\With\WithT2;
use App\Enum\Tavern\Menu\With\WithT3;
use App\Enum\Tavern\OverHeard;
use App\Model\AbstractEnum;
use App\Model\AbstractManager;
use App\Service\Helper\Tavern\PlateHelper;
use App\Util\WeightedRandTrait;

class TavernManager extends AbstractManager
{
    use WeightedRandTrait;

    /**
     * @return array<string, string>
     */
    public static function getRandomMiserableMeal(): array
    {
        return [
            'Soupe' => static::getRandomizedValue(Soup::toArray(), Soup::getWeights()),
            'avec un peu de' => static::getRandomizedValue(WithT1::toArray(), WithT1::getWeights()),
            'accompagnée de' => static::getRandomizedValue(AccompanimentT1::toArray(), AccompanimentT1::getWeights()),
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getRandomPoorMeal(): array
    {
        $withT2Value = static::getRandomizedValue(WithT2::toArray(), WithT2::getWeights());
        $withT2 = $withT2Value === WithT2::BROTH()->getValue()
            ? static::getRandomizedValue(Broth::toArray(), Broth::getWeights())
            : $withT2Value;

        return [
            'Bouillon aux' => static::getRandomizedValue(Broth::toArray(), Broth::getWeights()),
            'avec un peu de' => $withT2,
            'accompagné de' => static::getRandomizedValue(AccompanimentT2::toArray(), AccompanimentT2::getWeights()),
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getRandomModestMeal(): array
    {
        $firstIngredient = static::getRandomizedValue(Hotpot::toArray(), Hotpot::getWeights());
        $hotpot = sprintf('%s <i>ET</i> %s', $firstIngredient, Hotpot::getSecondIngredientValue($firstIngredient));

        return [
            'Potée aux' => $hotpot,
            'avec un peu de' => static::getRandomizedValue(WithT3::toArray(), WithT3::getWeights()),
            'accompagné de' => static::getRandomizedValue(AccompanimentT3::toArray(), AccompanimentT3::getWeights()),
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getRandomComfortableMeal(): array
    {
        return [
            'Plat:' => static::getRandomizedValue(Dish::toArray(), Dish::getWeights()),
            'accompagné(e) de' => static::getRandomizedValue(AccompanimentT4::toArray(), AccompanimentT4::getWeights()),
        ];
    }

    /**
     * @return array<string, string>
     */
    public static function getRandomRichMeal(bool $isAristocratic): array
    {
        return [
            'Plat:' => PlateHelper::getDish($isAristocratic),
            'accompagné(e) de' => static::getRandomizedValue(AccompanimentT5::toArray(), AccompanimentT5::getWeights()),
            'avec en dessert' => PlateHelper::getDessert($isAristocratic),
        ];
    }

    /**
     * @return array<string, array<string, string>>
     */
    public static function getRandomGame(): array
    {
        return Game::getRandomGame();
    }

    public static function getRandomComfort(): string
    {
        return AbstractEnum::getRandomEnum(Lifestyle::getNames());
    }

    public static function getRandomAmbience(): string
    {
        return AbstractEnum::getRandomEnum(Ambiance::toArray());
    }

    public static function getRandomRooms(): int
    {
        return static::getRandomizedValue([0, 1, 2, 3, 4, 5], [10, 35, 20, 10, 25]);
    }

    public static function getRandomInfo(): string
    {
        return AbstractEnum::getRandomEnum(OverHeard::toArray());
    }
}
