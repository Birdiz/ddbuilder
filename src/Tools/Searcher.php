<?php

declare(strict_types=1);

namespace App\Tools;

use App\Service\Helper\Mapper\PageTitleMapper;
use App\Service\Helper\Mapper\SearchMapperRegistry;
use Symfony\Component\Finder\Finder;

class Searcher
{
    private const ENUMS_PATH = __DIR__.'/../Enum';
    private const FORMS_PATH = __DIR__.'/../Form';
    private const TEMPLATES_PATH = __DIR__.'/../../templates';

    public function __construct(
        private readonly SearchMapperRegistry $searchMapperRegistry,
        private readonly PageTitleMapper $pageTitleMapper
    ) {
    }

    /**
     * @return array<string>|null
     */
    private function find(string $value): ?array
    {
        $finder = (new Finder())
            ->in([self::ENUMS_PATH, self::TEMPLATES_PATH, self::FORMS_PATH])
            ->files()
            ->contains("/$value/i")
        ;

        $files = [];
        foreach ($finder as $file) {
            $files[] = $file->getFilename();
        }

        return [] === $files ? null : $files;
    }

    /**
     * @return array<string, string>
     */
    public function search(string $value): array
    {
        $routeNames = $this->searchMapperRegistry->map($this->find($value));

        $links = [];
        foreach ($routeNames as $name) {
            $links[$name] = $this->pageTitleMapper->map()[$name];
        }

        return $links;
    }
}
