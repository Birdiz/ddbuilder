<?php

declare(strict_types=1);

namespace App\Tools;

use DateTime;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SpamChecker
{
    private string $endpoint;

    public function __construct(
        private HttpClientInterface $client,
        private string $env, string $akismetKey,
        private LoggerInterface $logger
    ) {
        $this->endpoint = sprintf('https://%s.rest.akismet.com/1.1/comment-check', $akismetKey);
    }

    /**
     * @return int Spam score: 0: not spam, 1: maybe spam, 2: blatant spam
     *
     * @codeCoverageIgnore
     */
    public function getSpamScore(Email $email, Request $request, ?string $honeyPot): int
    {
        try {
            $response = $this->client->request(
                Request::METHOD_POST,
                $this->endpoint,
                [
                    'body' => $this->getBody($email, $request, $honeyPot),
                ]
            );
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());

            throw $e;
        }

        try {
            $headers = $response->getHeaders();
        } catch (\Throwable $e) {
            $this->logger->error($e->getMessage());

            throw $e;
        }
        if ('discard' === ($headers['x-akismet-pro-tip'][0] ?? '')) {
            return 2;
        }

        try {
            $content = $response->getContent();
        } catch (\Throwable $e) {
            $this->logger->error($e);

            throw $e;
        }

        if (isset($headers['x-akismet-debug-help'][0])) {
            $help = $headers['x-akismet-debug-help'][0];

            throw new RuntimeException(sprintf('Unable to check for spam: %s (%s).', $content, $help));
        }

        return 'true' === $content ? 1 : 0;
    }

    /**
     * @return array<string, mixed>
     */
    private function getBody(Email $email, Request $request, ?string $honeyPot): array
    {
        $emailAddress = $email->getFrom()[0]->getAddress();
        $body = [
            'user_ip' => $request->getClientIp(),
            'user_agent' => $request->headers->get('user-agent'),
            'referrer' => $request->headers->get('referrer'),
            'permalink' => $request->getUri(),
            'blog' => 'https://ddbuilder.fr',
            'comment_type' => 'contact-form',
            'comment_author' => $emailAddress,
            'comment_author_email' => $emailAddress,
            'comment_content' => $email->getTextBody(),
            'comment_date_gmt' => (new DateTime())->format('c'),
            'blog_lang' => 'fr',
            'blog_charset' => 'UTF-8',
            'is_test' => 'prod' !== strtolower($this->env),
        ];

        if (is_string($honeyPot)) {
            $body['honeypot_field_name'] = 'bug_miellat';
            $body['bug_miellat'] = $honeyPot;
        }

        return $body;
    }
}
