<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AdjectivizeExtension extends AbstractExtension
{
    private const ADJECTIVES = [
        'Premier',
        'Deuxième',
        'troisième',
        'Quatrième',
        'Cinquième',
        'Sixième',
        'Septième',
        'Huitième',
        'Neuvième',
        'Dixième',
    ];

    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('adjectivize', [$this, 'convertToAdjective']),
        ];
    }

    public function convertToAdjective(int $value): string
    {
        return self::ADJECTIVES[$value];
    }
}
