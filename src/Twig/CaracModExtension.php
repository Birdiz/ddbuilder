<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CaracModExtension extends AbstractExtension
{
    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('mod', [$this, 'getModFromCarac']),
        ];
    }

    public function getModFromCarac(int $carac): int
    {
        $mod = ($carac - 10) / 2;

        return $mod < 0 ? (int) round($mod) : (int) floor($mod);
    }
}
