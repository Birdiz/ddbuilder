<?php

declare(strict_types=1);

namespace App\Twig;

use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Race\Race;
use App\Model\AbstractEnum;
use App\Util\StringTrait;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CharacterizeExtension extends AbstractExtension
{
    use StringTrait;

    /**
     * @return array<TwigFilter>
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('characterize', [$this, 'replaceCharacter']),
        ];
    }

    public function replaceCharacter(string $value): string
    {
        $needle = '{character}';
        for ($i = 0; $i <= substr_count($value, $needle); ++$i) {
            $name = Race::getName(
                AbstractEnum::getRandomEnum(Race::toArray())['name'],
                AbstractEnum::getRandomEnum(Gender::toArray())
            );

            static::replaceSubstring($value, $needle, "<b>$name</b>");
        }

        return $value;
    }
}
