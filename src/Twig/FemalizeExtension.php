<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FemalizeExtension extends AbstractExtension
{
    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('femalize', [$this, 'femalize']),
        ];
    }

    public function femalize(string $value, bool $isFemale = false): string
    {
        return false === $isFemale
            ? str_replace(['(e)', '(le)', '(te)', '(ne)'], '', $value)
            : str_replace(['(', ')'], '', $value);
    }
}
