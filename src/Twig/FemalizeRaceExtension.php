<?php

declare(strict_types=1);

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class FemalizeRaceExtension extends AbstractExtension
{
    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('femalizeRace', [$this, 'femalizeRace']),
        ];
    }

    public function femalizeRace(string $value, bool $isFemale = false): string
    {
        return false === $isFemale
            ? $value
            : str_replace(
                ['Nain', 'noir', 'Halfelin', 'Humain', 'Tieffelin'],
                ['Naine', 'noire,', 'Halfeline', 'Humaine', 'Tieffline'],
                $value
            );
    }
}
