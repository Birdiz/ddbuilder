<?php

declare(strict_types=1);

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class IsRouteActiveExtension extends AbstractExtension
{
    public function __construct(private RequestStack $requestStack)
    {
    }

    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('isRouteActive', [$this, 'isRouteActive']),
        ];
    }

    public function isRouteActive(string $route): string
    {
        $request = $this->requestStack->getCurrentRequest();

        return null !== $request && $request->attributes->get('_route') === $route ? 'is-active' : '';
    }
}
