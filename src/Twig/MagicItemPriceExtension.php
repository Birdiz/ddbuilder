<?php

declare(strict_types=1);

namespace App\Twig;

use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MagicItemPriceExtension extends AbstractExtension
{
    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('magicItemPrice', [$this, 'getMagicItemPrice']),
        ];
    }

    public function getMagicItemPrice(string $tableName, bool $isMerchantLoyal): int
    {
        return match ($tableName) {
            TableB::class, TableF::class => $isMerchantLoyal ? random_int(101, 500) : random_int(301, 500) + 50,
            TableC::class, TableG::class => $isMerchantLoyal ? random_int(501, 5000) : random_int(3001, 5000) + 250,
            TableD::class, TableH::class => $isMerchantLoyal
                ? random_int(5001, 50000)
                : random_int(15001, 50000) + 2000,
            default => $isMerchantLoyal ? random_int(50, 100) : random_int(75, 100) + 10,
        };
    }
}
