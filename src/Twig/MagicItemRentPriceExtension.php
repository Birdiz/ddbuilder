<?php

declare(strict_types=1);

namespace App\Twig;

use App\Util\StringTrait;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MagicItemRentPriceExtension extends AbstractExtension
{
    use StringTrait;

    /**
     * @return array<TwigFunction>
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('magicItemRentPrice', [$this, 'getMagicItemRentPrice']),
        ];
    }

    public function getMagicItemRentPrice(int $price): string
    {
        // Renting costs 10% from the item's price.
        return static::normalizeCoins(round((10 * $price) / 100));
    }
}
