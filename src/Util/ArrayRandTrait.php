<?php

declare(strict_types=1);

namespace App\Util;

use LogicException;
use MyCLabs\Enum\Enum;

trait ArrayRandTrait
{
    /**
     * @param array<mixed> $array
     */
    public static function arrayRand(array $array, int $nb = 1): mixed
    {
        return $array[array_rand($array, $nb)];
    }

    /**
     * @param array<mixed> $lists
     *
     * @return array<mixed>|int|string
     */
    public static function getXSimpleItems(int $x, array $lists): array|int|string
    {
        return 0 === $x ? [] : array_rand(array_flip($lists), $x);
    }

    /**
     * This method allows you to pick up elements in a list with complex consts (arrays).
     *
     * @param array<mixed> $lists
     *
     * @return array<mixed>
     */
    public static function getXComplexeItems(int $x, array $lists, bool $isRandomized = true): array
    {
        return 1 > $x
            ? []
            : self::getRandomValueFromIdx(
                array_rand($lists, true === $isRandomized ? random_int(1, $x) : $x),
                $lists
            );
    }

    /**
     * This method return an array of stacked elements from a given list.
     * $x represents how many times you pick up in the list
     * $y represents how many elements you want to pick up.
     *
     * @param array<mixed> $lists
     *
     * @return array<mixed>
     */
    public static function getMultipleItems(int $x, int $y, array $lists): array
    {
        if (1 > $x || 1 > $y) {
            throw new LogicException('First and second parameters should be positive.');
        }

        $randomValues = [];
        for ($i = 1; $i <= $x; ++$i) {
            $randomValues[] = self::getRandomValueFromIdx(array_rand($lists, random_int(1, $y)), $lists);
        }

        return self::arrayFlatten($randomValues);
    }

    /**
     * @param array<mixed> $array
     */
    public static function shuffle(array &$array): void
    {
        $randArr = [];
        $arrLength = count($array);
        // while my array is not empty I select a random position
        while ([] !== $array) {
            --$arrLength;
            $randPos = random_int(0, $arrLength);
            $randArr[] = $array[$randPos];

            /* If number of remaining elements in the array is the same as the
             * random position, take out the item in that position,
             * else use the negative offset.
             * This will prevent array_splice removing the last item.
             */
            array_splice($array, $randPos, ($randPos === $arrLength ? 1 : $randPos - $arrLength));
        }

        $array = $randArr;
    }

    /**
     * @param array<mixed> $items
     *
     * @return array<mixed>
     */
    private static function arrayFlatten(array $items): array
    {
        return array_reduce(
            $items,
            static fn ($carry, $item) => true === is_array($item)
                ? [...$carry, ...self::arrayFlatten($item)]
                : [...$carry, $item],
            []
        );
    }

    /**
     * @param int|string|array<mixed> $randomIdx
     * @param array<mixed>            $lists
     *
     * @return array<mixed>
     */
    private static function getRandomValueFromIdx(array|int|string $randomIdx, array $lists): array
    {
        if (false === is_iterable($randomIdx)) {
            $value = [$lists[$randomIdx] instanceof Enum ? $lists[$randomIdx]->getValue() : $lists[$randomIdx]];
        } else {
            $value = array_map(
                static function (int|string $idx) use ($lists) {
                    return $lists[$idx] instanceof Enum ? $lists[$idx]->getValue() : $lists[$idx];
                },
                $randomIdx
            );
        }

        return $value;
    }

    /**
     * @param array<mixed> $array
     */
    private static function shuffleAssoc(array &$array): bool
    {
        $keys = array_keys($array);

        static::shuffle($keys);

        $new = [];
        foreach ($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }
}
