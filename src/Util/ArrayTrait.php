<?php

declare(strict_types=1);

namespace App\Util;

trait ArrayTrait
{
    /**
     * @param array<mixed> $haystack
     */
    public static function arraySearchMultidimensional(mixed $needle, array $haystack): int|string|null
    {
        foreach ($haystack as $key => $val) {
            $found = in_array($needle, $val, true);

            if (false !== $found) {
                return $key;
            }
        }

        return null;
    }

    /**
     * @param array<mixed> $array
     */
    public static function sortBy(array &$array, string $column): void
    {
        $sortColumn = array_column($array, $column);
        array_multisort($sortColumn, SORT_ASC, $array);
    }

    /**
     * @param array<string, int> $values
     *
     * @return array<string, int>
     */
    public static function filterZeroValues(array $values): array
    {
        return array_filter(
            $values,
            static function (int $val) {
                return 0 !== $val;
            }
        );
    }

    /**
     * Count every occurence of each element in $array and return a new array with a formatted string: 'XXX item'.
     *
     * @param array<mixed> $array
     *
     * @return array<mixed>
     */
    public static function getStackedArray(array $array): array
    {
        $stackedArray = array_count_values($array);
        array_walk(
            $stackedArray,
            static function (&$value, int|string $key) {
                $value = "$value $key";
            }
        );

        return $stackedArray;
    }
}
