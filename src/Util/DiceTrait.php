<?php

declare(strict_types=1);

namespace App\Util;

trait DiceTrait
{
    public static function compute(int $dices, int $dice, int $mod = 0): int
    {
        $result = $mod;
        for ($i = 0; $i < $dices; ++$i) {
            $result += random_int(1, $dice);
        }

        return $result;
    }
}
