<?php

declare(strict_types=1);

namespace App\Util;

use App\Enum\Generator\PNJ\Gender;
use App\Enum\Generator\PNJ\Race\Race;
use App\Enum\Generator\PNJ\Relationship;
use App\Model\AbstractEnum;

trait FamilyTrait
{
    use ArrayRandTrait;

    /**
     * @return array<string, mixed>
     */
    public static function getFamily(string $race, int $nbMembers = 1, ?string $relationShip = null): array
    {
        $nbMales = random_int(1, $nbMembers);
        $nbFemales = $nbMembers - $nbMales;

        $members = self::getFamilyMembers($race, $nbMales, $nbFemales);

        $family['members'] = self::getMembersRelation(
            is_array($members['males']) ? $members['males'] : [$members['males']],
            $relationShip
        );
        $family['members'] = array_merge(
            $family['members'],
            self::getMembersRelation(
                is_array($members['females']) ? $members['females'] : [$members['females']],
                $relationShip,
                false
            )
        );

        $family['total'] = count($family['members']);

        return $family;
    }

    /**
     * @param array<string, mixed> $members
     *
     * @return array<int, array<string, mixed>>
     */
    private static function getMembersRelation(
        array $members,
        ?string $relationShip = null,
        bool $areMembersMale = true
    ): array {
        $relations = [];
        $relationNames = $areMembersMale ? \App\Enum\Generator\PNJ\Family\Male::toArray() : \App\Enum\Generator\PNJ\Family\Female::toArray();

        self::removeUnlogicalRelationShips($relationNames, $relationShip);

        $grandMothers = 0;
        $grandFathers = 0;
        $hasMother = false;
        $hasFather = false;
        $hasHusband = false;
        $hasWife = false;

        $grandMa = \App\Enum\Generator\PNJ\Family\Female::GRANDMA()->getValue();
        $grandPa = \App\Enum\Generator\PNJ\Family\Male::GRANDPA()->getValue();
        $mother = \App\Enum\Generator\PNJ\Family\Female::MOTHER()->getValue();
        $father = \App\Enum\Generator\PNJ\Family\Male::FATHER()->getValue();
        $husband = \App\Enum\Generator\PNJ\Family\Male::HUSBAND()->getValue();
        $wife = \App\Enum\Generator\PNJ\Family\Female::WIFE()->getValue();

        foreach ($members as $member) {
            $relationName = AbstractEnum::getRandomEnum($relationNames);

            while (true === $hasMother && $mother === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }
            while (true === $hasFather && $father === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }
            while (true === $hasHusband && $husband === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }
            while (true === $hasWife && $wife === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }
            while (2 === $grandMothers && $grandMa === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }
            while (2 === $grandFathers && $grandPa === $relationName) {
                $relationName = AbstractEnum::getRandomEnum($relationNames);
            }

            $hasMother = $hasMother || $mother === $relationName;
            $hasFather = $hasFather || $father === $relationName;
            $hasHusband = $hasHusband || $husband === $relationName;
            $hasWife = $hasWife || $wife === $relationName;

            $grandMothers += $grandMa === $relationName ? 1 : 0;
            $grandFathers += $grandPa === $relationName ? 1 : 0;

            $relations[] = [
                'name' => $member,
                'relation' => $relationName,
            ];
        }

        return $relations;
    }

    /**
     * @param array<string, mixed> $relationNames
     */
    private static function removeUnlogicalRelationShips(array &$relationNames, ?string $relationShip): void
    {
        if (true === in_array(
                $relationShip,
                [
                    Relationship::BROKE_UP()->getValue(),
                    Relationship::DIVORCED()->getValue(),
                    Relationship::SINGLE()->getValue(),
                    Relationship::WIDOWER()->getValue(),
                ],
                true)
        ) {
            $key = array_search(\App\Enum\Generator\PNJ\Family\Male::HUSBAND()->getValue(), $relationNames, true);
            if (false !== $key) {
                unset($relationNames[$key]);
            }
            $key = array_search(\App\Enum\Generator\PNJ\Family\Female::WIFE()->getValue(), $relationNames, true);
            if (false !== $key) {
                unset($relationNames[$key]);
            }
        }
    }

    /**
     * @return array<string, mixed>
     */
    private static function getFamilyMembers(string $race, int $nbMales, int $nbFemales): array
    {
        $races = Race::getByName();

        if (true === in_array(
                $race,
                [
                    Race::DRAGONBORN()->getValue()['name'],
                    Race::TIEFLING()->getValue()['name'],
                    Race::AARAKOCRA()->getValue()['name'],
                ],
                true)
        ) {
            return [
                'males' => $races[$race]['maleClass']::getName(Gender::MALE()->getValue(), $nbMales),
                'females' => $races[$race]['femaleClass']::getName(Gender::FEMALE()->getValue(), $nbFemales),
            ];
        }

        return [
            'males' => static::getXSimpleItems($nbMales, $races[$race]['maleClass']::toArray()),
            'females' => static::getXSimpleItems($nbFemales, $races[$race]['femaleClass']::toArray()),
        ];
    }
}
