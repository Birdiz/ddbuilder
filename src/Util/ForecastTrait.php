<?php

declare(strict_types=1);

namespace App\Util;

use App\Enum\Environment\Weather\Precipitation;
use App\Enum\Environment\Weather\TemperatureEffect;

trait ForecastTrait
{
    public static function getTemperature(TemperatureEffect $temperature, string $precipitation): ?float
    {
        $temperatureMod = TemperatureEffect::getTemperatureModFormKey($temperature->getKey());
        $precipitationDie = Precipitation::getTemperatureFormKey($precipitation); // température de base

        return round($temperatureMod + $precipitationDie);
    }
}
