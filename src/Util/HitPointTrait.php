<?php

declare(strict_types=1);

namespace App\Util;

use App\Enum\Object\Integrity;
use App\Enum\Object\Material;
use App\Enum\Object\Size;

trait HitPointTrait
{
    public static function getHitPointsForObject(string $integrity, Size $size, string $material): ?string
    {
        $integrityMultiplier = Integrity::getMultiplierFormKey($integrity);
        $sizeMultiplier = Size::getMultiplierFormKey($size->getKey());
        $materialDie = Material::getDieFromValue($material);

        return round($integrityMultiplier * $sizeMultiplier * random_int(1, $materialDie)).'PV';
    }
}
