<?php

declare(strict_types=1);

namespace App\Util;

trait PercentageTrait
{
    public static function percentage(
        float|int $value,
        float|int $percentage,
        bool $rounded = true
    ): float|int {
        $result = $value * $percentage / 100;

        return true === $rounded ? (int) floor($result) : $result;
    }
}
