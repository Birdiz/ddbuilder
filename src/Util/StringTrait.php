<?php

declare(strict_types=1);

namespace App\Util;

trait StringTrait
{
    public static function normalizeCoins(float $number): string
    {
        return str_replace(
            ['PO 0', 'PO 3', 'PO 5', 'PO 8'],
            ['PO', 'PO et 3 PA', 'PO et 5 PA', 'PO et 8 PA'],
            number_format(
                $number,
                1,
                ' PO ',
                ' '
            )
        );
    }

    public static function replaceSubstring(string &$haystack, string $needle, string $replace): void
    {
        $pos = strpos($haystack, $needle);
        if (false !== $pos) {
            $haystack = substr_replace($haystack, $replace, $pos, \strlen($needle));
        }
    }
}
