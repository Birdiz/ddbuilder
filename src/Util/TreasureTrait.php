<?php

declare(strict_types=1);

namespace App\Util;

use App\Enum\Dungeon\Treasure\Art\ArtT1;
use App\Enum\Dungeon\Treasure\Art\ArtT2;
use App\Enum\Dungeon\Treasure\Art\ArtT3;
use App\Enum\Dungeon\Treasure\Art\ArtT4;
use App\Enum\Dungeon\Treasure\Art\ArtT5;
use App\Enum\Dungeon\Treasure\Container;
use App\Enum\Dungeon\Treasure\Gem\GemT1;
use App\Enum\Dungeon\Treasure\Gem\GemT2;
use App\Enum\Dungeon\Treasure\Gem\GemT3;
use App\Enum\Dungeon\Treasure\Gem\GemT4;
use App\Enum\Dungeon\Treasure\Gem\GemT5;
use App\Enum\Dungeon\Treasure\Gem\GemT6;
use App\Enum\Dungeon\Treasure\Litterature;
use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use App\Enum\Dungeon\Treasure\Trinket;
use App\Enum\Hoard;
use App\Model\AbstractEnum;

trait TreasureTrait
{
    use WeightedRandTrait;
    use ArrayRandTrait;
    use ArrayTrait;

    /**
     * @return array<string, mixed>
     */
    public static function buildBeginnerHoard(): array
    {
        return array_merge(
            self::getBeginnerReward(),
            self::getBeginnerMagic(),
            self::getBeginnerHoardCoins()
        );
    }

    /**
     * @return array<string, mixed>
     */
    public static function buildExpertHoard(): array
    {
        return array_merge(self::getExpertReward(), self::getExpertMagic(), self::getExpertHoardCoins());
    }

    /**
     * @return array<string, mixed>
     */
    public static function buildHeroHoard(): array
    {
        return array_merge(self::getHeroReward(), self::getHeroMagic(), self::getHeroHoardCoins());
    }

    /**
     * @return array<string, mixed>
     */
    public static function buildGodLikeHoard(): array
    {
        return array_merge(
            self::getGodLikeReward(),
            self::getGodLikeMagic(),
            self::getGodLikeHoardCoins()
        );
    }

    /**
     * @return array<int>
     */
    private static function getCoinWeights(bool $isHeroLevel, bool $isDivinLevel): array
    {
        if (true === $isHeroLevel) {
            return [20, 15, 40, 15];
        }
        if (true === $isDivinLevel) {
            return [15, 40, 45];
        }

        return [30, 30, 10, 25, 5];
    }

    /**
     * @param array<int> $pcs
     * @param array<int> $pas
     * @param array<int> $pes
     * @param array<int> $pos
     * @param array<int> $pps
     *
     * @return array<string, int>
     */
    private static function getRandomCoins(
        array $pcs,
        array $pas,
        array $pes,
        array $pos,
        array $pps,
        bool $isHeroLevel = false,
        bool $isDivinLevel = false
    ): array {
        return self::filterZeroValues(
            [
                'PC' => static::getRandomizedValue($pcs, self::getCoinWeights($isHeroLevel, $isDivinLevel)),
                'PA' => static::getRandomizedValue($pas, self::getCoinWeights($isHeroLevel, $isDivinLevel)),
                'PE' => static::getRandomizedValue($pes, self::getCoinWeights($isHeroLevel, $isDivinLevel)),
                'PO' => static::getRandomizedValue($pos, self::getCoinWeights($isHeroLevel, $isDivinLevel)),
                'PP' => static::getRandomizedValue($pps, self::getCoinWeights($isHeroLevel, $isDivinLevel)),
            ]
        );
    }

    /**
     * @return array<string, int>
     */
    private static function getHoardCoins(int $pcs, int $pas, int $pes, int $pos, int $pps): array
    {
        return self::filterZeroValues(['PC' => $pcs, 'PA' => $pas, 'PE' => $pes, 'PO' => $pos, 'PP' => $pps]);
    }

    /**
     * @param array<string, string>              $types
     * @param array<int>                         $weights
     * @param array<string, array<mixed>|string> $values
     *
     * @return array<string, mixed>
     */
    private static function getRandomReward(array $types, array $weights, array $values): array
    {
        $rewardType = static::getRandomizedValue($types, $weights);

        $reward = [];
        foreach ($types as $label => $type) {
            if ($rewardType === $type) {
                $reward = [$label => $values[$type]];
            }
        }

        return $reward;
    }

    /**
     * @param array<AbstractEnum> $array1
     * @param array<AbstractEnum> $array2
     *
     * @return array<AbstractEnum>
     */
    private static function getMergedMagicItems(int $nbArray1, array $array1, int $nbArray2, array $array2): array
    {
        return array_merge(
            static::getXComplexeItems(random_int(1, $nbArray1), $array1),
            static::getXComplexeItems(random_int(1, $nbArray2), $array2),
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGodLikeReward(): array
    {
        return [
            'rewards' => self::getRandomReward(
                [
                    'Rien' => Hoard::NOTHING()->getValue(),
                    'Objet d\'art (estimé à 2500 PO)' => Hoard::ARTT4()->getValue(),
                    'Objet d\'art (estimé à 7500 PO)' => Hoard::ARTT5()->getValue(),
                    'Gemme (estimé à 1000 PO)' => Hoard::GEMT5()->getValue(),
                    'Gemme (estimé à 5000 PO)' => Hoard::GEMT6()->getValue(),
                ],
                [4, 24, 24, 24, 24],
                [
                    Hoard::NOTHING()->getValue() => ' - ',
                    Hoard::ARTT4()->getValue() => static::getMultipleItems(1, 10, ArtT4::toArray()),
                    Hoard::ARTT5()->getValue() => static::getMultipleItems(1, 4, ArtT5::toArray()),
                    Hoard::GEMT5()->getValue() => static::getMultipleItems(3, 6, GemT5::toArray()),
                    Hoard::GEMT6()->getValue() => static::getMultipleItems(2, 4, GemT6::toArray()),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGodLikeMagic(): array
    {
        return self::getRandomReward(
            [
                'Rien' => Hoard::NOTHING()->getValue(),
                'tableC' => Hoard::TABLEC()->getValue(),
                'tableD' => Hoard::TABLED()->getValue(),
                'tableE' => Hoard::TABLEE()->getValue(),
                'tableG' => Hoard::TABLEG()->getValue(),
                'tableH' => Hoard::TABLEH()->getValue(),
                'tableI' => Hoard::TABLEI()->getValue(),
            ],
            [4, 16, 16, 16, 16, 16, 16],
            [
                Hoard::NOTHING()->getValue() => ' - ',
                Hoard::TABLEC()->getValue() => static::getXComplexeItems(random_int(1, 8), TableC::toArray()),
                Hoard::TABLED()->getValue() => static::getXComplexeItems(random_int(1, 6), TableD::toArray()),
                Hoard::TABLEE()->getValue() => static::getXComplexeItems(random_int(1, 6), TableE::toArray()),
                Hoard::TABLEG()->getValue() => static::getXComplexeItems(random_int(1, 4), TableG::toArray()),
                Hoard::TABLEH()->getValue() => static::getXComplexeItems(random_int(1, 4), TableH::toArray()),
                Hoard::TABLEI()->getValue() => static::getXComplexeItems(random_int(1, 4), TableI::toArray()),
            ]
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getExpertReward(): array
    {
        return [
            'rewards' => self::getRandomReward(
                [
                    'Rien' => Hoard::NOTHING()->getValue(),
                    'Objet d\'art (estimé à 25 PO)' => Hoard::ARTT1()->getValue(),
                    'Objet d\'art (estimé à 250 PO)' => Hoard::ARTT2()->getValue(),
                    'Gemme (estimée à 50 PO)' => Hoard::GEMT2()->getValue(),
                    'Gemme (estimée à 100 PO)' => Hoard::GEMT3()->getValue(),
                ],
                [2, 21, 28, 21, 28],
                [
                    Hoard::NOTHING()->getValue() => ' - ',
                    Hoard::ARTT1()->getValue() => static::getMultipleItems(2, 4, ArtT1::toArray()),
                    Hoard::ARTT2()->getValue() => static::getMultipleItems(2, 4, ArtT2::toArray()),
                    Hoard::GEMT2()->getValue() => static::getMultipleItems(3, 6, GemT2::toArray()),
                    Hoard::GEMT3()->getValue() => static::getMultipleItems(3, 6, GemT3::toArray()),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getExpertMagic(): array
    {
        return self::getRandomReward(
            [
                'Rien' => Hoard::NOTHING()->getValue(),
                'tableA' => Hoard::TABLEA()->getValue(),
                'tableB' => Hoard::TABLEB()->getValue(),
                'tableC' => Hoard::TABLEC()->getValue(),
                'tableD' => Hoard::TABLED()->getValue(),
                'tableF' => Hoard::TABLEF()->getValue(),
                'tableG' => Hoard::TABLEG()->getValue(),
                'tableH' => Hoard::TABLEH()->getValue(),
            ],
            [17, 14, 14, 14, 14, 14, 7, 6],
            [
                Hoard::NOTHING()->getValue() => ' - ',
                Hoard::TABLEA()->getValue() => static::getXComplexeItems(random_int(1, 6), TableA::toArray()),
                Hoard::TABLEB()->getValue() => static::getXComplexeItems(random_int(1, 4), TableB::toArray()),
                Hoard::TABLEC()->getValue() => static::getXComplexeItems(random_int(1, 4), TableC::toArray()),
                Hoard::TABLED()->getValue() => static::getXComplexeItems(1, TableD::toArray()),
                Hoard::TABLEF()->getValue() => static::getXComplexeItems(random_int(1, 4), TableF::toArray()),
                Hoard::TABLEG()->getValue() => static::getXComplexeItems(random_int(1, 4), TableG::toArray()),
                Hoard::TABLEH()->getValue() => static::getXComplexeItems(1, TableH::toArray()),
            ]
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHeroReward(): array
    {
        return [
            'rewards' => self::getRandomReward(
                [
                    'Rien' => Hoard::NOTHING()->getValue(),
                    'Objet d\'art (estimé à 250 PO)' => Hoard::ARTT2()->getValue(),
                    'Objet d\'art (estimé à 750 PO)' => Hoard::ARTT3()->getValue(),
                    'Gemme (estimée à 500 PO)' => Hoard::GEMT4()->getValue(),
                    'Gemme (estimée à 1000 PO)' => Hoard::GEMT5()->getValue(),
                ],
                [4, 24, 24, 24, 24],
                [
                    Hoard::NOTHING()->getValue() => ' - ',
                    Hoard::ARTT2()->getValue() => static::getMultipleItems(2, 4, ArtT2::toArray()),
                    Hoard::ARTT3()->getValue() => static::getMultipleItems(2, 4, ArtT3::toArray()),
                    Hoard::GEMT4()->getValue() => static::getMultipleItems(3, 6, GemT4::toArray()),
                    Hoard::GEMT5()->getValue() => static::getMultipleItems(3, 6, GemT5::toArray()),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHeroMagic(): array
    {
        return self::getRandomReward(
            [
                'Rien' => Hoard::NOTHING()->getValue(),
                'tableA' => Hoard::TABLEA()->getValue(),
                'tableC' => Hoard::TABLEC()->getValue(),
                'tableD' => Hoard::TABLED()->getValue(),
                'tableE' => Hoard::TABLEE()->getValue(),
                'tableF' => Hoard::TABLEF()->getValue(),
                'tableH' => Hoard::TABLEH()->getValue(),
                'tableI' => Hoard::TABLEI()->getValue(),
            ],
            [16, 12, 12, 12, 12, 12, 12, 12],
            [
                Hoard::NOTHING()->getValue() => ' - ',
                Hoard::TABLEA()->getValue() => self::getMergedMagicItems(4, TableA::toArray(), 6, TableB::toArray()),
                Hoard::TABLEC()->getValue() => static::getXComplexeItems(random_int(1, 6), TableC::toArray()),
                Hoard::TABLED()->getValue() => static::getXComplexeItems(random_int(1, 4), TableD::toArray()),
                Hoard::TABLEE()->getValue() => static::getXComplexeItems(1, TableE::toArray()),
                Hoard::TABLEF()->getValue() => self::getMergedMagicItems(1, TableF::toArray(), 4, TableG::toArray()),
                Hoard::TABLEH()->getValue() => static::getXComplexeItems(random_int(1, 4), TableH::toArray()),
                Hoard::TABLEI()->getValue() => static::getXComplexeItems(1, TableI::toArray()),
            ]
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBeginnerReward(): array
    {
        return [
            'rewards' => self::getRandomReward(
                [
                    'Rien' => Hoard::NOTHING()->getValue(),
                    'Objet d\'art (estimé à 25 PO)' => Hoard::ARTT1()->getValue(),
                    'Gemme (estimée à 10 PO)' => Hoard::GEMT1()->getValue(),
                    'Gemme (estimée à 50 PO)' => Hoard::GEMT2()->getValue(),
                ],
                [4, 24, 36, 36],
                [
                    Hoard::NOTHING()->getValue() => ' - ',
                    Hoard::ARTT1()->getValue() => static::getMultipleItems(2, 4, ArtT1::toArray()),
                    Hoard::GEMT1()->getValue() => static::getMultipleItems(2, 6, GemT1::toArray()),
                    Hoard::GEMT2()->getValue() => static::getMultipleItems(2, 6, GemT2::toArray()),
                ]
            ),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBeginnerMagic(): array
    {
        return self::getRandomReward(
            [
                'Rien' => Hoard::NOTHING()->getValue(),
                'tableA' => Hoard::TABLEA()->getValue(),
                'tableB' => Hoard::TABLEB()->getValue(),
                'tableC' => Hoard::TABLEC()->getValue(),
                'tableF' => Hoard::TABLEF()->getValue(),
                'tableG' => Hoard::TABLEG()->getValue(),
            ],
            [25, 17, 17, 17, 12, 12],
            [
                Hoard::NOTHING()->getValue() => ' - ',
                Hoard::TABLEA()->getValue() => static::getXComplexeItems(random_int(1, 6), TableA::toArray()),
                Hoard::TABLEB()->getValue() => static::getXComplexeItems(random_int(1, 4), TableB::toArray()),
                Hoard::TABLEC()->getValue() => static::getXComplexeItems(random_int(1, 4), TableC::toArray()),
                Hoard::TABLEF()->getValue() => static::getXComplexeItems(1, TableF::toArray()),
                Hoard::TABLEG()->getValue() => static::getXComplexeItems(1, TableG::toArray()),
            ]
        );
    }

    /**
     * @return array<string, array<string, int>>
     */
    private static function getBeginnerHoardCoins(): array
    {
        return [
            'coins' => self::getHoardCoins(
                6 * random_int(1, 6) * 100,
                3 * random_int(1, 6) * 100,
                0,
                2 * random_int(1, 6) * 10,
                0
            ),
        ];
    }

    /**
     * @return array<string, array<string, int>>
     */
    private static function getExpertHoardCoins(): array
    {
        return [
            'coins' => self::getHoardCoins(
                2 * random_int(1, 6) * 100,
                2 * random_int(1, 6) * 1000,
                0,
                6 * random_int(1, 6) * 100,
                3 * random_int(1, 6) * 10
            ),
        ];
    }

    /**
     * @return array<string, array<string, int>>
     */
    private static function getHeroHoardCoins(): array
    {
        return [
            'coins' => self::getHoardCoins(
                0,
                0,
                0,
                4 * random_int(1, 6) * 1000,
                5 * random_int(1, 6) * 100
            ),
        ];
    }

    /**
     * @return array<string, array<string, int>>
     */
    private static function getGodLikeHoardCoins(): array
    {
        return [
            'coins' => self::getHoardCoins(
                0,
                0,
                0,
                12 * random_int(1, 6) * 1000,
                8 * random_int(1, 6) * 1000
            ),
        ];
    }

    /**
     * @return array<string, int>
     */
    private static function getBeginnerCoins(): array
    {
        return self::getRandomCoins(
            [5 * random_int(1, 6), 0, 0, 0, 0],
            [0, 4 * random_int(1, 6), 0, 0, 0],
            [0, 0, 3 * random_int(1, 6), 0, 0],
            [0, 0, 0, 3 * random_int(1, 6), 0],
            [0, 0, 0, 0, random_int(1, 6)]
        );
    }

    /**
     * @return array<string, int>
     */
    private static function getExpertCoins(): array
    {
        return self::getRandomCoins(
            [4 * random_int(1, 6) * 100, 0, 0, 0, 0],
            [0, 6 * random_int(1, 6) * 10, 0, 0, 0],
            [random_int(1, 6) * 10, 0, 3 * random_int(1, 6) * 10, 0, 0],
            [
                0,
                2 * random_int(1, 6) * 10,
                2 * random_int(1, 6) * 10,
                4 * random_int(1, 6) * 10,
                2 * random_int(1, 6) * 10,
            ],
            [0, 0, 0, 0, 3 * random_int(1, 6)]
        );
    }

    /**
     * @return array<string, int>
     */
    private static function getHeroCoins(): array
    {
        return self::getRandomCoins(
            [0, 0, 0, 0],
            [4 * random_int(1, 6) * 100, 0, 0, 0],
            [0, random_int(1, 6) * 100, 0, 0],
            [
                random_int(1, 6) * 100,
                random_int(1, 6) * 100,
                2 * random_int(1, 6) * 100,
                2 * random_int(1, 6) * 100,
            ],
            [0, 0, random_int(1, 6) * 10, 2 * random_int(1, 6) * 10],
            true
        );
    }

    /**
     * @return array<string, int>
     */
    private static function getGodLikeCoins(): array
    {
        return self::getRandomCoins(
            [0, 0, 0],
            [0, 0, 0],
            [0, 2 * random_int(1, 6) * 1000, 0],
            [8 * random_int(1, 6) * 100, random_int(1, 6) * 1000, random_int(1, 6) * 1000],
            [0, random_int(1, 6) * 100, 2 * random_int(1, 6) * 100],
            false,
            true
        );
    }

    /**
     * @return array<string, mixed>
     */
    private static function getBeginnerTreasure(bool $needOnlyCoins): array
    {
        return true === $needOnlyCoins
            ? ['coins' => self::getBeginnerCoins()]
            : [
                'litterature' => static::getXComplexeItems(1, Litterature::toArray()),
                'trinket' => static::getXComplexeItems(5, Trinket::toArray()),
                'container' => static::getXComplexeItems(3, Container::toArray()),
                'coins' => self::getBeginnerCoins(),
                'tableA' => 1 === random_int(1, 5) ? static::getXComplexeItems(1, TableA::toArray()) : [],
                'tableB' => 1 === random_int(1, 8) ? static::getXComplexeItems(1, TableB::toArray()) : [],
            ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getExpertTreasure(bool $needOnlyCoins): array
    {
        return true === $needOnlyCoins
            ? ['coins' => self::getExpertCoins()]
            : [
                'litterature' => static::getXComplexeItems(2, Litterature::toArray()),
                'trinket' => static::getXComplexeItems(3, Trinket::toArray()),
                'container' => static::getXComplexeItems(2, Container::toArray()),
                'coins' => self::getExpertCoins(),
                'tableB' => 1 === random_int(1, 5) ? static::getXComplexeItems(1, TableB::toArray()) : [],
                'tableC' => 1 === random_int(1, 5) ? static::getXComplexeItems(1, TableC::toArray()) : [],
            ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getHeroTreasure(bool $needOnlyCoins): array
    {
        return true === $needOnlyCoins
            ? ['coins' => self::getHeroCoins()]
            : [
                'litterature' => static::getXComplexeItems(1, Litterature::toArray()),
                'trinket' => static::getXComplexeItems(1, Trinket::toArray()),
                'container' => static::getXComplexeItems(1, Container::toArray()),
                'coins' => self::getHeroCoins(),
                'tableC' => 1 === random_int(1, 5) ? static::getXComplexeItems(1, TableC::toArray()) : [],
                'tableD' => 1 === random_int(1, 8) ? static::getXComplexeItems(1, TableD::toArray()) : [],
            ];
    }

    /**
     * @return array<string, mixed>
     */
    private static function getGodLikeTreasure(bool $needOnlyCoins): array
    {
        return true === $needOnlyCoins
            ? ['coins' => self::getGodLikeCoins()]
            : [
                'litterature' => static::getXComplexeItems(1, Litterature::toArray()),
                'trinket' => static::getXComplexeItems(1, Trinket::toArray()),
                'container' => static::getXComplexeItems(1, Container::toArray()),
                'coins' => self::getGodLikeCoins(),
                'tableD' => 1 === random_int(1, 8) ? static::getXComplexeItems(1, TableD::toArray()) : [],
                'tableE' => 1 === random_int(1, 15) ? static::getXComplexeItems(1, TableE::toArray()) : [],
            ];
    }
}
