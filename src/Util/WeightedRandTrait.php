<?php

declare(strict_types=1);

namespace App\Util;

use LogicException;

trait WeightedRandTrait
{
    /**
     * Given an array of values, and weights for those values (any positive int)
     * it will select a value randomly as often as the given weight allows.
     * for example:
     * values(A, B, C, D)
     * weights(30, 50, 100, 25)
     * Given these values C should come out twice as often as B, and 4 times as often as D.
     *
     * @param array<mixed> $values
     * @param array<int>   $weights
     */
    public static function getRandomizedValue(array $values, array $weights): mixed
    {
        $i = $n = 0;
        $num = random_int(0, array_sum($weights));
        $values = array_values($values);
        $valuesLength = count($values);

        while ($i <= $valuesLength) {
            $n += $weights[$i];
            if ($n >= $num) {
                break;
            }
            ++$i;
        }

        return $values[$i];
    }

    /**
     * @param array<string>   $values
     * @param array<int>|null $weights
     *
     * @return array<string, int>
     */
    public static function distributePopulation(array $values, int $population, ?array $weights = null): array
    {
        $populations = [];

        $i = 1;
        while ($i < count($values) && $population > 0) {
            $nb = random_int(1, $population);

            $item = null !== $weights
                ? (string) self::getRandomizedValue($values, $weights)
                : (string) array_rand($values);

            if (true === array_key_exists($item, $populations)) {
                $populations[$item] += $nb;
            } else {
                $populations[$item] = $nb;
            }

            $population -= $nb;
            ++$i;
        }

        arsort($populations);

        return $populations;
    }

    /**
     * This method return an array of stacked elements from a given list.
     * $x represents how many times you pick up in the list
     * $y represents how many elements you want to pick up.
     *
     * @param array<mixed> $lists
     * @param array<int>   $weigths
     *
     * @return array<mixed>
     */
    public static function getMultipleItemsWeigthed(int $x, array $lists, array $weigths): array
    {
        if (1 > $x) {
            throw new LogicException('First parameter should be positive.');
        }

        $randomValues = [];
        for ($i = 1; $i <= $x; ++$i) {
            $randomValues[] = self::getRandomizedValue($lists, $weigths);
        }

        return $randomValues;
    }
}
