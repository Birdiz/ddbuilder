<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class AdventureControllerTest extends WebTestCase
{
    private const TITLES = [
        'Éléments au hasard',
        'Course poursuite au hasard',
        'Petite histoire au hasard',
    ];

    public function testGetDmScreen(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/adventure');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
