<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ArsenalControllerTest extends WebTestCase
{
    private const TITLES = [
        'Règles additionnelles',
        'Armes',
        'Armures',
        'Argenture',
        'Extension des armes',
        'Extension des munitions',
        'Extension des armures',
        'Extension des boucliers',
        'Armes à feu',
    ];

    public function testGetArsenal(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/arsenal');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
