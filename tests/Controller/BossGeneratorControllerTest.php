<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Danger;
use App\Enum\Level;
use App\Enum\Stuff;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class BossGeneratorControllerTest extends WebTestCase
{
    private const TITLES = [
        'Boss',
    ];

    public function testGetBoss(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/generator/boss');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostBoss(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/boss');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#boss_Niveau');
        static::assertSelectorExists('#boss_Danger');
        static::assertSelectorExists('#boss_Stuff');

        foreach (Level::keys() as $level) {
            foreach (Danger::keys() as $danger) {
                foreach (Stuff::keys() as $stuff) {
                    $client->submitForm(
                        'boss_OK',
                        [
                            'boss[Niveau]' => $level,
                            'boss[Danger]' => $danger,
                            'boss[Stuff]' => $stuff,
                        ]
                    );

                    static::assertResponseIsSuccessful();
                }
            }
        }
    }
}
