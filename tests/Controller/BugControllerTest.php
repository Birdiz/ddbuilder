<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BugControllerTest extends WebTestCase
{
    public function testPostBugSpam(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/bug');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#bug_email');
        static::assertSelectorExists('#bug_message');

        $client->submitForm(
            'Envoyer',
            [
                'bug[email]' => 'akismet-guaranteed-spam@example.com',
                'bug[message]' => 'akismet-guaranteed-spam',
                'bug[miellat]' => 'viagra-test-123',
            ]
        );

        static::assertResponseStatusCodeSame(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function testPostBug(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/bug');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#bug_email');
        static::assertSelectorExists('#bug_message');

        $client->submitForm(
            'Envoyer',
            [
                'bug[email]' => 'john.doe@gmail.com',
                'bug[message]' => 'Voici un message pour vous dire qu\'il a un bug.',
            ]
        );

        static::assertResponseRedirects('/bug', Response::HTTP_FOUND);
    }
}
