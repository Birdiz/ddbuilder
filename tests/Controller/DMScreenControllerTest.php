<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DMScreenControllerTest extends WebTestCase
{
    private const TITLES = [
        'Exemples de dégâts',
        'Trains de vie',
        'Transport',
        'Propriétés et coûts d\'entretien',
        'Extension des armes',
    ];

    public function testGetDmScreen(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/dmscreen');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
