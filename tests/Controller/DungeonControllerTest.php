<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Dungeon\Treasure\Type;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class DungeonControllerTest extends WebTestCase
{
    private const TITLES = [
        'Éléments au hasard',
        'Trésors',
        'Piège au hasard',
    ];

    public function testGetDungeon(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/dungeon');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostDungeonTreasure(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/dungeon');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#treasure_Type');
        static::assertSelectorExists('#treasure_Coffres');
        static::assertSelectorExists('#treasure_Options_0');
        static::assertSelectorExists('#treasure_Options_1');
        static::assertSelectorExists('#treasure_Options_2');
        static::assertSelectorExists('#treasure_Options_3');

        foreach (Type::keys() as $type) {
            $client->submitForm(
                'treasure_OK',
                [
                    'treasure[Type]' => $type,
                    'treasure[Coffres]' => '1',
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostDungeonHoard(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/dungeon');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#treasure_Type');
        static::assertSelectorExists('#treasure_Coffres');
        static::assertSelectorExists('#treasure_Options_0');
        static::assertSelectorExists('#treasure_Options_1');
        static::assertSelectorExists('#treasure_Options_2');
        static::assertSelectorExists('#treasure_Options_3');

        foreach (Type::keys() as $type) {
            $client->submitForm(
                'treasure_OK',
                [
                    'treasure[Type]' => $type,
                    'treasure[Coffres]' => '6',
                    'treasure[Options]' => 'Butin',
                ]
            );
        }

        static::assertResponseIsSuccessful();
    }

    public function testPostDungeonCoins(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/dungeon');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#treasure_Type');
        static::assertSelectorExists('#treasure_Coffres');
        static::assertSelectorExists('#treasure_Options_0');
        static::assertSelectorExists('#treasure_Options_1');
        static::assertSelectorExists('#treasure_Options_2');
        static::assertSelectorExists('#treasure_Options_3');

        foreach (Type::keys() as $type) {
            $client->submitForm(
                'treasure_OK',
                [
                    'treasure[Type]' => $type,
                    'treasure[Coffres]' => '4',
                    'treasure[Options]' => 'Pièces',
                ]
            );
        }

        static::assertResponseIsSuccessful();
    }

    public function testPostDungeonGemsAndArt(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/dungeon');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#treasure_Type');
        static::assertSelectorExists('#treasure_Coffres');
        static::assertSelectorExists('#treasure_Options_0');
        static::assertSelectorExists('#treasure_Options_1');
        static::assertSelectorExists('#treasure_Options_2');
        static::assertSelectorExists('#treasure_Options_3');

        foreach (Type::keys() as $type) {
            $client->submitForm(
                'treasure_OK',
                [
                    'treasure[Type]' => $type,
                    'treasure[Coffres]' => '6',
                    'treasure[Options]' => 'Gemmes & Art',
                ]
            );
        }

        static::assertResponseIsSuccessful();
    }
}
