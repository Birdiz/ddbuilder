<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Environment\Disease;
use App\Enum\Environment\Madness;
use App\Enum\Environment\Month;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class EnvironmentControllerTest extends WebTestCase
{
    private const TITLES = [
        'Éléments au hasard',
        'Métaux',
        'Prévisions météo',
        'Maladies',
        'Folies',
        'Folies génériques',
    ];

    public function testGetEnvironment(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/environment');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostWeather(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/environment');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#month_Mois');

        foreach (Month::keys() as $month) {
            $client->submitForm(
                'month_OK',
                [
                    'month[Mois]' => $month,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostDisease(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/environment');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#disease_Maladie');

        foreach (array_column(Disease::toArray(), 'name') as $disease) {
            $client->submitForm(
                'disease_OK',
                [
                    'disease[Maladie]' => $disease,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostMadness(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/environment');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#madness_Folie');

        foreach (array_column(Madness::toArray(), 'name') as $madness) {
            $client->submitForm(
                'madness_OK',
                [
                    'madness[Folie]' => $madness,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }
}
