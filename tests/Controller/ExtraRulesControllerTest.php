<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class ExtraRulesControllerTest extends WebTestCase
{
    private const TITLES = [
        'Règles additionnelles',
        'Compétences cachées',
    ];

    public function testGetExtraRules(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/extra/rules');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
