<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class GeneratorControllerTest extends WebTestCase
{
    public function testGetGenerator(): void
    {
        static::createClient()->request(Request::METHOD_GET, '/generator/');

        static::assertResponseRedirects('/');
    }
}
