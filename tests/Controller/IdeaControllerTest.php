<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class IdeaControllerTest extends WebTestCase
{
    private const TITLES = [
        ' Idées',
    ];

    public function testGetRoadmap(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/idea');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
