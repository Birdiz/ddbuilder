<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Dungeon\Treasure\TableA;
use App\Enum\Dungeon\Treasure\TableB;
use App\Enum\Dungeon\Treasure\TableC;
use App\Enum\Dungeon\Treasure\TableD;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableF;
use App\Enum\Dungeon\Treasure\TableG;
use App\Enum\Dungeon\Treasure\TableH;
use App\Enum\Dungeon\Treasure\TableI;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MagicTradingPostControllerTest extends WebTestCase
{
    private const TITLES = [
        'Règle de la création et de la vente d\'objets magiques',
        'Objets Magiques',
    ];

    public function testGetMagicTradingPost(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/magic/trading/post');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }

        foreach ($crawler->filter('h2')->extract(['_text']) as $title) {
            static::assertContains($title, ['vend ses objets magiques !']);
        }
    }

    public function testPostItem(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/magic/trading/post');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#magic_item_item');

        $items = array_merge(
            TableA::toArray(),
            TableB::toArray(),
            TableC::toArray(),
            TableD::toArray(),
            TableE::toArray(),
            TableF::toArray(),
            TableG::toArray(),
            TableH::toArray(),
            TableI::toArray(),
        );
        foreach (array_column($items, 'name', 'name') as $key => $name) {
            $client->submitForm(
                'magic_item_OK',
                [
                    'magic_item[item]' => $key,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }
}
