<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Generator\PNJ\Alignment;
use App\Enum\Generator\PNJ\Race\Race;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PNJGeneratorControllerTest extends WebTestCase
{
    private const TITLES = [
        'Personnage Non-Joueur',
    ];

    public function testGetPNJ(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/generator/pnj');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostPNJFemale(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/pnj');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#pnj_Race');
        static::assertSelectorExists('#pnj_Gender');
        static::assertSelectorExists('#pnj_Job');
        static::assertSelectorExists('#pnj_Alignment');

        foreach (array_column(Race::toArray(), 'name') as $race) {
            $client->submitForm(
                'pnj_OK',
                [
                    'pnj[Race]' => $race,
                    'pnj[Gender]' => 'FEMALE',
                    'pnj[Job]' => 'ADVOCATE',
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostPNJMale(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/pnj');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#pnj_Race');
        static::assertSelectorExists('#pnj_Gender');
        static::assertSelectorExists('#pnj_Job');
        static::assertSelectorExists('#pnj_Alignment');

        foreach (array_column(Race::toArray(), 'name') as $race) {
            $client->submitForm(
                'pnj_OK',
                [
                    'pnj[Race]' => $race,
                    'pnj[Gender]' => 'MALE',
                    'pnj[Job]' => 'ADVOCATE',
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostPNJWithoutJob(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/pnj');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#pnj_Race');
        static::assertSelectorExists('#pnj_Gender');
        static::assertSelectorExists('#pnj_Job');
        static::assertSelectorExists('#pnj_Alignment');

        $i = 0;
        while ($i <= 20) {
            $client->submitForm(
                'pnj_OK',
                [
                    'pnj[Race]' => 'Nain des collines',
                    'pnj[Gender]' => 'FEMALE',
                ]
            );

            static::assertResponseIsSuccessful();

            $i++;
        }
    }

    public function testPostPNJWithAlignment(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/pnj');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#pnj_Race');
        static::assertSelectorExists('#pnj_Gender');
        static::assertSelectorExists('#pnj_Job');
        static::assertSelectorExists('#pnj_Alignment');

        $i = 0;
        while ($i <= 20) {
            $client->submitForm(
                'pnj_OK',
                [
                    'pnj[Race]' => 'Nain des collines',
                    'pnj[Gender]' => 'FEMALE',
                    'pnj[Alignment]' => Alignment::NEUTRAL()->getKey(),
                ]
            );

            static::assertResponseIsSuccessful();

            $i++;
        }
    }
}
