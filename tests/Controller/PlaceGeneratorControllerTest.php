<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Generator\Place\Settlement;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class PlaceGeneratorControllerTest extends WebTestCase
{
    private const TITLES = [
        'Lieu',
    ];

    public function testGetPlace(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostCity(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        foreach (Settlement::keys() as $settlement) {
            $client->submitForm(
                'place_OK',
                [
                    'place[Type]' => $settlement,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }

    public function testPostHouse(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        $client->submitForm(
            'place_OK',
            [
                'place[Type]' => 'RESIDENCE',
            ]
        );

        static::assertResponseIsSuccessful();
    }

    public function testPostReligious(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        $client->submitForm(
            'place_OK',
            [
                'place[Type]' => 'RELIGIOUS',
            ]
        );

        static::assertResponseIsSuccessful();
    }

    public function testPostTavern(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        $client->submitForm(
            'place_OK',
            [
                'place[Type]' => 'TAVERN',
            ]
        );

        static::assertResponseIsSuccessful();
    }

    public function testPostWarehouse(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        $client->submitForm(
            'place_OK',
            [
                'place[Type]' => 'WAREHOUSE',
            ]
        );

        static::assertResponseIsSuccessful();
    }

    public function testPostShop(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/place');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#place_Type');

        $client->submitForm(
            'place_OK',
            [
                'place[Type]' => 'SHOP',
            ]
        );

        static::assertResponseIsSuccessful();
    }
}
