<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Sailing\Ship\Ship;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SailingControllerTest extends WebTestCase
{
    private const TITLES = [
        'Équipage',
        'Bâtiments',
        'Officiers',
    ];

    public function testGetSailing(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/sailing');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testPostCrew(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/sailing');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#ship_Nom');

        foreach (array_column(Ship::toArray(), 'name') as $ship) {
            $client->submitForm(
                'ship_OK',
                [
                    'ship[Nom]' => $ship,
                ]
            );

            static::assertResponseIsSuccessful();
        }
    }
}
