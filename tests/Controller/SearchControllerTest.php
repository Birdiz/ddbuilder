<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SearchControllerTest extends WebTestCase
{
    public function testGetSearch(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#search_needle');

        $client->submitForm(
            'search_OK',
            [
                'needle' => 'coffre',
            ]
        );

        static::assertResponseIsSuccessful();
    }
}
