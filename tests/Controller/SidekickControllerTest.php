<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SidekickControllerTest extends WebTestCase
{
    private const TITLES = [
        'Compagnons',
        'Artificier',
        'Barde',
        'Sauvage',
    ];

    public function testGetSailing(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/sidekick');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
