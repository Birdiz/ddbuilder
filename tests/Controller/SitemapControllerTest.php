<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SitemapControllerTest extends WebTestCase
{
    public function testGetSitemap(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/sitemap');

        static::assertResponseIsSuccessful();
    }
}
