<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class TavernControllerTest extends WebTestCase
{
    private const TITLES = [
        'Éléments au hasard',
        'Menus du jour',
        'Jeu',
    ];

    public function testGetTavern(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/tavern');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }
}
