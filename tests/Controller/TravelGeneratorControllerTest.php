<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Enum\Generator\Travel\Pace;
use App\Enum\Level;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class TravelGeneratorControllerTest extends WebTestCase
{
    private const TITLES = [
        'Voyage',
    ];

    public function testGetTravel(): void
    {
        $crawler = static::createClient()->request(Request::METHOD_GET, '/generator/travel');

        static::assertResponseIsSuccessful();

        foreach ($crawler->filter('h4')->extract(['_text']) as $title) {
            static::assertContains($title, static::TITLES);
        }
    }

    public function testTravel(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/generator/travel');

        static::assertResponseIsSuccessful();
        static::assertSelectorExists('#travel_Pace');
        static::assertSelectorExists('#travel_Days');
        static::assertSelectorExists('#travel_Players');
        static::assertSelectorExists('#travel_Level');

        foreach (Level::keys() as $level) {
            foreach (Pace::values() as $pace) {
                $client->submitForm(
                    'travel_OK',
                    [
                        'travel[Pace]' => $pace->getValue()['name'],
                        'travel[Days]' => 31,
                        'travel[Players]' => 6,
                        'travel[Level]' => $level,
                    ]
                );

                static::assertResponseIsSuccessful();
            }
        }
    }
}