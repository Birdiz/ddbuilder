<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Generator\PNJ\Race\Race;
use PHPUnit\Framework\TestCase;

class AbstractEnumTest extends TestCase
{
    public function testGetByName(): void
    {
        $races = Race::getByName();

        static::assertCount(15, $races);
        static::assertSame(Race::HILL_DWARF()->getValue(), $races[Race::HILL_DWARF()->getValue()['name']]);
    }
}
