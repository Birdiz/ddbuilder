<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Generator\PNJ\Race\Race;
use App\Service\Helper\Mapper\PageFormMapper;
use PHPUnit\Framework\TestCase;

class AbstractSearchMapperTest extends TestCase
{
    public function testMapReturnNull(): void
    {
        static::assertNull((new PageFormMapper())->map(null));
    }

    public function testMapEmpty(): void
    {
        static::assertCount(0, (new PageFormMapper())->map(['test']));
    }
}
