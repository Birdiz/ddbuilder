<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Danger;
use App\Enum\Generator\Boss\Beginner;
use App\Enum\Generator\Boss\Divine;
use App\Enum\Generator\Boss\Expert;
use App\Enum\Generator\Boss\Hero;
use PHPUnit\Framework\TestCase;

class BossTest extends TestCase
{
    public function testGetMobsBeginner(): void {
        foreach (Beginner::keys() as $key) {
            foreach (Danger::keys() as $danger) {
                $mobs = Beginner::getMobs($key, $danger);

                if (array_key_exists(1, $mobs)) {
                    static::assertArrayHasKey('mob', $mobs[1]);
                }
            }
        }
    }

    public function testGetMobsExpert(): void {
        foreach (Expert::keys() as $key) {
            foreach (Danger::keys() as $danger) {
                $mobs = Expert::getMobs($key, $danger);

                if (array_key_exists(1, $mobs)) {
                    static::assertArrayHasKey('mob', $mobs[1]);
                }
            }
        }
    }

    public function testGetMobsHero(): void {
        foreach (Hero::keys() as $key) {
            foreach (Danger::keys() as $danger) {
                $mobs = Hero::getMobs($key, $danger);

                if (array_key_exists(1, $mobs)) {
                    static::assertArrayHasKey('mob', $mobs[1]);
                }
            }
        }
    }

    public function testGetMobsDivine(): void {
        foreach (Divine::keys() as $key) {
            foreach (Danger::keys() as $danger) {
                $mobs = Divine::getMobs($key, $danger);

                if (array_key_exists(1, $mobs)) {
                    static::assertArrayHasKey('mob', $mobs[1]);
                }
            }
        }
    }
}
