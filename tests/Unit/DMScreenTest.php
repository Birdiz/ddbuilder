<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Adventure\Plane\Layer;
use App\Enum\Adventure\Plane\Plane;
use PHPUnit\Framework\TestCase;

class DMScreenTest extends TestCase
{
    private static function getLayerKeys(array $array): array
    {
        $keys = [null];
        foreach ($array as $key => $value) {
            $keys[] = $key;

            if (is_array($value)) {
                $keys = array_merge($keys, static::getLayerKeys($value));
            }
        }

        return $keys;
    }

    public function testGetLayerUnexistingPlane(): void
    {
        static::assertNull(Layer::getLayer('test'));
    }

    public function testGetLayer(): void
    {
        $layers = static::getLayerKeys(Layer::toArray());
        foreach (Plane::toArray() as $plane) {
            $layer = Layer::getLayer($plane);

            static::assertContains($layer, $layers);
        }
    }
}
