<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Dungeon\Door;
use App\Enum\Object\AC;
use App\Enum\Object\Integrity;
use App\Enum\Object\Material;
use PHPUnit\Framework\TestCase;

class DoorTest extends TestCase
{
    public function testGetACFromValue(): void
    {
        foreach (Door::values() as $door) {
            static::assertContains(
                Door::getACFromValue($door->getValue()),
                [
                    AC::MEDIUM()->getValue(),
                    AC::HARD()->getValue(),
                    AC::VERY_HARD()->getValue(),
                    AC::NEARLY_INDESTRUCTIBLE()->getValue(),
                ]
            );
        }
    }

    public function testGetIntegrityFromValue(): void
    {
        foreach (Door::values() as $door) {
            static::assertContains(
                Door::getIntegrityFromValue($door->getValue()),
                [Integrity::NORMAL()->getKey(), Integrity::RESILIENT()->getKey()]
            );
        }
    }

    public function testGetMaterialFromValue(): void
    {
        foreach (Door::values() as $door) {
            static::assertContains(
                Door::getMaterialFromValue($door->getValue()),
                [
                    Material::WOOD()->getValue(),
                    Material::STONE()->getValue(),
                    Material::METAL()->getValue(),
                    Material::UNKNOW()->getValue(),
                ]
            );
        }
    }
}
