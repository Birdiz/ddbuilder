<?php

declare(strict_types=1);

namespace App\Tests\Unit\Generator;

use App\Service\Generator\Travel\EncounterRegistry;
use App\Service\Generator\Travel\FightStrategy;
use PHPUnit\Framework\TestCase;

class EncounterRegistryTest extends TestCase
{
    public function testGetStrategyNotImplemented(): void
    {
        $registry = new EncounterRegistry([new FightStrategy()]);

        $this->expectException(\LogicException::class);

        $registry->getStrategy(('test'));
    }
}
