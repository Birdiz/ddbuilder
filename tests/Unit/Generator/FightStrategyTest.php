<?php

declare(strict_types=1);

namespace App\Tests\Unit\Generator;

use App\Service\Generator\Travel\FightStrategy;
use PHPUnit\Framework\TestCase;

class FightStrategyTest extends TestCase
{
    public function testProcess(): void
    {
        static::assertCount(0, (new FightStrategy())->process());
    }
}
