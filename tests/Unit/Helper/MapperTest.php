<?php

declare(strict_types=1);

namespace App\Tests\Unit\Helper;

use App\Service\Helper\Mapper\PageEnumMapper;
use App\Service\Helper\Mapper\PageFormMapper;
use App\Service\Helper\Mapper\PageTemplateMapper;
use App\Service\Helper\Mapper\PageTitleMapper;
use App\Service\Helper\Mapper\SearchMapperRegistry;
use PHPUnit\Framework\TestCase;

class MapperTest extends TestCase
{
    public function testPaths(): void
    {
        static::assertCount(15, PageEnumMapper::paths());
        static::assertCount(15, PageFormMapper::paths());
        static::assertCount(15, PageTemplateMapper::paths());
    }

    public function testPageTitleMap(): void
    {
        static::assertCount(15, (new PageTitleMapper())->map());
    }

    public function testRegistryMap(): void
    {
        $registry = new SearchMapperRegistry([new PageFormMapper()]);

        static::assertCount(0, $registry->map(null));
        static::assertCount(0, $registry->map(['test']));
    }
}
