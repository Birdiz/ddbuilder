<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Object\Integrity;
use App\Enum\Object\Material;
use App\Enum\Object\Size;
use PHPUnit\Framework\TestCase;

class ObjectTest extends TestCase
{
    public function testGetMultiplierFromSize(): void
    {
        foreach (Size::toArray() as $key => $size) {
            static::assertSame(
                $size['multiplier'],
                Size::getMultiplierFormKey($key)
            );
        }
    }

    public function testGetMultiplierFromIntegrity(): void
    {
        foreach (Integrity::toArray() as $key => $integrity) {
            static::assertSame(
                (float) $integrity['multiplier'],
                Integrity::getMultiplierFormKey($key)
            );
        }
    }

    public function testGetDieFromMaterial(): void
    {
        foreach (Material::values() as $material) {
            static::assertGreaterThanOrEqual(4, Material::getDieFromValue($material->getValue()));
            static::assertLessThanOrEqual(12, Material::getDieFromValue($material->getValue()));
        }
    }
}
