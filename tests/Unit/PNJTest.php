<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Generator\PNJ\Name\Dragonborn;
use App\Enum\Generator\PNJ\Race\Talent;
use PHPUnit\Framework\TestCase;

class PNJTest extends TestCase
{
    public function testGetRaceTalentWithWrongRaceName(): void
    {
        static::assertCount(0, Talent::getTalentsFromRaceName('test'));
    }

    public function testGetDragonBornNames(): void
    {
        $result = Dragonborn::getName('MALE', 10);

        static::assertIsArray($result);
        static::assertCount(10, $result);
    }
}
