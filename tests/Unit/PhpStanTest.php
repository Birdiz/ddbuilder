<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\PHPStan\ConstantsExtension;
use PHPStan\Reflection\ConstantReflection;
use PHPUnit\Framework\TestCase;

class PhpStanTest extends TestCase
{
    public function testContentExtension(): void
    {
        $constantReflection = $this->createMock(ConstantReflection::class);

        static::assertTrue((new ConstantsExtension)->isAlwaysUsed($constantReflection));
    }
}
