<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Generator\Place\Hero\Status;
use App\Enum\Generator\Place\Religious;
use App\Enum\Generator\Place\Residence;
use App\Enum\Generator\Place\Settlement;
use App\Enum\Generator\Place\Shop;
use App\Service\Generator\PlaceManager;
use PHPUnit\Framework\TestCase;

class PlaceTest extends TestCase
{
    public function testGetWeightsShop(): void
    {
        static::assertCount(0, Shop::getWeights());
    }

    public function testGetWeightsHeroStatus(): void
    {
        static::assertCount(10, Status::getWeights());
    }

    public function testGetResidenceHabitants(): void {
        foreach (Residence::toArray() as $residence) {
            static::assertGreaterThanOrEqual(1, Residence::getHabitants($residence));
            static::assertLessThanOrEqual(50, Residence::getHabitants($residence));
        }
    }

    public function testGetReligiousGods(): void
    {
        $places = [
            Religious::TEMPLE_GOOD()->getValue(),
            Religious::TEMPLE_BAD()->getValue(),
            Religious::SHRINE_HIDDEN()->getValue(),
        ];

        foreach ($places as $place) {
            static::assertGreaterThanOrEqual(1, count(Religious::getGods($place)));
            static::assertLessThanOrEqual(3, count(Religious::getGods($place)));
        }
    }

    public function testGetSettlementBuildingsLimit(): void
    {
        static::assertCount(0, Settlement::getBuildings(0, [], 0, 1));
    }

    public function testGetQualityOfLifeForSettlement(): void
    {
        $data = [
            0 => Settlement\QualityOfLife::LOW()->getValue(),
            30 => Settlement\QualityOfLife::MEDIUM()->getValue(),
            100 => Settlement\QualityOfLife::HIGH()->getValue(),
        ];

        foreach ($data as $capital => $result) {
            static::assertSame(
                $result,
                Settlement\QualityOfLife::getQualityOfLifeForSettlement(Settlement::VILLAGE()->getKey(), $capital, 1)
            );
        }
    }

    public function testGetRandomPlaceSetUnexisting(): void
    {
        static::assertCount(0, PlaceManager::getRandomPlace('test'));
    }
}
