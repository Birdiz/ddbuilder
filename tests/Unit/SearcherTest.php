<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Service\Helper\Mapper\PageEnumMapper;
use App\Service\Helper\Mapper\PageFormMapper;
use App\Service\Helper\Mapper\PageTemplateMapper;
use App\Service\Helper\Mapper\PageTitleMapper;
use App\Service\Helper\Mapper\SearchMapperRegistry;
use App\Tools\Searcher;
use PHPUnit\Framework\TestCase;

class SearcherTest extends TestCase
{
    public function testSearch(): void
    {
        $registry = new SearchMapperRegistry([
            new PageEnumMapper(),
            new PageFormMapper(),
            new PageTemplateMapper(),
        ]);
        $pageTitleMapper = new PageTitleMapper();
        $searcher = new Searcher($registry, $pageTitleMapper);

        static::assertCount(0, $searcher->search('azerty'));
        static::assertCount(8, $searcher->search('test'));
        static::assertCount(4, $searcher->search('gemme'));
    }
}
