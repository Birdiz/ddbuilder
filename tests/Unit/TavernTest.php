<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Service\TavernManager;
use PHPUnit\Framework\TestCase;

class TavernTest extends TestCase
{
    public function testGetRandomPoorMeal(): void
    {
        $i = 0;
        while ($i <= 100) {
            static::assertCount(3, TavernManager::getRandomPoorMeal());
            $i++;
        }
    }
}
