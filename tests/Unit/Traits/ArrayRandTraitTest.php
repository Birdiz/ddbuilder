<?php

declare(strict_types=1);

namespace App\Tests\Unit\Traits;

use App\Util\ArrayRandTrait;
use PHPUnit\Framework\TestCase;

class ArrayRandTraitTest extends TestCase
{
    use ArrayRandTrait;

    public function testGetZeroComplexeItem(): void
    {
        static::assertCount(0, static::getXComplexeItems(0, []));
    }

    public function testGetMultipleItemsException(): void
    {
        $this->expectException(\LogicException::class);
        static::getMultipleItems(0, 0, []);
    }
}
