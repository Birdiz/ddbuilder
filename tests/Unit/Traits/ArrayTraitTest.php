<?php

declare(strict_types=1);

namespace App\Tests\Unit\Traits;

use App\Util\ArrayTrait;
use PHPUnit\Framework\TestCase;

class ArrayTraitTest extends TestCase
{
    use ArrayTrait;

    public function testArraySearchMultidimensional(): void
    {
        $array = [
            'test' => [
                'one',
                'two',
            ],
        ];

        static::assertSame('test', static::arraySearchMultidimensional('two', $array));
        static::assertNull(static::arraySearchMultidimensional('three', $array));
    }
}
