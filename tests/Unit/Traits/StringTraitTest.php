<?php

declare(strict_types=1);

namespace App\Tests\Unit\Traits;

use App\Util\StringTrait;
use PHPUnit\Framework\TestCase;

class StringTraitTest extends TestCase
{
    use StringTrait;

    public function testReplaceSubstring(): void
    {
        $string = 'Hello World';

        static::replaceSubstring($string, 'o W', 'W o');

        static::assertSame('HellW oorld', $string);
    }
}
