<?php

declare(strict_types=1);

namespace App\Tests\Unit\Traits;

use App\Util\WeightedRandTrait;
use PHPUnit\Framework\TestCase;

class WeightedRandTraitTest extends TestCase
{
    use WeightedRandTrait;

    public function testGetMultipleItemsWeigthedException(): void
    {
        $this->expectException(\LogicException::class);
        static::getMultipleItemsWeigthed(0, [], []);
    }

    public function testDistributePopulation(): void
    {
        $values = [
            'toto',
            'tata',
            'tutu',
        ];

        static::assertCount(1, static::distributePopulation($values, 1));
    }
}
