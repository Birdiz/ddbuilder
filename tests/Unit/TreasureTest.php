<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Dungeon\Treasure\Container;
use App\Enum\Dungeon\Treasure\Litterature;
use App\Enum\Dungeon\Treasure\TableE;
use App\Enum\Dungeon\Treasure\TableI;
use PHPUnit\Framework\TestCase;

class TreasureTest extends TestCase
{
    public function testGetWeightsContainer(): void
    {
        static::assertCount(27, Container::getWeights());
    }

    public function testGetWeightsLitterature(): void
    {
        static::assertCount(50, Litterature::getWeights());
    }

    public function testGetWeightsTableE(): void
    {
        static::assertCount(7, TableE::getWeights());
    }

    public function testGetWeightsTableI(): void
    {
        static::assertCount(53, TableI::getWeights());
    }
}
