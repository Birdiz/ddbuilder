<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Twig\CaracModExtension;
use App\Twig\FemalizeExtension;
use App\Twig\FemalizeRaceExtension;
use App\Twig\IsRouteActiveExtension;
use App\Twig\MagicItemPriceExtension;
use App\Twig\VersionExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\RequestStack;

class TwigTest extends TestCase
{
    public function testGetCaracModFunctions(): void
    {
        static::assertCount(1, (new CaracModExtension())->getFunctions());
    }

    public function testGetIsRouteActiveFunctions(): void
    {
        $requestStack = $this->createMock(RequestStack::class);

        static::assertCount(1, (new IsRouteActiveExtension($requestStack))->getFunctions());
    }

    public function testGetMagicItemPriceFunctions(): void
    {
        static::assertCount(1, (new MagicItemPriceExtension())->getFunctions());
    }

    public function testGetVersionFunctions(): void
    {
        static::assertCount(1, (new VersionExtension('1.0.0'))->getFunctions());
    }

    public function testGetFemalizeFunctions(): void
    {
        static::assertCount(1, (new FemalizeExtension())->getFunctions());
    }

    public function testGetFemalizeRaceFunctions(): void
    {
        static::assertCount(1, (new FemalizeRaceExtension())->getFunctions());
    }
}
