<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Enum\Dungeon\Treasure\Type;
use PHPUnit\Framework\TestCase;

class TypeTest extends TestCase
{
    public function testTreasureNull(): void
    {
        $this->expectException(\LogicException::class);
        Type::getTreasureFromKey('test', 1);
    }

    public function testHoardNull(): void
    {
        $this->expectException(\LogicException::class);
        Type::getHoardFromKey('test', 1);
    }

    public function testGemsAndArtNull(): void
    {
        $this->expectException(\LogicException::class);
        Type::getGemsAndArtFromKey('test', 1);
    }
}
